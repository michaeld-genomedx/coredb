--liquibase formatted sql
--changeset mpdillon:v2.0 endDelimiter:"\n--GO" stripComments:false comment:Using Intellij Compare
--
CREATE ROLE  mickael_leclercq WITH NOSUPERUSER INHERIT NOCREATEROLE NOCREATEDB LOGIN NOREPLICATION;
CREATE OR REPLACE VIEW samplesdb.bladder_qc_info AS
 SELECT s.sample_id,
    p.site,
    s.sample_type,
    r.rna_batch,
    cel.celfile_name,
    cel.qc_batch_id,
    cel.percentpresent,
    cel.posneg_auc,
    cel.hybcontrol,
    cel.hybpercentpresent,
    score.bca_subtyping_luminal_1,
    score.bca_subtyping_infiltrated_luminal_1,
    score.bca_subtyping_basal_1,
    score.bca_subtyping_claudin_low_1,
    class.bca_subtype AS bca_subtyping_class
   FROM ( SELECT celfiles.study_name,
            celfiles.celfile_name,
            celfiles.cdna_id,
            celfiles.created,
            celfiles.modified,
            celfiles.created_by,
            celfiles.modified_by,
            celfiles.chip_id,
            celfiles.erg_gene,
            celfiles.etv1_gene,
            celfiles.etv4_gene,
            celfiles.erg_fusion,
            celfiles.erg_type,
            celfiles.pten_deletion,
            celfiles.spink1_up,
            celfiles.celfile_lab,
            celfiles.qc_batch_id,
            celfiles.location,
            celfiles.qc_flag,
            celfiles.rsi_scan,
            celfiles.rf22_scan,
            celfiles.gcc_scan,
            celfiles.gcs_scan,
            celfiles.rf22_scan_5yr,
            celfiles.rf22_prod_5yr,
            celfiles.rf22_scan_3yrbcr,
            celfiles.rf22_prod_3yrbcr,
            celfiles.percentpresent,
            celfiles.rf22_percentpresent,
            celfiles.rf22_prod,
            celfiles.celfile_batch,
            celfiles.posneg_auc,
            celfiles.hybcontrol,
            celfiles.hybpercentpresent,
            celfiles.rehyb,
            celfiles.celfile_note,
            celfiles.scanner_celfile_name,
            celfiles.scanner,
            celfiles.bcm_version,
            celfiles.celfile_name_frma,
            celfiles.hybcontrol_v3,
            celfiles.hybpercentpresent_v3,
            celfiles.celfile_date,
            celfiles.qc_flag_v3,
            celfiles.scan_date,
            celfiles.qc_flag_v2,
            celfiles.hybcontrol_v2,
            celfiles.hybpercentpresent_v2,
            celfiles.rf15_percentpresent,
            celfiles.percentpresent_rd,
            celfiles.posneg_auc_rd,
            celfiles.qc_flag_v1,
            celfiles.hybcontrol_v1,
            celfiles.hybpercentpresent_v1,
            celfiles.wm_celfile_name,
            celfiles.geo_celfile_name,
            celfiles.qc_flag_bladder,
            celfiles.finalized_celfile
           FROM celfiles
          WHERE (celfiles.study_name::text IN ( SELECT studies.study_name
                   FROM studies
                  WHERE studies.disease::text = 'Bladder Cancer'::text))) cel
     LEFT JOIN cdna c USING (cdna_id)
     LEFT JOIN rna r USING (rna_id)
     LEFT JOIN samples s USING (sample_id)
     LEFT JOIN patients p USING (patient_id)
     LEFT JOIN signatures_clindev_v2.score score USING (celfile_name)
     LEFT JOIN signatures_clindev_v2.class class USING (celfile_name)
  WHERE s.sample_type IS NOT NULL;

ALTER TABLE samplesdb.bladder_qc_info
    OWNER TO samplesdb_table_owner;

GRANT ALL ON TABLE samplesdb.bladder_qc_info TO samplesdb_table_owner;
GRANT SELECT ON TABLE samplesdb.bladder_qc_info TO laura;

ALTER TABLE prndl.samplesdb.patients ADD gland_volume NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD mri_date DATE NULL;
ALTER TABLE prndl.samplesdb.patients ADD mri_pos_core NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD mri_strength NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD mri_total_core NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD mrigs NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD mrigs_p NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD mrigs_s NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD nodes_pos_left NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD nodes_pos_right NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD nodes_rmd_left NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD nodes_rmd_right NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD psa_density NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD psa_free NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD rp_max_length NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD rp_tumor_involvement NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD rp_volume NUMERIC NULL;
ALTER TABLE prndl.samplesdb.patients ADD rp_weight NUMERIC NULL;
ALTER TABLE prndl.samplesdb.permissions_external ADD view_mickael_leclercq BOOLEAN DEFAULT false NULL;
ALTER TABLE prndl.samplesdb.psa ADD psa_date TIMESTAMP(6) NULL;
ALTER TABLE prndl.samplesdb.psa ADD user_role VARCHAR NULL;
CREATE TABLE prndl.samplesdb.row_permissions_test
(
    user_role VARCHAR,
    data NUMERIC
);
CREATE OR REPLACE VIEW samplesdb.view_mickael_leclercq AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.pathgs,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.bcr_time,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.capra_s,
    patients_bioinfo.ece,
    patients_bioinfo.met,
    patients_bioinfo.met_time,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    samples_bioinfo.storage,
    celfiles.qc_batch_id,
    celfiles.celfile_name
   FROM patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id)
     JOIN rna USING (sample_id)
     JOIN cdna USING (rna_id)
     RIGHT JOIN celfiles USING (cdna_id)
  WHERE samples_bioinfo.user_role::text = 'bioinfo'::text AND (celfiles.celfile_name::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE (celfile_groups_analysis.celfile_group_name::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE permissions_external.view_mickael_leclercq IS TRUE))));

ALTER TABLE samplesdb.view_mickael_leclercq
    OWNER TO mandeep;

GRANT ALL ON TABLE samplesdb.view_mickael_leclercq TO samplesdb_table_owner;
GRANT SELECT ON TABLE samplesdb.view_mickael_leclercq TO mickael_leclercq;
GRANT SELECT ON TABLE samplesdb.view_mickael_leclercq TO bioinfo_readonly;
GRANT ALL ON TABLE samplesdb.view_mickael_leclercq TO mandeep;
GRANT ALL ON TABLE samplesdb.view_mickael_leclercq TO predictions_table_owner;
GRANT SELECT ON TABLE samplesdb.view_mickael_leclercq TO biostats;
GRANT SELECT ON TABLE samplesdb.view_mickael_leclercq TO bioinfo;
GRANT ALL ON TABLE samplesdb.view_mickael_leclercq TO tyler;
GRANT ALL ON TABLE samplesdb.view_mickael_leclercq TO administrator;

--GO
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP bca_subtyping_basal_1;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP bca_subtyping_class;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP bca_subtyping_claudin_low_1;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP bca_subtyping_infiltrated_luminal_1;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP bca_subtyping_luminal_1;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP celfile_name;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP hybcontrol;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP hybpercentpresent;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP percentpresent;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP posneg_auc;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP qc_batch_id;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP rna_batch;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP sample_id;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP sample_type;
--rollback ALTER TABLE prndl.samplesdb.bladder_qc_info DROP site;
--rollback ALTER TABLE prndl.samplesdb.patients DROP gland_volume;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mri_date;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mri_pos_core;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mri_strength;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mri_total_core;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mrigs;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mrigs_p;
--rollback ALTER TABLE prndl.samplesdb.patients DROP mrigs_s;
--rollback ALTER TABLE prndl.samplesdb.patients DROP nodes_pos_left;
--rollback ALTER TABLE prndl.samplesdb.patients DROP nodes_pos_right;
--rollback ALTER TABLE prndl.samplesdb.patients DROP nodes_rmd_left;
--rollback ALTER TABLE prndl.samplesdb.patients DROP nodes_rmd_right;
--rollback ALTER TABLE prndl.samplesdb.patients DROP psa_density;
--rollback ALTER TABLE prndl.samplesdb.patients DROP psa_free;
--rollback ALTER TABLE prndl.samplesdb.patients DROP rp_max_length;
--rollback ALTER TABLE prndl.samplesdb.patients DROP rp_tumor_involvement;
--rollback ALTER TABLE prndl.samplesdb.patients DROP rp_volume;
--rollback ALTER TABLE prndl.samplesdb.patients DROP rp_weight;
--rollback ALTER TABLE prndl.samplesdb.permissions_external DROP view_mickael_leclercq;
--rollback ALTER TABLE prndl.samplesdb.psa DROP psa_date;
--rollback ALTER TABLE prndl.samplesdb.psa DROP user_role;
--rollback DROP TABLE prndl.samplesdb.row_permissions_test CASCADE;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP adt;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP adt_a;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP adt_s;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP age;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP bcr;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP bcr_time;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP capra_s;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP celfile_name;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP cstage;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP ece;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP lni;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP mayo_def;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP met;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP met_time;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP os;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP os_time;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP pathgs;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP patient_id;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP pcsm;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP pcsm_time;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP preop_psa;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP pstage;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP qc_batch_id;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP rt;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP rt_a;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP rt_s;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP sm;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP storage;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP study_name;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP svi;
--rollback ALTER TABLE prndl.samplesdb.view_mickael_leclercq DROP tumor_vol;
