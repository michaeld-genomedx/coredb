--liquibase formatted sql
--changeset mpdillon:bladder_qc_info endDelimiter:"\n--GO" stripComments:false comment:InitialLoad

SET search_path = samplesdb, pg_catalog;

--
-- Name: bladder_qc_info; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW bladder_qc_info AS
 SELECT s.sample_id,
    p.site,
    s.sample_type,
    r.rna_batch,
    cel.celfile_name,
    cel.qc_batch_id,
    cel.percentpresent,
    cel.posneg_auc,
    cel.hybcontrol,
    cel.hybpercentpresent,
    score.bca_subtyping_luminal_1,
    score.bca_subtyping_infiltrated_luminal_1,
    score.bca_subtyping_basal_1,
    score.bca_subtyping_claudin_low_1,
    class.bca_subtype AS bca_subtyping_class
   FROM ((((((( SELECT celfiles.study_name,
            celfiles.celfile_name,
            celfiles.cdna_id,
            celfiles.created,
            celfiles.modified,
            celfiles.created_by,
            celfiles.modified_by,
            celfiles.chip_id,
            celfiles.erg_gene,
            celfiles.etv1_gene,
            celfiles.etv4_gene,
            celfiles.erg_fusion,
            celfiles.erg_type,
            celfiles.pten_deletion,
            celfiles.spink1_up,
            celfiles.celfile_lab,
            celfiles.qc_batch_id,
            celfiles.location,
            celfiles.qc_flag,
            celfiles.rsi_scan,
            celfiles.rf22_scan,
            celfiles.gcc_scan,
            celfiles.gcs_scan,
            celfiles.rf22_scan_5yr,
            celfiles.rf22_prod_5yr,
            celfiles.rf22_scan_3yrbcr,
            celfiles.rf22_prod_3yrbcr,
            celfiles.percentpresent,
            celfiles.rf22_percentpresent,
            celfiles.rf22_prod,
            celfiles.celfile_batch,
            celfiles.posneg_auc,
            celfiles.hybcontrol,
            celfiles.hybpercentpresent,
            celfiles.rehyb,
            celfiles.celfile_note,
            celfiles.scanner_celfile_name,
            celfiles.scanner,
            celfiles.bcm_version,
            celfiles.celfile_name_frma,
            celfiles.hybcontrol_v3,
            celfiles.hybpercentpresent_v3,
            celfiles.celfile_date,
            celfiles.qc_flag_v3,
            celfiles.scan_date,
            celfiles.qc_flag_v2,
            celfiles.hybcontrol_v2,
            celfiles.hybpercentpresent_v2,
            celfiles.rf15_percentpresent,
            celfiles.percentpresent_rd,
            celfiles.posneg_auc_rd,
            celfiles.qc_flag_v1,
            celfiles.hybcontrol_v1,
            celfiles.hybpercentpresent_v1,
            celfiles.wm_celfile_name,
            celfiles.geo_celfile_name,
            celfiles.qc_flag_bladder,
            celfiles.finalized_celfile
           FROM celfiles
          WHERE ((celfiles.study_name)::text IN ( SELECT studies.study_name
                   FROM studies
                  WHERE ((studies.disease)::text = 'Bladder Cancer'::text)))) cel
     LEFT JOIN cdna c USING (cdna_id))
     LEFT JOIN rna r USING (rna_id))
     LEFT JOIN samples s USING (sample_id))
     LEFT JOIN patients p USING (patient_id))
     LEFT JOIN signatures_clindev_v2.score score USING (celfile_name))
     LEFT JOIN signatures_clindev_v2.class class USING (celfile_name))
  WHERE (s.sample_type IS NOT NULL);


ALTER TABLE bladder_qc_info OWNER TO samplesdb_table_owner;

--
-- Name: bladder_qc_info; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE bladder_qc_info FROM PUBLIC;
REVOKE ALL ON TABLE bladder_qc_info FROM samplesdb_table_owner;
GRANT ALL ON TABLE bladder_qc_info TO samplesdb_table_owner;
GRANT SELECT ON TABLE bladder_qc_info TO laura;


--GO
--rollback DROP VIEW bladder_qc_info;
