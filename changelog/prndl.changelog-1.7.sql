--liquibase formatted sql
--changeset mpdillon:signatures_clindev_v2 endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: signatures_clindev_v2; Type: SCHEMA; Schema: -; Owner: grid_admin
--

CREATE SCHEMA signatures_clindev_v2;


ALTER SCHEMA signatures_clindev_v2 OWNER TO grid_admin;

--
-- Name: SCHEMA signatures_clindev_v2; Type: COMMENT; Schema: -; Owner: grid_admin
--

COMMENT ON SCHEMA signatures_clindev_v2 IS 'standard public schema';


SET search_path = signatures_clindev_v2, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: feature; Type: TABLE; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE TABLE feature (
    feature character varying(32) NOT NULL,
    signature_id bigint NOT NULL,
    level character varying(16) NOT NULL,
    normalization character varying(16) NOT NULL,
    normalization_environment character varying(32) NOT NULL,
    "order" bigint NOT NULL
);


ALTER TABLE feature OWNER TO grid_admin;

--
-- Name: prediction; Type: TABLE; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE TABLE prediction (
    signature_id bigint NOT NULL,
    celfile_name character varying(255) NOT NULL,
    score double precision NOT NULL,
    class character varying(64),
    percentile double precision
);


ALTER TABLE prediction OWNER TO grid_admin;

--
-- Name: signature; Type: TABLE; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE TABLE signature (
    signature_id bigint DEFAULT nextval('signatures.signature_signature_id_seq'::regclass) NOT NULL,
    name character varying(128) NOT NULL,
    version double precision,
    date date,
    disease character varying(128),
    endpoint character varying(128),
    model_type character varying(64),
    description text,
    institution character varying(256),
    inventor character varying(128),
    citation character varying(1024),
    predict_function text NOT NULL,
    predict_env character varying(32) NOT NULL,
    dev_script text,
    model_obj text,
    created timestamp without time zone NOT NULL
);


ALTER TABLE signature OWNER TO grid_admin;

--
-- Name: TABLE signature; Type: COMMENT; Schema: signatures_clindev_v2; Owner: grid_admin
--

COMMENT ON TABLE signature IS 'Please use signature_view instead unless you know what you are doing.';


--
-- Name: bca_subtype; Type: VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE VIEW bca_subtype AS
 SELECT pr.celfile_name,
        CASE
            WHEN ((si.name)::text = 'bca_subtyping_basal'::text) THEN 'Basal'::text
            WHEN ((si.name)::text = 'bca_subtyping_luminal'::text) THEN 'Luminal'::text
            WHEN ((si.name)::text = 'bca_subtyping_infiltrated_luminal'::text) THEN 'Infiltrated Luminal'::text
            WHEN ((si.name)::text = 'bca_subtyping_claudin_low'::text) THEN 'Claudin Low'::text
            ELSE NULL::text
        END AS bca_subtype
   FROM ((prediction pr
     RIGHT JOIN signature si USING (signature_id))
     JOIN ( SELECT prediction.celfile_name,
            max(prediction.score) AS score
           FROM (prediction
             RIGHT JOIN signature USING (signature_id))
          WHERE (signature.signature_id = ANY (ARRAY[(74)::bigint, (75)::bigint, (76)::bigint, (77)::bigint]))
          GROUP BY prediction.celfile_name) mq ON ((((pr.celfile_name)::text = (mq.celfile_name)::text) AND (pr.score = mq.score) AND (si.signature_id = ANY (ARRAY[(74)::bigint, (75)::bigint, (76)::bigint, (77)::bigint])))));


ALTER TABLE bca_subtype OWNER TO grid_admin;

--
-- Name: molecular_subtype; Type: VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE VIEW molecular_subtype AS
 SELECT ct.celfile_name,
        CASE
            WHEN ((pr.class IS NULL) OR (ct."ETV1" IS NULL) OR (ct."ETV4" IS NULL) OR (ct."ETV5" IS NULL) OR (ct."FLI1" IS NULL) OR (ct."SPINK1" IS NULL)) THEN NULL::text
            WHEN ((pr.class)::text = 'ERG positive'::text) THEN 'ERG+'::text
            WHEN (((ct."ETV1")::text = 'High'::text) OR ((ct."ETV4")::text = 'High'::text) OR ((ct."ETV5")::text = 'High'::text) OR ((ct."FLI1")::text = 'High'::text)) THEN 'ETS+'::text
            WHEN ((ct."SPINK1")::text = 'High'::text) THEN 'SPINK1+'::text
            ELSE 'Triple Negative'::text
        END AS molecular_subtype
   FROM (samplesdb.crosstab('SELECT celfile_name, biomarker, class FROM grid_clindev_v2.transcription WHERE biomarker IN (''ETV1'', ''ETV4'', ''ETV5'', ''FLI1'', ''SPINK1'') ORDER BY celfile_name'::text, 'SELECT DISTINCT biomarker FROM grid_clindev_v2.transcription WHERE biomarker IN (''ETV1'', ''ETV4'', ''ETV5'', ''FLI1'', ''SPINK1'') ORDER BY 1'::text) ct(celfile_name character varying(255), "ETV1" character varying(64), "ETV4" character varying(64), "ETV5" character varying(64), "FLI1" character varying(64), "SPINK1" character varying(64))
     JOIN prediction pr ON ((((pr.celfile_name)::text = (ct.celfile_name)::text) AND (pr.signature_id = 1))));


ALTER TABLE molecular_subtype OWNER TO grid_admin;

--
-- Name: class; Type: MATERIALIZED VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE MATERIALIZED VIEW class AS
 SELECT ct.celfile_name,
    ct.agell2012_lm_1,
    ct.aros_1,
    ct.ars_1,
    ct.beltran2016_1,
    ct.bibikova2007_lm_1,
    ct.bismar2006_lm_1,
    ct.cheville2008_lm_1,
    ct.cuzick2011_1,
    ct.cuzick2011_lm_1,
    ct.dasatinib_sens_1,
    ct.decipher_1,
    ct.decipherv2_1,
    ct.decipherv2_2,
    ct.docetaxel_sens_1,
    ct.ergmodel_1,
    ct.genomic_capras_1,
    ct.genomic_gleason_grade_1,
    ct.genomic_gleason_grade_2,
    ct.glinsky2004_lm_1,
    ct.glinsky2005_lm_1,
    ct.klein2014_lm_1,
    ct.lapointe2004_lm_1,
    ct.larkin2012_lm_1,
    ct.long2011_lm_1,
    ct.long2014_1,
    ct.lotan2016_1,
    ct.nakagawa2008_lm_1,
    ct.nelson2016_1,
    ct."nelson_2016_AR_1",
    ct.non_organ_confined_1,
    ct.normaltumor_1,
    ct.pca_vs_mibc_1,
    ct.penney2011_1,
    ct.penney2011_lm_1,
    ct.portos_1,
    ct.ramaswamy2003_lm_1,
    ct.saal2007_lm_1,
    ct.saal2007_pten_2,
    ct.singh2002_lm_1,
    ct.smallcell_1,
    ct.smallcell_2,
    ct.stephenson2005_lm_1,
    ct.talantov2010_lm_1,
    ct.torresroca2009_1,
    ct.varambally2005_lm_1,
    ct.wu2013_lm_1,
    ct.yu2007_lm_1,
    ct.zhang2016_basal_1,
    ms.molecular_subtype,
    bs.bca_subtype
   FROM ((samplesdb.crosstab('SELECT celfile_name, name || $$_$$ || version AS name, class FROM signatures_clindev_v2.prediction JOIN signatures_clindev_v2.signature USING (signature_id) ORDER BY celfile_name'::text, 'SELECT DISTINCT name || $$_$$ || version AS name FROM signatures_clindev_v2.signature JOIN signatures_clindev_v2.cutpoint USING (signature_id) ORDER BY 1'::text) ct(celfile_name character varying(255), agell2012_lm_1 character varying(64), aros_1 character varying(64), ars_1 character varying(64), beltran2016_1 character varying(64), bibikova2007_lm_1 character varying(64), bismar2006_lm_1 character varying(64), cheville2008_lm_1 character varying(64), cuzick2011_1 character varying(64), cuzick2011_lm_1 character varying(64), dasatinib_sens_1 character varying(64), decipher_1 character varying(64), decipherv2_1 character varying(64), decipherv2_2 character varying(64), docetaxel_sens_1 character varying(64), ergmodel_1 character varying(64), genomic_capras_1 character varying(64), genomic_gleason_grade_1 character varying(64), genomic_gleason_grade_2 character varying(64), glinsky2004_lm_1 character varying(64), glinsky2005_lm_1 character varying(64), klein2014_lm_1 character varying(64), lapointe2004_lm_1 character varying(64), larkin2012_lm_1 character varying(64), long2011_lm_1 character varying(64), long2014_1 character varying(64), lotan2016_1 character varying(64), nakagawa2008_lm_1 character varying(64), nelson2016_1 character varying(64), "nelson_2016_AR_1" character varying(64), non_organ_confined_1 character varying(64), normaltumor_1 character varying(64), pca_vs_mibc_1 character varying(64), penney2011_1 character varying(64), penney2011_lm_1 character varying(64), portos_1 character varying(64), ramaswamy2003_lm_1 character varying(64), saal2007_lm_1 character varying(64), saal2007_pten_2 character varying(64), singh2002_lm_1 character varying(64), smallcell_1 character varying(64), smallcell_2 character varying(64), stephenson2005_lm_1 character varying(64), talantov2010_lm_1 character varying(64), torresroca2009_1 character varying(64), varambally2005_lm_1 character varying(64), wu2013_lm_1 character varying(64), yu2007_lm_1 character varying(64), zhang2016_basal_1 character varying(64))
     LEFT JOIN molecular_subtype ms USING (celfile_name))
     LEFT JOIN bca_subtype bs USING (celfile_name))
  WITH NO DATA;


ALTER TABLE class OWNER TO grid_admin;

--
-- Name: score; Type: MATERIALIZED VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE MATERIALIZED VIEW score AS
 SELECT ct.celfile_name,
    ct.agell2012_lm_1,
    ct.aros_1,
    ct.ars_1,
    ct.bca_subtyping_basal_0,
    ct.bca_subtyping_basal_1,
    ct.bca_subtyping_claudin_low_0,
    ct.bca_subtyping_claudin_low_1,
    ct.bca_subtyping_infiltrated_luminal_0,
    ct.bca_subtyping_infiltrated_luminal_1,
    ct.bca_subtyping_luminal_0,
    ct.bca_subtyping_luminal_1,
    ct.beltran2016_1,
    ct.bibikova2007_lm_1,
    ct.bismar2006_lm_1,
    ct.cheville2008_lm_1,
    ct.cuzick2011_1,
    ct.cuzick2011_lm_1,
    ct.dasatinib_sens_1,
    ct.decipher_1,
    ct.decipherv2_1,
    ct.decipherv2_2,
    ct.docetaxel_sens_1,
    ct.ergmodel_1,
    ct.genomic_capras_1,
    ct.genomic_gleason_grade_1,
    ct.genomic_gleason_grade_2,
    ct.glinsky2004_lm_1,
    ct.glinsky2005_lm_1,
    ct.klein2014_lm_1,
    ct.lapointe2004_lm_1,
    ct.larkin2012_lm_1,
    ct.long2011_lm_1,
    ct.long2014_1,
    ct.lotan2016_1,
    ct.nakagawa2008_lm_1,
    ct.nelson2016_1,
    ct."nelson_2016_AR_1",
    ct.non_organ_confined_1,
    ct.normaltumor_1,
    ct.pca_vs_mibc_1,
    ct.penney2011_1,
    ct.penney2011_lm_1,
    ct.portos_1,
    ct.ragnum2015_1,
    ct.ramaswamy2003_lm_1,
    ct.saal2007_lm_1,
    ct.saal2007_pten_2,
    ct.singh2002_lm_1,
    ct.smallcell_1,
    ct.smallcell_2,
    ct.stephenson2005_lm_1,
    ct.talantov2010_lm_1,
    ct.torresroca2009_1,
    ct.varambally2005_lm_1,
    ct.wu2013_lm_1,
    ct.yu2007_lm_1,
    ct.zhang2016_basal_1
   FROM samplesdb.crosstab('SELECT celfile_name, name || $$_$$ || version AS name, score FROM signatures_clindev_v2.prediction JOIN signatures_clindev_v2.signature USING (signature_id) ORDER BY celfile_name'::text, 'SELECT name || $$_$$ || version AS name FROM signatures_clindev_v2.signature ORDER BY 1'::text) ct(celfile_name character varying(255), agell2012_lm_1 double precision, aros_1 double precision, ars_1 double precision, bca_subtyping_basal_0 double precision, bca_subtyping_basal_1 double precision, bca_subtyping_claudin_low_0 double precision, bca_subtyping_claudin_low_1 double precision, bca_subtyping_infiltrated_luminal_0 double precision, bca_subtyping_infiltrated_luminal_1 double precision, bca_subtyping_luminal_0 double precision, bca_subtyping_luminal_1 double precision, beltran2016_1 double precision, bibikova2007_lm_1 double precision, bismar2006_lm_1 double precision, cheville2008_lm_1 double precision, cuzick2011_1 double precision, cuzick2011_lm_1 double precision, dasatinib_sens_1 double precision, decipher_1 double precision, decipherv2_1 double precision, decipherv2_2 double precision, docetaxel_sens_1 double precision, ergmodel_1 double precision, genomic_capras_1 double precision, genomic_gleason_grade_1 double precision, genomic_gleason_grade_2 double precision, glinsky2004_lm_1 double precision, glinsky2005_lm_1 double precision, klein2014_lm_1 double precision, lapointe2004_lm_1 double precision, larkin2012_lm_1 double precision, long2011_lm_1 double precision, long2014_1 double precision, lotan2016_1 double precision, nakagawa2008_lm_1 double precision, nelson2016_1 double precision, "nelson_2016_AR_1" double precision, non_organ_confined_1 double precision, normaltumor_1 double precision, pca_vs_mibc_1 double precision, penney2011_1 double precision, penney2011_lm_1 double precision, portos_1 double precision, ragnum2015_1 double precision, ramaswamy2003_lm_1 double precision, saal2007_lm_1 double precision, saal2007_pten_2 double precision, singh2002_lm_1 double precision, smallcell_1 double precision, smallcell_2 double precision, stephenson2005_lm_1 double precision, talantov2010_lm_1 double precision, torresroca2009_1 double precision, varambally2005_lm_1 double precision, wu2013_lm_1 double precision, yu2007_lm_1 double precision, zhang2016_basal_1 double precision)
  WITH NO DATA;


ALTER TABLE score OWNER TO grid_admin;

--
-- Name: celfilegroup; Type: TABLE; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE TABLE celfilegroup (
    celfile_name character varying(255) NOT NULL,
    signature_id bigint NOT NULL
);


ALTER TABLE celfilegroup OWNER TO grid_admin;

--
-- Name: class_bioinfo; Type: VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE VIEW class_bioinfo AS
 SELECT class.celfile_name,
    class.agell2012_lm_1,
    class.aros_1,
    class.ars_1,
    class.beltran2016_1,
    class.bibikova2007_lm_1,
    class.bismar2006_lm_1,
    class.cheville2008_lm_1,
    class.cuzick2011_1,
    class.cuzick2011_lm_1,
    class.dasatinib_sens_1,
    class.decipher_1,
    class.decipherv2_1,
    class.decipherv2_2,
    class.docetaxel_sens_1,
    class.ergmodel_1,
    class.genomic_capras_1,
    class.genomic_gleason_grade_1,
    class.genomic_gleason_grade_2,
    class.glinsky2004_lm_1,
    class.glinsky2005_lm_1,
    class.klein2014_lm_1,
    class.lapointe2004_lm_1,
    class.larkin2012_lm_1,
    class.long2011_lm_1,
    class.long2014_1,
    class.lotan2016_1,
    class.nakagawa2008_lm_1,
    class.nelson2016_1,
    class."nelson_2016_AR_1",
    class.non_organ_confined_1,
    class.normaltumor_1,
    class.pca_vs_mibc_1,
    class.penney2011_1,
    class.penney2011_lm_1,
    class.portos_1,
    class.ramaswamy2003_lm_1,
    class.saal2007_lm_1,
    class.saal2007_pten_2,
    class.singh2002_lm_1,
    class.smallcell_1,
    class.smallcell_2,
    class.stephenson2005_lm_1,
    class.talantov2010_lm_1,
    class.torresroca2009_1,
    class.varambally2005_lm_1,
    class.wu2013_lm_1,
    class.yu2007_lm_1,
    class.zhang2016_basal_1,
    class.molecular_subtype,
    class.bca_subtype
   FROM class
  WHERE (((class.celfile_name)::text ~~ 'DP%'::text) OR ((class.celfile_name)::text IN ( SELECT bioinfo_celfile.celfile_name
           FROM commercial.bioinfo_celfile)));


ALTER TABLE class_bioinfo OWNER TO grid_admin;

--
-- Name: cutpoint; Type: TABLE; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE TABLE cutpoint (
    signature_id bigint NOT NULL,
    label character varying(64) NOT NULL,
    cutpoint double precision NOT NULL
);


ALTER TABLE cutpoint OWNER TO grid_admin;

--
-- Name: environment; Type: TABLE; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE TABLE environment (
    name character varying(32) NOT NULL,
    repository character varying(1028) NOT NULL,
    tag character varying(128) NOT NULL,
    port integer NOT NULL,
    description text,
    dockerfile text,
    created timestamp without time zone
);


ALTER TABLE environment OWNER TO grid_admin;

--
-- Name: percentile; Type: MATERIALIZED VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE MATERIALIZED VIEW percentile AS
 SELECT ct.celfile_name,
    ct.agell2012_lm_1,
    ct.aros_1,
    ct.ars_1,
    ct.bca_subtyping_basal_0,
    ct.bca_subtyping_basal_1,
    ct.bca_subtyping_claudin_low_0,
    ct.bca_subtyping_claudin_low_1,
    ct.bca_subtyping_infiltrated_luminal_0,
    ct.bca_subtyping_infiltrated_luminal_1,
    ct.bca_subtyping_luminal_0,
    ct.bca_subtyping_luminal_1,
    ct.beltran2016_1,
    ct.bibikova2007_lm_1,
    ct.bismar2006_lm_1,
    ct.cheville2008_lm_1,
    ct.cuzick2011_1,
    ct.cuzick2011_lm_1,
    ct.dasatinib_sens_1,
    ct.decipher_1,
    ct.decipherv2_1,
    ct.decipherv2_2,
    ct.docetaxel_sens_1,
    ct.ergmodel_1,
    ct.genomic_capras_1,
    ct.genomic_gleason_grade_1,
    ct.genomic_gleason_grade_2,
    ct.glinsky2004_lm_1,
    ct.glinsky2005_lm_1,
    ct.klein2014_lm_1,
    ct.lapointe2004_lm_1,
    ct.larkin2012_lm_1,
    ct.long2011_lm_1,
    ct.long2014_1,
    ct.lotan2016_1,
    ct.nakagawa2008_lm_1,
    ct.nelson2016_1,
    ct."nelson_2016_AR_1",
    ct.non_organ_confined_1,
    ct.normaltumor_1,
    ct.pca_vs_mibc_1,
    ct.penney2011_1,
    ct.penney2011_lm_1,
    ct.portos_1,
    ct.ragnum2015_1,
    ct.ramaswamy2003_lm_1,
    ct.saal2007_lm_1,
    ct.saal2007_pten_2,
    ct.singh2002_lm_1,
    ct.smallcell_1,
    ct.smallcell_2,
    ct.stephenson2005_lm_1,
    ct.talantov2010_lm_1,
    ct.torresroca2009_1,
    ct.varambally2005_lm_1,
    ct.wu2013_lm_1,
    ct.yu2007_lm_1,
    ct.zhang2016_basal_1
   FROM samplesdb.crosstab('SELECT celfile_name, name || $$_$$ || version AS name, percentile FROM signatures_clindev_v2.prediction JOIN signatures_clindev_v2.signature USING (signature_id) ORDER BY celfile_name'::text, 'SELECT name || $$_$$ || version AS name FROM signatures_clindev_v2.signature ORDER BY 1'::text) ct(celfile_name character varying(255), agell2012_lm_1 double precision, aros_1 double precision, ars_1 double precision, bca_subtyping_basal_0 double precision, bca_subtyping_basal_1 double precision, bca_subtyping_claudin_low_0 double precision, bca_subtyping_claudin_low_1 double precision, bca_subtyping_infiltrated_luminal_0 double precision, bca_subtyping_infiltrated_luminal_1 double precision, bca_subtyping_luminal_0 double precision, bca_subtyping_luminal_1 double precision, beltran2016_1 double precision, bibikova2007_lm_1 double precision, bismar2006_lm_1 double precision, cheville2008_lm_1 double precision, cuzick2011_1 double precision, cuzick2011_lm_1 double precision, dasatinib_sens_1 double precision, decipher_1 double precision, decipherv2_1 double precision, decipherv2_2 double precision, docetaxel_sens_1 double precision, ergmodel_1 double precision, genomic_capras_1 double precision, genomic_gleason_grade_1 double precision, genomic_gleason_grade_2 double precision, glinsky2004_lm_1 double precision, glinsky2005_lm_1 double precision, klein2014_lm_1 double precision, lapointe2004_lm_1 double precision, larkin2012_lm_1 double precision, long2011_lm_1 double precision, long2014_1 double precision, lotan2016_1 double precision, nakagawa2008_lm_1 double precision, nelson2016_1 double precision, "nelson_2016_AR_1" double precision, non_organ_confined_1 double precision, normaltumor_1 double precision, pca_vs_mibc_1 double precision, penney2011_1 double precision, penney2011_lm_1 double precision, portos_1 double precision, ragnum2015_1 double precision, ramaswamy2003_lm_1 double precision, saal2007_lm_1 double precision, saal2007_pten_2 double precision, singh2002_lm_1 double precision, smallcell_1 double precision, smallcell_2 double precision, stephenson2005_lm_1 double precision, talantov2010_lm_1 double precision, torresroca2009_1 double precision, varambally2005_lm_1 double precision, wu2013_lm_1 double precision, yu2007_lm_1 double precision, zhang2016_basal_1 double precision)
  WITH NO DATA;


ALTER TABLE percentile OWNER TO grid_admin;

--
-- Name: percentile_bioinfo; Type: VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE VIEW percentile_bioinfo AS
 SELECT percentile.celfile_name,
    percentile.agell2012_lm_1,
    percentile.aros_1,
    percentile.ars_1,
    percentile.bca_subtyping_basal_0,
    percentile.bca_subtyping_basal_1,
    percentile.bca_subtyping_claudin_low_0,
    percentile.bca_subtyping_claudin_low_1,
    percentile.bca_subtyping_infiltrated_luminal_0,
    percentile.bca_subtyping_infiltrated_luminal_1,
    percentile.bca_subtyping_luminal_0,
    percentile.bca_subtyping_luminal_1,
    percentile.beltran2016_1,
    percentile.bibikova2007_lm_1,
    percentile.bismar2006_lm_1,
    percentile.cheville2008_lm_1,
    percentile.cuzick2011_1,
    percentile.cuzick2011_lm_1,
    percentile.dasatinib_sens_1,
    percentile.decipher_1,
    percentile.decipherv2_1,
    percentile.decipherv2_2,
    percentile.docetaxel_sens_1,
    percentile.ergmodel_1,
    percentile.genomic_capras_1,
    percentile.genomic_gleason_grade_1,
    percentile.genomic_gleason_grade_2,
    percentile.glinsky2004_lm_1,
    percentile.glinsky2005_lm_1,
    percentile.klein2014_lm_1,
    percentile.lapointe2004_lm_1,
    percentile.larkin2012_lm_1,
    percentile.long2011_lm_1,
    percentile.long2014_1,
    percentile.lotan2016_1,
    percentile.nakagawa2008_lm_1,
    percentile.nelson2016_1,
    percentile."nelson_2016_AR_1",
    percentile.non_organ_confined_1,
    percentile.normaltumor_1,
    percentile.pca_vs_mibc_1,
    percentile.penney2011_1,
    percentile.penney2011_lm_1,
    percentile.portos_1,
    percentile.ragnum2015_1,
    percentile.ramaswamy2003_lm_1,
    percentile.saal2007_lm_1,
    percentile.saal2007_pten_2,
    percentile.singh2002_lm_1,
    percentile.smallcell_1,
    percentile.smallcell_2,
    percentile.stephenson2005_lm_1,
    percentile.talantov2010_lm_1,
    percentile.torresroca2009_1,
    percentile.varambally2005_lm_1,
    percentile.wu2013_lm_1,
    percentile.yu2007_lm_1,
    percentile.zhang2016_basal_1
   FROM percentile
  WHERE (((percentile.celfile_name)::text ~~ 'DP%'::text) OR ((percentile.celfile_name)::text IN ( SELECT bioinfo_celfile.celfile_name
           FROM commercial.bioinfo_celfile)));


ALTER TABLE percentile_bioinfo OWNER TO grid_admin;

--
-- Name: score_bioinfo; Type: VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE VIEW score_bioinfo AS
 SELECT score.celfile_name,
    score.agell2012_lm_1,
    score.aros_1,
    score.ars_1,
    score.bca_subtyping_basal_0,
    score.bca_subtyping_basal_1,
    score.bca_subtyping_claudin_low_0,
    score.bca_subtyping_claudin_low_1,
    score.bca_subtyping_infiltrated_luminal_0,
    score.bca_subtyping_infiltrated_luminal_1,
    score.bca_subtyping_luminal_0,
    score.bca_subtyping_luminal_1,
    score.beltran2016_1,
    score.bibikova2007_lm_1,
    score.bismar2006_lm_1,
    score.cheville2008_lm_1,
    score.cuzick2011_1,
    score.cuzick2011_lm_1,
    score.dasatinib_sens_1,
    score.decipher_1,
    score.decipherv2_1,
    score.decipherv2_2,
    score.docetaxel_sens_1,
    score.ergmodel_1,
    score.genomic_capras_1,
    score.genomic_gleason_grade_1,
    score.genomic_gleason_grade_2,
    score.glinsky2004_lm_1,
    score.glinsky2005_lm_1,
    score.klein2014_lm_1,
    score.lapointe2004_lm_1,
    score.larkin2012_lm_1,
    score.long2011_lm_1,
    score.long2014_1,
    score.lotan2016_1,
    score.nakagawa2008_lm_1,
    score.nelson2016_1,
    score."nelson_2016_AR_1",
    score.non_organ_confined_1,
    score.normaltumor_1,
    score.pca_vs_mibc_1,
    score.penney2011_1,
    score.penney2011_lm_1,
    score.portos_1,
    score.ragnum2015_1,
    score.ramaswamy2003_lm_1,
    score.saal2007_lm_1,
    score.saal2007_pten_2,
    score.singh2002_lm_1,
    score.smallcell_1,
    score.smallcell_2,
    score.stephenson2005_lm_1,
    score.talantov2010_lm_1,
    score.torresroca2009_1,
    score.varambally2005_lm_1,
    score.wu2013_lm_1,
    score.yu2007_lm_1,
    score.zhang2016_basal_1
   FROM score
  WHERE (((score.celfile_name)::text ~~ 'DP%'::text) OR ((score.celfile_name)::text IN ( SELECT bioinfo_celfile.celfile_name
           FROM commercial.bioinfo_celfile)));


ALTER TABLE score_bioinfo OWNER TO grid_admin;

--
-- Name: signature_view; Type: VIEW; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE VIEW signature_view AS
 SELECT s.signature_id,
    s.name,
    s.version,
    s.endpoint,
    s.model_type,
    s.description,
    f.feature_n,
    f.level,
    f.normalization,
    cgrp.disc_celfile_n,
    cgrp.disc_studies,
    c.cutoff,
    s.disease,
    s.date,
    s.institution,
    s.inventor,
    s.citation
   FROM (((signature s
     LEFT JOIN ( SELECT feature.signature_id,
            count(1) AS feature_n,
            string_agg(DISTINCT (feature.level)::text, ';'::text) AS level,
            string_agg(DISTINCT (feature.normalization)::text, ';'::text) AS normalization
           FROM feature
          GROUP BY feature.signature_id) f USING (signature_id))
     LEFT JOIN ( SELECT cutpoint.signature_id,
            string_agg(((cutpoint.cutpoint || ' > '::text) || (cutpoint.label)::text), '; '::text) AS cutoff
           FROM cutpoint
          GROUP BY cutpoint.signature_id) c USING (signature_id))
     LEFT JOIN ( SELECT celgroup.signature_id,
            count(celgroup.celfile_name) AS disc_celfile_n,
            string_agg(DISTINCT (celgroup.study_name)::text, ';'::text) AS disc_studies
           FROM ( SELECT cg.signature_id,
                    cg.celfile_name,
                        CASE
                            WHEN (((cg.celfile_name)::text ~~ 'DPR%'::text) OR ((cg.celfile_name)::text ~~ 'DPX%'::text)) THEN 'Prospective'::character varying
                            ELSE cel.study_name
                        END AS study_name
                   FROM (celfilegroup cg
                     LEFT JOIN samplesdb.celfiles cel USING (celfile_name))) celgroup
          GROUP BY celgroup.signature_id) cgrp USING (signature_id));


ALTER TABLE signature_view OWNER TO grid_admin;

--
-- Name: celfilegroup_pkey; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY celfilegroup
    ADD CONSTRAINT celfilegroup_pkey PRIMARY KEY (signature_id, celfile_name);


--
-- Name: clindev_v2_score_pkey; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY prediction
    ADD CONSTRAINT clindev_v2_score_pkey PRIMARY KEY (signature_id, celfile_name);


--
-- Name: cutpoint_pkey; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY cutpoint
    ADD CONSTRAINT cutpoint_pkey PRIMARY KEY (signature_id, label);


--
-- Name: environment_pkey; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY environment
    ADD CONSTRAINT environment_pkey PRIMARY KEY (name);


--
-- Name: environment_port_key; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY environment
    ADD CONSTRAINT environment_port_key UNIQUE (port);


--
-- Name: feature_pkey; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY feature
    ADD CONSTRAINT feature_pkey PRIMARY KEY (signature_id, feature, level);


--
-- Name: signature_name_version_key; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_name_version_key UNIQUE (name, version);


--
-- Name: signature_pkey; Type: CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_pkey PRIMARY KEY (signature_id);


--
-- Name: feature_feature_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX feature_feature_idx ON feature USING btree (feature);


--
-- Name: feature_level_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX feature_level_idx ON feature USING btree (level);


--
-- Name: feature_normalization_environment_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX feature_normalization_environment_idx ON feature USING btree (normalization_environment);


--
-- Name: feature_signature_id_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX feature_signature_id_idx ON feature USING btree (signature_id);


--
-- Name: prediction_score_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX prediction_score_idx ON prediction USING btree (score);


--
-- Name: score_celfile_name_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX score_celfile_name_idx ON prediction USING btree (celfile_name);


--
-- Name: score_class_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX score_class_idx ON prediction USING btree (class);


--
-- Name: score_signature_id_idx; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE INDEX score_signature_id_idx ON prediction USING btree (signature_id);


--
-- Name: signatures_class_celfile_name; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE UNIQUE INDEX signatures_class_celfile_name ON class USING btree (celfile_name);


--
-- Name: signatures_percentile_celfile_name; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE UNIQUE INDEX signatures_percentile_celfile_name ON percentile USING btree (celfile_name);


--
-- Name: signatures_score_celfile_name; Type: INDEX; Schema: signatures_clindev_v2; Owner: grid_admin
--

CREATE UNIQUE INDEX signatures_score_celfile_name ON score USING btree (celfile_name);


--
-- Name: clindev_v2_score_signature_id_fkey; Type: FK CONSTRAINT; Schema: signatures_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY prediction
    ADD CONSTRAINT clindev_v2_score_signature_id_fkey FOREIGN KEY (signature_id) REFERENCES signature(signature_id);


--
-- Name: signatures_clindev_v2; Type: ACL; Schema: -; Owner: grid_admin
--

REVOKE ALL ON SCHEMA signatures_clindev_v2 FROM PUBLIC;
REVOKE ALL ON SCHEMA signatures_clindev_v2 FROM grid_admin;
GRANT ALL ON SCHEMA signatures_clindev_v2 TO grid_admin;
GRANT USAGE ON SCHEMA signatures_clindev_v2 TO clinops;
GRANT USAGE ON SCHEMA signatures_clindev_v2 TO biostats;
GRANT USAGE ON SCHEMA signatures_clindev_v2 TO bioinfo;
GRANT USAGE ON SCHEMA signatures_clindev_v2 TO model_developer;
GRANT ALL ON SCHEMA signatures_clindev_v2 TO administrator;
GRANT USAGE ON SCHEMA signatures_clindev_v2 TO engineering;


--
-- Name: feature; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE feature FROM PUBLIC;
REVOKE ALL ON TABLE feature FROM grid_admin;
GRANT ALL ON TABLE feature TO grid_admin;
GRANT SELECT ON TABLE feature TO bioinfo;
GRANT SELECT ON TABLE feature TO biostats;
GRANT SELECT ON TABLE feature TO clinops;
GRANT SELECT ON TABLE feature TO engineering;


--
-- Name: prediction; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE prediction FROM PUBLIC;
REVOKE ALL ON TABLE prediction FROM grid_admin;
GRANT ALL ON TABLE prediction TO grid_admin;
GRANT SELECT ON TABLE prediction TO clinops;
GRANT SELECT ON TABLE prediction TO engineering;


--
-- Name: signature; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE signature FROM PUBLIC;
REVOKE ALL ON TABLE signature FROM grid_admin;
GRANT ALL ON TABLE signature TO grid_admin;
GRANT SELECT ON TABLE signature TO biostats;
GRANT SELECT ON TABLE signature TO bioinfo;
GRANT SELECT ON TABLE signature TO clinops;
GRANT SELECT ON TABLE signature TO engineering;


--
-- Name: class; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE class FROM PUBLIC;
REVOKE ALL ON TABLE class FROM grid_admin;
GRANT ALL ON TABLE class TO grid_admin;
GRANT SELECT ON TABLE class TO clinops;
GRANT SELECT ON TABLE class TO biostats;
GRANT SELECT ON TABLE class TO samplesdb_table_owner;
GRANT SELECT ON TABLE class TO engineering;


--
-- Name: score; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE score FROM PUBLIC;
REVOKE ALL ON TABLE score FROM grid_admin;
GRANT ALL ON TABLE score TO grid_admin;
GRANT SELECT ON TABLE score TO clinops;
GRANT SELECT ON TABLE score TO biostats;
GRANT SELECT ON TABLE score TO samplesdb_table_owner;
GRANT SELECT ON TABLE score TO engineering;


--
-- Name: celfilegroup; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE celfilegroup FROM PUBLIC;
REVOKE ALL ON TABLE celfilegroup FROM grid_admin;
GRANT ALL ON TABLE celfilegroup TO grid_admin;
GRANT SELECT ON TABLE celfilegroup TO bioinfo;
GRANT SELECT ON TABLE celfilegroup TO biostats;
GRANT SELECT ON TABLE celfilegroup TO clinops;
GRANT SELECT ON TABLE celfilegroup TO engineering;


--
-- Name: class_bioinfo; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE class_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE class_bioinfo FROM grid_admin;
GRANT ALL ON TABLE class_bioinfo TO grid_admin;
GRANT SELECT ON TABLE class_bioinfo TO clinops;
GRANT SELECT ON TABLE class_bioinfo TO biostats;
GRANT SELECT ON TABLE class_bioinfo TO bioinfo;
GRANT SELECT ON TABLE class_bioinfo TO engineering;


--
-- Name: cutpoint; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE cutpoint FROM PUBLIC;
REVOKE ALL ON TABLE cutpoint FROM grid_admin;
GRANT ALL ON TABLE cutpoint TO grid_admin;
GRANT SELECT ON TABLE cutpoint TO bioinfo;
GRANT SELECT ON TABLE cutpoint TO biostats;
GRANT SELECT ON TABLE cutpoint TO clinops;
GRANT SELECT ON TABLE cutpoint TO engineering;


--
-- Name: environment; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE environment FROM PUBLIC;
REVOKE ALL ON TABLE environment FROM grid_admin;
GRANT ALL ON TABLE environment TO grid_admin;
GRANT SELECT ON TABLE environment TO bioinfo;
GRANT SELECT ON TABLE environment TO biostats;
GRANT SELECT ON TABLE environment TO clinops;
GRANT SELECT ON TABLE environment TO engineering;


--
-- Name: percentile; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE percentile FROM PUBLIC;
REVOKE ALL ON TABLE percentile FROM grid_admin;
GRANT ALL ON TABLE percentile TO grid_admin;
GRANT SELECT ON TABLE percentile TO clinops;
GRANT SELECT ON TABLE percentile TO biostats;
GRANT SELECT ON TABLE percentile TO engineering;


--
-- Name: percentile_bioinfo; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE percentile_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE percentile_bioinfo FROM grid_admin;
GRANT ALL ON TABLE percentile_bioinfo TO grid_admin;
GRANT SELECT ON TABLE percentile_bioinfo TO clinops;
GRANT SELECT ON TABLE percentile_bioinfo TO biostats;
GRANT SELECT ON TABLE percentile_bioinfo TO bioinfo;
GRANT SELECT ON TABLE percentile_bioinfo TO engineering;


--
-- Name: score_bioinfo; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE score_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE score_bioinfo FROM grid_admin;
GRANT ALL ON TABLE score_bioinfo TO grid_admin;
GRANT SELECT ON TABLE score_bioinfo TO clinops;
GRANT SELECT ON TABLE score_bioinfo TO biostats;
GRANT SELECT ON TABLE score_bioinfo TO bioinfo;
GRANT SELECT ON TABLE score_bioinfo TO engineering;


--
-- Name: signature_view; Type: ACL; Schema: signatures_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE signature_view FROM PUBLIC;
REVOKE ALL ON TABLE signature_view FROM grid_admin;
GRANT ALL ON TABLE signature_view TO grid_admin;
GRANT SELECT ON TABLE signature_view TO bioinfo;
GRANT SELECT ON TABLE signature_view TO biostats;
GRANT SELECT ON TABLE signature_view TO clinops;
GRANT SELECT ON TABLE signature_view TO engineering;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA signatures_clindev_v2;
