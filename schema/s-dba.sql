--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: dba; Type: SCHEMA; Schema: -; Owner: administrator
--

CREATE SCHEMA dba;


ALTER SCHEMA dba OWNER TO administrator;

--
-- Name: SCHEMA dba; Type: COMMENT; Schema: -; Owner: administrator
--

COMMENT ON SCHEMA dba IS 'This schema is for Databse Administrator use in managing the overall db health';


SET search_path = dba, pg_catalog;

--
-- Name: database_privs(text); Type: FUNCTION; Schema: dba; Owner: administrator
--

CREATE FUNCTION database_privs(text) RETURNS TABLE(username text, dbname name, privileges text[])
    LANGUAGE sql
    AS $_$
SELECT $1, datname, array(select privs from unnest(ARRAY[
( CASE WHEN has_database_privilege($1,c.oid,'CONNECT') THEN 'CONNECT' ELSE NULL END),
(CASE WHEN has_database_privilege($1,c.oid,'CREATE') THEN 'CREATE' ELSE NULL END),
(CASE WHEN has_database_privilege($1,c.oid,'TEMPORARY') THEN 'TEMPORARY' ELSE NULL END),
(CASE WHEN has_database_privilege($1,c.oid,'TEMP') THEN 'CONNECT' ELSE NULL END)])foo(privs) WHERE privs IS NOT NULL) FROM pg_database c WHERE
has_database_privilege($1,c.oid,'CONNECT,CREATE,TEMPORARY,TEMP') AND datname <> 'template0';
$_$;


ALTER FUNCTION dba.database_privs(text) OWNER TO administrator;

--
-- Name: exec(text); Type: FUNCTION; Schema: dba; Owner: administrator
--

CREATE FUNCTION exec(text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
  BEGIN
     EXECUTE $1;
  RETURN $1;
END; $_$;


ALTER FUNCTION dba.exec(text) OWNER TO administrator;

--
-- Name: inherited_roles(text); Type: FUNCTION; Schema: dba; Owner: administrator
--

CREATE FUNCTION inherited_roles(text) RETURNS TABLE(anoid oid, arolname text)
    LANGUAGE plpgsql
    AS $_$
BEGIN
RETURN QUERY WITH RECURSIVE cte AS (
   SELECT oid FROM pg_roles WHERE rolname = $1

   UNION ALL
   SELECT m.roleid
   FROM   cte
   JOIN   pg_auth_members m ON m.member = cte.oid
)
SELECT cte.oid as anoid, rolname::text as arolname
FROM cte
join pg_roles on pg_roles.oid = cte.oid;
END;
$_$;


ALTER FUNCTION dba.inherited_roles(text) OWNER TO administrator;

--
-- Name: active_locks; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW active_locks AS
 SELECT t.schemaname,
    t.relname,
    l.locktype,
    l.page,
    l.virtualtransaction,
    l.pid,
    l.mode,
    l.granted
   FROM (pg_locks l
     JOIN pg_stat_all_tables t ON ((l.relation = t.relid)))
  WHERE ((t.schemaname <> 'pg_toast'::name) AND (t.schemaname <> 'pg_catalog'::name))
  ORDER BY t.schemaname, t.relname;


ALTER TABLE active_locks OWNER TO administrator;

--
-- Name: created_roles; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW created_roles AS
 SELECT concat_ws(' '::text, 'CREATE ROLE ', pg_roles.rolname, 'WITH',
        CASE pg_roles.rolsuper
            WHEN true THEN 'SUPERUSER'::text
            ELSE 'NOSUPERUSER'::text
        END,
        CASE pg_roles.rolinherit
            WHEN true THEN 'INHERIT'::text
            ELSE 'NOINHERIT'::text
        END,
        CASE pg_roles.rolcreaterole
            WHEN true THEN 'CREATEROLE'::text
            ELSE 'NOCREATEROLE'::text
        END,
        CASE pg_roles.rolcreatedb
            WHEN true THEN 'CREATEDB'::text
            ELSE 'NOCREATEDB'::text
        END,
        CASE pg_roles.rolcanlogin
            WHEN true THEN 'LOGIN'::text
            ELSE 'NOLOGIN'::text
        END,
        CASE pg_roles.rolreplication
            WHEN true THEN 'REPLICATION'::text
            ELSE 'NOREPLICATION'::text
        END,
        CASE pg_roles.rolcanlogin
            WHEN true THEN 'PASSWORD ''sneakret'''::text
            ELSE NULL::text
        END, ';') AS concat_ws
   FROM pg_roles;


ALTER TABLE created_roles OWNER TO administrator;

--
-- Name: current_settings; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW current_settings AS
 SELECT pg_settings.name,
    current_setting(pg_settings.name) AS current_setting,
    pg_settings.source
   FROM pg_settings
  WHERE (pg_settings.source <> ALL (ARRAY['default'::text, 'override'::text]));


ALTER TABLE current_settings OWNER TO administrator;

--
-- Name: dependency; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW dependency AS
 WITH RECURSIVE preference AS (
         SELECT 10 AS max_depth,
            16384 AS min_oid,
            '^(londiste|pgq|pg_toast)'::text AS schema_exclusion,
            '^pg_(conversion|language|ts_(dict|template))'::text AS class_exclusion,
            '{"SCHEMA":"00", "TABLE":"01", "TABLE CONSTRAINT":"02", "DEFAULT VALUE":"03",
        "INDEX":"05", "SEQUENCE":"06", "TRIGGER":"07", "FUNCTION":"08",
        "VIEW":"10", "MATERIALIZED VIEW":"11", "FOREIGN TABLE":"12"}'::json AS type_sort_orders
        ), dependency_pair AS (
         SELECT dep.objid,
            array_agg(dep.objsubid ORDER BY dep.objsubid) AS objsubids,
            upper(obj.type) AS object_type,
            COALESCE(obj.schema, "substring"(obj.identity, '(\w+?)\.'::text), ''::text) AS object_schema,
            obj.name AS object_name,
            obj.identity AS object_identity,
            dep.refobjid,
            array_agg(dep.refobjsubid ORDER BY dep.refobjsubid) AS refobjsubids,
            upper(refobj.type) AS refobj_type,
            COALESCE(
                CASE
                    WHEN (refobj.type = 'schema'::text) THEN refobj.identity
                    ELSE refobj.schema
                END, "substring"(refobj.identity, '(\w+?)\.'::text), ''::text) AS refobj_schema,
            refobj.name AS refobj_name,
            refobj.identity AS refobj_identity,
                CASE dep.deptype
                    WHEN 'n'::"char" THEN 'normal'::text
                    WHEN 'a'::"char" THEN 'automatic'::text
                    WHEN 'i'::"char" THEN 'internal'::text
                    WHEN 'e'::"char" THEN 'extension'::text
                    WHEN 'p'::"char" THEN 'pinned'::text
                    ELSE NULL::text
                END AS dependency_type
           FROM pg_depend dep,
            LATERAL pg_identify_object(dep.classid, dep.objid, 0) obj(type, schema, name, identity),
            LATERAL pg_identify_object(dep.refclassid, dep.refobjid, 0) refobj(type, schema, name, identity),
            preference
          WHERE ((dep.deptype = ANY ('{n,a}'::"char"[])) AND (dep.objid >= (preference.min_oid)::oid) AND ((dep.refobjid >= (preference.min_oid)::oid) OR (dep.refobjid = (2200)::oid)) AND (COALESCE(obj.schema, "substring"(obj.identity, '(\w+?)\.'::text), ''::text) !~ preference.schema_exclusion) AND (COALESCE(
                CASE
                    WHEN (refobj.type = 'schema'::text) THEN refobj.identity
                    ELSE refobj.schema
                END, "substring"(refobj.identity, '(\w+?)\.'::text), ''::text) !~ preference.schema_exclusion))
          GROUP BY dep.objid, obj.type, obj.schema, obj.name, obj.identity, dep.refobjid, refobj.type, refobj.schema, refobj.name, refobj.identity, dep.deptype
        ), dependency_hierarchy AS (
         SELECT DISTINCT 0 AS level,
            root.refobjid AS objid,
            root.refobj_type AS object_type,
            root.refobj_identity AS object_identity,
            NULL::text AS dependency_type,
            ARRAY[root.refobjid] AS dependency_chain,
            ARRAY[concat((preference.type_sort_orders ->> root.refobj_type), root.refobj_type, ':', root.refobj_identity)] AS dependency_sort_chain
           FROM dependency_pair root,
            preference
          WHERE ((NOT (EXISTS ( SELECT 'x'
                   FROM dependency_pair branch
                  WHERE (branch.objid = root.refobjid)))) AND (root.refobj_schema !~ preference.schema_exclusion))
        UNION ALL
         SELECT (parent.level + 1) AS level,
            child.objid,
            child.object_type,
            child.object_identity,
            child.dependency_type,
            (parent.dependency_chain || child.objid),
            (parent.dependency_sort_chain || concat((preference.type_sort_orders ->> child.object_type), child.object_type, ':', child.object_identity))
           FROM (dependency_pair child
             JOIN dependency_hierarchy parent ON ((parent.objid = child.refobjid))),
            preference
          WHERE ((parent.level < preference.max_depth) AND (child.object_schema !~ preference.schema_exclusion) AND (child.refobj_schema !~ preference.schema_exclusion) AND (NOT (child.objid = ANY (parent.dependency_chain))))
        )
 SELECT dependency_hierarchy.level,
    dependency_hierarchy.objid,
    dependency_hierarchy.object_type,
    dependency_hierarchy.object_identity,
    dependency_hierarchy.dependency_type,
    dependency_hierarchy.dependency_chain,
    dependency_hierarchy.dependency_sort_chain
   FROM dependency_hierarchy
  ORDER BY dependency_hierarchy.dependency_chain;


ALTER TABLE dependency OWNER TO administrator;

--
-- Name: give_schemas_to_rds; Type: VIEW; Schema: dba; Owner: rds_superuser
--

CREATE VIEW give_schemas_to_rds AS
 SELECT (((('ALTER SCHEMA '::text || (nsp.nspname)::text) || ' OWNER TO '::text) || 'rds_superuser'::text) || ';'::text) AS stmt
   FROM pg_namespace nsp
  WHERE (nsp.nspname !~~ 'pg_%'::text);


ALTER TABLE give_schemas_to_rds OWNER TO rds_superuser;

--
-- Name: give_tables_to_rds; Type: VIEW; Schema: dba; Owner: rds_superuser
--

CREATE VIEW give_tables_to_rds AS
 SELECT (((('ALTER TABLE '::text || quote_ident((s.nspname)::text)) || '.'::text) || quote_ident((s.relname)::text)) || ' OWNER TO rds_superuser;'::text) AS cmd
   FROM ( SELECT n.nspname,
            c.relname
           FROM (pg_class c
             JOIN pg_namespace n ON ((c.relnamespace = n.oid)))
          WHERE ((n.nspname IN ( SELECT nsp.nspname
                   FROM pg_namespace nsp
                  WHERE (nsp.nspname !~~ 'pg_%'::text))) AND (c.relkind = ANY (ARRAY['r'::"char", 'S'::"char", 'v'::"char"])))
          ORDER BY (c.relkind = 'S'::"char")) s;


ALTER TABLE give_tables_to_rds OWNER TO rds_superuser;

--
-- Name: granted_roles; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW granted_roles AS
 SELECT (((('GRANT '::text || x.arolname) || ' TO '::text) || (r.rolname)::text) || ';'::text) AS concat_ws
   FROM (pg_roles r
     LEFT JOIN LATERAL ( SELECT inherited_roles.anoid,
            inherited_roles.arolname
           FROM inherited_roles((r.rolname)::text) inherited_roles(anoid, arolname)) x ON ((r.oid <> x.anoid)))
  ORDER BY r.rolname;


ALTER TABLE granted_roles OWNER TO administrator;

--
-- Name: index_sequential_scan_ratio; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW index_sequential_scan_ratio AS
 SELECT (((pg_stat_user_tables.schemaname)::text || '.'::text) || (pg_stat_user_tables.relname)::text) AS tbl_name,
    round(((pg_relation_size(((((pg_stat_user_tables.schemaname)::text || '.'::text) || (pg_stat_user_tables.relname)::regclass))::regclass))::numeric / ((1024)::numeric ^ (2)::numeric)), 3) AS tbl_size_mb,
    pg_stat_user_tables.seq_tup_read AS seq_tpls_read,
    pg_stat_user_tables.idx_tup_fetch AS idx_tpls_read,
    round((((pg_stat_user_tables.idx_tup_fetch)::numeric / ((pg_stat_user_tables.idx_tup_fetch + pg_stat_user_tables.seq_tup_read))::numeric) * (100)::numeric), 3) AS idx_tup_pct
   FROM pg_stat_user_tables
  WHERE (((pg_stat_user_tables.idx_tup_fetch + pg_stat_user_tables.seq_tup_read) > 0) AND (round((((pg_stat_user_tables.idx_tup_fetch)::numeric / ((pg_stat_user_tables.idx_tup_fetch + pg_stat_user_tables.seq_tup_read))::numeric) * (100)::numeric), 3) < (85)::numeric))
  ORDER BY pg_stat_user_tables.idx_tup_fetch, (round((((pg_stat_user_tables.idx_tup_fetch)::numeric / ((pg_stat_user_tables.idx_tup_fetch + pg_stat_user_tables.seq_tup_read))::numeric) * (100)::numeric), 3)), (round(((pg_relation_size(((((pg_stat_user_tables.schemaname)::text || '.'::text) || (pg_stat_user_tables.relname)::regclass))::regclass))::numeric / ((1024)::numeric ^ (2)::numeric)), 3)) DESC;


ALTER TABLE index_sequential_scan_ratio OWNER TO administrator;

--
-- Name: schema_owners; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW schema_owners AS
 SELECT (((('ALTER SCHEMA '::text || (nsp.nspname)::text) || ' OWNER TO '::text) || (r.rolname)::text) || ';'::text) AS stmt
   FROM (pg_namespace nsp
     JOIN pg_roles r ON ((nsp.nspowner = r.oid)))
  WHERE (nsp.nspname !~~ 'pg_%'::text);


ALTER TABLE schema_owners OWNER TO administrator;

--
-- Name: table_owners; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW table_owners AS
 SELECT (((((('ALTER TABLE '::text || (t.schemaname)::text) || '.'::text) || (t.tablename)::text) || ' OWNER TO '::text) || (t.tableowner)::text) || ';'::text)
   FROM pg_tables t
  WHERE ((((t.schemaname)::text || '.'::text) || (t.tablename)::text) IN ( SELECT ((quote_ident((s.nspname)::text) || '.'::text) || quote_ident((s.relname)::text))
           FROM ( SELECT n.nspname,
                    c.relname
                   FROM (pg_class c
                     JOIN pg_namespace n ON ((c.relnamespace = n.oid)))
                  WHERE ((n.nspname IN ( SELECT nsp.nspname
                           FROM pg_namespace nsp
                          WHERE ((nsp.nspname !~~ 'pg_%'::text) AND (nsp.nspname <> 'information_schema'::name)))) AND (c.relkind = ANY (ARRAY['r'::"char", 'S'::"char", 'v'::"char"])))
                  ORDER BY (c.relkind = 'S'::"char")) s));


ALTER TABLE table_owners OWNER TO administrator;

--
-- Name: tbl_sizes; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW tbl_sizes AS
 SELECT a.oid,
    a.table_schema,
    a.table_name,
    a.row_estimate,
    a.total_bytes,
    a.index_bytes,
    a.toast_bytes,
    a.table_bytes,
    pg_size_pretty(a.total_bytes) AS total,
    pg_size_pretty(a.index_bytes) AS index,
    pg_size_pretty(a.toast_bytes) AS toast,
    pg_size_pretty(a.table_bytes) AS "table"
   FROM ( SELECT a_1.oid,
            a_1.table_schema,
            a_1.table_name,
            a_1.row_estimate,
            a_1.total_bytes,
            a_1.index_bytes,
            a_1.toast_bytes,
            ((a_1.total_bytes - a_1.index_bytes) - COALESCE(a_1.toast_bytes, (0)::bigint)) AS table_bytes
           FROM ( SELECT c.oid,
                    n.nspname AS table_schema,
                    c.relname AS table_name,
                    c.reltuples AS row_estimate,
                    pg_total_relation_size((c.oid)::regclass) AS total_bytes,
                    pg_indexes_size((c.oid)::regclass) AS index_bytes,
                    pg_total_relation_size((c.reltoastrelid)::regclass) AS toast_bytes
                   FROM (pg_class c
                     LEFT JOIN pg_namespace n ON ((n.oid = c.relnamespace)))
                  WHERE (c.relkind = 'r'::"char")) a_1) a
  ORDER BY a.total_bytes DESC;


ALTER TABLE tbl_sizes OWNER TO administrator;

--
-- Name: unindexed_foreign_keys; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW unindexed_foreign_keys AS
 WITH y AS (
         SELECT format('%I.%I'::text, n1.nspname, c1.relname) AS referencing_tbl,
            quote_ident((a1.attname)::text) AS referencing_column,
            t.conname AS existing_fk_on_referencing_tbl,
            format('%I.%I'::text, n2.nspname, c2.relname) AS referenced_tbl,
            quote_ident((a2.attname)::text) AS referenced_column,
            pg_relation_size((format('%I.%I'::text, n1.nspname, c1.relname))::regclass) AS referencing_tbl_bytes,
            pg_relation_size((format('%I.%I'::text, n2.nspname, c2.relname))::regclass) AS referenced_tbl_bytes,
            format('CREATE INDEX ON %I.%I(%I);'::text, n1.nspname, c1.relname, a1.attname) AS suggestion
           FROM ((((((pg_constraint t
             JOIN pg_attribute a1 ON (((a1.attrelid = t.conrelid) AND (a1.attnum = t.conkey[1]))))
             JOIN pg_class c1 ON ((c1.oid = t.conrelid)))
             JOIN pg_namespace n1 ON ((n1.oid = c1.relnamespace)))
             JOIN pg_class c2 ON ((c2.oid = t.confrelid)))
             JOIN pg_namespace n2 ON ((n2.oid = c2.relnamespace)))
             JOIN pg_attribute a2 ON (((a2.attrelid = t.confrelid) AND (a2.attnum = t.confkey[1]))))
          WHERE ((t.contype = 'f'::"char") AND (NOT (EXISTS ( SELECT 1
                   FROM pg_index i
                  WHERE ((i.indrelid = t.conrelid) AND (i.indkey[0] = t.conkey[1]))))))
        )
 SELECT y.referencing_tbl,
    y.referencing_column,
    y.existing_fk_on_referencing_tbl,
    y.referenced_tbl,
    y.referenced_column,
    pg_size_pretty(y.referencing_tbl_bytes) AS referencing_tbl_size,
    pg_size_pretty(y.referenced_tbl_bytes) AS referenced_tbl_size,
    y.suggestion
   FROM y
  ORDER BY y.referencing_tbl_bytes DESC, y.referenced_tbl_bytes DESC, y.referencing_tbl, y.referenced_tbl, y.referencing_column, y.referenced_column;


ALTER TABLE unindexed_foreign_keys OWNER TO administrator;

--
-- Name: user_perms; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW user_perms AS
 SELECT pg_namespace.nspname AS "Schema",
    pg_class.relname AS "Relation",
    pg_class.relacl AS "Access permissions"
   FROM (pg_class
     JOIN pg_namespace ON ((pg_namespace.oid = pg_class.relnamespace)))
  WHERE ((pg_class.relkind = ANY (ARRAY['r'::"char", 'v'::"char", 'S'::"char"])) AND (pg_class.relname !~ '^pg_'::text))
  ORDER BY pg_class.relname;


ALTER TABLE user_perms OWNER TO administrator;

--
-- Name: view_all_grants; Type: VIEW; Schema: dba; Owner: administrator
--

CREATE VIEW view_all_grants AS
 SELECT use.usename AS subject,
    nsp.nspname AS namespace,
    c.relname AS item,
    c.relkind AS type,
    use2.usename AS owner,
    c.relacl,
    ((use2.usename <> use.usename) AND ((c.relacl)::text !~ (('({|,)'::text || (use.usename)::text) || '='::text))) AS public
   FROM (((pg_user use
     CROSS JOIN pg_class c)
     LEFT JOIN pg_namespace nsp ON ((c.relnamespace = nsp.oid)))
     LEFT JOIN pg_user use2 ON ((c.relowner = use2.usesysid)))
  WHERE ((c.relowner = use.usesysid) OR ((c.relacl)::text ~ (('({|,)(|'::text || (use.usename)::text) || ')='::text)))
  ORDER BY use.usename, nsp.nspname, c.relname;


ALTER TABLE view_all_grants OWNER TO administrator;

--
-- Name: dba; Type: ACL; Schema: -; Owner: administrator
--

REVOKE ALL ON SCHEMA dba FROM PUBLIC;
REVOKE ALL ON SCHEMA dba FROM administrator;
GRANT ALL ON SCHEMA dba TO administrator;


--
-- Name: tbl_sizes; Type: ACL; Schema: dba; Owner: administrator
--

REVOKE ALL ON TABLE tbl_sizes FROM PUBLIC;
REVOKE ALL ON TABLE tbl_sizes FROM administrator;
GRANT ALL ON TABLE tbl_sizes TO administrator;


--
-- PostgreSQL database dump complete
--

