Core Database
=============

This repository is for managing the core database used by PRNDL and GRID.

The main folder is changelog which contains the full current database schema
changelogs managed by Liquibase. An empty database schema can be created on any
PostgreSQL server by running CREATE DATABASE prndl; and then running this
set of changelogs against it.

pgtap contains two folders, one for unit tests that are normally only run on
the development database and the other for assertions which can be run on
the production db as well.

The scripts folder is for shell scripts and others which would be run on the 
database server itself.
