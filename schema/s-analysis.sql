--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: analysis; Type: SCHEMA; Schema: -; Owner: predictions_table_owner
--

CREATE SCHEMA analysis;


ALTER SCHEMA analysis OWNER TO predictions_table_owner;

SET search_path = analysis, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: biomarker_description_reference_mapping; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE biomarker_description_reference_mapping (
    biomarker character varying(64) NOT NULL,
    reference_id numeric NOT NULL
);


ALTER TABLE biomarker_description_reference_mapping OWNER TO nicholas;

--
-- Name: biomarker_description_reference_mapping_new; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE biomarker_description_reference_mapping_new (
    biomarker character varying(64) NOT NULL,
    ge_id character varying(16) NOT NULL,
    reference_id numeric NOT NULL
);


ALTER TABLE biomarker_description_reference_mapping_new OWNER TO nicholas;

--
-- Name: biomarker_descriptions; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE biomarker_descriptions (
    biomarker character varying(64) NOT NULL,
    gene_name character varying(64) NOT NULL,
    biological_context character varying(128) NOT NULL,
    genomic_event character varying(128) NOT NULL,
    treatment_implication character varying(128) NOT NULL,
    intepretation text NOT NULL,
    common_name character varying(64) NOT NULL,
    group_id character varying(3),
    prominence integer
);


ALTER TABLE biomarker_descriptions OWNER TO nicholas;

--
-- Name: biomarker_descriptions_new; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE biomarker_descriptions_new (
    biomarker character varying(64) NOT NULL,
    ge_id character varying(10) NOT NULL,
    gene_name character varying(64) NOT NULL,
    genomic_event character varying(128) NOT NULL,
    treatment_implication character varying(128),
    interpretation text,
    common_name character varying(64),
    group_id character varying(3),
    prominence integer,
    biological_context character varying(128)
);


ALTER TABLE biomarker_descriptions_new OWNER TO nicholas;

--
-- Name: biomarker_genomic_events; Type: TABLE; Schema: analysis; Owner: administrator
--

CREATE TABLE biomarker_genomic_events (
    biomarker character varying(64) NOT NULL,
    ge_id character varying(10) NOT NULL,
    genomic_event character varying(64),
    interpretation text,
    treatment_implication character varying(128)
);


ALTER TABLE biomarker_genomic_events OWNER TO administrator;

--
-- Name: TABLE biomarker_genomic_events; Type: COMMENT; Schema: analysis; Owner: administrator
--

COMMENT ON TABLE biomarker_genomic_events IS 'This table identifies genomic events for a marker and the interpretation. For some markers there will be two events, one for high and one for low';


--
-- Name: COLUMN biomarker_genomic_events.ge_id; Type: COMMENT; Schema: analysis; Owner: administrator
--

COMMENT ON COLUMN biomarker_genomic_events.ge_id IS 'a key to identify genomic events such as low/high, neg/pos';


--
-- Name: COLUMN biomarker_genomic_events.interpretation; Type: COMMENT; Schema: analysis; Owner: administrator
--

COMMENT ON COLUMN biomarker_genomic_events.interpretation IS 'The interpretation to use when the class of the marker value matches the ge_id';


--
-- Name: biomarker_group; Type: TABLE; Schema: analysis; Owner: commercial_table_owner
--

CREATE TABLE biomarker_group (
    id character varying(3) NOT NULL,
    fullname character varying,
    view_order integer
);


ALTER TABLE biomarker_group OWNER TO commercial_table_owner;

--
-- Name: TABLE biomarker_group; Type: COMMENT; Schema: analysis; Owner: commercial_table_owner
--

COMMENT ON TABLE biomarker_group IS 'Type of pathway or grouping of several related biomarkers';


--
-- Name: COLUMN biomarker_group.id; Type: COMMENT; Schema: analysis; Owner: commercial_table_owner
--

COMMENT ON COLUMN biomarker_group.id IS 'Short form to identify the grouping';


--
-- Name: COLUMN biomarker_group.fullname; Type: COMMENT; Schema: analysis; Owner: commercial_table_owner
--

COMMENT ON COLUMN biomarker_group.fullname IS 'Full name of grouping for use in reports';


--
-- Name: COLUMN biomarker_group.view_order; Type: COMMENT; Schema: analysis; Owner: commercial_table_owner
--

COMMENT ON COLUMN biomarker_group.view_order IS 'Order to display groups when reporting';


--
-- Name: biomarker_references; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE biomarker_references (
    reference_id numeric NOT NULL,
    content text NOT NULL
);


ALTER TABLE biomarker_references OWNER TO nicholas;

--
-- Name: biomarker_references_new; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE biomarker_references_new (
    reference_id numeric NOT NULL,
    content text NOT NULL
);


ALTER TABLE biomarker_references_new OWNER TO nicholas;

--
-- Name: biomarker_sets; Type: TABLE; Schema: analysis; Owner: administrator
--

CREATE TABLE biomarker_sets (
    id character varying(10) NOT NULL,
    description text,
    markers text[]
);


ALTER TABLE biomarker_sets OWNER TO administrator;

--
-- Name: class; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE class (
    celfile_name character varying(100) NOT NULL,
    ccnd1 character varying(100),
    ar character varying(100),
    klk3 character varying(100),
    mki67 character varying(100),
    nkx31 character varying(100),
    rb1 character varying(100),
    etv1 character varying(100),
    sparcl1 character varying(100),
    top2a character varying(100),
    chga character varying(100),
    cd276 character varying(100),
    cd274 character varying(100),
    erbb3 character varying(100),
    alk character varying(100),
    kdm5a character varying(100),
    myc character varying(100),
    rad50 character varying(100),
    tp53 character varying(100),
    jun character varying(100),
    men1 character varying(100),
    chek1 character varying(100),
    gata3 character varying(100),
    znf217 character varying(100),
    grin2a character varying(100),
    kit character varying(100),
    pdpk1 character varying(100),
    met character varying(100),
    chek2 character varying(100),
    cdkn2b character varying(100),
    stat4 character varying(100),
    fance character varying(100),
    ezh2 character varying(100),
    pdgfra character varying(100),
    fbxw7 character varying(100),
    notch1 character varying(100),
    smarca4 character varying(100),
    fancd2 character varying(100),
    fgfr1 character varying(100),
    pax5 character varying(100),
    mycn character varying(100),
    idh2 character varying(100),
    ewsr1 character varying(100),
    bcor character varying(100),
    pbrm1 character varying(100),
    ikzf1 character varying(100),
    blm character varying(100),
    znf703 character varying(100),
    cebpa character varying(100),
    gata2 character varying(100),
    epha5 character varying(100),
    fancg character varying(100),
    tet2 character varying(100),
    bcl2l2 character varying(100),
    nras character varying(100),
    spop character varying(100),
    cdkn1b character varying(100),
    ccnd2 character varying(100),
    erbb4 character varying(100),
    stk11 character varying(100),
    cdk6 character varying(100),
    msh2 character varying(100),
    nf1 character varying(100),
    cdkn2c character varying(100),
    map2k4 character varying(100),
    jak2 character varying(100),
    bcl6 character varying(100),
    gna13 character varying(100),
    fancl character varying(100),
    mdm2 character varying(100),
    gnaq character varying(100),
    xpo1 character varying(100),
    cdkn2a character varying(100),
    cdh1 character varying(100),
    ddr2 character varying(100),
    irs2 character varying(100),
    fancf character varying(100),
    smarcb1 character varying(100),
    aurka character varying(100),
    fgf23 character varying(100),
    hgf character varying(100),
    prkar1a character varying(100),
    socs1 character varying(100),
    prkdc character varying(100),
    rnf43 character varying(100),
    hras character varying(100),
    setd2 character varying(100),
    npm1 character varying(100),
    pdk1 character varying(100),
    cd79b character varying(100),
    cbl character varying(100),
    akt2 character varying(100),
    araf character varying(100),
    med12 character varying(100),
    cd79a character varying(100),
    top1 character varying(100),
    tmprss2 character varying(100),
    sox2 character varying(100),
    fgf14 character varying(100),
    pik3cg character varying(100),
    runx1 character varying(100),
    lrp1b character varying(100),
    tsc2 character varying(100),
    ppp2r1a character varying(100),
    gsk3b character varying(100),
    keap1 character varying(100),
    brca2 character varying(100),
    pdgfrb character varying(100),
    smad2 character varying(100),
    bap1 character varying(100),
    vhl character varying(100),
    ntrk3 character varying(100),
    mlh1 character varying(100),
    igf1r character varying(100),
    rictor character varying(100),
    nf2 character varying(100),
    ptpn11 character varying(100),
    fgf19 character varying(100),
    atm character varying(100),
    brca1 character varying(100),
    fgfr2 character varying(100),
    idh1 character varying(100),
    btk character varying(100),
    nup93 character varying(100),
    braf character varying(100),
    crkl character varying(100),
    myd88 character varying(100),
    pik3ca character varying(100),
    ikbke character varying(100),
    abl1 character varying(100),
    mitf character varying(100),
    aurkb character varying(100),
    map2k1 character varying(100),
    gnas character varying(100),
    cbfb character varying(100),
    smo character varying(100),
    ephb1 character varying(100),
    flt1 character varying(100),
    mtor character varying(100),
    csf1r character varying(100),
    prdm1 character varying(100),
    kras character varying(100),
    fgf10 character varying(100),
    cdk4 character varying(100),
    inhba character varying(100),
    klhl6 character varying(100),
    erbb2 character varying(100),
    ctnnb1 character varying(100),
    wisp3 character varying(100),
    mcl1 character varying(100),
    fancc character varying(100),
    arfrp1 character varying(100),
    gpr124 character varying(100),
    daxx character varying(100),
    gna11 character varying(100),
    stag2 character varying(100),
    fgf6 character varying(100),
    ntrk1 character varying(100),
    apc character varying(100),
    dnmt3a character varying(100),
    card11 character varying(100),
    fam46c character varying(100),
    mdm4 character varying(100),
    ccnd3 character varying(100),
    rara character varying(100),
    tnfrsf14 character varying(100),
    esr1 character varying(100),
    nfe2l2 character varying(100),
    ntrk2 character varying(100),
    fgfr3 character varying(100),
    egfr character varying(100),
    epha3 character varying(100),
    fgf4 character varying(100),
    ep300 character varying(100),
    src character varying(100),
    nfkbia character varying(100),
    tnfaip3 character varying(100),
    palb2 character varying(100),
    sufu character varying(100),
    gata1 character varying(100),
    rptor character varying(100),
    raf1 character varying(100),
    mre11a character varying(100),
    ccne1 character varying(100),
    smad4 character varying(100),
    rad51 character varying(100),
    mef2b character varying(100),
    axl character varying(100),
    ret character varying(100),
    pik3r2 character varying(100),
    nkx21 character varying(100),
    ctnna1 character varying(100),
    notch2 character varying(100),
    ctcf character varying(100),
    bcl2 character varying(100),
    mutyh character varying(100),
    akt3 character varying(100),
    tshr character varying(100),
    jak3 character varying(100),
    msh6 character varying(100),
    flt4 character varying(100),
    irf4 character varying(100),
    il7r character varying(100),
    c11orf30 character varying(100),
    arid1a character varying(100),
    pik3r1 character varying(100),
    mpl character varying(100),
    kdr character varying(100),
    tsc1 character varying(100),
    sf3b1 character varying(100),
    spen character varying(100),
    etv6 character varying(100),
    dot1l character varying(100),
    crebbp character varying(100),
    bcorl1 character varying(100),
    cdk8 character varying(100),
    bcr character varying(100),
    pak3 character varying(100),
    map2k2 character varying(100),
    tgfbr2 character varying(100),
    ros1 character varying(100),
    brip1 character varying(100),
    wt1 character varying(100),
    cdk12 character varying(100),
    fanca character varying(100),
    asxl1 character varying(100),
    cic character varying(100),
    kdm6a character varying(100),
    akt1 character varying(100),
    atrx character varying(100),
    foxl2 character varying(100),
    sox10 character varying(100),
    cdc73 character varying(100),
    fgf3 character varying(100),
    atr character varying(100),
    pten character varying(100),
    flt3 character varying(100),
    kdm5c character varying(100),
    bard1 character varying(100),
    gstp1 character varying(100),
    hif1a character varying(100),
    il6 character varying(100),
    klk2 character varying(100),
    srd5a1 character varying(100),
    folh1 character varying(100),
    etv4 character varying(100),
    etv5 character varying(100),
    spink1 character varying(100),
    ergmodel character varying(100),
    fli1 character varying(100),
    neat1 character varying(100),
    schlap1 character varying(100),
    pdcd1 character varying(100),
    pca3 character varying(100),
    map3k1 character varying(100)
);


ALTER TABLE class OWNER TO nicholas;

--
-- Name: class_reporting; Type: VIEW; Schema: analysis; Owner: rds_superuser
--

CREATE VIEW class_reporting AS
 SELECT class.celfile_name,
    class.ccnd1,
    class.ar,
    class.klk3,
    class.mki67,
    class.nkx31,
    class.rb1,
    class.etv1,
    class.sparcl1,
    class.top2a,
    class.chga,
    class.cd276,
    class.cd274,
    class.erbb3,
    class.alk,
    class.kdm5a,
    class.myc,
    class.rad50,
    class.tp53,
    class.jun,
    class.men1,
    class.chek1,
    class.gata3,
    class.znf217,
    class.grin2a,
    class.kit,
    class.pdpk1,
    class.met,
    class.chek2,
    class.cdkn2b,
    class.stat4,
    class.fance,
    class.ezh2,
    class.pdgfra,
    class.fbxw7,
    class.notch1,
    class.smarca4,
    class.fancd2,
    class.fgfr1,
    class.pax5,
    class.mycn,
    class.idh2,
    class.ewsr1,
    class.bcor,
    class.pbrm1,
    class.ikzf1,
    class.blm,
    class.znf703,
    class.cebpa,
    class.gata2,
    class.epha5,
    class.fancg,
    class.tet2,
    class.bcl2l2,
    class.nras,
    class.spop,
    class.cdkn1b,
    class.ccnd2,
    class.erbb4,
    class.stk11,
    class.cdk6,
    class.msh2,
    class.nf1,
    class.cdkn2c,
    class.map2k4,
    class.jak2,
    class.bcl6,
    class.gna13,
    class.fancl,
    class.mdm2,
    class.gnaq,
    class.xpo1,
    class.cdkn2a,
    class.cdh1,
    class.ddr2,
    class.irs2,
    class.fancf,
    class.smarcb1,
    class.aurka,
    class.fgf23,
    class.hgf,
    class.prkar1a,
    class.socs1,
    class.prkdc,
    class.rnf43,
    class.hras,
    class.setd2,
    class.npm1,
    class.pdk1,
    class.cd79b,
    class.cbl,
    class.akt2,
    class.araf,
    class.med12,
    class.cd79a,
    class.top1,
    class.tmprss2,
    class.sox2,
    class.fgf14,
    class.pik3cg,
    class.runx1,
    class.lrp1b,
    class.tsc2,
    class.ppp2r1a,
    class.gsk3b,
    class.keap1,
    class.brca2,
    class.pdgfrb,
    class.smad2,
    class.bap1,
    class.vhl,
    class.ntrk3,
    class.mlh1,
    class.igf1r,
    class.rictor,
    class.nf2,
    class.ptpn11,
    class.fgf19,
    class.atm,
    class.brca1,
    class.fgfr2,
    class.idh1,
    class.btk,
    class.nup93,
    class.braf,
    class.crkl,
    class.myd88,
    class.pik3ca,
    class.ikbke,
    class.abl1,
    class.mitf,
    class.aurkb,
    class.map2k1,
    class.gnas,
    class.cbfb,
    class.smo,
    class.ephb1,
    class.flt1,
    class.mtor,
    class.csf1r,
    class.prdm1,
    class.kras,
    class.fgf10,
    class.cdk4,
    class.inhba,
    class.klhl6,
    class.erbb2,
    class.ctnnb1,
    class.wisp3,
    class.mcl1,
    class.fancc,
    class.arfrp1,
    class.gpr124,
    class.daxx,
    class.gna11,
    class.stag2,
    class.fgf6,
    class.ntrk1,
    class.apc,
    class.dnmt3a,
    class.card11,
    class.fam46c,
    class.mdm4,
    class.ccnd3,
    class.rara,
    class.tnfrsf14,
    class.esr1,
    class.nfe2l2,
    class.ntrk2,
    class.fgfr3,
    class.egfr,
    class.epha3,
    class.fgf4,
    class.ep300,
    class.src,
    class.nfkbia,
    class.tnfaip3,
    class.palb2,
    class.sufu,
    class.gata1,
    class.rptor,
    class.raf1,
    class.mre11a,
    class.ccne1,
    class.smad4,
    class.rad51,
    class.mef2b,
    class.axl,
    class.ret,
    class.pik3r2,
    class.nkx21,
    class.ctnna1,
    class.notch2,
    class.ctcf,
    class.bcl2,
    class.mutyh,
    class.akt3,
    class.tshr,
    class.jak3,
    class.msh6,
    class.flt4,
    class.irf4,
    class.il7r,
    class.c11orf30,
    class.arid1a,
    class.pik3r1,
    class.mpl,
    class.kdr,
    class.tsc1,
    class.sf3b1,
    class.spen,
    class.etv6,
    class.dot1l,
    class.crebbp,
    class.bcorl1,
    class.cdk8,
    class.bcr,
    class.pak3,
    class.map2k2,
    class.tgfbr2,
    class.ros1,
    class.brip1,
    class.wt1,
    class.cdk12,
    class.fanca,
    class.asxl1,
    class.cic,
    class.kdm6a,
    class.akt1,
    class.atrx,
    class.foxl2,
    class.sox10,
    class.cdc73,
    class.fgf3,
    class.atr,
    class.pten,
    class.flt3,
    class.kdm5c,
    class.bard1,
    class.gstp1,
    class.hif1a,
    class.il6,
    class.klk2,
    class.srd5a1,
    class.folh1,
    class.etv4,
    class.etv5,
    class.spink1,
    class.ergmodel,
    class.fli1,
    class.neat1,
    class.schlap1,
    class.pdcd1,
    class.pca3,
    class.map3k1,
        CASE
            WHEN ((class.ergmodel)::text = 'erg+'::text) THEN 'erg'::character varying
            WHEN (((class.fli1)::text = 'fli1+'::text) OR ((class.etv1)::text = 'etv1+'::text) OR ((class.etv4)::text = 'etv4+'::text) OR ((class.etv5)::text = 'etv5+'::text)) THEN 'ets'::character varying
            WHEN ((class.spink1)::text = 'spink1+'::text) THEN 'spink1'::character varying
            ELSE '-'::character varying
        END AS subtype
   FROM class
  WHERE (((class.ar)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.ccnd1)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.cd274)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.cd276)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.chga)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.klk3)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.mki67)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.neat1)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.nkx31)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.pca3)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.pdcd1)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.rb1)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.schlap1)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.sparcl1)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])) AND ((class.top2a)::text = ANY (ARRAY['High'::text, 'Low'::text, '-'::text])));


ALTER TABLE class_reporting OWNER TO rds_superuser;

--
-- Name: core; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE core (
    celfile_name character varying(100) NOT NULL,
    ccnd1 double precision,
    ar double precision,
    klk3 double precision,
    mki67 double precision,
    nkx31 double precision,
    rb1 double precision,
    erg double precision,
    etv1 double precision,
    etv4 double precision,
    etv5 double precision,
    spink1 double precision,
    fli1 double precision,
    sparcl1 double precision,
    top2a double precision,
    chga double precision,
    cd276 double precision,
    cd274 double precision,
    erbb3 double precision,
    alk double precision,
    kdm5a double precision,
    myc double precision,
    rad50 double precision,
    tp53 double precision,
    jun double precision,
    men1 double precision,
    chek1 double precision,
    gata3 double precision,
    znf217 double precision,
    grin2a double precision,
    kit double precision,
    pdpk1 double precision,
    met double precision,
    chek2 double precision,
    cdkn2b double precision,
    stat4 double precision,
    fance double precision,
    ezh2 double precision,
    pdgfra double precision,
    fbxw7 double precision,
    notch1 double precision,
    smarca4 double precision,
    fancd2 double precision,
    fgfr1 double precision,
    pax5 double precision,
    mycn double precision,
    idh2 double precision,
    ewsr1 double precision,
    bcor double precision,
    pbrm1 double precision,
    ikzf1 double precision,
    blm double precision,
    znf703 double precision,
    cebpa double precision,
    gata2 double precision,
    epha5 double precision,
    fancg double precision,
    tet2 double precision,
    bcl2l2 double precision,
    nras double precision,
    spop double precision,
    cdkn1b double precision,
    ccnd2 double precision,
    erbb4 double precision,
    stk11 double precision,
    cdk6 double precision,
    msh2 double precision,
    nf1 double precision,
    cdkn2c double precision,
    map2k4 double precision,
    jak2 double precision,
    bcl6 double precision,
    gna13 double precision,
    fancl double precision,
    mdm2 double precision,
    gnaq double precision,
    xpo1 double precision,
    cdkn2a double precision,
    cdh1 double precision,
    ddr2 double precision,
    irs2 double precision,
    fancf double precision,
    smarcb1 double precision,
    aurka double precision,
    fgf23 double precision,
    hgf double precision,
    prkar1a double precision,
    socs1 double precision,
    prkdc double precision,
    rnf43 double precision,
    hras double precision,
    setd2 double precision,
    npm1 double precision,
    pdk1 double precision,
    cd79b double precision,
    cbl double precision,
    akt2 double precision,
    araf double precision,
    med12 double precision,
    cd79a double precision,
    top1 double precision,
    tmprss2 double precision,
    sox2 double precision,
    fgf14 double precision,
    pik3cg double precision,
    runx1 double precision,
    lrp1b double precision,
    tsc2 double precision,
    ppp2r1a double precision,
    gsk3b double precision,
    keap1 double precision,
    brca2 double precision,
    pdgfrb double precision,
    smad2 double precision,
    bap1 double precision,
    vhl double precision,
    ntrk3 double precision,
    mlh1 double precision,
    igf1r double precision,
    rictor double precision,
    nf2 double precision,
    ptpn11 double precision,
    fgf19 double precision,
    atm double precision,
    brca1 double precision,
    fgfr2 double precision,
    idh1 double precision,
    btk double precision,
    nup93 double precision,
    braf double precision,
    crkl double precision,
    myd88 double precision,
    pik3ca double precision,
    ikbke double precision,
    abl1 double precision,
    mitf double precision,
    aurkb double precision,
    map2k1 double precision,
    gnas double precision,
    cbfb double precision,
    smo double precision,
    ephb1 double precision,
    flt1 double precision,
    mtor double precision,
    csf1r double precision,
    prdm1 double precision,
    kras double precision,
    fgf10 double precision,
    cdk4 double precision,
    inhba double precision,
    klhl6 double precision,
    erbb2 double precision,
    ctnnb1 double precision,
    wisp3 double precision,
    mcl1 double precision,
    fancc double precision,
    arfrp1 double precision,
    gpr124 double precision,
    daxx double precision,
    gna11 double precision,
    stag2 double precision,
    fgf6 double precision,
    ntrk1 double precision,
    apc double precision,
    dnmt3a double precision,
    card11 double precision,
    fam46c double precision,
    mdm4 double precision,
    ccnd3 double precision,
    rara double precision,
    tnfrsf14 double precision,
    esr1 double precision,
    nfe2l2 double precision,
    ntrk2 double precision,
    fgfr3 double precision,
    egfr double precision,
    epha3 double precision,
    fgf4 double precision,
    ep300 double precision,
    src double precision,
    nfkbia double precision,
    tnfaip3 double precision,
    palb2 double precision,
    sufu double precision,
    gata1 double precision,
    rptor double precision,
    raf1 double precision,
    mre11a double precision,
    ccne1 double precision,
    smad4 double precision,
    rad51 double precision,
    mef2b double precision,
    axl double precision,
    ret double precision,
    pik3r2 double precision,
    nkx21 double precision,
    ctnna1 double precision,
    notch2 double precision,
    ctcf double precision,
    bcl2 double precision,
    mutyh double precision,
    akt3 double precision,
    tshr double precision,
    jak3 double precision,
    msh6 double precision,
    flt4 double precision,
    irf4 double precision,
    il7r double precision,
    c11orf30 double precision,
    arid1a double precision,
    pik3r1 double precision,
    mpl double precision,
    kdr double precision,
    tsc1 double precision,
    sf3b1 double precision,
    spen double precision,
    etv6 double precision,
    dot1l double precision,
    crebbp double precision,
    bcorl1 double precision,
    cdk8 double precision,
    bcr double precision,
    pak3 double precision,
    map2k2 double precision,
    tgfbr2 double precision,
    ros1 double precision,
    brip1 double precision,
    wt1 double precision,
    cdk12 double precision,
    fanca double precision,
    asxl1 double precision,
    cic double precision,
    kdm6a double precision,
    akt1 double precision,
    atrx double precision,
    foxl2 double precision,
    sox10 double precision,
    cdc73 double precision,
    fgf3 double precision,
    atr double precision,
    pten double precision,
    flt3 double precision,
    kdm5c double precision,
    bard1 double precision,
    gstp1 double precision,
    hif1a double precision,
    il6 double precision,
    klk2 double precision,
    srd5a1 double precision,
    folh1 double precision
);


ALTER TABLE core OWNER TO nicholas;

--
-- Name: cutoffs; Type: TABLE; Schema: analysis; Owner: predictions_table_owner
--

CREATE TABLE cutoffs (
    cutoffs_id integer NOT NULL,
    signature_name character varying(64) NOT NULL,
    cutoff double precision,
    class_label character varying(100)
);


ALTER TABLE cutoffs OWNER TO predictions_table_owner;

--
-- Name: cutoff_signatures; Type: VIEW; Schema: analysis; Owner: rds_superuser
--

CREATE VIEW cutoff_signatures AS
 SELECT cutoffs.signature_name,
    max(cutoffs.cutoff) AS cutoff,
    string_agg((cutoffs.class_label)::text, ','::text) AS labels
   FROM cutoffs
  GROUP BY cutoffs.signature_name;


ALTER TABLE cutoff_signatures OWNER TO rds_superuser;

--
-- Name: cutoffs_cutoffs_id_seq; Type: SEQUENCE; Schema: analysis; Owner: predictions_table_owner
--

CREATE SEQUENCE cutoffs_cutoffs_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE cutoffs_cutoffs_id_seq OWNER TO predictions_table_owner;

--
-- Name: cutoffs_cutoffs_id_seq; Type: SEQUENCE OWNED BY; Schema: analysis; Owner: predictions_table_owner
--

ALTER SEQUENCE cutoffs_cutoffs_id_seq OWNED BY cutoffs.cutoffs_id;


--
-- Name: extended; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE extended (
    celfile_name character varying(100) NOT NULL,
    pca3 double precision,
    map3k1 double precision
);


ALTER TABLE extended OWNER TO nicholas;

--
-- Name: genomic_markers; Type: VIEW; Schema: analysis; Owner: rds_superuser
--

CREATE VIEW genomic_markers AS
 SELECT b2.group_id,
    b2.biomarker,
    b2.lit_refs,
    b2.gene_name,
    b2.biological_context,
    b2.genomic_event,
    b2.treatment_implication,
    b2.interpretation,
    b2.common_name,
    b3.prominence,
    bg.id,
    bg.fullname,
    bg.view_order
   FROM ((( SELECT b.group_id,
            brm.biomarker,
            string_agg((brm.reference_id)::text, ','::text) AS lit_refs,
            max((b.gene_name)::text) AS gene_name,
            max((b.biological_context)::text) AS biological_context,
            max((b.genomic_event)::text) AS genomic_event,
            max((b.treatment_implication)::text) AS treatment_implication,
            max(b.intepretation) AS interpretation,
            max((b.common_name)::text) AS common_name
           FROM ((biomarker_descriptions b
             JOIN biomarker_description_reference_mapping brm ON (((brm.biomarker)::text = (b.biomarker)::text)))
             JOIN biomarker_group bg_1 ON (((bg_1.id)::text = (b.group_id)::text)))
          GROUP BY b.group_id, brm.biomarker) b2
     JOIN biomarker_group bg ON (((bg.id)::text = (b2.group_id)::text)))
     JOIN biomarker_descriptions b3 ON (((b2.biomarker)::text = (b3.biomarker)::text)));


ALTER TABLE genomic_markers OWNER TO rds_superuser;

--
-- Name: genomic_markers_new; Type: VIEW; Schema: analysis; Owner: rds_superuser
--

CREATE VIEW genomic_markers_new AS
 SELECT DISTINCT b2.biomarker,
    b2.group_id,
    b2.lit_refs,
    b2.gene_name,
    b2.biological_context,
    b2.genomic_event,
    b2.treatment_implication,
    b2.interpretation,
    b2.common_name,
    b3.prominence,
    bg.id,
    bg.fullname,
    bg.view_order
   FROM ((( SELECT b.group_id,
            brm.biomarker,
            string_agg((brm.reference_id)::text, ','::text) AS lit_refs,
            max((b.gene_name)::text) AS gene_name,
            max((g.fullname)::text) AS biological_context,
            max((b.genomic_event)::text) AS genomic_event,
            max((b.treatment_implication)::text) AS treatment_implication,
            max(b.interpretation) AS interpretation,
            max((b.common_name)::text) AS common_name
           FROM (((biomarker_descriptions_new b
             JOIN biomarker_description_reference_mapping_new brm ON (((brm.biomarker)::text = (b.biomarker)::text)))
             JOIN biomarker_group bg_1 ON (((bg_1.id)::text = (b.group_id)::text)))
             JOIN biomarker_group g ON (((g.id)::text = (b.group_id)::text)))
          GROUP BY b.group_id, brm.biomarker) b2
     JOIN biomarker_group bg ON (((bg.id)::text = (b2.group_id)::text)))
     JOIN biomarker_descriptions_new b3 ON (((b2.biomarker)::text = (b3.biomarker)::text)));


ALTER TABLE genomic_markers_new OWNER TO rds_superuser;

--
-- Name: grid; Type: TABLE; Schema: analysis; Owner: predictions_table_owner
--

CREATE TABLE grid (
    celfile_name character varying(100) NOT NULL,
    decipherv2 double precision,
    ergmodel double precision,
    neat1 double precision,
    ccnd1 double precision,
    ar double precision,
    klk3 double precision,
    mki67 double precision,
    rb1 double precision,
    schlap1 double precision,
    sparcl1 double precision,
    top2a double precision,
    chga double precision,
    pca3 double precision,
    cd276 double precision,
    pdcd1 double precision,
    cd274 double precision,
    nkx31 double precision,
    erbb3 double precision,
    alk double precision,
    kdm5a double precision,
    myc double precision,
    rad50 double precision,
    tp53 double precision,
    jun double precision,
    men1 double precision,
    chek1 double precision,
    gata3 double precision,
    znf217 double precision,
    grin2a double precision,
    kit double precision,
    pdpk1 double precision,
    kat6a double precision,
    met double precision,
    chek2 double precision,
    cdkn2b double precision,
    stat4 double precision,
    fance double precision,
    ezh2 double precision,
    pdgfra double precision,
    fbxw7 double precision,
    notch1 double precision,
    smarca4 double precision,
    fancd2 double precision,
    fgfr1 double precision,
    pax5 double precision,
    mycn double precision,
    idh2 double precision,
    ewsr1 double precision,
    bcor double precision,
    pbrm1 double precision,
    ikzf1 double precision,
    blm double precision,
    znf703 double precision,
    kmt2a double precision,
    cebpa double precision,
    gata2 double precision,
    epha5 double precision,
    fancg double precision,
    tet2 double precision,
    bcl2l2 double precision,
    nras double precision,
    spop double precision,
    cdkn1b double precision,
    ccnd2 double precision,
    erbb4 double precision,
    stk11 double precision,
    cdk6 double precision,
    mycl double precision,
    msh2 double precision,
    nf1 double precision,
    cdkn2c double precision,
    map2k4 double precision,
    jak2 double precision,
    ptch1 double precision,
    bcl6 double precision,
    crlf2 double precision,
    gna13 double precision,
    fancl double precision,
    mdm2 double precision,
    gnaq double precision,
    xpo1 double precision,
    cdkn2a double precision,
    cdh1 double precision,
    ddr2 double precision,
    irs2 double precision,
    fancf double precision,
    smarcb1 double precision,
    aurka double precision,
    fgf23 double precision,
    hgf double precision,
    prkar1a double precision,
    socs1 double precision,
    prkdc double precision,
    rnf43 double precision,
    hras double precision,
    setd2 double precision,
    npm1 double precision,
    pdk1 double precision,
    cd79b double precision,
    cbl double precision,
    akt2 double precision,
    araf double precision,
    med12 double precision,
    cd79a double precision,
    top1 double precision,
    tmprss2 double precision,
    sox2 double precision,
    fgf14 double precision,
    pik3cg double precision,
    runx1 double precision,
    lrp1b double precision,
    tsc2 double precision,
    ppp2r1a double precision,
    gsk3b double precision,
    keap1 double precision,
    brca2 double precision,
    pdgfrb double precision,
    smad2 double precision,
    bap1 double precision,
    vhl double precision,
    ntrk3 double precision,
    mlh1 double precision,
    igf1r double precision,
    rictor double precision,
    nf2 double precision,
    ptpn11 double precision,
    fgf19 double precision,
    atm double precision,
    brca1 double precision,
    fgfr2 double precision,
    idh1 double precision,
    btk double precision,
    nup93 double precision,
    braf double precision,
    crkl double precision,
    myd88 double precision,
    pik3ca double precision,
    ikbke double precision,
    abl1 double precision,
    mitf double precision,
    aurkb double precision,
    map2k1 double precision,
    gnas double precision,
    cbfb double precision,
    smo double precision,
    ephb1 double precision,
    flt1 double precision,
    mtor double precision,
    csf1r double precision,
    prdm1 double precision,
    kras double precision,
    fgf10 double precision,
    cdk4 double precision,
    inhba double precision,
    arid2 double precision,
    klhl6 double precision,
    erbb2 double precision,
    ctnnb1 double precision,
    wisp3 double precision,
    mcl1 double precision,
    fancc double precision,
    arfrp1 double precision,
    gpr124 double precision,
    daxx double precision,
    gna11 double precision,
    stag2 double precision,
    fgf6 double precision,
    ntrk1 double precision,
    apc double precision,
    dnmt3a double precision,
    card11 double precision,
    fam46c double precision,
    mdm4 double precision,
    ccnd3 double precision,
    rara double precision,
    gid4 double precision,
    tnfrsf14 double precision,
    esr1 double precision,
    nfe2l2 double precision,
    ntrk2 double precision,
    fgfr3 double precision,
    egfr double precision,
    epha3 double precision,
    fgf4 double precision,
    kmt2d double precision,
    ep300 double precision,
    src double precision,
    nfkbia double precision,
    tnfaip3 double precision,
    palb2 double precision,
    sufu double precision,
    gata1 double precision,
    rptor double precision,
    raf1 double precision,
    mre11a double precision,
    ccne1 double precision,
    amer1 double precision,
    smad4 double precision,
    rad51 double precision,
    mef2b double precision,
    axl double precision,
    ret double precision,
    pik3r2 double precision,
    ctnna1 double precision,
    notch2 double precision,
    map3k1 double precision,
    ctcf double precision,
    bcl2 double precision,
    mutyh double precision,
    akt3 double precision,
    tshr double precision,
    jak3 double precision,
    msh6 double precision,
    flt4 double precision,
    irf4 double precision,
    il7r double precision,
    c11orf30 double precision,
    arid1a double precision,
    pik3r1 double precision,
    mpl double precision,
    kdr double precision,
    tsc1 double precision,
    sf3b1 double precision,
    spen double precision,
    etv6 double precision,
    dot1l double precision,
    crebbp double precision,
    bcorl1 double precision,
    cdk8 double precision,
    bcr double precision,
    pak3 double precision,
    map2k2 double precision,
    tgfbr2 double precision,
    ros1 double precision,
    brip1 double precision,
    wt1 double precision,
    cdk12 double precision,
    fanca double precision,
    asxl1 double precision,
    cic double precision,
    kdm6a double precision,
    akt1 double precision,
    atrx double precision,
    foxl2 double precision,
    sox10 double precision,
    cdc73 double precision,
    fgf3 double precision,
    atr double precision,
    pten double precision,
    flt3 double precision,
    kdm5c double precision,
    bard1 double precision,
    nkx21 double precision,
    erg double precision,
    etv1 double precision,
    etv4 double precision,
    etv5 double precision,
    fli1 double precision,
    spink1 double precision
);


ALTER TABLE grid OWNER TO predictions_table_owner;

--
-- Name: percentile; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE percentile (
    celfile_name character varying(100) NOT NULL,
    ccnd1 double precision,
    ar double precision,
    klk3 double precision,
    mki67 double precision,
    nkx31 double precision,
    rb1 double precision,
    erg double precision,
    etv1 double precision,
    etv4 double precision,
    etv5 double precision,
    spink1 double precision,
    fli1 double precision,
    sparcl1 double precision,
    top2a double precision,
    chga double precision,
    cd276 double precision,
    cd274 double precision,
    erbb3 double precision,
    alk double precision,
    kdm5a double precision,
    myc double precision,
    rad50 double precision,
    tp53 double precision,
    jun double precision,
    men1 double precision,
    chek1 double precision,
    gata3 double precision,
    znf217 double precision,
    grin2a double precision,
    kit double precision,
    pdpk1 double precision,
    met double precision,
    chek2 double precision,
    cdkn2b double precision,
    stat4 double precision,
    fance double precision,
    ezh2 double precision,
    pdgfra double precision,
    fbxw7 double precision,
    notch1 double precision,
    smarca4 double precision,
    fancd2 double precision,
    fgfr1 double precision,
    pax5 double precision,
    mycn double precision,
    idh2 double precision,
    ewsr1 double precision,
    bcor double precision,
    pbrm1 double precision,
    ikzf1 double precision,
    blm double precision,
    znf703 double precision,
    cebpa double precision,
    gata2 double precision,
    epha5 double precision,
    fancg double precision,
    tet2 double precision,
    bcl2l2 double precision,
    nras double precision,
    spop double precision,
    cdkn1b double precision,
    ccnd2 double precision,
    erbb4 double precision,
    stk11 double precision,
    cdk6 double precision,
    msh2 double precision,
    nf1 double precision,
    cdkn2c double precision,
    map2k4 double precision,
    jak2 double precision,
    bcl6 double precision,
    gna13 double precision,
    fancl double precision,
    mdm2 double precision,
    gnaq double precision,
    xpo1 double precision,
    cdkn2a double precision,
    cdh1 double precision,
    ddr2 double precision,
    irs2 double precision,
    fancf double precision,
    smarcb1 double precision,
    aurka double precision,
    fgf23 double precision,
    hgf double precision,
    prkar1a double precision,
    socs1 double precision,
    prkdc double precision,
    rnf43 double precision,
    hras double precision,
    setd2 double precision,
    npm1 double precision,
    pdk1 double precision,
    cd79b double precision,
    cbl double precision,
    akt2 double precision,
    araf double precision,
    med12 double precision,
    cd79a double precision,
    top1 double precision,
    tmprss2 double precision,
    sox2 double precision,
    fgf14 double precision,
    pik3cg double precision,
    runx1 double precision,
    lrp1b double precision,
    tsc2 double precision,
    ppp2r1a double precision,
    gsk3b double precision,
    keap1 double precision,
    brca2 double precision,
    pdgfrb double precision,
    smad2 double precision,
    bap1 double precision,
    vhl double precision,
    ntrk3 double precision,
    mlh1 double precision,
    igf1r double precision,
    rictor double precision,
    nf2 double precision,
    ptpn11 double precision,
    fgf19 double precision,
    atm double precision,
    brca1 double precision,
    fgfr2 double precision,
    idh1 double precision,
    btk double precision,
    nup93 double precision,
    braf double precision,
    crkl double precision,
    myd88 double precision,
    pik3ca double precision,
    ikbke double precision,
    abl1 double precision,
    mitf double precision,
    aurkb double precision,
    map2k1 double precision,
    gnas double precision,
    cbfb double precision,
    smo double precision,
    ephb1 double precision,
    flt1 double precision,
    mtor double precision,
    csf1r double precision,
    prdm1 double precision,
    kras double precision,
    fgf10 double precision,
    cdk4 double precision,
    inhba double precision,
    klhl6 double precision,
    erbb2 double precision,
    ctnnb1 double precision,
    wisp3 double precision,
    mcl1 double precision,
    fancc double precision,
    arfrp1 double precision,
    gpr124 double precision,
    daxx double precision,
    gna11 double precision,
    stag2 double precision,
    fgf6 double precision,
    ntrk1 double precision,
    apc double precision,
    dnmt3a double precision,
    card11 double precision,
    fam46c double precision,
    mdm4 double precision,
    ccnd3 double precision,
    rara double precision,
    tnfrsf14 double precision,
    esr1 double precision,
    nfe2l2 double precision,
    ntrk2 double precision,
    fgfr3 double precision,
    egfr double precision,
    epha3 double precision,
    fgf4 double precision,
    ep300 double precision,
    src double precision,
    nfkbia double precision,
    tnfaip3 double precision,
    palb2 double precision,
    sufu double precision,
    gata1 double precision,
    rptor double precision,
    raf1 double precision,
    mre11a double precision,
    ccne1 double precision,
    smad4 double precision,
    rad51 double precision,
    mef2b double precision,
    axl double precision,
    ret double precision,
    pik3r2 double precision,
    nkx21 double precision,
    ctnna1 double precision,
    notch2 double precision,
    ctcf double precision,
    bcl2 double precision,
    mutyh double precision,
    akt3 double precision,
    tshr double precision,
    jak3 double precision,
    msh6 double precision,
    flt4 double precision,
    irf4 double precision,
    il7r double precision,
    c11orf30 double precision,
    arid1a double precision,
    pik3r1 double precision,
    mpl double precision,
    kdr double precision,
    tsc1 double precision,
    sf3b1 double precision,
    spen double precision,
    etv6 double precision,
    dot1l double precision,
    crebbp double precision,
    bcorl1 double precision,
    cdk8 double precision,
    bcr double precision,
    pak3 double precision,
    map2k2 double precision,
    tgfbr2 double precision,
    ros1 double precision,
    brip1 double precision,
    wt1 double precision,
    cdk12 double precision,
    fanca double precision,
    asxl1 double precision,
    cic double precision,
    kdm6a double precision,
    akt1 double precision,
    atrx double precision,
    foxl2 double precision,
    sox10 double precision,
    cdc73 double precision,
    fgf3 double precision,
    atr double precision,
    pten double precision,
    flt3 double precision,
    kdm5c double precision,
    bard1 double precision,
    gstp1 double precision,
    hif1a double precision,
    il6 double precision,
    klk2 double precision,
    srd5a1 double precision,
    folh1 double precision,
    neat1 double precision,
    schlap1 double precision,
    pdcd1 double precision,
    pca3 double precision,
    map3k1 double precision
);


ALTER TABLE percentile OWNER TO nicholas;

--
-- Name: perc_reporting; Type: VIEW; Schema: analysis; Owner: rds_superuser
--

CREATE VIEW perc_reporting AS
 SELECT percentile.celfile_name,
    percentile.ccnd1,
    percentile.ar,
    percentile.klk3,
    percentile.mki67,
    percentile.nkx31,
    percentile.rb1,
    percentile.erg,
    percentile.etv1,
    percentile.etv4,
    percentile.etv5,
    percentile.spink1,
    percentile.fli1,
    percentile.sparcl1,
    percentile.top2a,
    percentile.chga,
    percentile.cd276,
    percentile.cd274,
    percentile.erbb3,
    percentile.alk,
    percentile.kdm5a,
    percentile.myc,
    percentile.rad50,
    percentile.tp53,
    percentile.jun,
    percentile.men1,
    percentile.chek1,
    percentile.gata3,
    percentile.znf217,
    percentile.grin2a,
    percentile.kit,
    percentile.pdpk1,
    percentile.met,
    percentile.chek2,
    percentile.cdkn2b,
    percentile.stat4,
    percentile.fance,
    percentile.ezh2,
    percentile.pdgfra,
    percentile.fbxw7,
    percentile.notch1,
    percentile.smarca4,
    percentile.fancd2,
    percentile.fgfr1,
    percentile.pax5,
    percentile.mycn,
    percentile.idh2,
    percentile.ewsr1,
    percentile.bcor,
    percentile.pbrm1,
    percentile.ikzf1,
    percentile.blm,
    percentile.znf703,
    percentile.cebpa,
    percentile.gata2,
    percentile.epha5,
    percentile.fancg,
    percentile.tet2,
    percentile.bcl2l2,
    percentile.nras,
    percentile.spop,
    percentile.cdkn1b,
    percentile.ccnd2,
    percentile.erbb4,
    percentile.stk11,
    percentile.cdk6,
    percentile.msh2,
    percentile.nf1,
    percentile.cdkn2c,
    percentile.map2k4,
    percentile.jak2,
    percentile.bcl6,
    percentile.gna13,
    percentile.fancl,
    percentile.mdm2,
    percentile.gnaq,
    percentile.xpo1,
    percentile.cdkn2a,
    percentile.cdh1,
    percentile.ddr2,
    percentile.irs2,
    percentile.fancf,
    percentile.smarcb1,
    percentile.aurka,
    percentile.fgf23,
    percentile.hgf,
    percentile.prkar1a,
    percentile.socs1,
    percentile.prkdc,
    percentile.rnf43,
    percentile.hras,
    percentile.setd2,
    percentile.npm1,
    percentile.pdk1,
    percentile.cd79b,
    percentile.cbl,
    percentile.akt2,
    percentile.araf,
    percentile.med12,
    percentile.cd79a,
    percentile.top1,
    percentile.tmprss2,
    percentile.sox2,
    percentile.fgf14,
    percentile.pik3cg,
    percentile.runx1,
    percentile.lrp1b,
    percentile.tsc2,
    percentile.ppp2r1a,
    percentile.gsk3b,
    percentile.keap1,
    percentile.brca2,
    percentile.pdgfrb,
    percentile.smad2,
    percentile.bap1,
    percentile.vhl,
    percentile.ntrk3,
    percentile.mlh1,
    percentile.igf1r,
    percentile.rictor,
    percentile.nf2,
    percentile.ptpn11,
    percentile.fgf19,
    percentile.atm,
    percentile.brca1,
    percentile.fgfr2,
    percentile.idh1,
    percentile.btk,
    percentile.nup93,
    percentile.braf,
    percentile.crkl,
    percentile.myd88,
    percentile.pik3ca,
    percentile.ikbke,
    percentile.abl1,
    percentile.mitf,
    percentile.aurkb,
    percentile.map2k1,
    percentile.gnas,
    percentile.cbfb,
    percentile.smo,
    percentile.ephb1,
    percentile.flt1,
    percentile.mtor,
    percentile.csf1r,
    percentile.prdm1,
    percentile.kras,
    percentile.fgf10,
    percentile.cdk4,
    percentile.inhba,
    percentile.klhl6,
    percentile.erbb2,
    percentile.ctnnb1,
    percentile.wisp3,
    percentile.mcl1,
    percentile.fancc,
    percentile.arfrp1,
    percentile.gpr124,
    percentile.daxx,
    percentile.gna11,
    percentile.stag2,
    percentile.fgf6,
    percentile.ntrk1,
    percentile.apc,
    percentile.dnmt3a,
    percentile.card11,
    percentile.fam46c,
    percentile.mdm4,
    percentile.ccnd3,
    percentile.rara,
    percentile.tnfrsf14,
    percentile.esr1,
    percentile.nfe2l2,
    percentile.ntrk2,
    percentile.fgfr3,
    percentile.egfr,
    percentile.epha3,
    percentile.fgf4,
    percentile.ep300,
    percentile.src,
    percentile.nfkbia,
    percentile.tnfaip3,
    percentile.palb2,
    percentile.sufu,
    percentile.gata1,
    percentile.rptor,
    percentile.raf1,
    percentile.mre11a,
    percentile.ccne1,
    percentile.smad4,
    percentile.rad51,
    percentile.mef2b,
    percentile.axl,
    percentile.ret,
    percentile.pik3r2,
    percentile.nkx21,
    percentile.ctnna1,
    percentile.notch2,
    percentile.ctcf,
    percentile.bcl2,
    percentile.mutyh,
    percentile.akt3,
    percentile.tshr,
    percentile.jak3,
    percentile.msh6,
    percentile.flt4,
    percentile.irf4,
    percentile.il7r,
    percentile.c11orf30,
    percentile.arid1a,
    percentile.pik3r1,
    percentile.mpl,
    percentile.kdr,
    percentile.tsc1,
    percentile.sf3b1,
    percentile.spen,
    percentile.etv6,
    percentile.dot1l,
    percentile.crebbp,
    percentile.bcorl1,
    percentile.cdk8,
    percentile.bcr,
    percentile.pak3,
    percentile.map2k2,
    percentile.tgfbr2,
    percentile.ros1,
    percentile.brip1,
    percentile.wt1,
    percentile.cdk12,
    percentile.fanca,
    percentile.asxl1,
    percentile.cic,
    percentile.kdm6a,
    percentile.akt1,
    percentile.atrx,
    percentile.foxl2,
    percentile.sox10,
    percentile.cdc73,
    percentile.fgf3,
    percentile.atr,
    percentile.pten,
    percentile.flt3,
    percentile.kdm5c,
    percentile.bard1,
    percentile.gstp1,
    percentile.hif1a,
    percentile.il6,
    percentile.klk2,
    percentile.srd5a1,
    percentile.folh1,
    percentile.neat1,
    percentile.schlap1,
    percentile.pdcd1,
    percentile.pca3,
    percentile.map3k1,
    0.0 AS ergmodel,
        CASE
            WHEN ((percentile.fli1 <> (0.0)::double precision) OR (percentile.etv1 <> (0.0)::double precision) OR (percentile.etv4 <> (0.0)::double precision) OR (percentile.etv5 <> (0.0)::double precision)) THEN 0.0
            ELSE 0.0
        END AS subtype
   FROM percentile;


ALTER TABLE perc_reporting OWNER TO rds_superuser;

--
-- Name: probeset; Type: TABLE; Schema: analysis; Owner: nicholas
--

CREATE TABLE probeset (
    celfile_name character varying(100) NOT NULL,
    neat1 double precision,
    schlap1 double precision,
    pdcd1 double precision
);


ALTER TABLE probeset OWNER TO nicholas;

--
-- Name: scores; Type: TABLE; Schema: analysis; Owner: predictions_table_owner
--

CREATE TABLE scores (
    celfile_name character varying(100) NOT NULL,
    ccp_orig double precision,
    gps_lm double precision,
    organ_confined double precision,
    genomic_capras double precision,
    gleason_grade_deepnet_v1 double precision,
    smallcellv1 double precision,
    decipherv2 double precision,
    decipher double precision,
    ar_signaling_v1 double precision
);


ALTER TABLE scores OWNER TO predictions_table_owner;

--
-- Name: signal_assessments; Type: TABLE; Schema: analysis; Owner: predictions_table_owner
--

CREATE TABLE signal_assessments (
    study_celfile_group_name character varying(100) NOT NULL,
    target character varying(16) NOT NULL,
    feature character(7) NOT NULL,
    min double precision,
    first_quantile double precision,
    median double precision,
    third_quantile double precision,
    max double precision,
    iqr double precision,
    lower_ci double precision,
    upper_ci double precision,
    percent_present double precision,
    ave_gc double precision,
    background double precision
);


ALTER TABLE signal_assessments OWNER TO predictions_table_owner;

--
-- Name: signatures; Type: TABLE; Schema: analysis; Owner: predictions_table_owner
--

CREATE TABLE signatures (
    signature_name character varying(64) NOT NULL,
    type character varying(8) NOT NULL,
    target character varying(16) NOT NULL,
    model character varying(1024),
    feature character varying(1024),
    formula text NOT NULL,
    vector_id character varying(128),
    cutoff double precision,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    CONSTRAINT target_check CHECK (((target)::text = ANY (ARRAY[('core'::character varying)::text, ('probeset'::character varying)::text, ('full'::character varying)::text, ('extended'::character varying)::text]))),
    CONSTRAINT type_check CHECK (((type)::text = ANY (ARRAY[('scan'::character varying)::text, ('frma'::character varying)::text, ('dabg'::character varying)::text])))
);


ALTER TABLE signatures OWNER TO predictions_table_owner;

--
-- Name: cutoffs_id; Type: DEFAULT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY cutoffs ALTER COLUMN cutoffs_id SET DEFAULT nextval('cutoffs_cutoffs_id_seq'::regclass);


--
-- Name: biomarker_description_pk; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_descriptions
    ADD CONSTRAINT biomarker_description_pk PRIMARY KEY (biomarker);


--
-- Name: biomarker_description_reference_mapping_new_pk; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_description_reference_mapping_new
    ADD CONSTRAINT biomarker_description_reference_mapping_new_pk PRIMARY KEY (biomarker, ge_id, reference_id);


--
-- Name: biomarker_description_reference_mapping_pk; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_description_reference_mapping
    ADD CONSTRAINT biomarker_description_reference_mapping_pk PRIMARY KEY (biomarker, reference_id);


--
-- Name: biomarker_descriptions_new_pk; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_descriptions_new
    ADD CONSTRAINT biomarker_descriptions_new_pk PRIMARY KEY (biomarker, ge_id);


--
-- Name: biomarker_genomic_events_pkey; Type: CONSTRAINT; Schema: analysis; Owner: administrator
--

ALTER TABLE ONLY biomarker_genomic_events
    ADD CONSTRAINT biomarker_genomic_events_pkey PRIMARY KEY (biomarker, ge_id);


--
-- Name: biomarker_group_pk; Type: CONSTRAINT; Schema: analysis; Owner: commercial_table_owner
--

ALTER TABLE ONLY biomarker_group
    ADD CONSTRAINT biomarker_group_pk PRIMARY KEY (id);


--
-- Name: biomarker_references_new_pk; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_references_new
    ADD CONSTRAINT biomarker_references_new_pk PRIMARY KEY (reference_id);


--
-- Name: biomarker_references_pk; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_references
    ADD CONSTRAINT biomarker_references_pk PRIMARY KEY (reference_id);


--
-- Name: biomarker_sets_pkey; Type: CONSTRAINT; Schema: analysis; Owner: administrator
--

ALTER TABLE ONLY biomarker_sets
    ADD CONSTRAINT biomarker_sets_pkey PRIMARY KEY (id);


--
-- Name: cutoffs_pkey; Type: CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY cutoffs
    ADD CONSTRAINT cutoffs_pkey PRIMARY KEY (cutoffs_id);


--
-- Name: grid_celfile_name_pkey; Type: CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY grid
    ADD CONSTRAINT grid_celfile_name_pkey PRIMARY KEY (celfile_name);


--
-- Name: pkey_class2_celfile_name; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY class
    ADD CONSTRAINT pkey_class2_celfile_name PRIMARY KEY (celfile_name);


--
-- Name: pkey_core_celfile_name; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY core
    ADD CONSTRAINT pkey_core_celfile_name PRIMARY KEY (celfile_name);


--
-- Name: pkey_extended_celfile_name; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY extended
    ADD CONSTRAINT pkey_extended_celfile_name PRIMARY KEY (celfile_name);


--
-- Name: pkey_percentile2_celfile_name; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY percentile
    ADD CONSTRAINT pkey_percentile2_celfile_name PRIMARY KEY (celfile_name);


--
-- Name: pkey_probeset_celfile_name; Type: CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY probeset
    ADD CONSTRAINT pkey_probeset_celfile_name PRIMARY KEY (celfile_name);


--
-- Name: scores_celfile_name_pkey; Type: CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY scores
    ADD CONSTRAINT scores_celfile_name_pkey PRIMARY KEY (celfile_name);


--
-- Name: signal_assess_pk; Type: CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY signal_assessments
    ADD CONSTRAINT signal_assess_pk PRIMARY KEY (study_celfile_group_name, target, feature);


--
-- Name: signature_pk; Type: CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY signatures
    ADD CONSTRAINT signature_pk PRIMARY KEY (signature_name);


--
-- Name: created_trigger; Type: TRIGGER; Schema: analysis; Owner: predictions_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON signatures FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: TRIGGER created_trigger ON signatures; Type: COMMENT; Schema: analysis; Owner: predictions_table_owner
--

COMMENT ON TRIGGER created_trigger ON signatures IS '
';


--
-- Name: modified_trigger; Type: TRIGGER; Schema: analysis; Owner: predictions_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON signatures FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: biomarker_description_mapping_fk; Type: FK CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_description_reference_mapping
    ADD CONSTRAINT biomarker_description_mapping_fk FOREIGN KEY (biomarker) REFERENCES biomarker_descriptions(biomarker);


--
-- Name: biomarker_description_mapping_new_fk; Type: FK CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_description_reference_mapping_new
    ADD CONSTRAINT biomarker_description_mapping_new_fk FOREIGN KEY (biomarker, ge_id) REFERENCES biomarker_descriptions_new(biomarker, ge_id);


--
-- Name: biomarker_reference_mapping_fk; Type: FK CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_description_reference_mapping
    ADD CONSTRAINT biomarker_reference_mapping_fk FOREIGN KEY (reference_id) REFERENCES biomarker_references(reference_id);


--
-- Name: biomarker_reference_mapping_new_fk; Type: FK CONSTRAINT; Schema: analysis; Owner: nicholas
--

ALTER TABLE ONLY biomarker_description_reference_mapping_new
    ADD CONSTRAINT biomarker_reference_mapping_new_fk FOREIGN KEY (reference_id) REFERENCES biomarker_references_new(reference_id);


--
-- Name: cutoffs_signature_name_fkey; Type: FK CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY cutoffs
    ADD CONSTRAINT cutoffs_signature_name_fkey FOREIGN KEY (signature_name) REFERENCES signatures(signature_name);


--
-- Name: vector_id_fk; Type: FK CONSTRAINT; Schema: analysis; Owner: predictions_table_owner
--

ALTER TABLE ONLY signatures
    ADD CONSTRAINT vector_id_fk FOREIGN KEY (vector_id) REFERENCES normalization.vectors(vector_id);


--
-- Name: analysis; Type: ACL; Schema: -; Owner: predictions_table_owner
--

REVOKE ALL ON SCHEMA analysis FROM PUBLIC;
REVOKE ALL ON SCHEMA analysis FROM predictions_table_owner;
GRANT ALL ON SCHEMA analysis TO predictions_table_owner;
GRANT USAGE ON SCHEMA analysis TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA analysis TO bioinfo;
GRANT USAGE ON SCHEMA analysis TO biostats;
GRANT USAGE ON SCHEMA analysis TO clinops;
GRANT USAGE ON SCHEMA analysis TO engineering;
GRANT ALL ON SCHEMA analysis TO nicholas;
GRANT ALL ON SCHEMA analysis TO administrator;


--
-- Name: biomarker_description_reference_mapping; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE biomarker_description_reference_mapping FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_description_reference_mapping FROM nicholas;
GRANT ALL ON TABLE biomarker_description_reference_mapping TO nicholas;
GRANT SELECT ON TABLE biomarker_description_reference_mapping TO engineering;
GRANT SELECT ON TABLE biomarker_description_reference_mapping TO readonly;


--
-- Name: biomarker_description_reference_mapping_new; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE biomarker_description_reference_mapping_new FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_description_reference_mapping_new FROM nicholas;
GRANT ALL ON TABLE biomarker_description_reference_mapping_new TO nicholas;
GRANT SELECT ON TABLE biomarker_description_reference_mapping_new TO engineering;
GRANT SELECT ON TABLE biomarker_description_reference_mapping_new TO readonly;


--
-- Name: biomarker_descriptions; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE biomarker_descriptions FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_descriptions FROM nicholas;
GRANT ALL ON TABLE biomarker_descriptions TO nicholas;
GRANT SELECT ON TABLE biomarker_descriptions TO engineering;
GRANT SELECT ON TABLE biomarker_descriptions TO readonly;


--
-- Name: biomarker_descriptions_new; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE biomarker_descriptions_new FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_descriptions_new FROM nicholas;
GRANT ALL ON TABLE biomarker_descriptions_new TO nicholas;
GRANT SELECT ON TABLE biomarker_descriptions_new TO engineering;
GRANT SELECT ON TABLE biomarker_descriptions_new TO readonly;


--
-- Name: biomarker_genomic_events; Type: ACL; Schema: analysis; Owner: administrator
--

REVOKE ALL ON TABLE biomarker_genomic_events FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_genomic_events FROM administrator;
GRANT ALL ON TABLE biomarker_genomic_events TO administrator;
GRANT ALL ON TABLE biomarker_genomic_events TO nicholas;
GRANT SELECT ON TABLE biomarker_genomic_events TO engineering;
GRANT SELECT ON TABLE biomarker_genomic_events TO readonly;


--
-- Name: biomarker_group; Type: ACL; Schema: analysis; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE biomarker_group FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_group FROM commercial_table_owner;
GRANT ALL ON TABLE biomarker_group TO commercial_table_owner;
GRANT ALL ON TABLE biomarker_group TO engineering;
GRANT ALL ON TABLE biomarker_group TO nicholas;
GRANT SELECT ON TABLE biomarker_group TO readonly;


--
-- Name: biomarker_references; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE biomarker_references FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_references FROM nicholas;
GRANT ALL ON TABLE biomarker_references TO nicholas;
GRANT SELECT ON TABLE biomarker_references TO engineering;
GRANT SELECT ON TABLE biomarker_references TO readonly;


--
-- Name: biomarker_references_new; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE biomarker_references_new FROM PUBLIC;
REVOKE ALL ON TABLE biomarker_references_new FROM nicholas;
GRANT ALL ON TABLE biomarker_references_new TO nicholas;
GRANT SELECT ON TABLE biomarker_references_new TO engineering;
GRANT SELECT ON TABLE biomarker_references_new TO readonly;


--
-- Name: class; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE class FROM PUBLIC;
REVOKE ALL ON TABLE class FROM nicholas;
GRANT ALL ON TABLE class TO nicholas;
GRANT SELECT ON TABLE class TO readonly;
GRANT SELECT ON TABLE class TO bioinfo;
GRANT SELECT ON TABLE class TO biostats;
GRANT SELECT ON TABLE class TO clinops;
GRANT SELECT ON TABLE class TO commercial_table_owner;


--
-- Name: core; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE core FROM PUBLIC;
REVOKE ALL ON TABLE core FROM nicholas;
GRANT ALL ON TABLE core TO nicholas;
GRANT SELECT ON TABLE core TO commercial_table_owner;
GRANT SELECT ON TABLE core TO clinops;
GRANT SELECT ON TABLE core TO biostats;
GRANT SELECT ON TABLE core TO bioinfo;
GRANT SELECT ON TABLE core TO readonly;


--
-- Name: cutoffs; Type: ACL; Schema: analysis; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE cutoffs FROM PUBLIC;
REVOKE ALL ON TABLE cutoffs FROM predictions_table_owner;
GRANT ALL ON TABLE cutoffs TO predictions_table_owner;
GRANT SELECT ON TABLE cutoffs TO samplesdb_table_owner;
GRANT SELECT ON TABLE cutoffs TO bioinfo;
GRANT SELECT ON TABLE cutoffs TO biostats;
GRANT SELECT ON TABLE cutoffs TO clinops;
GRANT SELECT ON TABLE cutoffs TO readonly;
GRANT ALL ON TABLE cutoffs TO administrator;


--
-- Name: cutoff_signatures; Type: ACL; Schema: analysis; Owner: rds_superuser
--

REVOKE ALL ON TABLE cutoff_signatures FROM PUBLIC;
REVOKE ALL ON TABLE cutoff_signatures FROM rds_superuser;
GRANT ALL ON TABLE cutoff_signatures TO rds_superuser;
GRANT SELECT ON TABLE cutoff_signatures TO readonly;


--
-- Name: extended; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE extended FROM PUBLIC;
REVOKE ALL ON TABLE extended FROM nicholas;
GRANT ALL ON TABLE extended TO nicholas;
GRANT SELECT ON TABLE extended TO commercial_table_owner;
GRANT SELECT ON TABLE extended TO clinops;
GRANT SELECT ON TABLE extended TO biostats;
GRANT SELECT ON TABLE extended TO bioinfo;
GRANT SELECT ON TABLE extended TO readonly;


--
-- Name: genomic_markers; Type: ACL; Schema: analysis; Owner: rds_superuser
--

REVOKE ALL ON TABLE genomic_markers FROM PUBLIC;
REVOKE ALL ON TABLE genomic_markers FROM rds_superuser;
GRANT ALL ON TABLE genomic_markers TO rds_superuser;
GRANT SELECT ON TABLE genomic_markers TO readonly;


--
-- Name: genomic_markers_new; Type: ACL; Schema: analysis; Owner: rds_superuser
--

REVOKE ALL ON TABLE genomic_markers_new FROM PUBLIC;
REVOKE ALL ON TABLE genomic_markers_new FROM rds_superuser;
GRANT ALL ON TABLE genomic_markers_new TO rds_superuser;
GRANT SELECT ON TABLE genomic_markers_new TO readonly;


--
-- Name: grid; Type: ACL; Schema: analysis; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE grid FROM PUBLIC;
REVOKE ALL ON TABLE grid FROM predictions_table_owner;
GRANT ALL ON TABLE grid TO predictions_table_owner;
GRANT SELECT ON TABLE grid TO readonly;
GRANT SELECT ON TABLE grid TO bioinfo;
GRANT SELECT ON TABLE grid TO biostats;
GRANT SELECT ON TABLE grid TO clinops;
GRANT ALL ON TABLE grid TO administrator;


--
-- Name: percentile; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE percentile FROM PUBLIC;
REVOKE ALL ON TABLE percentile FROM nicholas;
GRANT ALL ON TABLE percentile TO nicholas;
GRANT SELECT ON TABLE percentile TO predictions_table_owner;
GRANT SELECT ON TABLE percentile TO readonly;
GRANT SELECT ON TABLE percentile TO bioinfo;
GRANT SELECT ON TABLE percentile TO biostats;
GRANT SELECT ON TABLE percentile TO clinops;


--
-- Name: probeset; Type: ACL; Schema: analysis; Owner: nicholas
--

REVOKE ALL ON TABLE probeset FROM PUBLIC;
REVOKE ALL ON TABLE probeset FROM nicholas;
GRANT ALL ON TABLE probeset TO nicholas;
GRANT SELECT ON TABLE probeset TO commercial_table_owner;
GRANT SELECT ON TABLE probeset TO biostats;
GRANT SELECT ON TABLE probeset TO clinops;
GRANT SELECT ON TABLE probeset TO bioinfo;
GRANT SELECT ON TABLE probeset TO readonly;


--
-- Name: scores; Type: ACL; Schema: analysis; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE scores FROM PUBLIC;
REVOKE ALL ON TABLE scores FROM predictions_table_owner;
GRANT ALL ON TABLE scores TO predictions_table_owner;
GRANT SELECT ON TABLE scores TO readonly;
GRANT SELECT ON TABLE scores TO bioinfo;
GRANT SELECT ON TABLE scores TO biostats;
GRANT SELECT ON TABLE scores TO clinops;
GRANT ALL ON TABLE scores TO administrator;
GRANT SELECT ON TABLE scores TO commercial_table_owner;


--
-- Name: signal_assessments; Type: ACL; Schema: analysis; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE signal_assessments FROM PUBLIC;
REVOKE ALL ON TABLE signal_assessments FROM predictions_table_owner;
GRANT ALL ON TABLE signal_assessments TO predictions_table_owner;
GRANT SELECT ON TABLE signal_assessments TO samplesdb_table_owner;
GRANT SELECT ON TABLE signal_assessments TO bioinfo;
GRANT SELECT ON TABLE signal_assessments TO biostats;
GRANT SELECT ON TABLE signal_assessments TO clinops;
GRANT SELECT ON TABLE signal_assessments TO bioinfo_readonly;
GRANT SELECT ON TABLE signal_assessments TO biostats_readonly;
GRANT SELECT ON TABLE signal_assessments TO readonly;
GRANT ALL ON TABLE signal_assessments TO administrator;


--
-- Name: signatures; Type: ACL; Schema: analysis; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE signatures FROM PUBLIC;
REVOKE ALL ON TABLE signatures FROM predictions_table_owner;
GRANT ALL ON TABLE signatures TO predictions_table_owner;
GRANT SELECT ON TABLE signatures TO samplesdb_table_owner;
GRANT SELECT,INSERT,DELETE,UPDATE ON TABLE signatures TO nicholas;
GRANT SELECT ON TABLE signatures TO bioinfo;
GRANT SELECT ON TABLE signatures TO biostats;
GRANT SELECT ON TABLE signatures TO clinops;
GRANT SELECT ON TABLE signatures TO readonly;
GRANT ALL ON TABLE signatures TO administrator;


--
-- PostgreSQL database dump complete
--

