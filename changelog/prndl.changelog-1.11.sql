--liquibase formatted sql
--changeset mpdillon:hg38 endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: hg38; Type: SCHEMA; Schema: -; Owner: nicholas
--

CREATE SCHEMA hg38;


ALTER SCHEMA hg38 OWNER TO nicholas;

SET search_path = hg38, pg_catalog;

--
-- Name: alignment_type; Type: TYPE; Schema: hg38; Owner: nicholas
--

CREATE TYPE alignment_type AS ENUM (
    'Unmappable',
    'Partial',
    'Perfect'
);


ALTER TYPE alignment_type OWNER TO nicholas;

--
-- Name: chromosome_type; Type: TYPE; Schema: hg38; Owner: nicholas
--

CREATE TYPE chromosome_type AS ENUM (
    'chr2',
    'chr3',
    'chr1',
    'chr13',
    'chr5',
    'chr19',
    'chr8',
    'chr10',
    'chr12',
    'chr22',
    'chr7',
    'chr18',
    'chr14',
    'chr16',
    'chr4',
    'chr6',
    'chrY',
    'chr20',
    'chr11',
    'chr9',
    'chr17',
    'chrX',
    'chr21',
    'chr15',
    'chrM'
);


ALTER TYPE chromosome_type OWNER TO nicholas;

--
-- Name: probe_type; Type: TYPE; Schema: hg38; Owner: nicholas
--

CREATE TYPE probe_type AS ENUM (
    'mm:at',
    'mm:st',
    'pm:at',
    'pm:st'
);


ALTER TYPE probe_type OWNER TO nicholas;

--
-- Name: probeset_type; Type: TYPE; Schema: hg38; Owner: nicholas
--

CREATE TYPE probeset_type AS ENUM (
    'control->affx',
    'control->bgp->antigenomic',
    'control->bgp->genomic',
    'main',
    'normgene->exon:main',
    'normgene->intron',
    'rescue->FLmRNA->unmapped'
);


ALTER TYPE probeset_type OWNER TO nicholas;

--
-- Name: probetranscript_type; Type: TYPE; Schema: hg38; Owner: nicholas
--

CREATE TYPE probetranscript_type AS ENUM (
    'intronic',
    'intergenic',
    'exonic'
);


ALTER TYPE probetranscript_type OWNER TO nicholas;

--
-- Name: strand_type; Type: TYPE; Schema: hg38; Owner: nicholas
--

CREATE TYPE strand_type AS ENUM (
    '+',
    '-'
);


ALTER TYPE strand_type OWNER TO nicholas;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: probe; Type: TABLE; Schema: hg38; Owner: nicholas
--

CREATE TABLE probe (
    probe character(7) NOT NULL,
    probeset character(7) DEFAULT NULL::bpchar,
    chromosome chromosome_type,
    start bigint,
    "end" bigint,
    strand strand_type,
    numgcs smallint,
    gstacklength smallint,
    xhyb smallint,
    type probe_type,
    alignment alignment_type,
    chipx smallint,
    chipy smallint,
    sequence character(25) DEFAULT NULL::bpchar,
    atom character(7) DEFAULT NULL::bpchar
);


ALTER TABLE probe OWNER TO nicholas;

--
-- Name: probeset; Type: TABLE; Schema: hg38; Owner: nicholas
--

CREATE TABLE probeset (
    probeset character(7) NOT NULL,
    chromosome chromosome_type,
    start integer,
    "end" integer,
    strand strand_type,
    type probeset_type NOT NULL,
    core character(7) DEFAULT NULL::bpchar,
    extended character(7) DEFAULT NULL::bpchar,
    "full" character(7) DEFAULT NULL::bpchar,
    comprehensive character(7) DEFAULT NULL::bpchar
);


ALTER TABLE probeset OWNER TO nicholas;

--
-- Name: probesettranscript; Type: TABLE; Schema: hg38; Owner: nicholas
--

CREATE TABLE probesettranscript (
    probeset character(7) NOT NULL,
    transcriptid character(32) NOT NULL,
    type probetranscript_type,
    cds smallint,
    utr smallint
);


ALTER TABLE probesettranscript OWNER TO nicholas;

--
-- Name: probetranscript; Type: TABLE; Schema: hg38; Owner: nicholas
--

CREATE TABLE probetranscript (
    probe character(7) NOT NULL,
    transcriptid character(32) NOT NULL,
    type probetranscript_type,
    cds smallint,
    utr smallint
);


ALTER TABLE probetranscript OWNER TO nicholas;

--
-- Name: transcript; Type: TABLE; Schema: hg38; Owner: nicholas
--

CREATE TABLE transcript (
    transcriptid character(32) NOT NULL,
    chromosome chromosome_type,
    gene character(32) NOT NULL,
    transcript character(64) NOT NULL,
    strand strand_type,
    genetype character(64) DEFAULT NULL::bpchar,
    source character(32) DEFAULT NULL::bpchar,
    support integer,
    transcripttype character(64) DEFAULT NULL::bpchar,
    start integer,
    "end" integer,
    nearestref character(32) DEFAULT NULL::bpchar,
    class character(32) DEFAULT NULL::bpchar
);


ALTER TABLE transcript OWNER TO nicholas;

--
-- Name: probe_pkey; Type: CONSTRAINT; Schema: hg38; Owner: nicholas
--

ALTER TABLE ONLY probe
    ADD CONSTRAINT probe_pkey PRIMARY KEY (probe);


--
-- Name: probeset_pkey; Type: CONSTRAINT; Schema: hg38; Owner: nicholas
--

ALTER TABLE ONLY probeset
    ADD CONSTRAINT probeset_pkey PRIMARY KEY (probeset);


--
-- Name: probesettranscript_pkey; Type: CONSTRAINT; Schema: hg38; Owner: nicholas
--

ALTER TABLE ONLY probesettranscript
    ADD CONSTRAINT probesettranscript_pkey PRIMARY KEY (probeset, transcriptid);


--
-- Name: probetranscript_pkey; Type: CONSTRAINT; Schema: hg38; Owner: nicholas
--

ALTER TABLE ONLY probetranscript
    ADD CONSTRAINT probetranscript_pkey PRIMARY KEY (probe, transcriptid);


--
-- Name: transcript_pkey; Type: CONSTRAINT; Schema: hg38; Owner: nicholas
--

ALTER TABLE ONLY transcript
    ADD CONSTRAINT transcript_pkey PRIMARY KEY (transcriptid);


--
-- Name: probe_alignment_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_alignment_idx ON probe USING btree (alignment);


--
-- Name: probe_chromosome_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_chromosome_idx ON probe USING btree (chromosome);


--
-- Name: probe_gstacklength_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_gstacklength_idx ON probe USING btree (gstacklength);


--
-- Name: probe_numgcs_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_numgcs_idx ON probe USING btree (numgcs);


--
-- Name: probe_probeset_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_probeset_idx ON probe USING btree (probeset);


--
-- Name: probe_strand_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_strand_idx ON probe USING btree (strand);


--
-- Name: probe_type_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_type_idx ON probe USING btree (type);


--
-- Name: probe_xhyb_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probe_xhyb_idx ON probe USING btree (xhyb);


--
-- Name: probeset_chromosome_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_chromosome_idx ON probeset USING btree (chromosome);


--
-- Name: probeset_comprehensive_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_comprehensive_idx ON probeset USING btree (comprehensive);


--
-- Name: probeset_core_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_core_idx ON probeset USING btree (core);


--
-- Name: probeset_extended_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_extended_idx ON probeset USING btree (extended);


--
-- Name: probeset_full_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_full_idx ON probeset USING btree ("full");


--
-- Name: probeset_strand_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_strand_idx ON probeset USING btree (strand);


--
-- Name: probeset_type_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probeset_type_idx ON probeset USING btree (type);


--
-- Name: probesettranscript_cds_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probesettranscript_cds_idx ON probesettranscript USING btree (cds);


--
-- Name: probesettranscript_probeset_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probesettranscript_probeset_idx ON probesettranscript USING btree (probeset);


--
-- Name: probesettranscript_transcriptid_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probesettranscript_transcriptid_idx ON probesettranscript USING btree (transcriptid);


--
-- Name: probesettranscript_type_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probesettranscript_type_idx ON probesettranscript USING btree (type);


--
-- Name: probesettranscript_utr_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probesettranscript_utr_idx ON probesettranscript USING btree (utr);


--
-- Name: probetranscript_cds_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probetranscript_cds_idx ON probetranscript USING btree (cds);


--
-- Name: probetranscript_probe_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probetranscript_probe_idx ON probetranscript USING btree (probe);


--
-- Name: probetranscript_transcriptid_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probetranscript_transcriptid_idx ON probetranscript USING btree (transcriptid);


--
-- Name: probetranscript_type_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probetranscript_type_idx ON probetranscript USING btree (type);


--
-- Name: probetranscript_utr_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX probetranscript_utr_idx ON probetranscript USING btree (utr);


--
-- Name: transcript_chromosome_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_chromosome_idx ON transcript USING btree (chromosome);


--
-- Name: transcript_class_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_class_idx ON transcript USING btree (class);


--
-- Name: transcript_gene_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_gene_idx ON transcript USING btree (gene);


--
-- Name: transcript_genetype_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_genetype_idx ON transcript USING btree (genetype);


--
-- Name: transcript_nearestref_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_nearestref_idx ON transcript USING btree (nearestref);


--
-- Name: transcript_source_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_source_idx ON transcript USING btree (source);


--
-- Name: transcript_strand_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_strand_idx ON transcript USING btree (strand);


--
-- Name: transcript_support_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_support_idx ON transcript USING btree (support);


--
-- Name: transcript_transcript_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_transcript_idx ON transcript USING btree (transcript);


--
-- Name: transcript_transcripttype_idx; Type: INDEX; Schema: hg38; Owner: nicholas
--

CREATE INDEX transcript_transcripttype_idx ON transcript USING btree (transcripttype);


--
-- Name: hg38; Type: ACL; Schema: -; Owner: nicholas
--

REVOKE ALL ON SCHEMA hg38 FROM PUBLIC;
REVOKE ALL ON SCHEMA hg38 FROM nicholas;
GRANT ALL ON SCHEMA hg38 TO nicholas;
GRANT ALL ON SCHEMA hg38 TO administrator;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA hg38;
