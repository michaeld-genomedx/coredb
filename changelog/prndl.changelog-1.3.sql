--liquibase formatted sql
--changeset mpdillon:commercial endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: commercial; Type: SCHEMA; Schema: -; Owner: commercial_table_owner
--

CREATE SCHEMA commercial;


ALTER SCHEMA commercial OWNER TO commercial_table_owner;

SET search_path = commercial, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: bioinfo_celfile; Type: TABLE; Schema: commercial; Owner: commercial_table_owner
--

CREATE TABLE bioinfo_celfile (
    celfile_name character varying NOT NULL
);


ALTER TABLE bioinfo_celfile OWNER TO commercial_table_owner;

--
-- Name: deidentifier; Type: TABLE; Schema: commercial; Owner: commercial_table_owner
--

CREATE TABLE deidentifier (
    celfile_name character varying NOT NULL,
    grid_id character varying NOT NULL,
    assay_id character varying,
    s3_upload boolean,
    grid_celfile_name character varying,
    accession_id character varying,
    data_confirmed boolean,
    date_request date,
    s3 character varying,
    s3_rdaws character varying,
    grid_notes character varying,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    physician_name character varying,
    practice character varying,
    mp3_batch numeric,
    grid_batch numeric
);


ALTER TABLE deidentifier OWNER TO commercial_table_owner;

--
-- Name: patients; Type: TABLE; Schema: commercial; Owner: commercial_table_owner
--

CREATE TABLE patients (
    assay_id character varying NOT NULL,
    accession_id character varying,
    test_type character varying,
    age numeric,
    age_bx numeric,
    ordering_physician character varying,
    genomic_specialist character varying,
    practice_city character varying,
    practice_state character varying,
    practice_zipcode character varying,
    practice_country character varying,
    order_date date,
    date_received date,
    rp_year numeric,
    bx_year numeric,
    rp_to_order_time numeric,
    bx_to_order_time numeric,
    preop_psa numeric,
    cstage character varying,
    pstage character varying,
    bx_risk_group character varying,
    bx_percent_pos_cores numeric,
    clings numeric,
    clings_p numeric,
    clings_s numeric,
    pathgs numeric,
    pathgs_p numeric,
    pathgs_s numeric,
    pathgs_t numeric,
    epe numeric,
    svi numeric,
    sm numeric,
    lni numeric,
    bcr numeric,
    pni numeric,
    lvi numeric,
    bni numeric,
    xsn_pathfeatures_notes character varying,
    bcm_version character varying,
    task_id numeric,
    celfile_name character varying,
    celfile_date date,
    qc character varying,
    percentpresent numeric,
    rf22_percentpresent numeric,
    posneg_auc numeric,
    hybcontrol character varying,
    hybpercentpresent numeric,
    decipher_gc numeric,
    decipher_met5yr numeric,
    decipher_met5yr_lowerci numeric,
    decipher_met5yr_upperci numeric,
    decipher_pcsm10yr numeric,
    decipher_pcsm10yr_lowerci numeric,
    decipher_pcsm10yr_upperci numeric,
    decipher_pgg45 numeric,
    decipher_pgg45_lowerci numeric,
    decipher_pgg45_upperci numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying,
    modified_by character varying,
    race character varying
);


ALTER TABLE patients OWNER TO commercial_table_owner;

--
-- Name: prospective_release; Type: TABLE; Schema: commercial; Owner: commercial_table_owner
--

CREATE TABLE prospective_release (
    release_version character varying NOT NULL,
    grid_celfile_name character varying NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE prospective_release OWNER TO commercial_table_owner;

--
-- Name: COLUMN prospective_release.release_version; Type: COMMENT; Schema: commercial; Owner: commercial_table_owner
--

COMMENT ON COLUMN prospective_release.release_version IS 'String indicating - version number';


--
-- Name: COLUMN prospective_release.grid_celfile_name; Type: COMMENT; Schema: commercial; Owner: commercial_table_owner
--

COMMENT ON COLUMN prospective_release.grid_celfile_name IS 'string with deidentified grid_celfile_name';


--
-- Name: sdpath; Type: TABLE; Schema: commercial; Owner: commercial_table_owner
--

CREATE TABLE sdpath (
    assay_id character varying NOT NULL,
    original_id character varying,
    test_type character varying,
    tumor_percent numeric,
    benign_percent numeric,
    tissue_gleason_score numeric,
    tissue_gleason_p numeric,
    tissue_gleason_s numeric,
    bx_tumor_length numeric,
    sdpath_notes character varying,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying,
    modified_by character varying
);


ALTER TABLE sdpath OWNER TO commercial_table_owner;

--
-- Name: accession_id; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY deidentifier
    ADD CONSTRAINT accession_id UNIQUE (accession_id);


--
-- Name: commercial_accession_unique; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY patients
    ADD CONSTRAINT commercial_accession_unique UNIQUE (accession_id);


--
-- Name: commerical_assay_pkey; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY patients
    ADD CONSTRAINT commerical_assay_pkey PRIMARY KEY (assay_id);


--
-- Name: commerical_sdpath_assay_pkey; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY sdpath
    ADD CONSTRAINT commerical_sdpath_assay_pkey PRIMARY KEY (assay_id);


--
-- Name: deidentifier_celfile_name_unique; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY deidentifier
    ADD CONSTRAINT deidentifier_celfile_name_unique UNIQUE (celfile_name);


--
-- Name: deidentifier_grid_celfile_name_unique; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY deidentifier
    ADD CONSTRAINT deidentifier_grid_celfile_name_unique UNIQUE (grid_celfile_name);


--
-- Name: deidentifier_pkey; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY deidentifier
    ADD CONSTRAINT deidentifier_pkey PRIMARY KEY (grid_id);


--
-- Name: prospective_release_pkey; Type: CONSTRAINT; Schema: commercial; Owner: commercial_table_owner
--

ALTER TABLE ONLY prospective_release
    ADD CONSTRAINT prospective_release_pkey PRIMARY KEY (release_version, grid_celfile_name);


--
-- Name: created_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON deidentifier FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON prospective_release FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON patients FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON sdpath FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON deidentifier FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON prospective_release FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON patients FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: commercial; Owner: commercial_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON sdpath FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: commercial; Type: ACL; Schema: -; Owner: commercial_table_owner
--

REVOKE ALL ON SCHEMA commercial FROM PUBLIC;
REVOKE ALL ON SCHEMA commercial FROM commercial_table_owner;
GRANT ALL ON SCHEMA commercial TO commercial_table_owner;
GRANT USAGE ON SCHEMA commercial TO engineering;
GRANT USAGE ON SCHEMA commercial TO angie;
GRANT ALL ON SCHEMA commercial TO administrator;
GRANT USAGE ON SCHEMA commercial TO biostats;


--
-- Name: bioinfo_celfile; Type: ACL; Schema: commercial; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE bioinfo_celfile FROM PUBLIC;
REVOKE ALL ON TABLE bioinfo_celfile FROM commercial_table_owner;
GRANT ALL ON TABLE bioinfo_celfile TO commercial_table_owner;
GRANT ALL ON TABLE bioinfo_celfile TO grid_admin;
GRANT SELECT ON TABLE bioinfo_celfile TO biostats;


--
-- Name: deidentifier; Type: ACL; Schema: commercial; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE deidentifier FROM PUBLIC;
REVOKE ALL ON TABLE deidentifier FROM commercial_table_owner;
GRANT ALL ON TABLE deidentifier TO commercial_table_owner;
GRANT SELECT ON TABLE deidentifier TO engineering;
GRANT SELECT ON TABLE deidentifier TO angie;


--
-- Name: patients; Type: ACL; Schema: commercial; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE patients FROM PUBLIC;
REVOKE ALL ON TABLE patients FROM commercial_table_owner;
GRANT ALL ON TABLE patients TO commercial_table_owner;
GRANT ALL ON TABLE patients TO administrator;


--
-- Name: sdpath; Type: ACL; Schema: commercial; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE sdpath FROM PUBLIC;
REVOKE ALL ON TABLE sdpath FROM commercial_table_owner;
GRANT ALL ON TABLE sdpath TO commercial_table_owner;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA commercial;
