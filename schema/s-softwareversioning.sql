--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: softwareversioning; Type: SCHEMA; Schema: -; Owner: administrator
--

CREATE SCHEMA softwareversioning;


ALTER SCHEMA softwareversioning OWNER TO administrator;

SET search_path = softwareversioning, pg_catalog;

--
-- Name: software_version_id_seq; Type: SEQUENCE; Schema: softwareversioning; Owner: rds_superuser
--

CREATE SEQUENCE software_version_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE software_version_id_seq OWNER TO rds_superuser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: software_versions; Type: TABLE; Schema: softwareversioning; Owner: administrator
--

CREATE TABLE software_versions (
    software_name character varying(32) NOT NULL,
    version character varying(32),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    software_version_id integer NOT NULL
);


ALTER TABLE software_versions OWNER TO administrator;

--
-- Name: created_trigger; Type: TRIGGER; Schema: softwareversioning; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON software_versions FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: softwareversioning; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON software_versions FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: softwareversioning; Type: ACL; Schema: -; Owner: administrator
--

REVOKE ALL ON SCHEMA softwareversioning FROM PUBLIC;
REVOKE ALL ON SCHEMA softwareversioning FROM administrator;
GRANT ALL ON SCHEMA softwareversioning TO administrator;
GRANT USAGE ON SCHEMA softwareversioning TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA softwareversioning TO bioinfo;
GRANT USAGE ON SCHEMA softwareversioning TO biostats;
GRANT USAGE ON SCHEMA softwareversioning TO engineering;


--
-- Name: software_version_id_seq; Type: ACL; Schema: softwareversioning; Owner: rds_superuser
--

REVOKE ALL ON SEQUENCE software_version_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE software_version_id_seq FROM rds_superuser;
GRANT ALL ON SEQUENCE software_version_id_seq TO rds_superuser;
GRANT SELECT,USAGE ON SEQUENCE software_version_id_seq TO samplesdb_table_owner;
GRANT SELECT,USAGE ON SEQUENCE software_version_id_seq TO bioinfo;
GRANT SELECT,USAGE ON SEQUENCE software_version_id_seq TO biostats;


--
-- Name: software_versions; Type: ACL; Schema: softwareversioning; Owner: administrator
--

REVOKE ALL ON TABLE software_versions FROM PUBLIC;
REVOKE ALL ON TABLE software_versions FROM administrator;
GRANT ALL ON TABLE software_versions TO administrator;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE software_versions TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE software_versions TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE software_versions TO biostats;
GRANT SELECT ON TABLE software_versions TO readonly;
GRANT SELECT ON TABLE software_versions TO bioinfo_readonly;
GRANT SELECT ON TABLE software_versions TO biostats_readonly;


--
-- PostgreSQL database dump complete
--

