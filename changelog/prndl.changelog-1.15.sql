--liquibase formatted sql
--changeset mpdillon:expression_clindev_v2 endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: expression_clindev_v2; Type: SCHEMA; Schema: -; Owner: grid_admin
--

CREATE SCHEMA expression_clindev_v2;


ALTER SCHEMA expression_clindev_v2 OWNER TO grid_admin;

SET search_path = expression_clindev_v2, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: expression; Type: TABLE; Schema: expression_clindev_v2; Owner: grid_admin
--

CREATE TABLE expression (
    celfile_name character varying(255) NOT NULL,
    feature character varying(32) NOT NULL,
    level character varying(16) NOT NULL,
    normalization character varying(16) NOT NULL,
    normalization_environment character varying(32) NOT NULL,
    expression double precision NOT NULL
);


ALTER TABLE expression OWNER TO grid_admin;

--
-- Name: feature_import; Type: VIEW; Schema: expression_clindev_v2; Owner: grid_admin
--

CREATE VIEW feature_import AS
 SELECT DISTINCT sql1.feature,
    sql1.level,
    sql1.normalization,
    sql1.normalization_environment
   FROM ( SELECT feature.feature,
            feature.level,
            feature.normalization,
            feature.normalization_environment
           FROM signatures_clindev_v2.feature
        UNION
         SELECT biomarker.feature,
            biomarker.level,
            biomarker.normalization,
            biomarker.normalization_environment
           FROM grid_clindev_v2.biomarker) sql1;


ALTER TABLE feature_import OWNER TO grid_admin;

--
-- Name: expression_clindev_v2_pkey; Type: CONSTRAINT; Schema: expression_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY expression
    ADD CONSTRAINT expression_clindev_v2_pkey PRIMARY KEY (celfile_name, feature, level, normalization, normalization_environment);


--
-- Name: expression_celfile_name_idx; Type: INDEX; Schema: expression_clindev_v2; Owner: grid_admin
--

CREATE INDEX expression_celfile_name_idx ON expression USING btree (celfile_name);


--
-- Name: expression_feature_level_normalization_norm_env_idx; Type: INDEX; Schema: expression_clindev_v2; Owner: grid_admin
--

CREATE INDEX expression_feature_level_normalization_norm_env_idx ON expression USING btree (feature, level, normalization, normalization_environment);


--
-- Name: expression_level_idx; Type: INDEX; Schema: expression_clindev_v2; Owner: grid_admin
--

CREATE INDEX expression_level_idx ON expression USING btree (level);


--
-- Name: fki_expression_environment_name_fkey; Type: INDEX; Schema: expression_clindev_v2; Owner: grid_admin
--

CREATE INDEX fki_expression_environment_name_fkey ON expression USING btree (normalization_environment);


--
-- Name: expression_clindev_v2_environment_name_fkey; Type: FK CONSTRAINT; Schema: expression_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY expression
    ADD CONSTRAINT expression_clindev_v2_environment_name_fkey FOREIGN KEY (normalization_environment) REFERENCES signatures_clindev_v2.environment(name);


--
-- Name: expression_clindev_v2; Type: ACL; Schema: -; Owner: grid_admin
--

REVOKE ALL ON SCHEMA expression_clindev_v2 FROM PUBLIC;
REVOKE ALL ON SCHEMA expression_clindev_v2 FROM grid_admin;
GRANT ALL ON SCHEMA expression_clindev_v2 TO grid_admin;
GRANT USAGE ON SCHEMA expression_clindev_v2 TO clinops;
GRANT USAGE ON SCHEMA expression_clindev_v2 TO biostats;
GRANT USAGE ON SCHEMA expression_clindev_v2 TO bioinfo;
GRANT USAGE ON SCHEMA expression_clindev_v2 TO model_developer;
GRANT ALL ON SCHEMA expression_clindev_v2 TO administrator;
GRANT USAGE ON SCHEMA expression_clindev_v2 TO engineering;


--
-- Name: expression; Type: ACL; Schema: expression_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE expression FROM PUBLIC;
REVOKE ALL ON TABLE expression FROM grid_admin;
GRANT ALL ON TABLE expression TO grid_admin;
GRANT SELECT ON TABLE expression TO engineering;


--
-- Name: feature_import; Type: ACL; Schema: expression_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE feature_import FROM PUBLIC;
REVOKE ALL ON TABLE feature_import FROM grid_admin;
GRANT ALL ON TABLE feature_import TO grid_admin;
GRANT SELECT ON TABLE feature_import TO engineering;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA expression_clindev_v2;
