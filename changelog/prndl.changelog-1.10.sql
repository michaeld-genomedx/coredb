--liquibase formatted sql
--changeset mpdillon:gridreport endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: gridreport; Type: SCHEMA; Schema: -; Owner: nicholas
--

CREATE SCHEMA gridreport;


ALTER SCHEMA gridreport OWNER TO nicholas;

SET search_path = gridreport, pg_catalog;

--
-- Name: arsignaling(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION arsignaling(character varying) RETURNS TABLE(celfile_name character varying, percentile double precision, score double precision, class text)
    LANGUAGE sql
    AS $_$
SELECT celfile_name, AVG(percentile) AS percentile, AVG(score) AS score, CASE 
WHEN AVG(percentile) <= 0.2 THEN 'Lower AR Activity'
ELSE 'Average AR Activity' END AS class
FROM signatures.prediction WHERE signature_id IN (10, 66) AND celfile_name = $1 GROUP BY celfile_name;
$_$;


ALTER FUNCTION gridreport.arsignaling(character varying) OWNER TO nicholas;

--
-- Name: biomarkers(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION biomarkers(character varying) RETURNS TABLE(name character varying, category character varying, expression double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
SELECT name, category, expression, percentile, class FROM gridreport.biomarkersmeta JOIN grid.transcription USING (biomarker) WHERE celfile_name = $1 ORDER BY category ASC, "order" ASC;
$_$;


ALTER FUNCTION gridreport.biomarkers(character varying) OWNER TO nicholas;

--
-- Name: bottom_biomarker(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION bottom_biomarker(character varying) RETURNS TABLE(biomarker character varying, expression double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
    SELECT name, expression, percentile, class FROM gridreport.biomarkers($1) ORDER BY percentile ASC, expression ASC LIMIT 1;
$_$;


ALTER FUNCTION gridreport.bottom_biomarker(character varying) OWNER TO nicholas;

--
-- Name: completed(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION completed(character varying) RETURNS boolean
    LANGUAGE sql
    AS $_$
SELECT SUM((present IS TRUE)::integer) = 1 AND  SUM((present IS FALSE)::integer) = 248 FROM (
	SELECT class IS NULL AS present FROM gridreport.summary($1)
	UNION ALL
	SELECT percentile IS NULL AS present FROM gridreport.summary($1)
	UNION ALL
	SELECT class IS NULL AS present FROM gridreport.summary($1)
	UNION ALL
	SELECT score IS NULL AS present FROM gridreport.signatures($1)
	UNION ALL
	SELECT percentile IS NULL AS present FROM gridreport.signatures($1)
	UNION ALL
	SELECT class IS NULL AS present FROM gridreport.signatures($1)
	UNION ALL
	SELECT expression IS NULL AS present FROM gridreport.biomarkers($1)
	UNION ALL
	SELECT percentile IS NULL AS present FROM gridreport.biomarkers($1)
	UNION ALL
	SELECT class IS NULL AS present FROM gridreport.biomarkers($1)
) AS sql1;
$_$;


ALTER FUNCTION gridreport.completed(character varying) OWNER TO nicholas;

--
-- Name: molecular_subtype(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION molecular_subtype(character varying) RETURNS character varying
    LANGUAGE sql
    AS $_$
SELECT 
molecular_subtype
FROM signatures.molecular_subtype
WHERE celfile_name = $1;
$_$;


ALTER FUNCTION gridreport.molecular_subtype(character varying) OWNER TO nicholas;

--
-- Name: prognosticavg(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION prognosticavg(character varying) RETURNS TABLE(celfile_name character varying, percentile double precision, score double precision, class text)
    LANGUAGE sql
    AS $_$
SELECT celfile_name, AVG(percentile) AS percentile, AVG(score) AS score, CASE 
WHEN AVG(percentile) < 0.4 THEN 'Low Metastasis Risk' 
WHEN AVG(percentile) > 0.7 THEN 'High Metastasis Risk'
ELSE 'Average Metastasis Risk' END AS class
FROM signatures.prediction WHERE signature_id IN (2, 3, 6, 12, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33,  35) AND celfile_name = $1 GROUP BY celfile_name;
$_$;


ALTER FUNCTION gridreport.prognosticavg(character varying) OWNER TO nicholas;

--
-- Name: proliferation(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION proliferation(character varying) RETURNS TABLE(celfile_name character varying, percentile double precision, score double precision, class text)
    LANGUAGE sql
    AS $_$
SELECT celfile_name, AVG(percentile) AS percentile, AVG(score) AS score, CASE 
WHEN AVG(percentile) > 0.75 THEN 'High Proliferation'
ELSE 'Average Proliferation' END AS class
FROM signatures.prediction WHERE signature_id IN (4, 32, 23) AND celfile_name = $1 GROUP BY celfile_name;
$_$;


ALTER FUNCTION gridreport.proliferation(character varying) OWNER TO nicholas;

--
-- Name: signatures(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION signatures(character varying) RETURNS TABLE(section character varying, section_description character varying, subsection character varying, subsection_description character varying, signature character varying, signature_description character varying, institution character varying, author character varying, year smallint, citation character varying, score double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
SELECT 
section.name AS section, 
section.description AS section_description, 
subsection.name AS subsection, 
subsection.description AS subsection_description, 
signaturemeta.name AS signature, 
signaturemeta.description AS signature_description, 
institution, author, year, citation, score, percentile, class 
FROM gridreport.section 
JOIN gridreport.subsection USING (sectionid) 
JOIN gridreport.signaturemeta USING (subsectionid) 
JOIN signatures.prediction USING (signature_id)
WHERE celfile_name = $1
ORDER BY section.order, subsection.order, signaturemeta.order;
$_$;


ALTER FUNCTION gridreport.signatures(character varying) OWNER TO nicholas;

--
-- Name: summary(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION summary(character varying) RETURNS TABLE(signature character varying, percentile double precision, score double precision, class character varying)
    LANGUAGE sql
    AS $_$
SELECT name, percentile, score, class FROM (
	SELECT 'prognosticavg' AS name, celfile_name, score, percentile, class FROM gridreport.prognosticavg($1)
	UNION
	SELECT 'arsignaling' AS name, celfile_name, score, percentile, class FROM gridreport.arsignaling($1)
	UNION
	SELECT 'proliferation' AS name, celfile_name, score, percentile, class FROM gridreport.proliferation($1)
	UNION
	SELECT 'gleasongrade' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 15
	UNION
	SELECT 'ars' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 46
	UNION
	SELECT 'portos' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 65
	UNION
	SELECT 'docetaxel' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 38
	UNION
	SELECT 'dasatinib' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 41
	UNION
	SELECT 'basalluminal' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 11
	UNION
	SELECT 'smallcell' AS name, celfile_name, score, percentile, class FROM signatures.prediction WHERE celfile_name = $1 AND signature_id = 68
	UNION
	SELECT 'molecularsubtype' AS name, $1 AS celfile_name, NULL AS score, NULL AS percentile, gridreport.molecular_subtype($1) AS class
) AS sql1;
$_$;


ALTER FUNCTION gridreport.summary(character varying) OWNER TO nicholas;

--
-- Name: top_biomarker(character varying); Type: FUNCTION; Schema: gridreport; Owner: nicholas
--

CREATE FUNCTION top_biomarker(character varying) RETURNS TABLE(biomarker character varying, expression double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
    SELECT name, expression, percentile, class FROM gridreport.biomarkers($1) ORDER BY percentile DESC, expression DESC LIMIT 1;
$_$;


ALTER FUNCTION gridreport.top_biomarker(character varying) OWNER TO nicholas;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: biomarkersmeta; Type: TABLE; Schema: gridreport; Owner: nicholas
--

CREATE TABLE biomarkersmeta (
    biomarker character varying(32) NOT NULL,
    name character varying(32),
    category character varying(32),
    "order" smallint
);


ALTER TABLE biomarkersmeta OWNER TO nicholas;

--
-- Name: section; Type: TABLE; Schema: gridreport; Owner: nicholas
--

CREATE TABLE section (
    sectionid bigint NOT NULL,
    description character varying(256),
    "order" real,
    name character varying(256)
);


ALTER TABLE section OWNER TO nicholas;

--
-- Name: section_sectionid_seq; Type: SEQUENCE; Schema: gridreport; Owner: nicholas
--

CREATE SEQUENCE section_sectionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE section_sectionid_seq OWNER TO nicholas;

--
-- Name: section_sectionid_seq; Type: SEQUENCE OWNED BY; Schema: gridreport; Owner: nicholas
--

ALTER SEQUENCE section_sectionid_seq OWNED BY section.sectionid;


--
-- Name: signaturemeta; Type: TABLE; Schema: gridreport; Owner: nicholas
--

CREATE TABLE signaturemeta (
    signature_id bigint NOT NULL,
    name character varying(128),
    description text,
    institution character varying(256),
    author character varying(128),
    citation character varying(1024),
    year smallint,
    subsectionid bigint,
    "order" real
);


ALTER TABLE signaturemeta OWNER TO nicholas;

--
-- Name: subsection; Type: TABLE; Schema: gridreport; Owner: nicholas
--

CREATE TABLE subsection (
    subsectionid bigint NOT NULL,
    name character varying(256),
    description character varying(256),
    "order" real NOT NULL,
    sectionid bigint
);


ALTER TABLE subsection OWNER TO nicholas;

--
-- Name: subsection_subsectionid_seq; Type: SEQUENCE; Schema: gridreport; Owner: nicholas
--

CREATE SEQUENCE subsection_subsectionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subsection_subsectionid_seq OWNER TO nicholas;

--
-- Name: subsection_subsectionid_seq; Type: SEQUENCE OWNED BY; Schema: gridreport; Owner: nicholas
--

ALTER SEQUENCE subsection_subsectionid_seq OWNED BY subsection.subsectionid;


--
-- Name: sectionid; Type: DEFAULT; Schema: gridreport; Owner: nicholas
--

ALTER TABLE ONLY section ALTER COLUMN sectionid SET DEFAULT nextval('section_sectionid_seq'::regclass);


--
-- Name: subsectionid; Type: DEFAULT; Schema: gridreport; Owner: nicholas
--

ALTER TABLE ONLY subsection ALTER COLUMN subsectionid SET DEFAULT nextval('subsection_subsectionid_seq'::regclass);


--
-- Name: biomarkersmeta_pkey; Type: CONSTRAINT; Schema: gridreport; Owner: nicholas
--

ALTER TABLE ONLY biomarkersmeta
    ADD CONSTRAINT biomarkersmeta_pkey PRIMARY KEY (biomarker);


--
-- Name: section_pkey; Type: CONSTRAINT; Schema: gridreport; Owner: nicholas
--

ALTER TABLE ONLY section
    ADD CONSTRAINT section_pkey PRIMARY KEY (sectionid);


--
-- Name: signaturemeta_pkey; Type: CONSTRAINT; Schema: gridreport; Owner: nicholas
--

ALTER TABLE ONLY signaturemeta
    ADD CONSTRAINT signaturemeta_pkey PRIMARY KEY (signature_id);


--
-- Name: subsection_pkey; Type: CONSTRAINT; Schema: gridreport; Owner: nicholas
--

ALTER TABLE ONLY subsection
    ADD CONSTRAINT subsection_pkey PRIMARY KEY (subsectionid);


--
-- Name: gridreport; Type: ACL; Schema: -; Owner: nicholas
--

REVOKE ALL ON SCHEMA gridreport FROM PUBLIC;
REVOKE ALL ON SCHEMA gridreport FROM nicholas;
GRANT ALL ON SCHEMA gridreport TO nicholas;
GRANT ALL ON SCHEMA gridreport TO engineering;
GRANT ALL ON SCHEMA gridreport TO administrator;
GRANT ALL ON SCHEMA gridreport TO bioinfo;
GRANT ALL ON SCHEMA gridreport TO clinops;


--
-- Name: completed(character varying); Type: ACL; Schema: gridreport; Owner: nicholas
--

REVOKE ALL ON FUNCTION completed(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION completed(character varying) FROM nicholas;
GRANT ALL ON FUNCTION completed(character varying) TO nicholas;
GRANT ALL ON FUNCTION completed(character varying) TO PUBLIC;
GRANT ALL ON FUNCTION completed(character varying) TO bioinfo;


--
-- Name: biomarkersmeta; Type: ACL; Schema: gridreport; Owner: nicholas
--

REVOKE ALL ON TABLE biomarkersmeta FROM PUBLIC;
REVOKE ALL ON TABLE biomarkersmeta FROM nicholas;
GRANT ALL ON TABLE biomarkersmeta TO nicholas;
GRANT SELECT ON TABLE biomarkersmeta TO bioinfo;
GRANT SELECT ON TABLE biomarkersmeta TO PUBLIC;


--
-- Name: section; Type: ACL; Schema: gridreport; Owner: nicholas
--

REVOKE ALL ON TABLE section FROM PUBLIC;
REVOKE ALL ON TABLE section FROM nicholas;
GRANT ALL ON TABLE section TO nicholas;
GRANT SELECT ON TABLE section TO PUBLIC;


--
-- Name: signaturemeta; Type: ACL; Schema: gridreport; Owner: nicholas
--

REVOKE ALL ON TABLE signaturemeta FROM PUBLIC;
REVOKE ALL ON TABLE signaturemeta FROM nicholas;
GRANT ALL ON TABLE signaturemeta TO nicholas;
GRANT SELECT ON TABLE signaturemeta TO PUBLIC;


--
-- Name: subsection; Type: ACL; Schema: gridreport; Owner: nicholas
--

REVOKE ALL ON TABLE subsection FROM PUBLIC;
REVOKE ALL ON TABLE subsection FROM nicholas;
GRANT ALL ON TABLE subsection TO nicholas;
GRANT SELECT ON TABLE subsection TO PUBLIC;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA gridreport;
