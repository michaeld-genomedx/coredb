--liquibase formatted sql
--changeset mpdillon:samplesdb endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: samplesdb; Type: SCHEMA; Schema: -; Owner: administrator
--

CREATE SCHEMA samplesdb;


ALTER SCHEMA samplesdb OWNER TO administrator;

SET search_path = samplesdb, pg_catalog;

--
-- Name: _time_trial_type; Type: TYPE; Schema: samplesdb; Owner: postgres
--

CREATE TYPE _time_trial_type AS (
	a_time numeric
);


ALTER TYPE _time_trial_type OWNER TO postgres;

--
-- Name: role_type; Type: TYPE; Schema: samplesdb; Owner: administrator
--

CREATE TYPE role_type AS ENUM (
    'biostats',
    'bioinfo',
    'clinops',
    'thomas'
);


ALTER TYPE role_type OWNER TO administrator;

--
-- Name: _add(text, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _add(text, integer) RETURNS integer
    LANGUAGE sql
    AS $_$
    SELECT _add($1, $2, '')
$_$;


ALTER FUNCTION samplesdb._add(text, integer) OWNER TO postgres;

--
-- Name: _add(text, integer, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _add(text, integer, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE 'INSERT INTO __tcache__ (label, value, note) values (' ||
    quote_literal($1) || ', ' || $2 || ', ' || quote_literal(COALESCE($3, '')) || ')';
    RETURN $2;
END;
$_$;


ALTER FUNCTION samplesdb._add(text, integer, text) OWNER TO postgres;

--
-- Name: _agg(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _agg(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_agg FROM tap_funky WHERE name = $1 AND is_visible;
$_$;


ALTER FUNCTION samplesdb._agg(name) OWNER TO postgres;

--
-- Name: _agg(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _agg(name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_agg
      FROM tap_funky
     WHERE name = $1
       AND args = array_to_string($2, ',')
       AND is_visible;
$_$;


ALTER FUNCTION samplesdb._agg(name, name[]) OWNER TO postgres;

--
-- Name: _agg(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _agg(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_agg FROM tap_funky WHERE schema = $1 AND name = $2
$_$;


ALTER FUNCTION samplesdb._agg(name, name) OWNER TO postgres;

--
-- Name: _agg(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _agg(name, name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_agg
      FROM tap_funky
     WHERE schema = $1
       AND name   = $2
       AND args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._agg(name, name, name[]) OWNER TO postgres;

--
-- Name: _alike(boolean, anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _alike(boolean, anyelement, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    result ALIAS FOR $1;
    got    ALIAS FOR $2;
    rx     ALIAS FOR $3;
    descr  ALIAS FOR $4;
    output TEXT;
BEGIN
    output := ok( result, descr );
    RETURN output || CASE result WHEN TRUE THEN '' ELSE E'\n' || diag(
           '                  ' || COALESCE( quote_literal(got), 'NULL' ) ||
       E'\n   doesn''t match: ' || COALESCE( quote_literal(rx), 'NULL' )
    ) END;
END;
$_$;


ALTER FUNCTION samplesdb._alike(boolean, anyelement, text, text) OWNER TO postgres;

--
-- Name: _are(text, name[], name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _are(text, name[], name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    what    ALIAS FOR $1;
    extras  ALIAS FOR $2;
    missing ALIAS FOR $3;
    descr   ALIAS FOR $4;
    msg     TEXT    := '';
    res     BOOLEAN := TRUE;
BEGIN
    IF extras[1] IS NOT NULL THEN
        res = FALSE;
        msg := E'\n' || diag(
            '    Extra ' || what || E':\n        '
            ||  _ident_array_to_string( extras, E'\n        ' )
        );
    END IF;
    IF missing[1] IS NOT NULL THEN
        res = FALSE;
        msg := msg || E'\n' || diag(
            '    Missing ' || what || E':\n        '
            ||  _ident_array_to_string( missing, E'\n        ' )
        );
    END IF;

    RETURN ok(res, descr) || msg;
END;
$_$;


ALTER FUNCTION samplesdb._are(text, name[], name[], text) OWNER TO postgres;

--
-- Name: _areni(text, text[], text[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _areni(text, text[], text[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    what    ALIAS FOR $1;
    extras  ALIAS FOR $2;
    missing ALIAS FOR $3;
    descr   ALIAS FOR $4;
    msg     TEXT    := '';
    res     BOOLEAN := TRUE;
BEGIN
    IF extras[1] IS NOT NULL THEN
        res = FALSE;
        msg := E'\n' || diag(
            '    Extra ' || what || E':\n        '
            ||  array_to_string( extras, E'\n        ' )
        );
    END IF;
    IF missing[1] IS NOT NULL THEN
        res = FALSE;
        msg := msg || E'\n' || diag(
            '    Missing ' || what || E':\n        '
            ||  array_to_string( missing, E'\n        ' )
        );
    END IF;

    RETURN ok(res, descr) || msg;
END;
$_$;


ALTER FUNCTION samplesdb._areni(text, text[], text[], text) OWNER TO postgres;

--
-- Name: _assets_are(text, text[], text[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _assets_are(text, text[], text[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _areni(
        $1,
        ARRAY(
            SELECT UPPER($2[i]) AS thing
              FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
             ORDER BY thing
        ),
        ARRAY(
            SELECT $3[i] AS thing
              FROM generate_series(1, array_upper($3, 1)) s(i)
            EXCEPT
            SELECT UPPER($2[i])
              FROM generate_series(1, array_upper($2, 1)) s(i)
             ORDER BY thing
        ),
        $4
    );
$_$;


ALTER FUNCTION samplesdb._assets_are(text, text[], text[], text) OWNER TO postgres;

--
-- Name: _cast_exists(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cast_exists(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
       SELECT TRUE
         FROM pg_catalog.pg_cast c
        WHERE _cmp_types(castsource, $1)
          AND _cmp_types(casttarget, $2)
   );
$_$;


ALTER FUNCTION samplesdb._cast_exists(name, name) OWNER TO postgres;

--
-- Name: _cast_exists(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cast_exists(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
       SELECT TRUE
         FROM pg_catalog.pg_cast c
         JOIN pg_catalog.pg_proc p ON c.castfunc = p.oid
        WHERE _cmp_types(castsource, $1)
          AND _cmp_types(casttarget, $2)
          AND p.proname   = $3
   );
$_$;


ALTER FUNCTION samplesdb._cast_exists(name, name, name) OWNER TO postgres;

--
-- Name: _cast_exists(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cast_exists(name, name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
       SELECT TRUE
         FROM pg_catalog.pg_cast c
         JOIN pg_catalog.pg_proc p ON c.castfunc = p.oid
         JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid
        WHERE _cmp_types(castsource, $1)
          AND _cmp_types(casttarget, $2)
          AND n.nspname   = $3
          AND p.proname   = $4
   );
$_$;


ALTER FUNCTION samplesdb._cast_exists(name, name, name, name) OWNER TO postgres;

--
-- Name: _cdi(name, name, anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cdi(name, name, anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_default_is(
        $1, $2, $3,
        'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should default to '
        || COALESCE( quote_literal($3), 'NULL')
    );
$_$;


ALTER FUNCTION samplesdb._cdi(name, name, anyelement) OWNER TO postgres;

--
-- Name: _cdi(name, name, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cdi(name, name, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2 ) THEN
        RETURN fail( $4 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist' );
    END IF;

    IF NOT _has_def( $1, $2 ) THEN
        RETURN fail( $4 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || ' has no default' );
    END IF;

    RETURN _def_is(
        pg_catalog.pg_get_expr(d.adbin, d.adrelid),
        pg_catalog.format_type(a.atttypid, a.atttypmod),
        $3, $4
    )
      FROM pg_catalog.pg_class c, pg_catalog.pg_attribute a, pg_catalog.pg_attrdef d
     WHERE c.oid = a.attrelid
       AND pg_table_is_visible(c.oid)
       AND a.atthasdef
       AND a.attrelid = d.adrelid
       AND a.attnum = d.adnum
       AND c.relname = $1
       AND a.attnum > 0
       AND NOT a.attisdropped
       AND a.attname = $2;
END;
$_$;


ALTER FUNCTION samplesdb._cdi(name, name, anyelement, text) OWNER TO postgres;

--
-- Name: _cdi(name, name, name, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cdi(name, name, name, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2, $3 ) THEN
        RETURN fail( $5 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3) || ' does not exist' );
    END IF;

    IF NOT _has_def( $1, $2, $3 ) THEN
        RETURN fail( $5 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3) || ' has no default' );
    END IF;

    RETURN _def_is(
        pg_catalog.pg_get_expr(d.adbin, d.adrelid),
        pg_catalog.format_type(a.atttypid, a.atttypmod),
        $4, $5
    )
      FROM pg_catalog.pg_namespace n, pg_catalog.pg_class c, pg_catalog.pg_attribute a,
           pg_catalog.pg_attrdef d
     WHERE n.oid = c.relnamespace
       AND c.oid = a.attrelid
       AND a.atthasdef
       AND a.attrelid = d.adrelid
       AND a.attnum = d.adnum
       AND n.nspname = $1
       AND c.relname = $2
       AND a.attnum > 0
       AND NOT a.attisdropped
       AND a.attname = $3;
END;
$_$;


ALTER FUNCTION samplesdb._cdi(name, name, name, anyelement, text) OWNER TO postgres;

--
-- Name: _cexists(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cexists(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_class c
          JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
         WHERE c.relname = $1
           AND pg_catalog.pg_table_is_visible(c.oid)
           AND a.attnum > 0
           AND NOT a.attisdropped
           AND a.attname = $2
    );
$_$;


ALTER FUNCTION samplesdb._cexists(name, name) OWNER TO postgres;

--
-- Name: _cexists(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cexists(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
          JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
         WHERE n.nspname = $1
           AND c.relname = $2
           AND a.attnum > 0
           AND NOT a.attisdropped
           AND a.attname = $3
    );
$_$;


ALTER FUNCTION samplesdb._cexists(name, name, name) OWNER TO postgres;

--
-- Name: _ckeys(name, character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _ckeys(name, character) RETURNS name[]
    LANGUAGE sql
    AS $_$
    SELECT * FROM _keys($1, $2) LIMIT 1;
$_$;


ALTER FUNCTION samplesdb._ckeys(name, character) OWNER TO postgres;

--
-- Name: _ckeys(name, name, character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _ckeys(name, name, character) RETURNS name[]
    LANGUAGE sql
    AS $_$
    SELECT * FROM _keys($1, $2, $3) LIMIT 1;
$_$;


ALTER FUNCTION samplesdb._ckeys(name, name, character) OWNER TO postgres;

--
-- Name: _cleanup(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cleanup() RETURNS boolean
    LANGUAGE sql
    AS $$
    DROP SEQUENCE __tresults___numb_seq;
    DROP TABLE __tcache__;
    DROP SEQUENCE __tcache___id_seq;
    SELECT TRUE;
$$;


ALTER FUNCTION samplesdb._cleanup() OWNER TO postgres;

--
-- Name: _cmp_types(oid, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _cmp_types(oid, name) RETURNS boolean
    LANGUAGE plpgsql
    AS $_$
DECLARE
    dtype TEXT := pg_catalog.format_type($1, NULL);
BEGIN
    RETURN dtype = _quote_ident_like($2, dtype);
END;
$_$;


ALTER FUNCTION samplesdb._cmp_types(oid, name) OWNER TO postgres;

--
-- Name: _col_is_null(name, name, text, boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _col_is_null(name, name, text, boolean) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2 ) THEN
        RETURN fail( $3 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist' );
    END IF;
    RETURN ok(
        EXISTS(
            SELECT true
              FROM pg_catalog.pg_class c
              JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
             WHERE pg_catalog.pg_table_is_visible(c.oid)
               AND c.relname = $1
               AND a.attnum > 0
               AND NOT a.attisdropped
               AND a.attname    = $2
               AND a.attnotnull = $4
        ), $3
    );
END;
$_$;


ALTER FUNCTION samplesdb._col_is_null(name, name, text, boolean) OWNER TO postgres;

--
-- Name: _col_is_null(name, name, name, text, boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _col_is_null(name, name, name, text, boolean) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2, $3 ) THEN
        RETURN fail( $4 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3) || ' does not exist' );
    END IF;
    RETURN ok(
        EXISTS(
            SELECT true
              FROM pg_catalog.pg_namespace n
              JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
              JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
             WHERE n.nspname = $1
               AND c.relname = $2
               AND a.attnum  > 0
               AND NOT a.attisdropped
               AND a.attname    = $3
               AND a.attnotnull = $5
        ), $4
    );
END;
$_$;


ALTER FUNCTION samplesdb._col_is_null(name, name, name, text, boolean) OWNER TO postgres;

--
-- Name: _constraint(name, character, name[], text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _constraint(name, character, name[], text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    akey NAME[];
    keys TEXT[] := '{}';
    have TEXT;
BEGIN
    FOR akey IN SELECT * FROM _keys($1, $2) LOOP
        IF akey = $3 THEN RETURN pass($4); END IF;
        keys = keys || akey::text;
    END LOOP;
    IF array_upper(keys, 0) = 1 THEN
        have := 'No ' || $5 || ' constraints';
    ELSE
        have := array_to_string(keys, E'\n              ');
    END IF;

    RETURN fail($4) || E'\n' || diag(
             '        have: ' || have
       || E'\n        want: ' || CASE WHEN $3 IS NULL THEN 'NULL' ELSE $3::text END
    );
END;
$_$;


ALTER FUNCTION samplesdb._constraint(name, character, name[], text, text) OWNER TO postgres;

--
-- Name: _constraint(name, name, character, name[], text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _constraint(name, name, character, name[], text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    akey NAME[];
    keys TEXT[] := '{}';
    have TEXT;
BEGIN
    FOR akey IN SELECT * FROM _keys($1, $2, $3) LOOP
        IF akey = $4 THEN RETURN pass($5); END IF;
        keys = keys || akey::text;
    END LOOP;
    IF array_upper(keys, 0) = 1 THEN
        have := 'No ' || $6 || ' constraints';
    ELSE
        have := array_to_string(keys, E'\n              ');
    END IF;

    RETURN fail($5) || E'\n' || diag(
             '        have: ' || have
       || E'\n        want: ' || CASE WHEN $4 IS NULL THEN 'NULL' ELSE $4::text END
    );
END;
$_$;


ALTER FUNCTION samplesdb._constraint(name, name, character, name[], text, text) OWNER TO postgres;

--
-- Name: _contract_on(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _contract_on(text) RETURNS "char"
    LANGUAGE sql IMMUTABLE
    AS $_$
   SELECT CASE substring(LOWER($1) FROM 1 FOR 1)
          WHEN 's' THEN '1'::"char"
          WHEN 'u' THEN '2'::"char"
          WHEN 'i' THEN '3'::"char"
          WHEN 'd' THEN '4'::"char"
          ELSE          '0'::"char" END
$_$;


ALTER FUNCTION samplesdb._contract_on(text) OWNER TO postgres;

--
-- Name: _currtest(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _currtest() RETURNS integer
    LANGUAGE plpgsql
    AS $$
BEGIN
    RETURN currval('__tresults___numb_seq');
EXCEPTION
    WHEN object_not_in_prerequisite_state THEN RETURN 0;
END;
$$;


ALTER FUNCTION samplesdb._currtest() OWNER TO postgres;

--
-- Name: _db_privs(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _db_privs() RETURNS name[]
    LANGUAGE plpgsql
    AS $$
DECLARE
    pgversion INTEGER := pg_version_num();
BEGIN
    IF pgversion < 80200 THEN
        RETURN ARRAY['CREATE', 'TEMPORARY'];
    ELSE
        RETURN ARRAY['CREATE', 'CONNECT', 'TEMPORARY'];
    END IF;
END;
$$;


ALTER FUNCTION samplesdb._db_privs() OWNER TO postgres;

--
-- Name: _def_is(text, text, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _def_is(text, text, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    thing text;
BEGIN
    IF $1 ~ '^[^'']+[(]' THEN
        -- It's a functional default.
        RETURN is( $1, $3, $4 );
    END IF;

    EXECUTE 'SELECT is('
             || COALESCE($1, 'NULL' || '::' || $2) || '::' || $2 || ', '
             || COALESCE(quote_literal($3), 'NULL') || '::' || $2 || ', '
             || COALESCE(quote_literal($4), 'NULL')
    || ')' INTO thing;
    RETURN thing;
END;
$_$;


ALTER FUNCTION samplesdb._def_is(text, text, anyelement, text) OWNER TO postgres;

--
-- Name: _definer(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _definer(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_definer FROM tap_funky WHERE name = $1 AND is_visible;
$_$;


ALTER FUNCTION samplesdb._definer(name) OWNER TO postgres;

--
-- Name: _definer(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _definer(name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_definer
      FROM tap_funky
     WHERE name = $1
       AND args = array_to_string($2, ',')
       AND is_visible;
$_$;


ALTER FUNCTION samplesdb._definer(name, name[]) OWNER TO postgres;

--
-- Name: _definer(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _definer(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_definer FROM tap_funky WHERE schema = $1 AND name = $2
$_$;


ALTER FUNCTION samplesdb._definer(name, name) OWNER TO postgres;

--
-- Name: _definer(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _definer(name, name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_definer
      FROM tap_funky
     WHERE schema = $1
       AND name   = $2
       AND args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._definer(name, name, name[]) OWNER TO postgres;

--
-- Name: _dexists(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _dexists(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
   SELECT EXISTS(
       SELECT true
         FROM pg_catalog.pg_type t
        WHERE t.typname = $1
          AND pg_catalog.pg_type_is_visible(t.oid)
   );
$_$;


ALTER FUNCTION samplesdb._dexists(name) OWNER TO postgres;

--
-- Name: _dexists(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _dexists(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
   SELECT EXISTS(
       SELECT true
         FROM pg_catalog.pg_namespace n
         JOIN pg_catalog.pg_type t on n.oid = t.typnamespace
        WHERE n.nspname = $1
          AND t.typname = $2
   );
$_$;


ALTER FUNCTION samplesdb._dexists(name, name) OWNER TO postgres;

--
-- Name: _do_ne(text, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _do_ne(text, text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have    ALIAS FOR $1;
    want    ALIAS FOR $2;
    extras  TEXT[]  := '{}';
    missing TEXT[]  := '{}';
    res     BOOLEAN := TRUE;
    msg     TEXT    := '';
BEGIN
    BEGIN
        -- Find extra records.
        EXECUTE 'SELECT EXISTS ( '
             || '( SELECT * FROM ' || have || ' EXCEPT ' || $4
             || '  SELECT * FROM ' || want
             || ' ) UNION ( '
             || '  SELECT * FROM ' || want || ' EXCEPT ' || $4
             || '  SELECT * FROM ' || have
             || ' ) LIMIT 1 )' INTO res;

        -- Drop the temporary tables.
        EXECUTE 'DROP TABLE ' || have;
        EXECUTE 'DROP TABLE ' || want;
    EXCEPTION WHEN syntax_error OR datatype_mismatch THEN
        msg := E'\n' || diag(
            E'    Columns differ between queries:\n'
            || '        have: (' || _temptypes(have) || E')\n'
            || '        want: (' || _temptypes(want) || ')'
        );
        EXECUTE 'DROP TABLE ' || have;
        EXECUTE 'DROP TABLE ' || want;
        RETURN ok(FALSE, $3) || msg;
    END;

    -- Return the value from the query.
    RETURN ok(res, $3);
END;
$_$;


ALTER FUNCTION samplesdb._do_ne(text, text, text, text) OWNER TO postgres;

--
-- Name: _docomp(text, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _docomp(text, text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have    ALIAS FOR $1;
    want    ALIAS FOR $2;
    extras  TEXT[]  := '{}';
    missing TEXT[]  := '{}';
    res     BOOLEAN := TRUE;
    msg     TEXT    := '';
    rec     RECORD;
BEGIN
    BEGIN
        -- Find extra records.
        FOR rec in EXECUTE 'SELECT * FROM ' || have || ' EXCEPT ' || $4
                        || 'SELECT * FROM ' || want LOOP
            extras := extras || rec::text;
        END LOOP;

        -- Find missing records.
        FOR rec in EXECUTE 'SELECT * FROM ' || want || ' EXCEPT ' || $4
                        || 'SELECT * FROM ' || have LOOP
            missing := missing || rec::text;
        END LOOP;

        -- Drop the temporary tables.
        EXECUTE 'DROP TABLE ' || have;
        EXECUTE 'DROP TABLE ' || want;
    EXCEPTION WHEN syntax_error OR datatype_mismatch THEN
        msg := E'\n' || diag(
            E'    Columns differ between queries:\n'
            || '        have: (' || _temptypes(have) || E')\n'
            || '        want: (' || _temptypes(want) || ')'
        );
        EXECUTE 'DROP TABLE ' || have;
        EXECUTE 'DROP TABLE ' || want;
        RETURN ok(FALSE, $3) || msg;
    END;

    -- What extra records do we have?
    IF extras[1] IS NOT NULL THEN
        res := FALSE;
        msg := E'\n' || diag(
            E'    Extra records:\n        '
            ||  array_to_string( extras, E'\n        ' )
        );
    END IF;

    -- What missing records do we have?
    IF missing[1] IS NOT NULL THEN
        res := FALSE;
        msg := msg || E'\n' || diag(
            E'    Missing records:\n        '
            ||  array_to_string( missing, E'\n        ' )
        );
    END IF;

    RETURN ok(res, $3) || msg;
END;
$_$;


ALTER FUNCTION samplesdb._docomp(text, text, text, text) OWNER TO postgres;

--
-- Name: _expand_context(character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _expand_context(character) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
   SELECT CASE $1
          WHEN 'i' THEN 'implicit'
          WHEN 'a' THEN 'assignment'
          WHEN 'e' THEN 'explicit'
          ELSE          'unknown' END
$_$;


ALTER FUNCTION samplesdb._expand_context(character) OWNER TO postgres;

--
-- Name: _expand_on(character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _expand_on(character) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
   SELECT CASE $1
          WHEN '1' THEN 'SELECT'
          WHEN '2' THEN 'UPDATE'
          WHEN '3' THEN 'INSERT'
          WHEN '4' THEN 'DELETE'
          ELSE          'UNKNOWN' END
$_$;


ALTER FUNCTION samplesdb._expand_on(character) OWNER TO postgres;

--
-- Name: _expand_vol(character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _expand_vol(character) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
   SELECT CASE $1
          WHEN 'i' THEN 'IMMUTABLE'
          WHEN 's' THEN 'STABLE'
          WHEN 'v' THEN 'VOLATILE'
          ELSE          'UNKNOWN' END
$_$;


ALTER FUNCTION samplesdb._expand_vol(character) OWNER TO postgres;

--
-- Name: _extras(character, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _extras(character, name[]) RETURNS name[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT c.relname
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
         WHERE pg_catalog.pg_table_is_visible(c.oid)
           AND n.nspname <> 'pg_catalog'
           AND c.relkind = $1
           AND c.relname NOT IN ('__tcache__', 'pg_all_foreign_keys', 'tap_funky', '__tresults___numb_seq', '__tcache___id_seq')
        EXCEPT
        SELECT $2[i]
          FROM generate_series(1, array_upper($2, 1)) s(i)
    );
$_$;


ALTER FUNCTION samplesdb._extras(character, name[]) OWNER TO postgres;

--
-- Name: _extras(character, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _extras(character, name, name[]) RETURNS name[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT c.relname
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
         WHERE c.relkind = $1
           AND n.nspname = $2
           AND c.relname NOT IN('pg_all_foreign_keys', 'tap_funky', '__tresults___numb_seq', '__tcache___id_seq')
        EXCEPT
        SELECT $3[i]
          FROM generate_series(1, array_upper($3, 1)) s(i)
    );
$_$;


ALTER FUNCTION samplesdb._extras(character, name, name[]) OWNER TO postgres;

--
-- Name: _finish(integer, integer, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _finish(integer, integer, integer) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    curr_test ALIAS FOR $1;
    exp_tests INTEGER := $2;
    num_faild ALIAS FOR $3;
    plural    CHAR;
BEGIN
    plural    := CASE exp_tests WHEN 1 THEN '' ELSE 's' END;

    IF curr_test IS NULL THEN
        RAISE EXCEPTION '# No tests run!';
    END IF;

    IF exp_tests = 0 OR exp_tests IS NULL THEN
         -- No plan. Output one now.
        exp_tests = curr_test;
        RETURN NEXT '1..' || exp_tests;
    END IF;

    IF curr_test <> exp_tests THEN
        RETURN NEXT diag(
            'Looks like you planned ' || exp_tests || ' test' ||
            plural || ' but ran ' || curr_test
        );
    ELSIF num_faild > 0 THEN
        RETURN NEXT diag(
            'Looks like you failed ' || num_faild || ' test' ||
            CASE num_faild WHEN 1 THEN '' ELSE 's' END
            || ' of ' || exp_tests
        );
    ELSE

    END IF;
    RETURN;
END;
$_$;


ALTER FUNCTION samplesdb._finish(integer, integer, integer) OWNER TO postgres;

--
-- Name: _fkexists(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _fkexists(name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT TRUE
           FROM pg_all_foreign_keys
          WHERE quote_ident(fk_table_name)     = quote_ident($1)
            AND pg_catalog.pg_table_is_visible(fk_table_oid)
            AND fk_columns = $2
    );
$_$;


ALTER FUNCTION samplesdb._fkexists(name, name[]) OWNER TO postgres;

--
-- Name: _fkexists(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _fkexists(name, name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT TRUE
           FROM pg_all_foreign_keys
          WHERE fk_schema_name    = $1
            AND quote_ident(fk_table_name)     = quote_ident($2)
            AND fk_columns = $3
    );
$_$;


ALTER FUNCTION samplesdb._fkexists(name, name, name[]) OWNER TO postgres;

--
-- Name: _fprivs_are(text, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _fprivs_are(text, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_func_privs($2, $1);
BEGIN
    IF grants[1] = 'undefined_function' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Function ' || $1 || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb._fprivs_are(text, name, name[], text) OWNER TO postgres;

--
-- Name: _func_compare(name, name, boolean, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _func_compare(name, name, boolean, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $3 IS NULL
      THEN ok( FALSE, $4 ) || _nosuch($1, $2, '{}')
      ELSE ok( $3, $4 )
      END;
$_$;


ALTER FUNCTION samplesdb._func_compare(name, name, boolean, text) OWNER TO postgres;

--
-- Name: _func_compare(name, name, name[], boolean, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _func_compare(name, name, name[], boolean, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $4 IS NULL
      THEN ok( FALSE, $5 ) || _nosuch($1, $2, $3)
      ELSE ok( $4, $5 )
      END;
$_$;


ALTER FUNCTION samplesdb._func_compare(name, name, name[], boolean, text) OWNER TO postgres;

--
-- Name: _func_compare(name, name, anyelement, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _func_compare(name, name, anyelement, anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $3 IS NULL
      THEN ok( FALSE, $5 ) || _nosuch($1, $2, '{}')
      ELSE is( $3, $4, $5 )
      END;
$_$;


ALTER FUNCTION samplesdb._func_compare(name, name, anyelement, anyelement, text) OWNER TO postgres;

--
-- Name: _func_compare(name, name, name[], anyelement, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _func_compare(name, name, name[], anyelement, anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $4 IS NULL
      THEN ok( FALSE, $6 ) || _nosuch($1, $2, $3)
      ELSE is( $4, $5, $6 )
      END;
$_$;


ALTER FUNCTION samplesdb._func_compare(name, name, name[], anyelement, anyelement, text) OWNER TO postgres;

--
-- Name: _get(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get(text) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    ret integer;
BEGIN
    EXECUTE 'SELECT value FROM __tcache__ WHERE label = ' || quote_literal($1) || ' LIMIT 1' INTO ret;
    RETURN ret;
END;
$_$;


ALTER FUNCTION samplesdb._get(text) OWNER TO postgres;

--
-- Name: _get_ac_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_ac_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE
    privs  TEXT[] := ARRAY['INSERT', 'REFERENCES', 'SELECT', 'UPDATE'];
    grants TEXT[] := '{}';
BEGIN
    FOR i IN 1..array_upper(privs, 1) LOOP
        BEGIN
            IF pg_catalog.has_any_column_privilege($1, $2, privs[i]) THEN
                grants := grants || privs[i];
            END IF;
        EXCEPTION WHEN undefined_table THEN
            -- Not a valid table name.
            RETURN '{undefined_table}';
        WHEN undefined_object THEN
            -- Not a valid role.
            RETURN '{undefined_role}';
        WHEN invalid_parameter_value THEN
            -- Not a valid permission on this version of PostgreSQL; ignore;
        END;
    END LOOP;
    RETURN grants;
END;
$_$;


ALTER FUNCTION samplesdb._get_ac_privs(name, text) OWNER TO postgres;

--
-- Name: _get_col_ns_type(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_col_ns_type(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    -- Always include the namespace.
    SELECT CASE WHEN pg_catalog.pg_type_is_visible(t.oid)
                THEN quote_ident(tn.nspname) || '.'
                ELSE ''
           END || pg_catalog.format_type(a.atttypid, a.atttypmod)
      FROM pg_catalog.pg_namespace n
      JOIN pg_catalog.pg_class c      ON n.oid = c.relnamespace
      JOIN pg_catalog.pg_attribute a  ON c.oid = a.attrelid
      JOIN pg_catalog.pg_type t       ON a.atttypid = t.oid
      JOIN pg_catalog.pg_namespace tn ON t.typnamespace = tn.oid
     WHERE n.nspname = $1
       AND c.relname = $2
       AND a.attname = $3
       AND attnum    > 0
       AND NOT a.attisdropped
$_$;


ALTER FUNCTION samplesdb._get_col_ns_type(name, name, name) OWNER TO postgres;

--
-- Name: _get_col_privs(name, text, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_col_privs(name, text, name) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE
    privs  TEXT[] := ARRAY['INSERT', 'REFERENCES', 'SELECT', 'UPDATE'];
    grants TEXT[] := '{}';
BEGIN
    FOR i IN 1..array_upper(privs, 1) LOOP
        IF pg_catalog.has_column_privilege($1, $2, $3, privs[i]) THEN
            grants := grants || privs[i];
        END IF;
    END LOOP;
    RETURN grants;
EXCEPTION
    -- Not a valid column name.
    WHEN undefined_column THEN RETURN '{undefined_column}';
    -- Not a valid table name.
    WHEN undefined_table THEN RETURN '{undefined_table}';
    -- Not a valid role.
    WHEN undefined_object THEN RETURN '{undefined_role}';
END;
$_$;


ALTER FUNCTION samplesdb._get_col_privs(name, text, name) OWNER TO postgres;

--
-- Name: _get_col_type(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_col_type(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.format_type(a.atttypid, a.atttypmod)
      FROM pg_catalog.pg_attribute a
      JOIN pg_catalog.pg_class c ON  a.attrelid = c.oid
     WHERE pg_catalog.pg_table_is_visible(c.oid)
       AND c.relname = $1
       AND a.attname = $2
       AND attnum    > 0
       AND NOT a.attisdropped
$_$;


ALTER FUNCTION samplesdb._get_col_type(name, name) OWNER TO postgres;

--
-- Name: _get_col_type(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_col_type(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.format_type(a.atttypid, a.atttypmod)
      FROM pg_catalog.pg_namespace n
      JOIN pg_catalog.pg_class c     ON n.oid = c.relnamespace
      JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
     WHERE n.nspname = $1
       AND c.relname = $2
       AND a.attname = $3
       AND attnum    > 0
       AND NOT a.attisdropped
$_$;


ALTER FUNCTION samplesdb._get_col_type(name, name, name) OWNER TO postgres;

--
-- Name: _get_context(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_context(name, name) RETURNS "char"
    LANGUAGE sql
    AS $_$
   SELECT c.castcontext
     FROM pg_catalog.pg_cast c
    WHERE _cmp_types(castsource, $1)
      AND _cmp_types(casttarget, $2)
$_$;


ALTER FUNCTION samplesdb._get_context(name, name) OWNER TO postgres;

--
-- Name: _get_db_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_db_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(datdba)
      FROM pg_catalog.pg_database
     WHERE datname = $1;
$_$;


ALTER FUNCTION samplesdb._get_db_owner(name) OWNER TO postgres;

--
-- Name: _get_db_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_db_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE
    privs  TEXT[] := _db_privs();
    grants TEXT[] := '{}';
BEGIN
    FOR i IN 1..array_upper(privs, 1) LOOP
        BEGIN
            IF pg_catalog.has_database_privilege($1, $2, privs[i]) THEN
                grants := grants || privs[i];
            END IF;
        EXCEPTION WHEN invalid_catalog_name THEN
            -- Not a valid db name.
            RETURN '{invalid_catalog_name}';
        WHEN undefined_object THEN
            -- Not a valid role.
            RETURN '{undefined_role}';
        WHEN invalid_parameter_value THEN
            -- Not a valid permission on this version of PostgreSQL; ignore;
        END;
    END LOOP;
    RETURN grants;
END;
$_$;


ALTER FUNCTION samplesdb._get_db_privs(name, text) OWNER TO postgres;

--
-- Name: _get_dtype(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_dtype(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.format_type(t.oid, t.typtypmod)
      FROM pg_catalog.pg_type d
      JOIN pg_catalog.pg_type t  ON d.typbasetype  = t.oid
     WHERE d.typisdefined
       AND pg_catalog.pg_type_is_visible(d.oid)
       AND d.typname = LOWER($1)
       AND d.typtype = 'd'
$_$;


ALTER FUNCTION samplesdb._get_dtype(name) OWNER TO postgres;

--
-- Name: _get_dtype(name, text, boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_dtype(name, text, boolean) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $3 AND pg_catalog.pg_type_is_visible(t.oid)
                THEN quote_ident(tn.nspname) || '.'
                ELSE ''
            END || pg_catalog.format_type(t.oid, t.typtypmod)
      FROM pg_catalog.pg_type d
      JOIN pg_catalog.pg_namespace dn ON d.typnamespace = dn.oid
      JOIN pg_catalog.pg_type t       ON d.typbasetype  = t.oid
      JOIN pg_catalog.pg_namespace tn ON t.typnamespace = tn.oid
     WHERE d.typisdefined
       AND dn.nspname = $1
       AND d.typname  = LOWER($2)
       AND d.typtype  = 'd'
$_$;


ALTER FUNCTION samplesdb._get_dtype(name, text, boolean) OWNER TO postgres;

--
-- Name: _get_fdw_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_fdw_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF pg_catalog.has_foreign_data_wrapper_privilege($1, $2, 'USAGE') THEN
        RETURN '{USAGE}';
    ELSE
        RETURN '{}';
    END IF;
EXCEPTION WHEN undefined_object THEN
    -- Same error code for unknown user or fdw. So figure out which.
    RETURN CASE WHEN SQLERRM LIKE '%' || $1 || '%' THEN
        '{undefined_role}'
    ELSE
        '{undefined_fdw}'
    END;
END;
$_$;


ALTER FUNCTION samplesdb._get_fdw_privs(name, text) OWNER TO postgres;

--
-- Name: _get_func_owner(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_func_owner(name, name[]) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT owner
      FROM tap_funky
     WHERE name = $1
       AND args = array_to_string($2, ',')
       AND is_visible
$_$;


ALTER FUNCTION samplesdb._get_func_owner(name, name[]) OWNER TO postgres;

--
-- Name: _get_func_owner(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_func_owner(name, name, name[]) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT owner
      FROM tap_funky
     WHERE schema = $1
       AND name   = $2
       AND args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._get_func_owner(name, name, name[]) OWNER TO postgres;

--
-- Name: _get_func_privs(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_func_privs(text, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF pg_catalog.has_function_privilege($1, $2, 'EXECUTE') THEN
        RETURN '{EXECUTE}';
    ELSE
        RETURN '{}';
    END IF;
EXCEPTION
    -- Not a valid func name.
    WHEN undefined_function THEN RETURN '{undefined_function}';
    -- Not a valid role.
    WHEN undefined_object   THEN RETURN '{undefined_role}';
END;
$_$;


ALTER FUNCTION samplesdb._get_func_privs(text, text) OWNER TO postgres;

--
-- Name: _get_index_owner(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_index_owner(name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(ci.relowner)
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
     WHERE ct.relname = $1
       AND ci.relname = $2
       AND pg_catalog.pg_table_is_visible(ct.oid);
$_$;


ALTER FUNCTION samplesdb._get_index_owner(name, name) OWNER TO postgres;

--
-- Name: _get_index_owner(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_index_owner(name, name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(ci.relowner)
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
     WHERE n.nspname  = $1
       AND ct.relname = $2
       AND ci.relname = $3;
$_$;


ALTER FUNCTION samplesdb._get_index_owner(name, name, name) OWNER TO postgres;

--
-- Name: _get_lang_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_lang_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF pg_catalog.has_language_privilege($1, $2, 'USAGE') THEN
        RETURN '{USAGE}';
    ELSE
        RETURN '{}';
    END IF;
EXCEPTION WHEN undefined_object THEN
    -- Same error code for unknown user or language. So figure out which.
    RETURN CASE WHEN SQLERRM LIKE '%' || $1 || '%' THEN
        '{undefined_role}'
    ELSE
        '{undefined_language}'
    END;
END;
$_$;


ALTER FUNCTION samplesdb._get_lang_privs(name, text) OWNER TO postgres;

--
-- Name: _get_language_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_language_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(lanowner)
      FROM pg_catalog.pg_language
     WHERE lanname = $1;
$_$;


ALTER FUNCTION samplesdb._get_language_owner(name) OWNER TO postgres;

--
-- Name: _get_latest(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_latest(text) RETURNS integer[]
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    ret integer[];
BEGIN
    EXECUTE 'SELECT ARRAY[id, value] FROM __tcache__ WHERE label = ' ||
    quote_literal($1) || ' AND id = (SELECT MAX(id) FROM __tcache__ WHERE label = ' ||
    quote_literal($1) || ') LIMIT 1' INTO ret;
    RETURN ret;
EXCEPTION WHEN undefined_table THEN
   RAISE EXCEPTION 'You tried to run a test without a plan! Gotta have a plan';
END;
$_$;


ALTER FUNCTION samplesdb._get_latest(text) OWNER TO postgres;

--
-- Name: _get_latest(text, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_latest(text, integer) RETURNS integer
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    ret integer;
BEGIN
    EXECUTE 'SELECT MAX(id) FROM __tcache__ WHERE label = ' ||
    quote_literal($1) || ' AND value = ' || $2 INTO ret;
    RETURN ret;
END;
$_$;


ALTER FUNCTION samplesdb._get_latest(text, integer) OWNER TO postgres;

--
-- Name: _get_note(integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_note(integer) RETURNS text
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    ret text;
BEGIN
    EXECUTE 'SELECT note FROM __tcache__ WHERE id = ' || $1 || ' LIMIT 1' INTO ret;
    RETURN ret;
END;
$_$;


ALTER FUNCTION samplesdb._get_note(integer) OWNER TO postgres;

--
-- Name: _get_note(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_note(text) RETURNS text
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    ret text;
BEGIN
    EXECUTE 'SELECT note FROM __tcache__ WHERE label = ' || quote_literal($1) || ' LIMIT 1' INTO ret;
    RETURN ret;
END;
$_$;


ALTER FUNCTION samplesdb._get_note(text) OWNER TO postgres;

--
-- Name: _get_opclass_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_opclass_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(opcowner)
      FROM pg_catalog.pg_opclass
     WHERE opcname = $1
       AND pg_catalog.pg_opclass_is_visible(oid);
$_$;


ALTER FUNCTION samplesdb._get_opclass_owner(name) OWNER TO postgres;

--
-- Name: _get_opclass_owner(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_opclass_owner(name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(opcowner)
      FROM pg_catalog.pg_opclass oc
      JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid
     WHERE n.nspname = $1
       AND opcname   = $2;
$_$;


ALTER FUNCTION samplesdb._get_opclass_owner(name, name) OWNER TO postgres;

--
-- Name: _get_rel_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_rel_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(c.relowner)
      FROM pg_catalog.pg_class c
     WHERE c.relname = $1
       AND pg_catalog.pg_table_is_visible(c.oid)
$_$;


ALTER FUNCTION samplesdb._get_rel_owner(name) OWNER TO postgres;

--
-- Name: _get_rel_owner(character, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_rel_owner(character, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(c.relowner)
      FROM pg_catalog.pg_class c
     WHERE c.relkind = $1
       AND c.relname = $2
       AND pg_catalog.pg_table_is_visible(c.oid)
$_$;


ALTER FUNCTION samplesdb._get_rel_owner(character, name) OWNER TO postgres;

--
-- Name: _get_rel_owner(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_rel_owner(name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(c.relowner)
      FROM pg_catalog.pg_class c
      JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
     WHERE n.nspname = $1
       AND c.relname = $2
$_$;


ALTER FUNCTION samplesdb._get_rel_owner(name, name) OWNER TO postgres;

--
-- Name: _get_rel_owner(character, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_rel_owner(character, name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(c.relowner)
      FROM pg_catalog.pg_class c
      JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
     WHERE c.relkind = $1
       AND n.nspname = $2
       AND c.relname = $3
$_$;


ALTER FUNCTION samplesdb._get_rel_owner(character, name, name) OWNER TO postgres;

--
-- Name: _get_schema_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_schema_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(nspowner)
      FROM pg_catalog.pg_namespace
     WHERE nspname = $1;
$_$;


ALTER FUNCTION samplesdb._get_schema_owner(name) OWNER TO postgres;

--
-- Name: _get_schema_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_schema_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE
    privs  TEXT[] := ARRAY['CREATE', 'USAGE'];
    grants TEXT[] := '{}';
BEGIN
    FOR i IN 1..array_upper(privs, 1) LOOP
        IF pg_catalog.has_schema_privilege($1, $2, privs[i]) THEN
            grants := grants || privs[i];
        END IF;
    END LOOP;
    RETURN grants;
EXCEPTION
    -- Not a valid schema name.
    WHEN invalid_schema_name THEN RETURN '{invalid_schema_name}';
    -- Not a valid role.
    WHEN undefined_object   THEN RETURN '{undefined_role}';
END;
$_$;


ALTER FUNCTION samplesdb._get_schema_privs(name, text) OWNER TO postgres;

--
-- Name: _get_sequence_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_sequence_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE
    privs  TEXT[] := ARRAY['SELECT', 'UPDATE', 'USAGE'];
    grants TEXT[] := '{}';
BEGIN
    FOR i IN 1..array_upper(privs, 1) LOOP
        BEGIN
            IF pg_catalog.has_sequence_privilege($1, $2, privs[i]) THEN
                grants := grants || privs[i];
            END IF;
        EXCEPTION WHEN undefined_table THEN
            -- Not a valid sequence name.
            RETURN '{undefined_table}';
        WHEN undefined_object THEN
            -- Not a valid role.
            RETURN '{undefined_role}';
        WHEN invalid_parameter_value THEN
            -- Not a valid permission on this version of PostgreSQL; ignore;
        END;
    END LOOP;
    RETURN grants;
END;
$_$;


ALTER FUNCTION samplesdb._get_sequence_privs(name, text) OWNER TO postgres;

--
-- Name: _get_server_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_server_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF pg_catalog.has_server_privilege($1, $2, 'USAGE') THEN
        RETURN '{USAGE}';
    ELSE
        RETURN '{}';
    END IF;
EXCEPTION WHEN undefined_object THEN
    -- Same error code for unknown user or server. So figure out which.
    RETURN CASE WHEN SQLERRM LIKE '%' || $1 || '%' THEN
        '{undefined_role}'
    ELSE
        '{undefined_server}'
    END;
END;
$_$;


ALTER FUNCTION samplesdb._get_server_privs(name, text) OWNER TO postgres;

--
-- Name: _get_table_privs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_table_privs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
DECLARE
    privs  TEXT[] := _table_privs();
    grants TEXT[] := '{}';
BEGIN
    FOR i IN 1..array_upper(privs, 1) LOOP
        BEGIN
            IF pg_catalog.has_table_privilege($1, $2, privs[i]) THEN
                grants := grants || privs[i];
            END IF;
        EXCEPTION WHEN undefined_table THEN
            -- Not a valid table name.
            RETURN '{undefined_table}';
        WHEN undefined_object THEN
            -- Not a valid role.
            RETURN '{undefined_role}';
        WHEN invalid_parameter_value THEN
            -- Not a valid permission on this version of PostgreSQL; ignore;
        END;
    END LOOP;
    RETURN grants;
END;
$_$;


ALTER FUNCTION samplesdb._get_table_privs(name, text) OWNER TO postgres;

--
-- Name: _get_tablespace_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_tablespace_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(spcowner)
      FROM pg_catalog.pg_tablespace
     WHERE spcname = $1;
$_$;


ALTER FUNCTION samplesdb._get_tablespace_owner(name) OWNER TO postgres;

--
-- Name: _get_tablespaceprivs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_tablespaceprivs(name, text) RETURNS text[]
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF pg_catalog.has_tablespace_privilege($1, $2, 'CREATE') THEN
        RETURN '{CREATE}';
    ELSE
        RETURN '{}';
    END IF;
EXCEPTION WHEN undefined_object THEN
    -- Same error code for unknown user or tablespace. So figure out which.
    RETURN CASE WHEN SQLERRM LIKE '%' || $1 || '%' THEN
        '{undefined_role}'
    ELSE
        '{undefined_tablespace}'
    END;
END;
$_$;


ALTER FUNCTION samplesdb._get_tablespaceprivs(name, text) OWNER TO postgres;

--
-- Name: _get_type_owner(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_type_owner(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(typowner)
      FROM pg_catalog.pg_type
     WHERE typname = $1
       AND pg_catalog.pg_type_is_visible(oid)
$_$;


ALTER FUNCTION samplesdb._get_type_owner(name) OWNER TO postgres;

--
-- Name: _get_type_owner(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _get_type_owner(name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT pg_catalog.pg_get_userbyid(t.typowner)
      FROM pg_catalog.pg_type t
      JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
     WHERE n.nspname = $1
       AND t.typname = $2
$_$;


ALTER FUNCTION samplesdb._get_type_owner(name, name) OWNER TO postgres;

--
-- Name: _got_func(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _got_func(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS( SELECT TRUE FROM tap_funky WHERE name = $1 AND is_visible);
$_$;


ALTER FUNCTION samplesdb._got_func(name) OWNER TO postgres;

--
-- Name: _got_func(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _got_func(name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT TRUE
          FROM tap_funky
         WHERE name = $1
           AND args = array_to_string($2, ',')
           AND is_visible
    );
$_$;


ALTER FUNCTION samplesdb._got_func(name, name[]) OWNER TO postgres;

--
-- Name: _got_func(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _got_func(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS( SELECT TRUE FROM tap_funky WHERE schema = $1 AND name = $2 );
$_$;


ALTER FUNCTION samplesdb._got_func(name, name) OWNER TO postgres;

--
-- Name: _got_func(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _got_func(name, name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT TRUE
          FROM tap_funky
         WHERE schema = $1
           AND name   = $2
           AND args   = array_to_string($3, ',')
    );
$_$;


ALTER FUNCTION samplesdb._got_func(name, name, name[]) OWNER TO postgres;

--
-- Name: _grolist(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _grolist(name) RETURNS oid[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT member
          FROM pg_catalog.pg_auth_members m
          JOIN pg_catalog.pg_roles r ON m.roleid = r.oid
         WHERE r.rolname =  $1
    );
$_$;


ALTER FUNCTION samplesdb._grolist(name) OWNER TO postgres;

--
-- Name: _has_def(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_def(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT a.atthasdef
      FROM pg_catalog.pg_class c
      JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
     WHERE c.relname = $1
       AND a.attnum > 0
       AND NOT a.attisdropped
       AND a.attname = $2
       AND pg_catalog.pg_table_is_visible(c.oid)
$_$;


ALTER FUNCTION samplesdb._has_def(name, name) OWNER TO postgres;

--
-- Name: _has_def(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_def(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT a.atthasdef
      FROM pg_catalog.pg_namespace n
      JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
      JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
     WHERE n.nspname = $1
       AND c.relname = $2
       AND a.attnum > 0
       AND NOT a.attisdropped
       AND a.attname = $3
$_$;


ALTER FUNCTION samplesdb._has_def(name, name, name) OWNER TO postgres;

--
-- Name: _has_group(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_group(name) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_group
         WHERE groname = $1
    );
$_$;


ALTER FUNCTION samplesdb._has_group(name) OWNER TO postgres;

--
-- Name: _has_role(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_role(name) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_roles
         WHERE rolname = $1
    );
$_$;


ALTER FUNCTION samplesdb._has_role(name) OWNER TO postgres;

--
-- Name: _has_type(name, character[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_type(name, character[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_type t
         WHERE t.typisdefined
           AND pg_catalog.pg_type_is_visible(t.oid)
           AND t.typname = $1
           AND t.typtype = ANY( COALESCE($2, ARRAY['b', 'c', 'd', 'p', 'e']) )
    );
$_$;


ALTER FUNCTION samplesdb._has_type(name, character[]) OWNER TO postgres;

--
-- Name: _has_type(name, name, character[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_type(name, name, character[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_type t
          JOIN pg_catalog.pg_namespace n ON t.typnamespace = n.oid
         WHERE t.typisdefined
           AND n.nspname = $1
           AND t.typname = $2
           AND t.typtype = ANY( COALESCE($3, ARRAY['b', 'c', 'd', 'p', 'e']) )
    );
$_$;


ALTER FUNCTION samplesdb._has_type(name, name, character[]) OWNER TO postgres;

--
-- Name: _has_user(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _has_user(name) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$
    SELECT EXISTS( SELECT true FROM pg_catalog.pg_user WHERE usename = $1);
$_$;


ALTER FUNCTION samplesdb._has_user(name) OWNER TO postgres;

--
-- Name: _hasc(name, character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _hasc(name, character) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
            SELECT true
              FROM pg_catalog.pg_class c
              JOIN pg_catalog.pg_constraint x ON c.oid = x.conrelid
             WHERE c.relhaspkey = true
               AND pg_table_is_visible(c.oid)
               AND c.relname = $1
               AND x.contype = $2
    );
$_$;


ALTER FUNCTION samplesdb._hasc(name, character) OWNER TO postgres;

--
-- Name: _hasc(name, name, character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _hasc(name, name, character) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
            SELECT true
              FROM pg_catalog.pg_namespace n
              JOIN pg_catalog.pg_class c      ON c.relnamespace = n.oid
              JOIN pg_catalog.pg_constraint x ON c.oid = x.conrelid
             WHERE c.relhaspkey = true
               AND n.nspname = $1
               AND c.relname = $2
               AND x.contype = $3
    );
$_$;


ALTER FUNCTION samplesdb._hasc(name, name, character) OWNER TO postgres;

--
-- Name: _have_index(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _have_index(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
    SELECT TRUE
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
     WHERE ct.relname = $1
       AND ci.relname = $2
       AND pg_catalog.pg_table_is_visible(ct.oid)
    );
$_$;


ALTER FUNCTION samplesdb._have_index(name, name) OWNER TO postgres;

--
-- Name: _have_index(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _have_index(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
    SELECT TRUE
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
     WHERE n.nspname  = $1
       AND ct.relname = $2
       AND ci.relname = $3
    );
$_$;


ALTER FUNCTION samplesdb._have_index(name, name, name) OWNER TO postgres;

--
-- Name: _ident_array_to_string(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _ident_array_to_string(name[], text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
    SELECT array_to_string(ARRAY(
        SELECT quote_ident($1[i])
          FROM generate_series(1, array_upper($1, 1)) s(i)
         ORDER BY i
    ), $2);
$_$;


ALTER FUNCTION samplesdb._ident_array_to_string(name[], text) OWNER TO postgres;

--
-- Name: _ikeys(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _ikeys(name, name) RETURNS text[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT pg_catalog.pg_get_indexdef( ci.oid, s.i + 1, false)
          FROM pg_catalog.pg_index x
          JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
          JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
          JOIN generate_series(0, current_setting('max_index_keys')::int - 1) s(i)
            ON x.indkey[s.i] IS NOT NULL
         WHERE ct.relname = $1
           AND ci.relname = $2
           AND pg_catalog.pg_table_is_visible(ct.oid)
         ORDER BY s.i
    );
$_$;


ALTER FUNCTION samplesdb._ikeys(name, name) OWNER TO postgres;

--
-- Name: _ikeys(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _ikeys(name, name, name) RETURNS text[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT pg_catalog.pg_get_indexdef( ci.oid, s.i + 1, false)
          FROM pg_catalog.pg_index x
          JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
          JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
          JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
          JOIN generate_series(0, current_setting('max_index_keys')::int - 1) s(i)
            ON x.indkey[s.i] IS NOT NULL
         WHERE ct.relname = $2
           AND ci.relname = $3
           AND n.nspname  = $1
         ORDER BY s.i
    );
$_$;


ALTER FUNCTION samplesdb._ikeys(name, name, name) OWNER TO postgres;

--
-- Name: _is_instead(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _is_instead(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT r.is_instead
      FROM pg_catalog.pg_rewrite r
      JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
     WHERE r.rulename = $2
       AND c.relname  = $1
       AND pg_catalog.pg_table_is_visible(c.oid)
$_$;


ALTER FUNCTION samplesdb._is_instead(name, name) OWNER TO postgres;

--
-- Name: _is_instead(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _is_instead(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT r.is_instead
      FROM pg_catalog.pg_rewrite r
      JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
      JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid
     WHERE r.rulename = $3
       AND c.relname  = $2
       AND n.nspname  = $1
$_$;


ALTER FUNCTION samplesdb._is_instead(name, name, name) OWNER TO postgres;

--
-- Name: _is_schema(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _is_schema(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_namespace
          WHERE nspname = $1
    );
$_$;


ALTER FUNCTION samplesdb._is_schema(name) OWNER TO postgres;

--
-- Name: _is_super(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _is_super(name) RETURNS boolean
    LANGUAGE sql STRICT
    AS $_$
    SELECT rolsuper
      FROM pg_catalog.pg_roles
     WHERE rolname = $1
$_$;


ALTER FUNCTION samplesdb._is_super(name) OWNER TO postgres;

--
-- Name: _is_trusted(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _is_trusted(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT lanpltrusted FROM pg_catalog.pg_language WHERE lanname = $1;
$_$;


ALTER FUNCTION samplesdb._is_trusted(name) OWNER TO postgres;

--
-- Name: _is_verbose(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _is_verbose() RETURNS boolean
    LANGUAGE sql STABLE
    AS $$
    SELECT current_setting('client_min_messages') NOT IN (
        'warning', 'error', 'fatal', 'panic'
    );
$$;


ALTER FUNCTION samplesdb._is_verbose() OWNER TO postgres;

--
-- Name: _keys(name, character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _keys(name, character) RETURNS SETOF name[]
    LANGUAGE sql
    AS $_$
    SELECT _pg_sv_column_array(x.conrelid,x.conkey)
      FROM pg_catalog.pg_class c
      JOIN pg_catalog.pg_constraint x  ON c.oid = x.conrelid
       AND c.relname = $1
       AND x.contype = $2
     WHERE pg_catalog.pg_table_is_visible(c.oid)
$_$;


ALTER FUNCTION samplesdb._keys(name, character) OWNER TO postgres;

--
-- Name: _keys(name, name, character); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _keys(name, name, character) RETURNS SETOF name[]
    LANGUAGE sql
    AS $_$
    SELECT _pg_sv_column_array(x.conrelid,x.conkey)
      FROM pg_catalog.pg_namespace n
      JOIN pg_catalog.pg_class c       ON n.oid = c.relnamespace
      JOIN pg_catalog.pg_constraint x  ON c.oid = x.conrelid
     WHERE n.nspname = $1
       AND c.relname = $2
       AND x.contype = $3
$_$;


ALTER FUNCTION samplesdb._keys(name, name, character) OWNER TO postgres;

--
-- Name: _lang(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _lang(name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT l.lanname
      FROM tap_funky f
      JOIN pg_catalog.pg_language l ON f.langoid = l.oid
     WHERE f.name = $1
       AND f.is_visible;
$_$;


ALTER FUNCTION samplesdb._lang(name) OWNER TO postgres;

--
-- Name: _lang(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _lang(name, name[]) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT l.lanname
      FROM tap_funky f
      JOIN pg_catalog.pg_language l ON f.langoid = l.oid
     WHERE f.name = $1
       AND f.args = array_to_string($2, ',')
       AND f.is_visible;
$_$;


ALTER FUNCTION samplesdb._lang(name, name[]) OWNER TO postgres;

--
-- Name: _lang(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _lang(name, name) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT l.lanname
      FROM tap_funky f
      JOIN pg_catalog.pg_language l ON f.langoid = l.oid
     WHERE f.schema = $1
       and f.name   = $2
$_$;


ALTER FUNCTION samplesdb._lang(name, name) OWNER TO postgres;

--
-- Name: _lang(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _lang(name, name, name[]) RETURNS name
    LANGUAGE sql
    AS $_$
    SELECT l.lanname
      FROM tap_funky f
      JOIN pg_catalog.pg_language l ON f.langoid = l.oid
     WHERE f.schema = $1
       and f.name   = $2
       AND f.args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._lang(name, name, name[]) OWNER TO postgres;

--
-- Name: _missing(character, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _missing(character, name[]) RETURNS name[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT $2[i]
          FROM generate_series(1, array_upper($2, 1)) s(i)
        EXCEPT
        SELECT c.relname
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
         WHERE pg_catalog.pg_table_is_visible(c.oid)
           AND n.nspname NOT IN ('pg_catalog', 'information_schema')
           AND c.relkind = $1
    );
$_$;


ALTER FUNCTION samplesdb._missing(character, name[]) OWNER TO postgres;

--
-- Name: _missing(character, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _missing(character, name, name[]) RETURNS name[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT $3[i]
          FROM generate_series(1, array_upper($3, 1)) s(i)
        EXCEPT
        SELECT c.relname
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
         WHERE c.relkind = $1
           AND n.nspname = $2
    );
$_$;


ALTER FUNCTION samplesdb._missing(character, name, name[]) OWNER TO postgres;

--
-- Name: _nosuch(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _nosuch(name, name, name[]) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
    SELECT E'\n' || diag(
        '    Function '
          || CASE WHEN $1 IS NOT NULL THEN quote_ident($1) || '.' ELSE '' END
          || quote_ident($2) || '('
          || array_to_string($3, ', ') || ') does not exist'
    );
$_$;


ALTER FUNCTION samplesdb._nosuch(name, name, name[]) OWNER TO postgres;

--
-- Name: _op_exists(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _op_exists(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
       SELECT TRUE
         FROM pg_catalog.pg_operator o
        WHERE pg_catalog.pg_operator_is_visible(o.oid)
          AND o.oprname = $2
          AND CASE o.oprkind WHEN 'l' THEN $1 IS NULL
              ELSE _cmp_types(o.oprleft, $1) END
          AND CASE o.oprkind WHEN 'r' THEN $3 IS NULL
              ELSE _cmp_types(o.oprright, $3) END
   );
$_$;


ALTER FUNCTION samplesdb._op_exists(name, name, name) OWNER TO postgres;

--
-- Name: _op_exists(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _op_exists(name, name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
       SELECT TRUE
         FROM pg_catalog.pg_operator o
        WHERE pg_catalog.pg_operator_is_visible(o.oid)
          AND o.oprname = $2
          AND CASE o.oprkind WHEN 'l' THEN $1 IS NULL
              ELSE _cmp_types(o.oprleft, $1) END
          AND CASE o.oprkind WHEN 'r' THEN $3 IS NULL
              ELSE _cmp_types(o.oprright, $3) END
          AND _cmp_types(o.oprresult, $4)
   );
$_$;


ALTER FUNCTION samplesdb._op_exists(name, name, name, name) OWNER TO postgres;

--
-- Name: _op_exists(name, name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _op_exists(name, name, name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
       SELECT TRUE
         FROM pg_catalog.pg_operator o
         JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid
        WHERE n.nspname = $2
          AND o.oprname = $3
          AND CASE o.oprkind WHEN 'l' THEN $1 IS NULL
              ELSE _cmp_types(o.oprleft, $1) END
          AND CASE o.oprkind WHEN 'r' THEN $4 IS NULL
              ELSE _cmp_types(o.oprright, $4) END
          AND _cmp_types(o.oprresult, $5)
   );
$_$;


ALTER FUNCTION samplesdb._op_exists(name, name, name, name, name) OWNER TO postgres;

--
-- Name: _opc_exists(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _opc_exists(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
        SELECT TRUE
          FROM pg_catalog.pg_opclass oc
         WHERE oc.opcname = $1
           AND pg_opclass_is_visible(oid)
    );
$_$;


ALTER FUNCTION samplesdb._opc_exists(name) OWNER TO postgres;

--
-- Name: _opc_exists(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _opc_exists(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS (
        SELECT TRUE
          FROM pg_catalog.pg_opclass oc
          JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid
         WHERE n.nspname  = $1
           AND oc.opcname = $2
    );
$_$;


ALTER FUNCTION samplesdb._opc_exists(name, name) OWNER TO postgres;

--
-- Name: _pg_sv_column_array(oid, smallint[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _pg_sv_column_array(oid, smallint[]) RETURNS name[]
    LANGUAGE sql STABLE
    AS $_$
    SELECT ARRAY(
        SELECT a.attname
          FROM pg_catalog.pg_attribute a
          JOIN generate_series(1, array_upper($2, 1)) s(i) ON a.attnum = $2[i]
         WHERE attrelid = $1
         ORDER BY i
    )
$_$;


ALTER FUNCTION samplesdb._pg_sv_column_array(oid, smallint[]) OWNER TO postgres;

--
-- Name: _pg_sv_table_accessible(oid, oid); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _pg_sv_table_accessible(oid, oid) RETURNS boolean
    LANGUAGE sql IMMUTABLE STRICT
    AS $_$
    SELECT CASE WHEN has_schema_privilege($1, 'USAGE') THEN (
                  has_table_privilege($2, 'SELECT')
               OR has_table_privilege($2, 'INSERT')
               or has_table_privilege($2, 'UPDATE')
               OR has_table_privilege($2, 'DELETE')
               OR has_table_privilege($2, 'RULE')
               OR has_table_privilege($2, 'REFERENCES')
               OR has_table_privilege($2, 'TRIGGER')
           ) ELSE FALSE
    END;
$_$;


ALTER FUNCTION samplesdb._pg_sv_table_accessible(oid, oid) OWNER TO postgres;

--
-- Name: _pg_sv_type_array(oid[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _pg_sv_type_array(oid[]) RETURNS name[]
    LANGUAGE sql STABLE
    AS $_$
    SELECT ARRAY(
        SELECT t.typname
          FROM pg_catalog.pg_type t
          JOIN generate_series(1, array_upper($1, 1)) s(i) ON t.oid = $1[i]
         ORDER BY i
    )
$_$;


ALTER FUNCTION samplesdb._pg_sv_type_array(oid[]) OWNER TO postgres;

--
-- Name: _query(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _query(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE
        WHEN $1 LIKE '"%' OR $1 !~ '[[:space:]]' THEN 'EXECUTE ' || $1
        ELSE $1
    END;
$_$;


ALTER FUNCTION samplesdb._query(text) OWNER TO postgres;

--
-- Name: _quote_ident_like(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _quote_ident_like(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have    TEXT;
    pcision TEXT;
BEGIN
    -- Just return it if rhs isn't quoted.
    IF $2 !~ '"' THEN RETURN $1; END IF;

    -- If it's quoted ident without precision, return it quoted.
    IF $2 ~ '"$' THEN RETURN quote_ident($1); END IF;

    pcision := substring($1 FROM '[(][^")]+[)]$');

    -- Just quote it if thre is no precision.
    if pcision IS NULL THEN RETURN quote_ident($1); END IF;

    -- Quote the non-precision part and concatenate with precision.
    RETURN quote_ident(substring($1 FOR char_length($1) - char_length(pcision)))
        || pcision;
END;
$_$;


ALTER FUNCTION samplesdb._quote_ident_like(text, text) OWNER TO postgres;

--
-- Name: _refine_vol(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _refine_vol(text) RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $_$
    SELECT _expand_vol(substring(LOWER($1) FROM 1 FOR 1)::char);
$_$;


ALTER FUNCTION samplesdb._refine_vol(text) OWNER TO postgres;

--
-- Name: _relcomp(text, anyarray, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relcomp(text, anyarray, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _docomp(
        _temptable( $1, '__taphave__' ),
        _temptable( $2, '__tapwant__' ),
        $3, $4
    );
$_$;


ALTER FUNCTION samplesdb._relcomp(text, anyarray, text, text) OWNER TO postgres;

--
-- Name: _relcomp(text, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relcomp(text, text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _docomp(
        _temptable( $1, '__taphave__' ),
        _temptable( $2, '__tapwant__' ),
        $3, $4
    );
$_$;


ALTER FUNCTION samplesdb._relcomp(text, text, text, text) OWNER TO postgres;

--
-- Name: _relcomp(text, text, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relcomp(text, text, text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have    TEXT    := _temptable( $1, '__taphave__' );
    want    TEXT    := _temptable( $2, '__tapwant__' );
    results TEXT[]  := '{}';
    res     BOOLEAN := TRUE;
    msg     TEXT    := '';
    rec     RECORD;
BEGIN
    BEGIN
        -- Find relevant records.
        FOR rec in EXECUTE 'SELECT * FROM ' || want || ' ' || $4
                       || ' SELECT * FROM ' || have LOOP
            results := results || rec::text;
        END LOOP;

        -- Drop the temporary tables.
        EXECUTE 'DROP TABLE ' || have;
        EXECUTE 'DROP TABLE ' || want;
    EXCEPTION WHEN syntax_error OR datatype_mismatch THEN
        msg := E'\n' || diag(
            E'    Columns differ between queries:\n'
            || '        have: (' || _temptypes(have) || E')\n'
            || '        want: (' || _temptypes(want) || ')'
        );
        EXECUTE 'DROP TABLE ' || have;
        EXECUTE 'DROP TABLE ' || want;
        RETURN ok(FALSE, $3) || msg;
    END;

    -- What records do we have?
    IF results[1] IS NOT NULL THEN
        res := FALSE;
        msg := msg || E'\n' || diag(
            '    ' || $5 || E' records:\n        '
            ||  array_to_string( results, E'\n        ' )
        );
    END IF;

    RETURN ok(res, $3) || msg;
END;
$_$;


ALTER FUNCTION samplesdb._relcomp(text, text, text, text, text) OWNER TO postgres;

--
-- Name: _relexists(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relexists(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_class c
         WHERE pg_catalog.pg_table_is_visible(c.oid)
           AND c.relname = $1
    );
$_$;


ALTER FUNCTION samplesdb._relexists(name) OWNER TO postgres;

--
-- Name: _relexists(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relexists(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
         WHERE n.nspname = $1
           AND c.relname = $2
    );
$_$;


ALTER FUNCTION samplesdb._relexists(name, name) OWNER TO postgres;

--
-- Name: _relne(text, anyarray, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relne(text, anyarray, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _do_ne(
        _temptable( $1, '__taphave__' ),
        _temptable( $2, '__tapwant__' ),
        $3, $4
    );
$_$;


ALTER FUNCTION samplesdb._relne(text, anyarray, text, text) OWNER TO postgres;

--
-- Name: _relne(text, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _relne(text, text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _do_ne(
        _temptable( $1, '__taphave__' ),
        _temptable( $2, '__tapwant__' ),
        $3, $4
    );
$_$;


ALTER FUNCTION samplesdb._relne(text, text, text, text) OWNER TO postgres;

--
-- Name: _returns(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _returns(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT returns FROM tap_funky WHERE name = $1 AND is_visible;
$_$;


ALTER FUNCTION samplesdb._returns(name) OWNER TO postgres;

--
-- Name: _returns(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _returns(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT returns
      FROM tap_funky
     WHERE name = $1
       AND args = array_to_string($2, ',')
       AND is_visible;
$_$;


ALTER FUNCTION samplesdb._returns(name, name[]) OWNER TO postgres;

--
-- Name: _returns(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _returns(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT returns FROM tap_funky WHERE schema = $1 AND name = $2
$_$;


ALTER FUNCTION samplesdb._returns(name, name) OWNER TO postgres;

--
-- Name: _returns(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _returns(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT returns
      FROM tap_funky
     WHERE schema = $1
       AND name   = $2
       AND args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._returns(name, name, name[]) OWNER TO postgres;

--
-- Name: _rexists(character, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _rexists(character, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_class c
         WHERE c.relkind = $1
           AND pg_catalog.pg_table_is_visible(c.oid)
           AND c.relname = $2
    );
$_$;


ALTER FUNCTION samplesdb._rexists(character, name) OWNER TO postgres;

--
-- Name: _rexists(character, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _rexists(character, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_namespace n
          JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
         WHERE c.relkind = $1
           AND n.nspname = $2
           AND c.relname = $3
    );
$_$;


ALTER FUNCTION samplesdb._rexists(character, name, name) OWNER TO postgres;

--
-- Name: _rule_on(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _rule_on(name, name) RETURNS "char"
    LANGUAGE sql
    AS $_$
    SELECT r.ev_type
      FROM pg_catalog.pg_rewrite r
      JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
     WHERE r.rulename = $2
       AND c.relname  = $1
$_$;


ALTER FUNCTION samplesdb._rule_on(name, name) OWNER TO postgres;

--
-- Name: _rule_on(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _rule_on(name, name, name) RETURNS "char"
    LANGUAGE sql
    AS $_$
    SELECT r.ev_type
      FROM pg_catalog.pg_rewrite r
      JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
      JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid
     WHERE r.rulename = $3
       AND c.relname  = $2
       AND n.nspname  = $1
$_$;


ALTER FUNCTION samplesdb._rule_on(name, name, name) OWNER TO postgres;

--
-- Name: _runem(text[], boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _runem(text[], boolean) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    tap    text;
    lbound int := array_lower($1, 1);
BEGIN
    IF lbound IS NULL THEN RETURN; END IF;
    FOR i IN lbound..array_upper($1, 1) LOOP
        -- Send the name of the function to diag if warranted.
        IF $2 THEN RETURN NEXT diag( $1[i] || '()' ); END IF;
        -- Execute the tap function and return its results.
        FOR tap IN EXECUTE 'SELECT * FROM ' || $1[i] || '()' LOOP
            RETURN NEXT tap;
        END LOOP;
    END LOOP;
    RETURN;
END;
$_$;


ALTER FUNCTION samplesdb._runem(text[], boolean) OWNER TO postgres;

--
-- Name: _runner(text[], text[], text[], text[], text[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _runner(text[], text[], text[], text[], text[]) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    startup  ALIAS FOR $1;
    shutdown ALIAS FOR $2;
    setup    ALIAS FOR $3;
    teardown ALIAS FOR $4;
    tests    ALIAS FOR $5;
    tap      TEXT;
    tfaild   INTEGER := 0;
    ffaild   INTEGER := 0;
    tnumb    INTEGER := 0;
    fnumb    INTEGER := 0;
    tok      BOOLEAN := TRUE;
    errmsg   TEXT;
BEGIN
    BEGIN
        -- No plan support.
        PERFORM * FROM no_plan();
        FOR tap IN SELECT * FROM _runem(startup, false) LOOP RETURN NEXT tap; END LOOP;
    EXCEPTION
        -- Catch all exceptions and simply rethrow custom exceptions. This
        -- will roll back everything in the above block.
        WHEN raise_exception THEN RAISE EXCEPTION '%', SQLERRM;
    END;

    -- Record how startup tests have failed.
    tfaild := num_failed();

    FOR i IN 1..COALESCE(array_upper(tests, 1), 0) LOOP
        -- What subtest are we running?
        RETURN NEXT '    ' || diag_test_name('Subtest: ' || tests[i]);

        -- Reset the results.
        tok := TRUE;
        tnumb := COALESCE(_get('curr_test'), 0);

        IF tnumb > 0 THEN
            EXECUTE 'ALTER SEQUENCE __tresults___numb_seq RESTART WITH 1';
            PERFORM _set('curr_test', 0);
            PERFORM _set('failed', 0);
        END IF;

        BEGIN
            BEGIN
                -- Run the setup functions.
                FOR tap IN SELECT * FROM _runem(setup, false) LOOP
                    RETURN NEXT regexp_replace(tap, '^', '    ', 'gn');
                END LOOP;

                -- Run the actual test function.
                FOR tap IN EXECUTE 'SELECT * FROM ' || tests[i] || '()' LOOP
                    RETURN NEXT regexp_replace(tap, '^', '    ', 'gn');
                END LOOP;

                -- Run the teardown functions.
                FOR tap IN SELECT * FROM _runem(teardown, false) LOOP
                    RETURN NEXT regexp_replace(tap, '^', '    ', 'gn');
                END LOOP;

                -- Emit the plan.
                fnumb := COALESCE(_get('curr_test'), 0);
                RETURN NEXT '    1..' || fnumb;

                -- Emit any error messages.
                IF fnumb = 0 THEN
                    RETURN NEXT '    # No tests run!';
                    tok = false;
                ELSE
                    -- Report failures.
                    ffaild := num_failed();
                    IF ffaild > 0 THEN
                        tok := FALSE;
                        RETURN NEXT '    ' || diag(
                            'Looks like you failed ' || ffaild || ' test' ||
                             CASE tfaild WHEN 1 THEN '' ELSE 's' END
                             || ' of ' || fnumb
                        );
                    END IF;
                END IF;

            EXCEPTION WHEN raise_exception THEN
                -- Something went wrong. Record that fact.
                errmsg := SQLERRM;
            END;

            -- Always raise an exception to rollback any changes.
            RAISE EXCEPTION '__TAP_ROLLBACK__';

        EXCEPTION WHEN raise_exception THEN
            IF errmsg IS NOT NULL THEN
                -- Something went wrong. Emit the error message.
                tok := FALSE;
                RETURN NEXT '    ' || diag('Test died: ' || errmsg);
                errmsg := NULL;
            END IF;
       END;

        -- Restore the sequence.
        EXECUTE 'ALTER SEQUENCE __tresults___numb_seq RESTART WITH ' || tnumb + 1;
        PERFORM _set('curr_test', tnumb);
        PERFORM _set('failed', tfaild);

        -- Record this test.
        RETURN NEXT ok(tok, tests[i]);
        IF NOT tok THEN tfaild := tfaild + 1; END IF;

    END LOOP;

    -- Run the shutdown functions.
    FOR tap IN SELECT * FROM _runem(shutdown, false) LOOP RETURN NEXT tap; END LOOP;

    -- Finish up.
    FOR tap IN SELECT * FROM _finish( COALESCE(_get('curr_test'), 0), 0, tfaild ) LOOP
        RETURN NEXT tap;
    END LOOP;

    -- Clean up and return.
    PERFORM _cleanup();
    RETURN;
END;
$_$;


ALTER FUNCTION samplesdb._runner(text[], text[], text[], text[], text[]) OWNER TO postgres;

--
-- Name: _set(integer, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _set(integer, integer) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE 'UPDATE __tcache__ SET value = ' || $2
        || ' WHERE id = ' || $1;
    RETURN $2;
END;
$_$;


ALTER FUNCTION samplesdb._set(integer, integer) OWNER TO postgres;

--
-- Name: _set(text, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _set(text, integer) RETURNS integer
    LANGUAGE sql
    AS $_$
    SELECT _set($1, $2, '')
$_$;


ALTER FUNCTION samplesdb._set(text, integer) OWNER TO postgres;

--
-- Name: _set(text, integer, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _set(text, integer, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
DECLARE
    rcount integer;
BEGIN
    EXECUTE 'UPDATE __tcache__ SET value = ' || $2
        || CASE WHEN $3 IS NULL THEN '' ELSE ', note = ' || quote_literal($3) END
        || ' WHERE label = ' || quote_literal($1);
    GET DIAGNOSTICS rcount = ROW_COUNT;
    IF rcount = 0 THEN
       RETURN _add( $1, $2, $3 );
    END IF;
    RETURN $2;
END;
$_$;


ALTER FUNCTION samplesdb._set(text, integer, text) OWNER TO postgres;

--
-- Name: _strict(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _strict(name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_strict FROM tap_funky WHERE name = $1 AND is_visible;
$_$;


ALTER FUNCTION samplesdb._strict(name) OWNER TO postgres;

--
-- Name: _strict(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _strict(name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_strict
      FROM tap_funky
     WHERE name = $1
       AND args = array_to_string($2, ',')
       AND is_visible;
$_$;


ALTER FUNCTION samplesdb._strict(name, name[]) OWNER TO postgres;

--
-- Name: _strict(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _strict(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_strict FROM tap_funky WHERE schema = $1 AND name = $2
$_$;


ALTER FUNCTION samplesdb._strict(name, name) OWNER TO postgres;

--
-- Name: _strict(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _strict(name, name, name[]) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT is_strict
      FROM tap_funky
     WHERE schema = $1
       AND name   = $2
       AND args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._strict(name, name, name[]) OWNER TO postgres;

--
-- Name: _table_privs(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _table_privs() RETURNS name[]
    LANGUAGE plpgsql
    AS $$
DECLARE
    pgversion INTEGER := pg_version_num();
BEGIN
    IF pgversion < 80200 THEN RETURN ARRAY[
        'DELETE', 'INSERT', 'REFERENCES', 'RULE', 'SELECT', 'TRIGGER', 'UPDATE'
    ];
    ELSIF pgversion < 80400 THEN RETURN ARRAY[
        'DELETE', 'INSERT', 'REFERENCES', 'SELECT', 'TRIGGER', 'UPDATE'
    ];
    ELSE RETURN ARRAY[
        'DELETE', 'INSERT', 'REFERENCES', 'SELECT', 'TRIGGER', 'TRUNCATE', 'UPDATE'
    ];
    END IF;
END;
$$;


ALTER FUNCTION samplesdb._table_privs() OWNER TO postgres;

--
-- Name: _temptable(anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _temptable(anyarray, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    CREATE TEMP TABLE _____coltmp___ AS
    SELECT $1[i]
    FROM generate_series(array_lower($1, 1), array_upper($1, 1)) s(i);
    EXECUTE 'ALTER TABLE _____coltmp___ RENAME TO ' || $2;
    return $2;
END;
$_$;


ALTER FUNCTION samplesdb._temptable(anyarray, text) OWNER TO postgres;

--
-- Name: _temptable(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _temptable(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE 'CREATE TEMP TABLE ' || $2 || ' AS ' || _query($1);
    return $2;
END;
$_$;


ALTER FUNCTION samplesdb._temptable(text, text) OWNER TO postgres;

--
-- Name: _temptypes(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _temptypes(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT array_to_string(ARRAY(
        SELECT pg_catalog.format_type(a.atttypid, a.atttypmod)
          FROM pg_catalog.pg_attribute a
          JOIN pg_catalog.pg_class c ON a.attrelid = c.oid
         WHERE c.oid = ('pg_temp.' || $1)::pg_catalog.regclass
           AND attnum > 0
           AND NOT attisdropped
         ORDER BY attnum
    ), ',');
$_$;


ALTER FUNCTION samplesdb._temptypes(text) OWNER TO postgres;

--
-- Name: _time_trials(text, integer, numeric); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _time_trials(text, integer, numeric) RETURNS SETOF _time_trial_type
    LANGUAGE plpgsql
    AS $_$
DECLARE
    query            TEXT := _query($1);
    iterations       ALIAS FOR $2;
    return_percent   ALIAS FOR $3;
    start_time       TEXT;
    act_time         NUMERIC;
    times            NUMERIC[];
    offset_it        INT;
    limit_it         INT;
    offset_percent   NUMERIC;
    a_time	     _time_trial_type;
BEGIN
    -- Execute the query over and over
    FOR i IN 1..iterations LOOP
        start_time := timeofday();
        EXECUTE query;
        -- Store the execution time for the run in an array of times
        times[i] := extract(millisecond from timeofday()::timestamptz - start_time::timestamptz);
    END LOOP;
    offset_percent := (1.0 - return_percent) / 2.0;
    -- Ensure that offset skips the bottom X% of runs, or set it to 0
    SELECT GREATEST((offset_percent * iterations)::int, 0) INTO offset_it;
    -- Ensure that with limit the query to returning only the middle X% of runs
    SELECT GREATEST((return_percent * iterations)::int, 1) INTO limit_it;

    FOR a_time IN SELECT times[i]
		  FROM generate_series(array_lower(times, 1), array_upper(times, 1)) i
                  ORDER BY 1
                  OFFSET offset_it
                  LIMIT limit_it LOOP
	RETURN NEXT a_time;
    END LOOP;
END;
$_$;


ALTER FUNCTION samplesdb._time_trials(text, integer, numeric) OWNER TO postgres;

--
-- Name: _tlike(boolean, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _tlike(boolean, text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( $1, $4 ) || CASE WHEN $1 THEN '' ELSE E'\n' || diag(
           '   error message: ' || COALESCE( quote_literal($2), 'NULL' ) ||
       E'\n   doesn''t match: ' || COALESCE( quote_literal($3), 'NULL' )
    ) END;
$_$;


ALTER FUNCTION samplesdb._tlike(boolean, text, text, text) OWNER TO postgres;

--
-- Name: _todo(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _todo() RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    todos INT[];
    note text;
BEGIN
    -- Get the latest id and value, because todo() might have been called
    -- again before the todos ran out for the first call to todo(). This
    -- allows them to nest.
    todos := _get_latest('todo');
    IF todos IS NULL THEN
        -- No todos.
        RETURN NULL;
    END IF;
    IF todos[2] = 0 THEN
        -- Todos depleted. Clean up.
        EXECUTE 'DELETE FROM __tcache__ WHERE id = ' || todos[1];
        RETURN NULL;
    END IF;
    -- Decrement the count of counted todos and return the reason.
    IF todos[2] <> -1 THEN
        PERFORM _set(todos[1], todos[2] - 1);
    END IF;
    note := _get_note(todos[1]);

    IF todos[2] = 1 THEN
        -- This was the last todo, so delete the record.
        EXECUTE 'DELETE FROM __tcache__ WHERE id = ' || todos[1];
    END IF;

    RETURN note;
END;
$$;


ALTER FUNCTION samplesdb._todo() OWNER TO postgres;

--
-- Name: _trig(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _trig(name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_trigger t
          JOIN pg_catalog.pg_class c     ON c.oid = t.tgrelid
         WHERE c.relname = $1
           AND t.tgname  = $2
           AND pg_catalog.pg_table_is_visible(c.oid)
    );
$_$;


ALTER FUNCTION samplesdb._trig(name, name) OWNER TO postgres;

--
-- Name: _trig(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _trig(name, name, name) RETURNS boolean
    LANGUAGE sql
    AS $_$
    SELECT EXISTS(
        SELECT true
          FROM pg_catalog.pg_trigger t
          JOIN pg_catalog.pg_class c     ON c.oid = t.tgrelid
          JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
         WHERE n.nspname = $1
           AND c.relname = $2
           AND t.tgname  = $3
    );
$_$;


ALTER FUNCTION samplesdb._trig(name, name, name) OWNER TO postgres;

--
-- Name: _types_are(name[], text, character[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _types_are(name[], text, character[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'types',
        ARRAY(
            SELECT t.typname
              FROM pg_catalog.pg_type t
              LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
             WHERE (
                     t.typrelid = 0
                 OR (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid)
             )
               AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid)
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_type_is_visible(t.oid)
               AND t.typtype = ANY( COALESCE($3, ARRAY['b', 'c', 'd', 'p', 'e']) )
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
               FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT t.typname
              FROM pg_catalog.pg_type t
              LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
             WHERE (
                     t.typrelid = 0
                 OR (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid)
             )
               AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid)
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_type_is_visible(t.oid)
               AND t.typtype = ANY( COALESCE($3, ARRAY['b', 'c', 'd', 'p', 'e']) )
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb._types_are(name[], text, character[]) OWNER TO postgres;

--
-- Name: _types_are(name, name[], text, character[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _types_are(name, name[], text, character[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'types',
        ARRAY(
            SELECT t.typname
              FROM pg_catalog.pg_type t
              LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
             WHERE (
                     t.typrelid = 0
                 OR (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid)
             )
               AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid)
               AND n.nspname = $1
               AND t.typtype = ANY( COALESCE($4, ARRAY['b', 'c', 'd', 'p', 'e']) )
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
               FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT t.typname
              FROM pg_catalog.pg_type t
              LEFT JOIN pg_catalog.pg_namespace n ON n.oid = t.typnamespace
             WHERE (
                     t.typrelid = 0
                 OR (SELECT c.relkind = 'c' FROM pg_catalog.pg_class c WHERE c.oid = t.typrelid)
             )
               AND NOT EXISTS(SELECT 1 FROM pg_catalog.pg_type el WHERE el.oid = t.typelem AND el.typarray = t.oid)
               AND n.nspname = $1
               AND t.typtype = ANY( COALESCE($4, ARRAY['b', 'c', 'd', 'p', 'e']) )
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb._types_are(name, name[], text, character[]) OWNER TO postgres;

--
-- Name: _unalike(boolean, anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _unalike(boolean, anyelement, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    result ALIAS FOR $1;
    got    ALIAS FOR $2;
    rx     ALIAS FOR $3;
    descr  ALIAS FOR $4;
    output TEXT;
BEGIN
    output := ok( result, descr );
    RETURN output || CASE result WHEN TRUE THEN '' ELSE E'\n' || diag(
           '                  ' || COALESCE( quote_literal(got), 'NULL' ) ||
        E'\n         matches: ' || COALESCE( quote_literal(rx), 'NULL' )
    ) END;
END;
$_$;


ALTER FUNCTION samplesdb._unalike(boolean, anyelement, text, text) OWNER TO postgres;

--
-- Name: _vol(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _vol(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _expand_vol(volatility) FROM tap_funky f
     WHERE f.name = $1 AND f.is_visible;
$_$;


ALTER FUNCTION samplesdb._vol(name) OWNER TO postgres;

--
-- Name: _vol(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _vol(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _expand_vol(volatility)
      FROM tap_funky f
     WHERE f.name = $1
       AND f.args = array_to_string($2, ',')
       AND f.is_visible;
$_$;


ALTER FUNCTION samplesdb._vol(name, name[]) OWNER TO postgres;

--
-- Name: _vol(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _vol(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _expand_vol(volatility) FROM tap_funky f
     WHERE f.schema = $1 and f.name = $2
$_$;


ALTER FUNCTION samplesdb._vol(name, name) OWNER TO postgres;

--
-- Name: _vol(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION _vol(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _expand_vol(volatility)
      FROM tap_funky f
     WHERE f.schema = $1
       and f.name   = $2
       AND f.args   = array_to_string($3, ',')
$_$;


ALTER FUNCTION samplesdb._vol(name, name, name[]) OWNER TO postgres;

--
-- Name: add_result(boolean, boolean, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION add_result(boolean, boolean, text, text, text) RETURNS integer
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT $1 THEN PERFORM _set('failed', _get('failed') + 1); END IF;
    RETURN nextval('__tresults___numb_seq');
END;
$_$;


ALTER FUNCTION samplesdb.add_result(boolean, boolean, text, text, text) OWNER TO postgres;

--
-- Name: alike(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION alike(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~~ $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.alike(anyelement, text) OWNER TO postgres;

--
-- Name: alike(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION alike(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~~ $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.alike(anyelement, text, text) OWNER TO postgres;

--
-- Name: any_column_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION any_column_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT any_column_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on any column in ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.any_column_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: any_column_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION any_column_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_ac_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.any_column_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: any_column_privs_are(name, name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION any_column_privs_are(name, name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT any_column_privs_are(
        $1, $2, $3, $4,
        'Role ' || quote_ident($3) || ' should be granted '
            || CASE WHEN $4[1] IS NULL THEN 'no privileges' ELSE array_to_string($4, ', ') END
            || ' on any column in '|| quote_ident($1) || '.' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.any_column_privs_are(name, name, name, name[]) OWNER TO postgres;

--
-- Name: any_column_privs_are(name, name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION any_column_privs_are(name, name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_ac_privs( $3, quote_ident($1) || '.' || quote_ident($2) );
BEGIN
    IF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Role ' || quote_ident($3) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $4, $5);
END;
$_$;


ALTER FUNCTION samplesdb.any_column_privs_are(name, name, name, name[], text) OWNER TO postgres;

--
-- Name: bag_eq(text, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_eq(text, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::text, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_eq(text, anyarray) OWNER TO postgres;

--
-- Name: bag_eq(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_eq(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::text, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_eq(text, text) OWNER TO postgres;

--
-- Name: bag_eq(text, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_eq(text, anyarray, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_eq(text, anyarray, text) OWNER TO postgres;

--
-- Name: bag_eq(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_eq(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_eq(text, text, text) OWNER TO postgres;

--
-- Name: bag_has(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_has(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::TEXT, 'EXCEPT ALL', 'Missing' );
$_$;


ALTER FUNCTION samplesdb.bag_has(text, text) OWNER TO postgres;

--
-- Name: bag_has(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_has(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, 'EXCEPT ALL', 'Missing' );
$_$;


ALTER FUNCTION samplesdb.bag_has(text, text, text) OWNER TO postgres;

--
-- Name: bag_hasnt(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_hasnt(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::TEXT, 'INTERSECT ALL', 'Extra' );
$_$;


ALTER FUNCTION samplesdb.bag_hasnt(text, text) OWNER TO postgres;

--
-- Name: bag_hasnt(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_hasnt(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, 'INTERSECT ALL', 'Extra' );
$_$;


ALTER FUNCTION samplesdb.bag_hasnt(text, text, text) OWNER TO postgres;

--
-- Name: bag_ne(text, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_ne(text, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, NULL::text, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_ne(text, anyarray) OWNER TO postgres;

--
-- Name: bag_ne(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_ne(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, NULL::text, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_ne(text, text) OWNER TO postgres;

--
-- Name: bag_ne(text, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_ne(text, anyarray, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, $3, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_ne(text, anyarray, text) OWNER TO postgres;

--
-- Name: bag_ne(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION bag_ne(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, $3, 'ALL ' );
$_$;


ALTER FUNCTION samplesdb.bag_ne(text, text, text) OWNER TO postgres;

--
-- Name: can(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION can(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT can( $1, 'Schema ' || _ident_array_to_string(current_schemas(true), ' or ') || ' can' );
$_$;


ALTER FUNCTION samplesdb.can(name[]) OWNER TO postgres;

--
-- Name: can(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION can(name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    missing text[];
BEGIN
    SELECT ARRAY(
        SELECT quote_ident($1[i])
          FROM generate_series(1, array_upper($1, 1)) s(i)
          LEFT JOIN pg_catalog.pg_proc p
            ON $1[i] = p.proname
           AND pg_catalog.pg_function_is_visible(p.oid)
         WHERE p.oid IS NULL
         ORDER BY s.i
    ) INTO missing;
    IF missing[1] IS NULL THEN
        RETURN ok( true, $2 );
    END IF;
    RETURN ok( false, $2 ) || E'\n' || diag(
        '    ' ||
        array_to_string( missing, E'() missing\n    ') ||
        '() missing'
    );
END;
$_$;


ALTER FUNCTION samplesdb.can(name[], text) OWNER TO postgres;

--
-- Name: can(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION can(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT can( $1, $2, 'Schema ' || quote_ident($1) || ' can' );
$_$;


ALTER FUNCTION samplesdb.can(name, name[]) OWNER TO postgres;

--
-- Name: can(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION can(name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    missing text[];
BEGIN
    SELECT ARRAY(
        SELECT quote_ident($2[i])
          FROM generate_series(1, array_upper($2, 1)) s(i)
          LEFT JOIN tap_funky ON name = $2[i] AND schema = $1
         WHERE oid IS NULL
         GROUP BY $2[i], s.i
         ORDER BY MIN(s.i)
    ) INTO missing;
    IF missing[1] IS NULL THEN
        RETURN ok( true, $3 );
    END IF;
    RETURN ok( false, $3 ) || E'\n' || diag(
        '    ' || quote_ident($1) || '.' ||
        array_to_string( missing, E'() missing\n    ' || quote_ident($1) || '.') ||
        '() missing'
    );
END;
$_$;


ALTER FUNCTION samplesdb.can(name, name[], text) OWNER TO postgres;

--
-- Name: cast_context_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION cast_context_is(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT cast_context_is(
        $1, $2, $3,
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') context should be ' || _expand_context(substring(LOWER($3) FROM 1 FOR 1))
    );
$_$;


ALTER FUNCTION samplesdb.cast_context_is(name, name, text) OWNER TO postgres;

--
-- Name: cast_context_is(name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION cast_context_is(name, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want char = substring(LOWER($3) FROM 1 FOR 1);
    have char := _get_context($1, $2);
BEGIN
    IF have IS NOT NULL THEN
        RETURN is( _expand_context(have), _expand_context(want), $4 );
    END IF;

    RETURN ok( false, $4 ) || E'\n' || diag(
       '    Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
      || ') does not exist'
    );
END;
$_$;


ALTER FUNCTION samplesdb.cast_context_is(name, name, text, text) OWNER TO postgres;

--
-- Name: casts_are(text[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION casts_are(text[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT casts_are( $1, 'There should be the correct casts');
$_$;


ALTER FUNCTION samplesdb.casts_are(text[]) OWNER TO postgres;

--
-- Name: casts_are(text[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION casts_are(text[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _areni(
        'casts',
        ARRAY(
            SELECT pg_catalog.format_type(castsource, NULL)
                   || ' AS ' || pg_catalog.format_type(casttarget, NULL)
              FROM pg_catalog.pg_cast c
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT pg_catalog.format_type(castsource, NULL)
                   || ' AS ' || pg_catalog.format_type(casttarget, NULL)
              FROM pg_catalog.pg_cast c
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.casts_are(text[], text) OWNER TO postgres;

--
-- Name: check_test(text, boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION check_test(text, boolean) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM check_test( $1, $2, NULL, NULL, NULL, FALSE );
$_$;


ALTER FUNCTION samplesdb.check_test(text, boolean) OWNER TO postgres;

--
-- Name: check_test(text, boolean, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION check_test(text, boolean, text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM check_test( $1, $2, $3, NULL, NULL, FALSE );
$_$;


ALTER FUNCTION samplesdb.check_test(text, boolean, text) OWNER TO postgres;

--
-- Name: check_test(text, boolean, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION check_test(text, boolean, text, text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM check_test( $1, $2, $3, $4, NULL, FALSE );
$_$;


ALTER FUNCTION samplesdb.check_test(text, boolean, text, text) OWNER TO postgres;

--
-- Name: check_test(text, boolean, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION check_test(text, boolean, text, text, text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM check_test( $1, $2, $3, $4, $5, FALSE );
$_$;


ALTER FUNCTION samplesdb.check_test(text, boolean, text, text, text) OWNER TO postgres;

--
-- Name: check_test(text, boolean, text, text, text, boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION check_test(text, boolean, text, text, text, boolean) RETURNS SETOF text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    tnumb   INTEGER;
    aok     BOOLEAN;
    adescr  TEXT;
    res     BOOLEAN;
    descr   TEXT;
    adiag   TEXT;
    have    ALIAS FOR $1;
    eok     ALIAS FOR $2;
    name    ALIAS FOR $3;
    edescr  ALIAS FOR $4;
    ediag   ALIAS FOR $5;
    matchit ALIAS FOR $6;
BEGIN
    -- What test was it that just ran?
    tnumb := currval('__tresults___numb_seq');

    -- Fetch the results.
    aok    := substring(have, 1, 2) = 'ok';
    adescr := COALESCE(substring(have FROM  E'(?:not )?ok [[:digit:]]+ - ([^\n]+)'), '');

    -- Now delete those results.
    EXECUTE 'ALTER SEQUENCE __tresults___numb_seq RESTART WITH ' || tnumb;
    IF NOT aok THEN PERFORM _set('failed', _get('failed') - 1); END IF;

    -- Set up the description.
    descr := coalesce( name || ' ', 'Test ' ) || 'should ';

    -- So, did the test pass?
    RETURN NEXT is(
        aok,
        eok,
        descr || CASE eok WHEN true then 'pass' ELSE 'fail' END
    );

    -- Was the description as expected?
    IF edescr IS NOT NULL THEN
        RETURN NEXT is(
            adescr,
            edescr,
            descr || 'have the proper description'
        );
    END IF;

    -- Were the diagnostics as expected?
    IF ediag IS NOT NULL THEN
        -- Remove ok and the test number.
        adiag := substring(
            have
            FROM CASE WHEN aok THEN 4 ELSE 9 END + char_length(tnumb::text)
        );

        -- Remove the description, if there is one.
        IF adescr <> '' THEN
            adiag := substring(
                adiag FROM 1 + char_length( ' - ' || substr(diag( adescr ), 3) )
            );
        END IF;

        IF NOT aok THEN
            -- Remove failure message from ok().
            adiag := substring(adiag FROM 1 + char_length(diag(
                'Failed test ' || tnumb ||
                CASE adescr WHEN '' THEN '' ELSE COALESCE(': "' || adescr || '"', '') END
            )));
        END IF;

        IF ediag <> '' THEN
           -- Remove the space before the diagnostics.
           adiag := substring(adiag FROM 2);
        END IF;

        -- Remove the #s.
        adiag := replace( substring(adiag from 3), E'\n# ', E'\n' );

        -- Now compare the diagnostics.
        IF matchit THEN
            RETURN NEXT matches(
                adiag,
                ediag,
                descr || 'have the proper diagnostics'
            );
        ELSE
            RETURN NEXT is(
                adiag,
                ediag,
                descr || 'have the proper diagnostics'
            );
        END IF;
    END IF;

    -- And we're done
    RETURN;
END;
$_$;


ALTER FUNCTION samplesdb.check_test(text, boolean, text, text, text, boolean) OWNER TO postgres;

--
-- Name: cmp_ok(anyelement, text, anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION cmp_ok(anyelement, text, anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT cmp_ok( $1, $2, $3, NULL );
$_$;


ALTER FUNCTION samplesdb.cmp_ok(anyelement, text, anyelement) OWNER TO postgres;

--
-- Name: cmp_ok(anyelement, text, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION cmp_ok(anyelement, text, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have   ALIAS FOR $1;
    op     ALIAS FOR $2;
    want   ALIAS FOR $3;
    descr  ALIAS FOR $4;
    result BOOLEAN;
    output TEXT;
BEGIN
    EXECUTE 'SELECT ' ||
            COALESCE(quote_literal( have ), 'NULL') || '::' || pg_typeof(have) || ' '
            || op || ' ' ||
            COALESCE(quote_literal( want ), 'NULL') || '::' || pg_typeof(want)
       INTO result;
    output := ok( COALESCE(result, FALSE), descr );
    RETURN output || CASE result WHEN TRUE THEN '' ELSE E'\n' || diag(
           '    ' || COALESCE( quote_literal(have), 'NULL' ) ||
           E'\n        ' || op ||
           E'\n    ' || COALESCE( quote_literal(want), 'NULL' )
    ) END;
END;
$_$;


ALTER FUNCTION samplesdb.cmp_ok(anyelement, text, anyelement, text) OWNER TO postgres;

--
-- Name: col_default_is(name, name, anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_default_is(name, name, anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _cdi( $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.col_default_is(name, name, anyelement) OWNER TO postgres;

--
-- Name: col_default_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_default_is(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _cdi( $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.col_default_is(name, name, text) OWNER TO postgres;

--
-- Name: col_default_is(name, name, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_default_is(name, name, anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _cdi( $1, $2, $3, $4 );
$_$;


ALTER FUNCTION samplesdb.col_default_is(name, name, anyelement, text) OWNER TO postgres;

--
-- Name: col_default_is(name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_default_is(name, name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _cdi( $1, $2, $3, $4 );
$_$;


ALTER FUNCTION samplesdb.col_default_is(name, name, text, text) OWNER TO postgres;

--
-- Name: col_default_is(name, name, name, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_default_is(name, name, name, anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _cdi( $1, $2, $3, $4, $5 );
$_$;


ALTER FUNCTION samplesdb.col_default_is(name, name, name, anyelement, text) OWNER TO postgres;

--
-- Name: col_default_is(name, name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_default_is(name, name, name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _cdi( $1, $2, $3, $4, $5 );
$_$;


ALTER FUNCTION samplesdb.col_default_is(name, name, name, text, text) OWNER TO postgres;

--
-- Name: col_has_check(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_check(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_has_check( $1, $2, 'Columns ' || quote_ident($1) || '(' || _ident_array_to_string($2, ', ') || ') should have a check constraint' );
$_$;


ALTER FUNCTION samplesdb.col_has_check(name, name[]) OWNER TO postgres;

--
-- Name: col_has_check(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_check(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_has_check( $1, $2, 'Column ' || quote_ident($1) || '(' || quote_ident($2) || ') should have a check constraint' );
$_$;


ALTER FUNCTION samplesdb.col_has_check(name, name) OWNER TO postgres;

--
-- Name: col_has_check(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_check(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _constraint( $1, 'c', $2, $3, 'check' );
$_$;


ALTER FUNCTION samplesdb.col_has_check(name, name[], text) OWNER TO postgres;

--
-- Name: col_has_check(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_check(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_has_check( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.col_has_check(name, name, text) OWNER TO postgres;

--
-- Name: col_has_check(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_check(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _constraint( $1, $2, 'c', $3, $4, 'check' );
$_$;


ALTER FUNCTION samplesdb.col_has_check(name, name, name[], text) OWNER TO postgres;

--
-- Name: col_has_check(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_check(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_has_check( $1, $2, ARRAY[$3], $4 );
$_$;


ALTER FUNCTION samplesdb.col_has_check(name, name, name, text) OWNER TO postgres;

--
-- Name: col_has_default(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_default(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_has_default( $1, $2, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should have a default' );
$_$;


ALTER FUNCTION samplesdb.col_has_default(name, name) OWNER TO postgres;

--
-- Name: col_has_default(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_default(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2 ) THEN
        RETURN fail( $3 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist' );
    END IF;
    RETURN ok( _has_def( $1, $2 ), $3 );
END;
$_$;


ALTER FUNCTION samplesdb.col_has_default(name, name, text) OWNER TO postgres;

--
-- Name: col_has_default(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_has_default(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2, $3 ) THEN
        RETURN fail( $4 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3) || ' does not exist' );
    END IF;
    RETURN ok( _has_def( $1, $2, $3 ), $4 );
END
$_$;


ALTER FUNCTION samplesdb.col_has_default(name, name, name, text) OWNER TO postgres;

--
-- Name: col_hasnt_default(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_hasnt_default(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_hasnt_default( $1, $2, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should not have a default' );
$_$;


ALTER FUNCTION samplesdb.col_hasnt_default(name, name) OWNER TO postgres;

--
-- Name: col_hasnt_default(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_hasnt_default(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2 ) THEN
        RETURN fail( $3 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist' );
    END IF;
    RETURN ok( NOT _has_def( $1, $2 ), $3 );
END;
$_$;


ALTER FUNCTION samplesdb.col_hasnt_default(name, name, text) OWNER TO postgres;

--
-- Name: col_hasnt_default(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_hasnt_default(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF NOT _cexists( $1, $2, $3 ) THEN
        RETURN fail( $4 ) || E'\n'
            || diag ('    Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3) || ' does not exist' );
    END IF;
    RETURN ok( NOT _has_def( $1, $2, $3 ), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.col_hasnt_default(name, name, name, text) OWNER TO postgres;

--
-- Name: col_is_fk(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_fk(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_fk( $1, $2, 'Columns ' || quote_ident($1) || '(' || _ident_array_to_string($2, ', ') || ') should be a foreign key' );
$_$;


ALTER FUNCTION samplesdb.col_is_fk(name, name[]) OWNER TO postgres;

--
-- Name: col_is_fk(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_fk(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_fk( $1, $2, 'Column ' || quote_ident($1) || '(' || quote_ident($2) || ') should be a foreign key' );
$_$;


ALTER FUNCTION samplesdb.col_is_fk(name, name) OWNER TO postgres;

--
-- Name: col_is_fk(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_fk(name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    names text[];
BEGIN
    IF _fkexists($1, $2) THEN
        RETURN pass( $3 );
    END IF;

    -- Try to show the columns.
    SELECT ARRAY(
        SELECT _ident_array_to_string(fk_columns, ', ')
          FROM pg_all_foreign_keys
         WHERE fk_table_name  = $1
         ORDER BY fk_columns
    ) INTO names;

    IF NAMES[1] IS NOT NULL THEN
        RETURN fail($3) || E'\n' || diag(
            '    Table ' || quote_ident($1) || E' has foreign key constraints on these columns:\n        '
            || array_to_string( names, E'\n        ' )
        );
    END IF;

    -- No FKs in this table.
    RETURN fail($3) || E'\n' || diag(
        '    Table ' || quote_ident($1) || ' has no foreign key columns'
    );
END;
$_$;


ALTER FUNCTION samplesdb.col_is_fk(name, name[], text) OWNER TO postgres;

--
-- Name: col_is_fk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_fk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_fk( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.col_is_fk(name, name, text) OWNER TO postgres;

--
-- Name: col_is_fk(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_fk(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    names text[];
BEGIN
    IF _fkexists($1, $2, $3) THEN
        RETURN pass( $4 );
    END IF;

    -- Try to show the columns.
    SELECT ARRAY(
        SELECT _ident_array_to_string(fk_columns, ', ')
          FROM pg_all_foreign_keys
         WHERE fk_schema_name = $1
           AND fk_table_name  = $2
         ORDER BY fk_columns
    ) INTO names;

    IF names[1] IS NOT NULL THEN
        RETURN fail($4) || E'\n' || diag(
            '    Table ' || quote_ident($1) || '.' || quote_ident($2) || E' has foreign key constraints on these columns:\n        '
            ||  array_to_string( names, E'\n        ' )
        );
    END IF;

    -- No FKs in this table.
    RETURN fail($4) || E'\n' || diag(
        '    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' has no foreign key columns'
    );
END;
$_$;


ALTER FUNCTION samplesdb.col_is_fk(name, name, name[], text) OWNER TO postgres;

--
-- Name: col_is_fk(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_fk(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_fk( $1, $2, ARRAY[$3], $4 );
$_$;


ALTER FUNCTION samplesdb.col_is_fk(name, name, name, text) OWNER TO postgres;

--
-- Name: col_is_null(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_null(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _col_is_null( $1, $2, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should allow NULL', false );
$_$;


ALTER FUNCTION samplesdb.col_is_null(name, name) OWNER TO postgres;

--
-- Name: col_is_null(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_null(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _col_is_null( $1, $2, $3, false );
$_$;


ALTER FUNCTION samplesdb.col_is_null(name, name, name) OWNER TO postgres;

--
-- Name: col_is_null(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_null(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _col_is_null( $1, $2, $3, $4, false );
$_$;


ALTER FUNCTION samplesdb.col_is_null(name, name, name, text) OWNER TO postgres;

--
-- Name: col_is_pk(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_pk(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_pk( $1, $2, 'Columns ' || quote_ident($1) || '(' || _ident_array_to_string($2, ', ') || ') should be a primary key' );
$_$;


ALTER FUNCTION samplesdb.col_is_pk(name, name[]) OWNER TO postgres;

--
-- Name: col_is_pk(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_pk(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_pk( $1, $2, 'Column ' || quote_ident($1) || '(' || quote_ident($2) || ') should be a primary key' );
$_$;


ALTER FUNCTION samplesdb.col_is_pk(name, name) OWNER TO postgres;

--
-- Name: col_is_pk(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_pk(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is( _ckeys( $1, 'p' ), $2, $3 );
$_$;


ALTER FUNCTION samplesdb.col_is_pk(name, name[], text) OWNER TO postgres;

--
-- Name: col_is_pk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_pk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_pk( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.col_is_pk(name, name, text) OWNER TO postgres;

--
-- Name: col_is_pk(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_pk(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is( _ckeys( $1, $2, 'p' ), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.col_is_pk(name, name, name[], text) OWNER TO postgres;

--
-- Name: col_is_pk(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_pk(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_pk( $1, $2, ARRAY[$3], $4 );
$_$;


ALTER FUNCTION samplesdb.col_is_pk(name, name, name, text) OWNER TO postgres;

--
-- Name: col_is_unique(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_unique( $1, $2, 'Columns ' || quote_ident($1) || '(' || _ident_array_to_string($2, ', ') || ') should have a unique constraint' );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name[]) OWNER TO postgres;

--
-- Name: col_is_unique(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_unique( $1, $2, 'Column ' || quote_ident($1) || '(' || quote_ident($2) || ') should have a unique constraint' );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name) OWNER TO postgres;

--
-- Name: col_is_unique(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _constraint( $1, 'u', $2, $3, 'unique' );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name[], text) OWNER TO postgres;

--
-- Name: col_is_unique(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_unique( $1, $2, $3, 'Columns ' || quote_ident($2) || '(' || _ident_array_to_string($3, ', ') || ') should have a unique constraint' );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name, name[]) OWNER TO postgres;

--
-- Name: col_is_unique(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_unique( $1, $2, ARRAY[$3], 'Column ' || quote_ident($2) || '(' || quote_ident($3) || ') should have a unique constraint' );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name, name) OWNER TO postgres;

--
-- Name: col_is_unique(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_unique( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name, text) OWNER TO postgres;

--
-- Name: col_is_unique(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _constraint( $1, $2, 'u', $3, $4, 'unique' );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name, name[], text) OWNER TO postgres;

--
-- Name: col_is_unique(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_is_unique(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_is_unique( $1, $2, ARRAY[$3], $4 );
$_$;


ALTER FUNCTION samplesdb.col_is_unique(name, name, name, text) OWNER TO postgres;

--
-- Name: col_isnt_fk(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_fk(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_fk( $1, $2, 'Columns ' || quote_ident($1) || '(' || _ident_array_to_string($2, ', ') || ') should not be a foreign key' );
$_$;


ALTER FUNCTION samplesdb.col_isnt_fk(name, name[]) OWNER TO postgres;

--
-- Name: col_isnt_fk(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_fk(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_fk( $1, $2, 'Column ' || quote_ident($1) || '(' || quote_ident($2) || ') should not be a foreign key' );
$_$;


ALTER FUNCTION samplesdb.col_isnt_fk(name, name) OWNER TO postgres;

--
-- Name: col_isnt_fk(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_fk(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _fkexists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_fk(name, name[], text) OWNER TO postgres;

--
-- Name: col_isnt_fk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_fk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_fk( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_fk(name, name, text) OWNER TO postgres;

--
-- Name: col_isnt_fk(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_fk(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _fkexists( $1, $2, $3 ), $4 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_fk(name, name, name[], text) OWNER TO postgres;

--
-- Name: col_isnt_fk(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_fk(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_fk( $1, $2, ARRAY[$3], $4 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_fk(name, name, name, text) OWNER TO postgres;

--
-- Name: col_isnt_pk(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_pk(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_pk( $1, $2, 'Columns ' || quote_ident($1) || '(' || _ident_array_to_string($2, ', ') || ') should not be a primary key' );
$_$;


ALTER FUNCTION samplesdb.col_isnt_pk(name, name[]) OWNER TO postgres;

--
-- Name: col_isnt_pk(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_pk(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_pk( $1, $2, 'Column ' || quote_ident($1) || '(' || quote_ident($2) || ') should not be a primary key' );
$_$;


ALTER FUNCTION samplesdb.col_isnt_pk(name, name) OWNER TO postgres;

--
-- Name: col_isnt_pk(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_pk(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT isnt( _ckeys( $1, 'p' ), $2, $3 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_pk(name, name[], text) OWNER TO postgres;

--
-- Name: col_isnt_pk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_pk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_pk( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_pk(name, name, text) OWNER TO postgres;

--
-- Name: col_isnt_pk(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_pk(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT isnt( _ckeys( $1, $2, 'p' ), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_pk(name, name, name[], text) OWNER TO postgres;

--
-- Name: col_isnt_pk(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_isnt_pk(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_isnt_pk( $1, $2, ARRAY[$3], $4 );
$_$;


ALTER FUNCTION samplesdb.col_isnt_pk(name, name, name, text) OWNER TO postgres;

--
-- Name: col_not_null(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_not_null(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _col_is_null( $1, $2, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should be NOT NULL', true );
$_$;


ALTER FUNCTION samplesdb.col_not_null(name, name) OWNER TO postgres;

--
-- Name: col_not_null(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_not_null(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _col_is_null( $1, $2, $3, true );
$_$;


ALTER FUNCTION samplesdb.col_not_null(name, name, text) OWNER TO postgres;

--
-- Name: col_not_null(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_not_null(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _col_is_null( $1, $2, $3, $4, true );
$_$;


ALTER FUNCTION samplesdb.col_not_null(name, name, name, text) OWNER TO postgres;

--
-- Name: col_type_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_type_is(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_type_is( $1, $2, $3, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should be type ' || $3 );
$_$;


ALTER FUNCTION samplesdb.col_type_is(name, name, text) OWNER TO postgres;

--
-- Name: col_type_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_type_is(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_type_is( $1, $2, $3, $4, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3) || ' should be type ' || $4 );
$_$;


ALTER FUNCTION samplesdb.col_type_is(name, name, name, text) OWNER TO postgres;

--
-- Name: col_type_is(name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_type_is(name, name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_type_is( NULL, $1, $2, $3, $4 );
$_$;


ALTER FUNCTION samplesdb.col_type_is(name, name, text, text) OWNER TO postgres;

--
-- Name: col_type_is(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_type_is(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT col_type_is( $1, $2, $3, $4, $5, 'Column ' || quote_ident($1) || '.' || quote_ident($2)
        || '.' || quote_ident($3) || ' should be type ' || quote_ident($4) || '.' || $5);
$_$;


ALTER FUNCTION samplesdb.col_type_is(name, name, name, name, text) OWNER TO postgres;

--
-- Name: col_type_is(name, name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_type_is(name, name, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have_type TEXT;
    want_type TEXT;
BEGIN
    -- Get the data type.
    IF $1 IS NULL THEN
        have_type := _get_col_type($2, $3);
    ELSE
        have_type := _get_col_type($1, $2, $3);
    END IF;

    IF have_type IS NULL THEN
        RETURN fail( $5 ) || E'\n' || diag (
            '   Column ' || COALESCE(quote_ident($1) || '.', '')
            || quote_ident($2) || '.' || quote_ident($3) || ' does not exist'
        );
    END IF;

    want_type := _quote_ident_like($4, have_type);
    IF have_type = want_type THEN
        -- We're good to go.
        RETURN ok( true, $5 );
    END IF;

    -- Wrong data type. tell 'em what we really got.
    RETURN ok( false, $5 ) || E'\n' || diag(
           '        have: ' || have_type ||
        E'\n        want: ' || want_type
    );
END;
$_$;


ALTER FUNCTION samplesdb.col_type_is(name, name, name, text, text) OWNER TO postgres;

--
-- Name: col_type_is(name, name, name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION col_type_is(name, name, name, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have_type TEXT := _get_col_ns_type($1, $2, $3);
    want_type TEXT;
BEGIN
    IF have_type IS NULL THEN
        RETURN fail( $6 ) || E'\n' || diag (
            '   Column ' || COALESCE(quote_ident($1) || '.', '')
            || quote_ident($2) || '.' || quote_ident($3) || ' does not exist'
        );
    END IF;

    want_type := quote_ident($4) || '.' || _quote_ident_like($5, have_type);
    IF have_type = want_type THEN
        -- We're good to go.
        RETURN ok( true, $6 );
    END IF;

    -- Wrong data type. tell 'em what we really got.
    RETURN ok( false, $6 ) || E'\n' || diag(
           '        have: ' || have_type ||
        E'\n        want: ' || want_type
    );
END;
$_$;


ALTER FUNCTION samplesdb.col_type_is(name, name, name, name, text, text) OWNER TO postgres;

--
-- Name: collect_tap(text[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION collect_tap(VARIADIC text[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT array_to_string($1, E'\n');
$_$;


ALTER FUNCTION samplesdb.collect_tap(VARIADIC text[]) OWNER TO postgres;

--
-- Name: collect_tap(character varying[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION collect_tap(character varying[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT array_to_string($1, E'\n');
$_$;


ALTER FUNCTION samplesdb.collect_tap(character varying[]) OWNER TO postgres;

--
-- Name: column_privs_are(name, name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION column_privs_are(name, name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT column_privs_are(
        $1, $2, $3, $4,
        'Role ' || quote_ident($3) || ' should be granted '
            || CASE WHEN $4[1] IS NULL THEN 'no privileges' ELSE array_to_string($4, ', ') END
            || ' on column ' || quote_ident($1) || '.' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.column_privs_are(name, name, name, name[]) OWNER TO postgres;

--
-- Name: column_privs_are(name, name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION column_privs_are(name, name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_col_privs( $3, quote_ident($1), $2 );
BEGIN
    IF grants[1] = 'undefined_column' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Column ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Table ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Role ' || quote_ident($3) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $4, $5);
END;
$_$;


ALTER FUNCTION samplesdb.column_privs_are(name, name, name, name[], text) OWNER TO postgres;

--
-- Name: column_privs_are(name, name, name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION column_privs_are(name, name, name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT column_privs_are(
        $1, $2, $3, $4, $5,
        'Role ' || quote_ident($4) || ' should be granted '
            || CASE WHEN $5[1] IS NULL THEN 'no privileges' ELSE array_to_string($5, ', ') END
            || ' on column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.column_privs_are(name, name, name, name, name[]) OWNER TO postgres;

--
-- Name: column_privs_are(name, name, name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION column_privs_are(name, name, name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_col_privs( $4, quote_ident($1) || '.' || quote_ident($2), $3 );
BEGIN
    IF grants[1] = 'undefined_column' THEN
        RETURN ok(FALSE, $6) || E'\n' || diag(
            '    Column ' || quote_ident($1) || '.' || quote_ident($2) || '.' || quote_ident($3)
            || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $6) || E'\n' || diag(
            '    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $6) || E'\n' || diag(
            '    Role ' || quote_ident($4) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $5, $6);
END;
$_$;


ALTER FUNCTION samplesdb.column_privs_are(name, name, name, name, name[], text) OWNER TO postgres;

--
-- Name: columns_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION columns_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT columns_are( $1, $2, 'Table ' || quote_ident($1) || ' should have the correct columns' );
$_$;


ALTER FUNCTION samplesdb.columns_are(name, name[]) OWNER TO postgres;

--
-- Name: columns_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION columns_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'columns',
        ARRAY(
            SELECT a.attname
              FROM pg_catalog.pg_namespace n
              JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
              JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
             WHERE n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_table_is_visible(c.oid)
               AND c.relname = $1
               AND a.attnum > 0
               AND NOT a.attisdropped
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT a.attname
              FROM pg_catalog.pg_namespace n
              JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
              JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
             WHERE n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_table_is_visible(c.oid)
               AND c.relname = $1
               AND a.attnum > 0
               AND NOT a.attisdropped
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.columns_are(name, name[], text) OWNER TO postgres;

--
-- Name: columns_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION columns_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT columns_are( $1, $2, $3, 'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should have the correct columns' );
$_$;


ALTER FUNCTION samplesdb.columns_are(name, name, name[]) OWNER TO postgres;

--
-- Name: columns_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION columns_are(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'columns',
        ARRAY(
            SELECT a.attname
              FROM pg_catalog.pg_namespace n
              JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
              JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
             WHERE n.nspname = $1
               AND c.relname = $2
               AND a.attnum > 0
               AND NOT a.attisdropped
            EXCEPT
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
        ),
        ARRAY(
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
            EXCEPT
            SELECT a.attname
              FROM pg_catalog.pg_namespace n
              JOIN pg_catalog.pg_class c ON n.oid = c.relnamespace
              JOIN pg_catalog.pg_attribute a ON c.oid = a.attrelid
             WHERE n.nspname = $1
               AND c.relname = $2
               AND a.attnum > 0
               AND NOT a.attisdropped
        ),
        $4
    );
$_$;


ALTER FUNCTION samplesdb.columns_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: composite_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION composite_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT composite_owner_is(
        $1, $2,
        'Composite type ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.composite_owner_is(name, name) OWNER TO postgres;

--
-- Name: composite_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION composite_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT composite_owner_is(
        $1, $2, $3,
        'Composite type ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.composite_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: composite_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION composite_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('c'::char, $1);
BEGIN
    -- Make sure the composite exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Composite type ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.composite_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: composite_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION composite_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('c'::char, $1, $2);
BEGIN
    -- Make sure the composite exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Composite type ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.composite_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: database_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION database_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT database_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on database ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.database_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: database_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION database_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_db_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'invalid_catalog_name' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Database ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.database_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: db_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION db_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT db_owner_is(
        $1, $2,
        'Database ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.db_owner_is(name, name) OWNER TO postgres;

--
-- Name: db_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION db_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    dbowner NAME := _get_db_owner($1);
BEGIN
    -- Make sure the database exists.
    IF dbowner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Database ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(dbowner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.db_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: diag(text[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION diag(VARIADIC text[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT diag(array_to_string($1, ''));
$_$;


ALTER FUNCTION samplesdb.diag(VARIADIC text[]) OWNER TO postgres;

--
-- Name: diag(anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION diag(VARIADIC anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT diag(array_to_string($1, ''));
$_$;


ALTER FUNCTION samplesdb.diag(VARIADIC anyarray) OWNER TO postgres;

--
-- Name: diag(anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION diag(msg anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT diag($1::text);
$_$;


ALTER FUNCTION samplesdb.diag(msg anyelement) OWNER TO postgres;

--
-- Name: diag(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION diag(msg text) RETURNS text
    LANGUAGE sql STRICT
    AS $_$
    SELECT '# ' || replace(
       replace(
            replace( $1, E'\r\n', E'\n# ' ),
            E'\n',
            E'\n# '
        ),
        E'\r',
        E'\n# '
    );
$_$;


ALTER FUNCTION samplesdb.diag(msg text) OWNER TO postgres;

--
-- Name: diag_test_name(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION diag_test_name(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT diag($1 || '()');
$_$;


ALTER FUNCTION samplesdb.diag_test_name(text) OWNER TO postgres;

--
-- Name: display_oper(name, oid); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION display_oper(name, oid) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT $1 || substring($2::regoperator::text, '[(][^)]+[)]$')
$_$;


ALTER FUNCTION samplesdb.display_oper(name, oid) OWNER TO postgres;

--
-- Name: do_tap(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION do_tap() RETURNS SETOF text
    LANGUAGE sql
    AS $$
    SELECT * FROM _runem( findfuncs('^test'), _is_verbose());
$$;


ALTER FUNCTION samplesdb.do_tap() OWNER TO postgres;

--
-- Name: do_tap(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION do_tap(name) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM _runem( findfuncs($1, '^test'), _is_verbose() );
$_$;


ALTER FUNCTION samplesdb.do_tap(name) OWNER TO postgres;

--
-- Name: do_tap(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION do_tap(text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM _runem( findfuncs($1), _is_verbose() );
$_$;


ALTER FUNCTION samplesdb.do_tap(text) OWNER TO postgres;

--
-- Name: do_tap(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION do_tap(name, text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM _runem( findfuncs($1, $2), _is_verbose() );
$_$;


ALTER FUNCTION samplesdb.do_tap(name, text) OWNER TO postgres;

--
-- Name: doesnt_imatch(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION doesnt_imatch(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~* $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.doesnt_imatch(anyelement, text) OWNER TO postgres;

--
-- Name: doesnt_imatch(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION doesnt_imatch(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~* $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.doesnt_imatch(anyelement, text, text) OWNER TO postgres;

--
-- Name: doesnt_match(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION doesnt_match(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~ $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.doesnt_match(anyelement, text) OWNER TO postgres;

--
-- Name: doesnt_match(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION doesnt_match(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~ $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.doesnt_match(anyelement, text, text) OWNER TO postgres;

--
-- Name: domain_type_is(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_is(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT domain_type_is(
        $1, $2,
        'Domain ' || $1 || ' should extend type ' || $2
    );
$_$;


ALTER FUNCTION samplesdb.domain_type_is(text, text) OWNER TO postgres;

--
-- Name: domain_type_is(name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_is(name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT domain_type_is(
        $1, $2, $3,
        'Domain ' || quote_ident($1) || '.' || $2
        || ' should extend type ' || $3
    );
$_$;


ALTER FUNCTION samplesdb.domain_type_is(name, text, text) OWNER TO postgres;

--
-- Name: domain_type_is(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_is(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    actual_type TEXT := _get_dtype($1);
BEGIN
    IF actual_type IS NULL THEN
        RETURN fail( $3 ) || E'\n' || diag (
            '   Domain ' ||  $1 || ' does not exist'
        );
    END IF;

    RETURN is( actual_type, _quote_ident_like($2, actual_type), $3 );
END;
$_$;


ALTER FUNCTION samplesdb.domain_type_is(text, text, text) OWNER TO postgres;

--
-- Name: domain_type_is(name, text, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_is(name, text, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT domain_type_is(
        $1, $2, $3, $4,
        'Domain ' || quote_ident($1) || '.' || $2
        || ' should extend type ' || quote_ident($3) || '.' || $4
    );
$_$;


ALTER FUNCTION samplesdb.domain_type_is(name, text, name, text) OWNER TO postgres;

--
-- Name: domain_type_is(name, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_is(name, text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    actual_type TEXT := _get_dtype($1, $2, false);
BEGIN
    IF actual_type IS NULL THEN
        RETURN fail( $4 ) || E'\n' || diag (
            '   Domain ' || quote_ident($1) || '.' || $2
            || ' does not exist'
        );
    END IF;

    RETURN is( actual_type, _quote_ident_like($3, actual_type), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.domain_type_is(name, text, text, text) OWNER TO postgres;

--
-- Name: domain_type_is(name, text, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_is(name, text, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    actual_type TEXT := _get_dtype($1, $2, true);
BEGIN
    IF actual_type IS NULL THEN
        RETURN fail( $5 ) || E'\n' || diag (
            '   Domain ' || quote_ident($1) || '.' || $2
            || ' does not exist'
        );
    END IF;

    RETURN is( actual_type, quote_ident($3) || '.' || _quote_ident_like($4, actual_type), $5 );
END;
$_$;


ALTER FUNCTION samplesdb.domain_type_is(name, text, name, text, text) OWNER TO postgres;

--
-- Name: domain_type_isnt(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_isnt(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT domain_type_isnt(
        $1, $2,
        'Domain ' || $1 || ' should not extend type ' || $2
    );
$_$;


ALTER FUNCTION samplesdb.domain_type_isnt(text, text) OWNER TO postgres;

--
-- Name: domain_type_isnt(name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_isnt(name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT domain_type_isnt(
        $1, $2, $3,
        'Domain ' || quote_ident($1) || '.' || $2
        || ' should not extend type ' || $3
    );
$_$;


ALTER FUNCTION samplesdb.domain_type_isnt(name, text, text) OWNER TO postgres;

--
-- Name: domain_type_isnt(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_isnt(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    actual_type TEXT := _get_dtype($1);
BEGIN
    IF actual_type IS NULL THEN
        RETURN fail( $3 ) || E'\n' || diag (
            '   Domain ' ||  $1 || ' does not exist'
        );
    END IF;

    RETURN isnt( actual_type, _quote_ident_like($2, actual_type), $3 );
END;
$_$;


ALTER FUNCTION samplesdb.domain_type_isnt(text, text, text) OWNER TO postgres;

--
-- Name: domain_type_isnt(name, text, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_isnt(name, text, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT domain_type_isnt(
        $1, $2, $3, $4,
        'Domain ' || quote_ident($1) || '.' || $2
        || ' should not extend type ' || quote_ident($3) || '.' || $4
    );
$_$;


ALTER FUNCTION samplesdb.domain_type_isnt(name, text, name, text) OWNER TO postgres;

--
-- Name: domain_type_isnt(name, text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_isnt(name, text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    actual_type TEXT := _get_dtype($1, $2, false);
BEGIN
    IF actual_type IS NULL THEN
        RETURN fail( $4 ) || E'\n' || diag (
            '   Domain ' || quote_ident($1) || '.' || $2
            || ' does not exist'
        );
    END IF;

    RETURN isnt( actual_type, _quote_ident_like($3, actual_type), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.domain_type_isnt(name, text, text, text) OWNER TO postgres;

--
-- Name: domain_type_isnt(name, text, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domain_type_isnt(name, text, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    actual_type TEXT := _get_dtype($1, $2, true);
BEGIN
    IF actual_type IS NULL THEN
        RETURN fail( $5 ) || E'\n' || diag (
            '   Domain ' || quote_ident($1) || '.' || $2
            || ' does not exist'
        );
    END IF;

    RETURN isnt( actual_type, quote_ident($3) || '.' || _quote_ident_like($4, actual_type), $5 );
END;
$_$;


ALTER FUNCTION samplesdb.domain_type_isnt(name, text, name, text, text) OWNER TO postgres;

--
-- Name: domains_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domains_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, 'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct domains', ARRAY['d'] );
$_$;


ALTER FUNCTION samplesdb.domains_are(name[]) OWNER TO postgres;

--
-- Name: domains_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domains_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, ARRAY['d'] );
$_$;


ALTER FUNCTION samplesdb.domains_are(name[], text) OWNER TO postgres;

--
-- Name: domains_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domains_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, 'Schema ' || quote_ident($1) || ' should have the correct domains', ARRAY['d'] );
$_$;


ALTER FUNCTION samplesdb.domains_are(name, name[]) OWNER TO postgres;

--
-- Name: domains_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION domains_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, $3, ARRAY['d'] );
$_$;


ALTER FUNCTION samplesdb.domains_are(name, name[], text) OWNER TO postgres;

--
-- Name: enum_has_labels(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enum_has_labels(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT enum_has_labels(
        $1, $2,
        'Enum ' || quote_ident($1) || ' should have labels (' || array_to_string( $2, ', ' ) || ')'
    );
$_$;


ALTER FUNCTION samplesdb.enum_has_labels(name, name[]) OWNER TO postgres;

--
-- Name: enum_has_labels(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enum_has_labels(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is(
        ARRAY(
            SELECT e.enumlabel
              FROM pg_catalog.pg_type t
              JOIN pg_catalog.pg_enum e ON t.oid = e.enumtypid
              WHERE t.typisdefined
               AND pg_catalog.pg_type_is_visible(t.oid)
               AND t.typname = $1
               AND t.typtype = 'e'
             ORDER BY e.enumsortorder
        ),
        $2,
        $3
    );
$_$;


ALTER FUNCTION samplesdb.enum_has_labels(name, name[], text) OWNER TO postgres;

--
-- Name: enum_has_labels(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enum_has_labels(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT enum_has_labels(
        $1, $2, $3,
        'Enum ' || quote_ident($1) || '.' || quote_ident($2) || ' should have labels (' || array_to_string( $3, ', ' ) || ')'
    );
$_$;


ALTER FUNCTION samplesdb.enum_has_labels(name, name, name[]) OWNER TO postgres;

--
-- Name: enum_has_labels(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enum_has_labels(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is(
        ARRAY(
            SELECT e.enumlabel
              FROM pg_catalog.pg_type t
              JOIN pg_catalog.pg_enum e      ON t.oid = e.enumtypid
              JOIN pg_catalog.pg_namespace n ON t.typnamespace = n.oid
              WHERE t.typisdefined
               AND n.nspname = $1
               AND t.typname = $2
               AND t.typtype = 'e'
             ORDER BY e.enumsortorder
        ),
        $3,
        $4
    );
$_$;


ALTER FUNCTION samplesdb.enum_has_labels(name, name, name[], text) OWNER TO postgres;

--
-- Name: enums_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enums_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, 'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct enums', ARRAY['e'] );
$_$;


ALTER FUNCTION samplesdb.enums_are(name[]) OWNER TO postgres;

--
-- Name: enums_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enums_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, ARRAY['e'] );
$_$;


ALTER FUNCTION samplesdb.enums_are(name[], text) OWNER TO postgres;

--
-- Name: enums_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enums_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, 'Schema ' || quote_ident($1) || ' should have the correct enums', ARRAY['e'] );
$_$;


ALTER FUNCTION samplesdb.enums_are(name, name[]) OWNER TO postgres;

--
-- Name: enums_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION enums_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, $3, ARRAY['e'] );
$_$;


ALTER FUNCTION samplesdb.enums_are(name, name[], text) OWNER TO postgres;

--
-- Name: fail(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fail() RETURNS text
    LANGUAGE sql
    AS $$
    SELECT ok( FALSE, NULL );
$$;


ALTER FUNCTION samplesdb.fail() OWNER TO postgres;

--
-- Name: fail(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fail(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( FALSE, $1 );
$_$;


ALTER FUNCTION samplesdb.fail(text) OWNER TO postgres;

--
-- Name: fdw_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fdw_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fdw_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on FDW ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.fdw_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: fdw_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fdw_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_fdw_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_fdw' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    FDW ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.fdw_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: findfuncs(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION findfuncs(text) RETURNS text[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT DISTINCT quote_ident(n.nspname) || '.' || quote_ident(p.proname) AS pname
          FROM pg_catalog.pg_proc p
          JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid
         WHERE pg_catalog.pg_function_is_visible(p.oid)
           AND p.proname ~ $1
         ORDER BY pname
    );
$_$;


ALTER FUNCTION samplesdb.findfuncs(text) OWNER TO postgres;

--
-- Name: findfuncs(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION findfuncs(name, text) RETURNS text[]
    LANGUAGE sql
    AS $_$
    SELECT ARRAY(
        SELECT DISTINCT quote_ident(n.nspname) || '.' || quote_ident(p.proname) AS pname
          FROM pg_catalog.pg_proc p
          JOIN pg_catalog.pg_namespace n ON p.pronamespace = n.oid
         WHERE n.nspname = $1
           AND p.proname ~ $2
         ORDER BY pname
    );
$_$;


ALTER FUNCTION samplesdb.findfuncs(name, text) OWNER TO postgres;

--
-- Name: finish(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION finish() RETURNS SETOF text
    LANGUAGE sql
    AS $$
    SELECT * FROM _finish(
        _get('curr_test'),
        _get('plan'),
        num_failed()
    );
$$;


ALTER FUNCTION samplesdb.finish() OWNER TO postgres;

--
-- Name: fk_ok(name, name[], name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name[], name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fk_ok( $1, $2, $3, $4,
        $1 || '(' || _ident_array_to_string( $2, ', ' )
        || ') should reference ' ||
        $3 || '(' || _ident_array_to_string( $4, ', ' ) || ')'
    );
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name[], name, name[]) OWNER TO postgres;

--
-- Name: fk_ok(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fk_ok( $1, ARRAY[$2], $3, ARRAY[$4] );
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name, name, name) OWNER TO postgres;

--
-- Name: fk_ok(name, name[], name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name[], name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    tab  name;
    cols name[];
BEGIN
    SELECT pk_table_name, pk_columns
      FROM pg_all_foreign_keys
     WHERE fk_table_name = $1
       AND fk_columns    = $2
       AND pg_catalog.pg_table_is_visible(fk_table_oid)
      INTO tab, cols;

    RETURN is(
        -- have
        $1 || '(' || _ident_array_to_string( $2, ', ' )
        || ') REFERENCES ' || COALESCE( tab || '(' || _ident_array_to_string( cols, ', ' ) || ')', 'NOTHING'),
        -- want
        $1 || '(' || _ident_array_to_string( $2, ', ' )
        || ') REFERENCES ' ||
        $3 || '(' || _ident_array_to_string( $4, ', ' ) || ')',
        $5
    );
END;
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name[], name, name[], text) OWNER TO postgres;

--
-- Name: fk_ok(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fk_ok( $1, ARRAY[$2], $3, ARRAY[$4], $5 );
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name, name, name, text) OWNER TO postgres;

--
-- Name: fk_ok(name, name, name[], name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name, name[], name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fk_ok( $1, $2, $3, $4, $5, $6,
        quote_ident($1) || '.' || quote_ident($2) || '(' || _ident_array_to_string( $3, ', ' )
        || ') should reference ' ||
        $4 || '.' || $5 || '(' || _ident_array_to_string( $6, ', ' ) || ')'
    );
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name, name[], name, name, name[]) OWNER TO postgres;

--
-- Name: fk_ok(name, name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fk_ok( $1, $2, ARRAY[$3], $4, $5, ARRAY[$6] );
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name, name, name, name, text) OWNER TO postgres;

--
-- Name: fk_ok(name, name, name[], name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name, name[], name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    sch  name;
    tab  name;
    cols name[];
BEGIN
    SELECT pk_schema_name, pk_table_name, pk_columns
      FROM pg_all_foreign_keys
      WHERE fk_schema_name = $1
        AND fk_table_name  = $2
        AND fk_columns     = $3
      INTO sch, tab, cols;

    RETURN is(
        -- have
        quote_ident($1) || '.' || quote_ident($2) || '(' || _ident_array_to_string( $3, ', ' )
        || ') REFERENCES ' || COALESCE ( sch || '.' || tab || '(' || _ident_array_to_string( cols, ', ' ) || ')', 'NOTHING' ),
        -- want
        quote_ident($1) || '.' || quote_ident($2) || '(' || _ident_array_to_string( $3, ', ' )
        || ') REFERENCES ' ||
        $4 || '.' || $5 || '(' || _ident_array_to_string( $6, ', ' ) || ')',
        $7
    );
END;
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name, name[], name, name, name[], text) OWNER TO postgres;

--
-- Name: fk_ok(name, name, name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION fk_ok(name, name, name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT fk_ok( $1, $2, ARRAY[$3], $4, $5, ARRAY[$6], $7 );
$_$;


ALTER FUNCTION samplesdb.fk_ok(name, name, name, name, name, name, text) OWNER TO postgres;

--
-- Name: foreign_table_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_table_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT foreign_table_owner_is(
        $1, $2,
        'Foreign table ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.foreign_table_owner_is(name, name) OWNER TO postgres;

--
-- Name: foreign_table_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_table_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT foreign_table_owner_is(
        $1, $2, $3,
        'Foreign table ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.foreign_table_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: foreign_table_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_table_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('f'::char, $1);
BEGIN
    -- Make sure the table exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Foreign table ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.foreign_table_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: foreign_table_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_table_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('f'::char, $1, $2);
BEGIN
    -- Make sure the table exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Foreign table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.foreign_table_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: foreign_tables_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_tables_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'foreign tables', _extras('f', $1), _missing('f', $1),
        'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct foreign tables'
    );
$_$;


ALTER FUNCTION samplesdb.foreign_tables_are(name[]) OWNER TO postgres;

--
-- Name: foreign_tables_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_tables_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'foreign tables', _extras('f', $1), _missing('f', $1), $2);
$_$;


ALTER FUNCTION samplesdb.foreign_tables_are(name[], text) OWNER TO postgres;

--
-- Name: foreign_tables_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_tables_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'foreign tables', _extras('f', $1, $2), _missing('f', $1, $2),
        'Schema ' || quote_ident($1) || ' should have the correct foreign tables'
    );
$_$;


ALTER FUNCTION samplesdb.foreign_tables_are(name, name[]) OWNER TO postgres;

--
-- Name: foreign_tables_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION foreign_tables_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'foreign tables', _extras('f', $1, $2), _missing('f', $1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.foreign_tables_are(name, name[], text) OWNER TO postgres;

--
-- Name: function_lang_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_lang_is(
        $1, $2,
        'Function ' || quote_ident($1)
        || '() should be written in ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name) OWNER TO postgres;

--
-- Name: function_lang_is(name, name[], name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name[], name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_lang_is(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should be written in ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name[], name) OWNER TO postgres;

--
-- Name: function_lang_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_lang_is(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '.' || quote_ident($2)
        || '() should be written in ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name, name) OWNER TO postgres;

--
-- Name: function_lang_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, _lang($1), $2, $3 );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name, text) OWNER TO postgres;

--
-- Name: function_lang_is(name, name[], name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name[], name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, _lang($1, $2), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name[], name, text) OWNER TO postgres;

--
-- Name: function_lang_is(name, name, name[], name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name, name[], name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_lang_is(
        $1, $2, $3, $4,
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should be written in ' || quote_ident($4)
    );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name, name[], name) OWNER TO postgres;

--
-- Name: function_lang_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, _lang($1, $2), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name, name, text) OWNER TO postgres;

--
-- Name: function_lang_is(name, name, name[], name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_lang_is(name, name, name[], name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, _lang($1, $2, $3), $4, $5 );
$_$;


ALTER FUNCTION samplesdb.function_lang_is(name, name, name[], name, text) OWNER TO postgres;

--
-- Name: function_owner_is(name, name[], name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_owner_is(name, name[], name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_owner_is(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.function_owner_is(name, name[], name) OWNER TO postgres;

--
-- Name: function_owner_is(name, name[], name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_owner_is(name, name[], name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_func_owner($1, $2);
BEGIN
    -- Make sure the function exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Function ' || quote_ident($1) || '(' ||
                    array_to_string($2, ', ') || ') does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.function_owner_is(name, name[], name, text) OWNER TO postgres;

--
-- Name: function_owner_is(name, name, name[], name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_owner_is(name, name, name[], name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_owner_is(
        $1, $2, $3, $4,
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should be owned by ' || quote_ident($4)
    );
$_$;


ALTER FUNCTION samplesdb.function_owner_is(name, name, name[], name) OWNER TO postgres;

--
-- Name: function_owner_is(name, name, name[], name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_owner_is(name, name, name[], name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_func_owner($1, $2, $3);
BEGIN
    -- Make sure the function exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            E'    Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
                    array_to_string($3, ', ') || ') does not exist'
        );
    END IF;

    RETURN is(owner, $4, $5);
END;
$_$;


ALTER FUNCTION samplesdb.function_owner_is(name, name, name[], name, text) OWNER TO postgres;

--
-- Name: function_privs_are(name, name[], name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_privs_are(name, name[], name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_privs_are(
        $1, $2, $3, $4,
        'Role ' || quote_ident($3) || ' should be granted '
            || CASE WHEN $4[1] IS NULL THEN 'no privileges' ELSE array_to_string($4, ', ') END
            || ' on function ' || quote_ident($1) || '(' || array_to_string($2, ', ') || ')'
    );
$_$;


ALTER FUNCTION samplesdb.function_privs_are(name, name[], name, name[]) OWNER TO postgres;

--
-- Name: function_privs_are(name, name[], name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_privs_are(name, name[], name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _fprivs_are(
        quote_ident($1) || '(' || array_to_string($2, ', ') || ')',
        $3, $4, $5
    );
$_$;


ALTER FUNCTION samplesdb.function_privs_are(name, name[], name, name[], text) OWNER TO postgres;

--
-- Name: function_privs_are(name, name, name[], name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_privs_are(name, name, name[], name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_privs_are(
        $1, $2, $3, $4, $5,
        'Role ' || quote_ident($4) || ' should be granted '
            || CASE WHEN $5[1] IS NULL THEN 'no privileges' ELSE array_to_string($5, ', ') END
            || ' on function ' || quote_ident($1) || '.' || quote_ident($2)
            || '(' || array_to_string($3, ', ') || ')'
    );
$_$;


ALTER FUNCTION samplesdb.function_privs_are(name, name, name[], name, name[]) OWNER TO postgres;

--
-- Name: function_privs_are(name, name, name[], name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_privs_are(name, name, name[], name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _fprivs_are(
        quote_ident($1) || '.' || quote_ident($2) || '(' || array_to_string($3, ', ') || ')',
        $4, $5, $6
    );
$_$;


ALTER FUNCTION samplesdb.function_privs_are(name, name, name[], name, name[], text) OWNER TO postgres;

--
-- Name: function_returns(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_returns(
        $1, $2,
        'Function ' || quote_ident($1) || '() should return ' || $2
    );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, text) OWNER TO postgres;

--
-- Name: function_returns(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_returns(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should return ' || $3
    );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, name[], text) OWNER TO postgres;

--
-- Name: function_returns(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_returns(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '.' || quote_ident($2)
        || '() should return ' || $3
    );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, name, text) OWNER TO postgres;

--
-- Name: function_returns(name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, _returns($1), $2, $3 );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, text, text) OWNER TO postgres;

--
-- Name: function_returns(name, name[], text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, name[], text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, _returns($1, $2), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, name[], text, text) OWNER TO postgres;

--
-- Name: function_returns(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT function_returns(
        $1, $2, $3, $4,
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should return ' || $4
    );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, name, name[], text) OWNER TO postgres;

--
-- Name: function_returns(name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, _returns($1, $2), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, name, text, text) OWNER TO postgres;

--
-- Name: function_returns(name, name, name[], text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION function_returns(name, name, name[], text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, _returns($1, $2, $3), $4, $5 );
$_$;


ALTER FUNCTION samplesdb.function_returns(name, name, name[], text, text) OWNER TO postgres;

--
-- Name: functions_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION functions_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT functions_are( $1, 'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct functions' );
$_$;


ALTER FUNCTION samplesdb.functions_are(name[]) OWNER TO postgres;

--
-- Name: functions_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION functions_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'functions',
        ARRAY(
            SELECT name FROM tap_funky WHERE is_visible
            AND schema NOT IN ('pg_catalog', 'information_schema')
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
               FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT name FROM tap_funky WHERE is_visible
            AND schema NOT IN ('pg_catalog', 'information_schema')
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.functions_are(name[], text) OWNER TO postgres;

--
-- Name: functions_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION functions_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT functions_are( $1, $2, 'Schema ' || quote_ident($1) || ' should have the correct functions' );
$_$;


ALTER FUNCTION samplesdb.functions_are(name, name[]) OWNER TO postgres;

--
-- Name: functions_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION functions_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'functions',
        ARRAY(
            SELECT name FROM tap_funky WHERE schema = $1
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
               FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT name FROM tap_funky WHERE schema = $1
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.functions_are(name, name[], text) OWNER TO postgres;

--
-- Name: groups_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION groups_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT groups_are( $1, 'There should be the correct groups' );
$_$;


ALTER FUNCTION samplesdb.groups_are(name[]) OWNER TO postgres;

--
-- Name: groups_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION groups_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'groups',
        ARRAY(
            SELECT groname
              FROM pg_catalog.pg_group
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT groname
              FROM pg_catalog.pg_group
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.groups_are(name[], text) OWNER TO postgres;

--
-- Name: has_cast(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_cast(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _cast_exists( $1, $2 ),
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_cast(name, name) OWNER TO postgres;

--
-- Name: has_cast(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_cast(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok(
        _cast_exists( $1, $2, $3 ),
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') WITH FUNCTION ' || quote_ident($3) || '() should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_cast(name, name, name) OWNER TO postgres;

--
-- Name: has_cast(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_cast(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _cast_exists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_cast(name, name, text) OWNER TO postgres;

--
-- Name: has_cast(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_cast(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok(
       _cast_exists( $1, $2, $3, $4 ),
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') WITH FUNCTION ' || quote_ident($3)
        || '.' || quote_ident($4) || '() should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_cast(name, name, name, name) OWNER TO postgres;

--
-- Name: has_cast(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_cast(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok( _cast_exists( $1, $2, $3 ), $4 );
$_$;


ALTER FUNCTION samplesdb.has_cast(name, name, name, text) OWNER TO postgres;

--
-- Name: has_cast(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_cast(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok( _cast_exists( $1, $2, $3, $4 ), $5 );
$_$;


ALTER FUNCTION samplesdb.has_cast(name, name, name, name, text) OWNER TO postgres;

--
-- Name: has_check(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_check(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_check( $1, 'Table ' || quote_ident($1) || ' should have a check constraint' );
$_$;


ALTER FUNCTION samplesdb.has_check(name) OWNER TO postgres;

--
-- Name: has_check(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_check(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, 'c' ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_check(name, text) OWNER TO postgres;

--
-- Name: has_check(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_check(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, $2, 'c' ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_check(name, name, text) OWNER TO postgres;

--
-- Name: has_column(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_column(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_column( $1, $2, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_column(name, name) OWNER TO postgres;

--
-- Name: has_column(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_column(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _cexists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_column(name, name, text) OWNER TO postgres;

--
-- Name: has_column(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_column(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _cexists( $1, $2, $3 ), $4 );
$_$;


ALTER FUNCTION samplesdb.has_column(name, name, name, text) OWNER TO postgres;

--
-- Name: has_composite(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_composite(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_composite( $1, 'Composite type ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_composite(name) OWNER TO postgres;

--
-- Name: has_composite(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_composite(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'c', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_composite(name, text) OWNER TO postgres;

--
-- Name: has_composite(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_composite(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'c', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_composite(name, name, text) OWNER TO postgres;

--
-- Name: has_domain(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_domain(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, ARRAY['d'] ), ('Domain ' || quote_ident($1) || ' should exist')::text );
$_$;


ALTER FUNCTION samplesdb.has_domain(name) OWNER TO postgres;

--
-- Name: has_domain(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_domain(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_domain( $1, $2, 'Domain ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_domain(name, name) OWNER TO postgres;

--
-- Name: has_domain(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_domain(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, ARRAY['d'] ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_domain(name, text) OWNER TO postgres;

--
-- Name: has_domain(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_domain(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, $2, ARRAY['d'] ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_domain(name, name, text) OWNER TO postgres;

--
-- Name: has_enum(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_enum(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, ARRAY['e'] ), ('Enum ' || quote_ident($1) || ' should exist')::text );
$_$;


ALTER FUNCTION samplesdb.has_enum(name) OWNER TO postgres;

--
-- Name: has_enum(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_enum(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_enum( $1, $2, 'Enum ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_enum(name, name) OWNER TO postgres;

--
-- Name: has_enum(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_enum(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, ARRAY['e'] ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_enum(name, text) OWNER TO postgres;

--
-- Name: has_enum(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_enum(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, $2, ARRAY['e'] ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_enum(name, name, text) OWNER TO postgres;

--
-- Name: has_fk(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_fk(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_fk( $1, 'Table ' || quote_ident($1) || ' should have a foreign key constraint' );
$_$;


ALTER FUNCTION samplesdb.has_fk(name) OWNER TO postgres;

--
-- Name: has_fk(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_fk(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, 'f' ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_fk(name, text) OWNER TO postgres;

--
-- Name: has_fk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_fk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, $2, 'f' ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_fk(name, name, text) OWNER TO postgres;

--
-- Name: has_foreign_table(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_foreign_table(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_foreign_table( $1, 'Foreign table ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_foreign_table(name) OWNER TO postgres;

--
-- Name: has_foreign_table(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_foreign_table(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _rexists( 'f', $1, $2 ),
        'Foreign table ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_foreign_table(name, name) OWNER TO postgres;

--
-- Name: has_foreign_table(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_foreign_table(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'f', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_foreign_table(name, text) OWNER TO postgres;

--
-- Name: has_foreign_table(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_foreign_table(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'f', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_foreign_table(name, name, text) OWNER TO postgres;

--
-- Name: has_function(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _got_func($1), 'Function ' || quote_ident($1) || '() should exist' );
$_$;


ALTER FUNCTION samplesdb.has_function(name) OWNER TO postgres;

--
-- Name: has_function(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _got_func($1, $2),
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_function(name, name[]) OWNER TO postgres;

--
-- Name: has_function(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _got_func($1, $2),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '() should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_function(name, name) OWNER TO postgres;

--
-- Name: has_function(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _got_func($1), $2 );
$_$;


ALTER FUNCTION samplesdb.has_function(name, text) OWNER TO postgres;

--
-- Name: has_function(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _got_func($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.has_function(name, name[], text) OWNER TO postgres;

--
-- Name: has_function(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _got_func($1, $2, $3),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_function(name, name, name[]) OWNER TO postgres;

--
-- Name: has_function(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _got_func($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.has_function(name, name, text) OWNER TO postgres;

--
-- Name: has_function(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_function(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _got_func($1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.has_function(name, name, name[], text) OWNER TO postgres;

--
-- Name: has_group(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_group(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_group($1), 'Group ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_group(name) OWNER TO postgres;

--
-- Name: has_group(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_group(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_group($1), $2 );
$_$;


ALTER FUNCTION samplesdb.has_group(name, text) OWNER TO postgres;

--
-- Name: has_index(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _have_index( $1, $2 ), 'Index ' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_index(name, name) OWNER TO postgres;

--
-- Name: has_index(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT has_index( $1, $2, $3, 'Index ' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name[]) OWNER TO postgres;

--
-- Name: has_index(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
   IF _is_schema($1) THEN
       -- ( schema, table, index )
       RETURN ok( _have_index( $1, $2, $3 ), 'Index ' || quote_ident($3) || ' should exist' );
   ELSE
       -- ( table, index, column/expression )
       RETURN has_index( $1, $2, $3, 'Index ' || quote_ident($2) || ' should exist' );
   END IF;
END;
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name) OWNER TO postgres;

--
-- Name: has_index(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN $3 LIKE '%(%'
           THEN has_index( $1, $2, $3::name )
           ELSE ok( _have_index( $1, $2 ), $3 )
           END;
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, text) OWNER TO postgres;

--
-- Name: has_index(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
     index_cols name[];
BEGIN
    index_cols := _ikeys($1, $2 );

    IF index_cols IS NULL OR index_cols = '{}'::name[] THEN
        RETURN ok( false, $4 ) || E'\n'
            || diag( 'Index ' || quote_ident($2) || ' ON ' || quote_ident($1) || ' not found');
    END IF;

    RETURN is(
        quote_ident($2) || ' ON ' || quote_ident($1) || '(' || array_to_string( index_cols, ', ' ) || ')',
        quote_ident($2) || ' ON ' || quote_ident($1) || '(' || array_to_string( $3, ', ' ) || ')',
        $4
    );
END;
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name[], text) OWNER TO postgres;

--
-- Name: has_index(name, name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT has_index( $1, $2, $3, $4, 'Index ' || quote_ident($3) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name, name[]) OWNER TO postgres;

--
-- Name: has_index(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT has_index( $1, $2, $3, $4, 'Index ' || quote_ident($3) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name, name) OWNER TO postgres;

--
-- Name: has_index(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT CASE WHEN _is_schema( $1 ) THEN
        -- Looking for schema.table index.
            ok ( _have_index( $1, $2, $3 ), $4)
        ELSE
        -- Looking for particular columns.
            has_index( $1, $2, ARRAY[$3], $4 )
      END;
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name, text) OWNER TO postgres;

--
-- Name: has_index(name, name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
     index_cols name[];
BEGIN
    index_cols := _ikeys($1, $2, $3 );

    IF index_cols IS NULL OR index_cols = '{}'::name[] THEN
        RETURN ok( false, $5 ) || E'\n'
            || diag( 'Index ' || quote_ident($3) || ' ON ' || quote_ident($1) || '.' || quote_ident($2) || ' not found');
    END IF;

    RETURN is(
        quote_ident($3) || ' ON ' || quote_ident($1) || '.' || quote_ident($2) || '(' || array_to_string( index_cols, ', ' ) || ')',
        quote_ident($3) || ' ON ' || quote_ident($1) || '.' || quote_ident($2) || '(' || array_to_string( $4, ', ' ) || ')',
        $5
    );
END;
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name, name[], text) OWNER TO postgres;

--
-- Name: has_index(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_index(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_index( $1, $2, $3, ARRAY[$4], $5 );
$_$;


ALTER FUNCTION samplesdb.has_index(name, name, name, name, text) OWNER TO postgres;

--
-- Name: has_language(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_language(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_trusted($1) IS NOT NULL, 'Procedural language ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_language(name) OWNER TO postgres;

--
-- Name: has_language(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_language(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_trusted($1) IS NOT NULL, $2 );
$_$;


ALTER FUNCTION samplesdb.has_language(name, text) OWNER TO postgres;

--
-- Name: has_leftop(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_leftop(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists(NULL, $1, $2 ),
        'Left operator ' || $1 || '(NONE,' || $2 || ') should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_leftop(name, name) OWNER TO postgres;

--
-- Name: has_leftop(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_leftop(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists(NULL, $1, $2, $3 ),
        'Left operator ' || $1 || '(NONE,' || $2 || ') RETURNS ' || $3 || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_leftop(name, name, name) OWNER TO postgres;

--
-- Name: has_leftop(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_leftop(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists(NULL, $1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.has_leftop(name, name, text) OWNER TO postgres;

--
-- Name: has_leftop(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_leftop(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists(NULL, $1, $2, $3, $4 ),
        'Left operator ' || quote_ident($1) || '.' || $2 || '(NONE,'
        || $3 || ') RETURNS ' || $4 || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_leftop(name, name, name, name) OWNER TO postgres;

--
-- Name: has_leftop(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_leftop(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists(NULL, $1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.has_leftop(name, name, name, text) OWNER TO postgres;

--
-- Name: has_leftop(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_leftop(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists(NULL, $1, $2, $3, $4), $5 );
$_$;


ALTER FUNCTION samplesdb.has_leftop(name, name, name, name, text) OWNER TO postgres;

--
-- Name: has_materialized_view(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_materialized_view(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_materialized_view( $1, 'Materialized view ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_materialized_view(name) OWNER TO postgres;

--
-- Name: has_materialized_view(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_materialized_view(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'm', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_materialized_view(name, text) OWNER TO postgres;

--
-- Name: has_materialized_view(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_materialized_view(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'm', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_materialized_view(name, name, text) OWNER TO postgres;

--
-- Name: has_opclass(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_opclass(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _opc_exists( $1 ), 'Operator class ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_opclass(name) OWNER TO postgres;

--
-- Name: has_opclass(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_opclass(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _opc_exists( $1, $2 ), 'Operator class ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_opclass(name, name) OWNER TO postgres;

--
-- Name: has_opclass(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_opclass(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _opc_exists( $1 ), $2)
$_$;


ALTER FUNCTION samplesdb.has_opclass(name, text) OWNER TO postgres;

--
-- Name: has_opclass(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_opclass(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _opc_exists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_opclass(name, name, text) OWNER TO postgres;

--
-- Name: has_operator(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_operator(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists($1, $2, $3 ),
        'Operator ' ||  $2 || '(' || $1 || ',' || $3
        || ') should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_operator(name, name, name) OWNER TO postgres;

--
-- Name: has_operator(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_operator(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists($1, $2, $3, $4 ),
        'Operator ' ||  $2 || '(' || $1 || ',' || $3
        || ') RETURNS ' || $4 || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_operator(name, name, name, name) OWNER TO postgres;

--
-- Name: has_operator(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_operator(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists($1, $2, $3 ), $4 );
$_$;


ALTER FUNCTION samplesdb.has_operator(name, name, name, text) OWNER TO postgres;

--
-- Name: has_operator(name, name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_operator(name, name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists($1, $2, $3, $4, $5 ),
        'Operator ' || quote_ident($2) || '.' || $3 || '(' || $1 || ',' || $4
        || ') RETURNS ' || $5 || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_operator(name, name, name, name, name) OWNER TO postgres;

--
-- Name: has_operator(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_operator(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists($1, $2, $3, $4 ), $5 );
$_$;


ALTER FUNCTION samplesdb.has_operator(name, name, name, name, text) OWNER TO postgres;

--
-- Name: has_operator(name, name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_operator(name, name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists($1, $2, $3, $4, $5 ), $6 );
$_$;


ALTER FUNCTION samplesdb.has_operator(name, name, name, name, name, text) OWNER TO postgres;

--
-- Name: has_pk(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_pk(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_pk( $1, 'Table ' || quote_ident($1) || ' should have a primary key' );
$_$;


ALTER FUNCTION samplesdb.has_pk(name) OWNER TO postgres;

--
-- Name: has_pk(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_pk(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, 'p' ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_pk(name, text) OWNER TO postgres;

--
-- Name: has_pk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_pk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, $2, 'p' ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_pk(name, name, text) OWNER TO postgres;

--
-- Name: has_relation(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_relation(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_relation( $1, 'Relation ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_relation(name) OWNER TO postgres;

--
-- Name: has_relation(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_relation(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _relexists( $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_relation(name, text) OWNER TO postgres;

--
-- Name: has_relation(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_relation(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _relexists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_relation(name, name, text) OWNER TO postgres;

--
-- Name: has_rightop(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rightop(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists($1, $2, NULL ),
        'Right operator ' || $2 || '(' || $1 || ',NONE) should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_rightop(name, name) OWNER TO postgres;

--
-- Name: has_rightop(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rightop(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists($1, $2, NULL, $3 ),
        'Right operator ' || $2 || '('
        || $1 || ',NONE) RETURNS ' || $3 || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_rightop(name, name, name) OWNER TO postgres;

--
-- Name: has_rightop(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rightop(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists( $1, $2, NULL), $3 );
$_$;


ALTER FUNCTION samplesdb.has_rightop(name, name, text) OWNER TO postgres;

--
-- Name: has_rightop(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rightop(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
         _op_exists($1, $2, $3, NULL, $4 ),
        'Right operator ' || quote_ident($2) || '.' || $3 || '('
        || $1 || ',NONE) RETURNS ' || $4 || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_rightop(name, name, name, name) OWNER TO postgres;

--
-- Name: has_rightop(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rightop(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists( $1, $2, NULL, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.has_rightop(name, name, name, text) OWNER TO postgres;

--
-- Name: has_rightop(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rightop(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _op_exists( $1, $2, $3, NULL, $4), $5 );
$_$;


ALTER FUNCTION samplesdb.has_rightop(name, name, name, name, text) OWNER TO postgres;

--
-- Name: has_role(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_role(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_role($1), 'Role ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_role(name) OWNER TO postgres;

--
-- Name: has_role(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_role(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_role($1), $2 );
$_$;


ALTER FUNCTION samplesdb.has_role(name, text) OWNER TO postgres;

--
-- Name: has_rule(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rule(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2) IS NOT NULL, 'Relation ' || quote_ident($1) || ' should have rule ' || quote_ident($2) );
$_$;


ALTER FUNCTION samplesdb.has_rule(name, name) OWNER TO postgres;

--
-- Name: has_rule(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rule(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2, $3) IS NOT NULL, 'Relation ' || quote_ident($1) || '.' || quote_ident($2) || ' should have rule ' || quote_ident($3) );
$_$;


ALTER FUNCTION samplesdb.has_rule(name, name, name) OWNER TO postgres;

--
-- Name: has_rule(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rule(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2) IS NOT NULL, $3 );
$_$;


ALTER FUNCTION samplesdb.has_rule(name, name, text) OWNER TO postgres;

--
-- Name: has_rule(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_rule(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2, $3) IS NOT NULL, $4 );
$_$;


ALTER FUNCTION samplesdb.has_rule(name, name, name, text) OWNER TO postgres;

--
-- Name: has_schema(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_schema(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_schema( $1, 'Schema ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_schema(name) OWNER TO postgres;

--
-- Name: has_schema(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_schema(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        EXISTS(
            SELECT true
              FROM pg_catalog.pg_namespace
             WHERE nspname = $1
        ), $2
    );
$_$;


ALTER FUNCTION samplesdb.has_schema(name, text) OWNER TO postgres;

--
-- Name: has_sequence(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_sequence(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_sequence( $1, 'Sequence ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_sequence(name) OWNER TO postgres;

--
-- Name: has_sequence(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_sequence(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _rexists( 'S', $1, $2 ),
        'Sequence ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_sequence(name, name) OWNER TO postgres;

--
-- Name: has_sequence(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_sequence(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'S', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_sequence(name, text) OWNER TO postgres;

--
-- Name: has_sequence(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_sequence(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'S', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_sequence(name, name, text) OWNER TO postgres;

--
-- Name: has_table(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_table(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_table( $1, 'Table ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_table(name) OWNER TO postgres;

--
-- Name: has_table(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_table(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _rexists( 'r', $1, $2 ),
        'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist'
    );
$_$;


ALTER FUNCTION samplesdb.has_table(name, name) OWNER TO postgres;

--
-- Name: has_table(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_table(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'r', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_table(name, text) OWNER TO postgres;

--
-- Name: has_table(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_table(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'r', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_table(name, name, text) OWNER TO postgres;

--
-- Name: has_tablespace(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_tablespace(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_tablespace( $1, 'Tablespace ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_tablespace(name) OWNER TO postgres;

--
-- Name: has_tablespace(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_tablespace(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        EXISTS(
            SELECT true
              FROM pg_catalog.pg_tablespace
             WHERE spcname = $1
        ), $2
    );
$_$;


ALTER FUNCTION samplesdb.has_tablespace(name, text) OWNER TO postgres;

--
-- Name: has_tablespace(name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_tablespace(name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF pg_version_num() >= 90200 THEN
        RETURN ok(
            EXISTS(
                SELECT true
                  FROM pg_catalog.pg_tablespace
                 WHERE spcname = $1
                   AND pg_tablespace_location(oid) = $2
            ), $3
        );
    ELSE
        RETURN ok(
            EXISTS(
                SELECT true
                  FROM pg_catalog.pg_tablespace
                 WHERE spcname = $1
                   AND spclocation = $2
            ), $3
        );
    END IF;
END;
$_$;


ALTER FUNCTION samplesdb.has_tablespace(name, text, text) OWNER TO postgres;

--
-- Name: has_trigger(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_trigger(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _trig($1, $2), 'Table ' || quote_ident($1) || ' should have trigger ' || quote_ident($2));
$_$;


ALTER FUNCTION samplesdb.has_trigger(name, name) OWNER TO postgres;

--
-- Name: has_trigger(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_trigger(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_trigger(
        $1, $2, $3,
        'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should have trigger ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.has_trigger(name, name, name) OWNER TO postgres;

--
-- Name: has_trigger(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_trigger(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _trig($1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.has_trigger(name, name, text) OWNER TO postgres;

--
-- Name: has_trigger(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_trigger(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _trig($1, $2, $3), $4);
$_$;


ALTER FUNCTION samplesdb.has_trigger(name, name, name, text) OWNER TO postgres;

--
-- Name: has_type(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_type(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, NULL ), ('Type ' || quote_ident($1) || ' should exist')::text );
$_$;


ALTER FUNCTION samplesdb.has_type(name) OWNER TO postgres;

--
-- Name: has_type(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_type(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_type( $1, $2, 'Type ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_type(name, name) OWNER TO postgres;

--
-- Name: has_type(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_type(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, NULL ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_type(name, text) OWNER TO postgres;

--
-- Name: has_type(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_type(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_type( $1, $2, NULL ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_type(name, name, text) OWNER TO postgres;

--
-- Name: has_unique(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_unique(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_unique( $1, 'Table ' || quote_ident($1) || ' should have a unique constraint' );
$_$;


ALTER FUNCTION samplesdb.has_unique(text) OWNER TO postgres;

--
-- Name: has_unique(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_unique(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, 'u' ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_unique(text, text) OWNER TO postgres;

--
-- Name: has_unique(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_unique(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _hasc( $1, $2, 'u' ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_unique(text, text, text) OWNER TO postgres;

--
-- Name: has_user(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_user(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_user( $1 ), 'User ' || quote_ident($1) || ' should exist');
$_$;


ALTER FUNCTION samplesdb.has_user(name) OWNER TO postgres;

--
-- Name: has_user(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_user(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _has_user($1), $2 );
$_$;


ALTER FUNCTION samplesdb.has_user(name, text) OWNER TO postgres;

--
-- Name: has_view(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_view(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT has_view( $1, 'View ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.has_view(name) OWNER TO postgres;

--
-- Name: has_view(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_view(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'v', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.has_view(name, text) OWNER TO postgres;

--
-- Name: has_view(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION has_view(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _rexists( 'v', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.has_view(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_cast(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_cast(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _cast_exists( $1, $2 ),
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_cast(name, name) OWNER TO postgres;

--
-- Name: hasnt_cast(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_cast(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok(
        NOT _cast_exists( $1, $2, $3 ),
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') WITH FUNCTION ' || quote_ident($3) || '() should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_cast(name, name, name) OWNER TO postgres;

--
-- Name: hasnt_cast(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_cast(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _cast_exists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_cast(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_cast(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_cast(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok(
       NOT _cast_exists( $1, $2, $3, $4 ),
        'Cast (' || quote_ident($1) || ' AS ' || quote_ident($2)
        || ') WITH FUNCTION ' || quote_ident($3)
        || '.' || quote_ident($4) || '() should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_cast(name, name, name, name) OWNER TO postgres;

--
-- Name: hasnt_cast(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_cast(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok( NOT _cast_exists( $1, $2, $3 ), $4 );
$_$;


ALTER FUNCTION samplesdb.hasnt_cast(name, name, name, text) OWNER TO postgres;

--
-- Name: hasnt_cast(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_cast(name, name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
   SELECT ok( NOT _cast_exists( $1, $2, $3, $4 ), $5 );
$_$;


ALTER FUNCTION samplesdb.hasnt_cast(name, name, name, name, text) OWNER TO postgres;

--
-- Name: hasnt_column(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_column(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_column( $1, $2, 'Column ' || quote_ident($1) || '.' || quote_ident($2) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_column(name, name) OWNER TO postgres;

--
-- Name: hasnt_column(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_column(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _cexists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_column(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_column(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_column(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _cexists( $1, $2, $3 ), $4 );
$_$;


ALTER FUNCTION samplesdb.hasnt_column(name, name, name, text) OWNER TO postgres;

--
-- Name: hasnt_composite(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_composite(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_composite( $1, 'Composite type ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_composite(name) OWNER TO postgres;

--
-- Name: hasnt_composite(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_composite(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'c', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_composite(name, text) OWNER TO postgres;

--
-- Name: hasnt_composite(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_composite(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'c', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_composite(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_domain(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_domain(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, ARRAY['d'] ), ('Domain ' || quote_ident($1) || ' should not exist')::text );
$_$;


ALTER FUNCTION samplesdb.hasnt_domain(name) OWNER TO postgres;

--
-- Name: hasnt_domain(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_domain(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_domain( $1, $2, 'Domain ' || quote_ident($1) || '.' || quote_ident($2) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_domain(name, name) OWNER TO postgres;

--
-- Name: hasnt_domain(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_domain(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, ARRAY['d'] ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_domain(name, text) OWNER TO postgres;

--
-- Name: hasnt_domain(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_domain(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, $2, ARRAY['d'] ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_domain(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_enum(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_enum(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, ARRAY['e'] ), ('Enum ' || quote_ident($1) || ' should not exist')::text );
$_$;


ALTER FUNCTION samplesdb.hasnt_enum(name) OWNER TO postgres;

--
-- Name: hasnt_enum(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_enum(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_enum( $1, $2, 'Enum ' || quote_ident($1) || '.' || quote_ident($2) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_enum(name, name) OWNER TO postgres;

--
-- Name: hasnt_enum(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_enum(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, ARRAY['e'] ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_enum(name, text) OWNER TO postgres;

--
-- Name: hasnt_enum(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_enum(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, $2, ARRAY['e'] ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_enum(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_fk(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_fk(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_fk( $1, 'Table ' || quote_ident($1) || ' should not have a foreign key constraint' );
$_$;


ALTER FUNCTION samplesdb.hasnt_fk(name) OWNER TO postgres;

--
-- Name: hasnt_fk(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_fk(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _hasc( $1, 'f' ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_fk(name, text) OWNER TO postgres;

--
-- Name: hasnt_fk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_fk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _hasc( $1, $2, 'f' ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_fk(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_foreign_table(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_foreign_table(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_foreign_table( $1, 'Foreign table ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_foreign_table(name) OWNER TO postgres;

--
-- Name: hasnt_foreign_table(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_foreign_table(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _rexists( 'f', $1, $2 ),
        'Foreign table ' || quote_ident($1) || '.' || quote_ident($2) || ' should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_foreign_table(name, name) OWNER TO postgres;

--
-- Name: hasnt_foreign_table(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_foreign_table(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'f', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_foreign_table(name, text) OWNER TO postgres;

--
-- Name: hasnt_foreign_table(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_foreign_table(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'f', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_foreign_table(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_function(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _got_func($1), 'Function ' || quote_ident($1) || '() should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name) OWNER TO postgres;

--
-- Name: hasnt_function(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _got_func($1, $2),
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, name[]) OWNER TO postgres;

--
-- Name: hasnt_function(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _got_func($1, $2),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '() should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, name) OWNER TO postgres;

--
-- Name: hasnt_function(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _got_func($1), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, text) OWNER TO postgres;

--
-- Name: hasnt_function(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _got_func($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, name[], text) OWNER TO postgres;

--
-- Name: hasnt_function(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _got_func($1, $2, $3),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, name, name[]) OWNER TO postgres;

--
-- Name: hasnt_function(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _got_func($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_function(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_function(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _got_func($1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.hasnt_function(name, name, name[], text) OWNER TO postgres;

--
-- Name: hasnt_group(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_group(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_group($1), 'Group ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_group(name) OWNER TO postgres;

--
-- Name: hasnt_group(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_group(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_group($1), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_group(name, text) OWNER TO postgres;

--
-- Name: hasnt_index(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_index(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _have_index( $1, $2 ),
        'Index ' || quote_ident($2) || ' should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_index(name, name) OWNER TO postgres;

--
-- Name: hasnt_index(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_index(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _have_index( $1, $2, $3 ),
        'Index ' || quote_ident($3) || ' should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_index(name, name, name) OWNER TO postgres;

--
-- Name: hasnt_index(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_index(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _have_index( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_index(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_index(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_index(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    RETURN ok( NOT _have_index( $1, $2, $3 ), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.hasnt_index(name, name, name, text) OWNER TO postgres;

--
-- Name: hasnt_language(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_language(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_trusted($1) IS NULL, 'Procedural language ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_language(name) OWNER TO postgres;

--
-- Name: hasnt_language(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_language(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_trusted($1) IS NULL, $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_language(name, text) OWNER TO postgres;

--
-- Name: hasnt_materialized_view(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_materialized_view(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_materialized_view( $1, 'Materialized view ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_materialized_view(name) OWNER TO postgres;

--
-- Name: hasnt_materialized_view(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_materialized_view(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'm', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_materialized_view(name, text) OWNER TO postgres;

--
-- Name: hasnt_materialized_view(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_materialized_view(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'm', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_materialized_view(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_opclass(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_opclass(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _opc_exists( $1 ), 'Operator class ' || quote_ident($1) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_opclass(name) OWNER TO postgres;

--
-- Name: hasnt_opclass(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_opclass(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _opc_exists( $1, $2 ), 'Operator class ' || quote_ident($1) || '.' || quote_ident($2) || ' should exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_opclass(name, name) OWNER TO postgres;

--
-- Name: hasnt_opclass(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_opclass(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _opc_exists( $1 ), $2)
$_$;


ALTER FUNCTION samplesdb.hasnt_opclass(name, text) OWNER TO postgres;

--
-- Name: hasnt_opclass(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_opclass(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _opc_exists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_opclass(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_pk(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_pk(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_pk( $1, 'Table ' || quote_ident($1) || ' should not have a primary key' );
$_$;


ALTER FUNCTION samplesdb.hasnt_pk(name) OWNER TO postgres;

--
-- Name: hasnt_pk(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_pk(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _hasc( $1, 'p' ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_pk(name, text) OWNER TO postgres;

--
-- Name: hasnt_pk(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_pk(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _hasc( $1, $2, 'p' ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_pk(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_relation(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_relation(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_relation( $1, 'Relation ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_relation(name) OWNER TO postgres;

--
-- Name: hasnt_relation(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_relation(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _relexists( $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_relation(name, text) OWNER TO postgres;

--
-- Name: hasnt_relation(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_relation(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _relexists( $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_relation(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_role(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_role(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_role($1), 'Role ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_role(name) OWNER TO postgres;

--
-- Name: hasnt_role(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_role(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_role($1), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_role(name, text) OWNER TO postgres;

--
-- Name: hasnt_rule(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_rule(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2) IS NULL, 'Relation ' || quote_ident($1) || ' should not have rule ' || quote_ident($2) );
$_$;


ALTER FUNCTION samplesdb.hasnt_rule(name, name) OWNER TO postgres;

--
-- Name: hasnt_rule(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_rule(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2, $3) IS NULL, 'Relation ' || quote_ident($1) || '.' || quote_ident($2) || ' should not have rule ' || quote_ident($3) );
$_$;


ALTER FUNCTION samplesdb.hasnt_rule(name, name, name) OWNER TO postgres;

--
-- Name: hasnt_rule(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_rule(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2) IS NULL, $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_rule(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_rule(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_rule(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _is_instead($1, $2, $3) IS NULL, $4 );
$_$;


ALTER FUNCTION samplesdb.hasnt_rule(name, name, name, text) OWNER TO postgres;

--
-- Name: hasnt_schema(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_schema(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_schema( $1, 'Schema ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_schema(name) OWNER TO postgres;

--
-- Name: hasnt_schema(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_schema(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT EXISTS(
            SELECT true
              FROM pg_catalog.pg_namespace
             WHERE nspname = $1
        ), $2
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_schema(name, text) OWNER TO postgres;

--
-- Name: hasnt_sequence(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_sequence(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_sequence( $1, 'Sequence ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_sequence(name) OWNER TO postgres;

--
-- Name: hasnt_sequence(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_sequence(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'S', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_sequence(name, text) OWNER TO postgres;

--
-- Name: hasnt_sequence(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_sequence(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'S', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_sequence(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_table(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_table(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_table( $1, 'Table ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_table(name) OWNER TO postgres;

--
-- Name: hasnt_table(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_table(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _rexists( 'r', $1, $2 ),
        'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should not exist'
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_table(name, name) OWNER TO postgres;

--
-- Name: hasnt_table(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_table(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'r', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_table(name, text) OWNER TO postgres;

--
-- Name: hasnt_table(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_table(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'r', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_table(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_tablespace(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_tablespace(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_tablespace( $1, 'Tablespace ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_tablespace(name) OWNER TO postgres;

--
-- Name: hasnt_tablespace(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_tablespace(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT EXISTS(
            SELECT true
              FROM pg_catalog.pg_tablespace
             WHERE spcname = $1
        ), $2
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_tablespace(name, text) OWNER TO postgres;

--
-- Name: hasnt_trigger(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_trigger(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _trig($1, $2), 'Table ' || quote_ident($1) || ' should not have trigger ' || quote_ident($2));
$_$;


ALTER FUNCTION samplesdb.hasnt_trigger(name, name) OWNER TO postgres;

--
-- Name: hasnt_trigger(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_trigger(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _trig($1, $2, $3),
        'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should not have trigger ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.hasnt_trigger(name, name, name) OWNER TO postgres;

--
-- Name: hasnt_trigger(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_trigger(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _trig($1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.hasnt_trigger(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_trigger(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_trigger(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _trig($1, $2, $3), $4);
$_$;


ALTER FUNCTION samplesdb.hasnt_trigger(name, name, name, text) OWNER TO postgres;

--
-- Name: hasnt_type(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_type(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, NULL ), ('Type ' || quote_ident($1) || ' should not exist')::text );
$_$;


ALTER FUNCTION samplesdb.hasnt_type(name) OWNER TO postgres;

--
-- Name: hasnt_type(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_type(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_type( $1, $2, 'Type ' || quote_ident($1) || '.' || quote_ident($2) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_type(name, name) OWNER TO postgres;

--
-- Name: hasnt_type(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_type(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, NULL ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_type(name, text) OWNER TO postgres;

--
-- Name: hasnt_type(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_type(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_type( $1, $2, NULL ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_type(name, name, text) OWNER TO postgres;

--
-- Name: hasnt_user(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_user(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_user( $1 ), 'User ' || quote_ident($1) || ' should not exist');
$_$;


ALTER FUNCTION samplesdb.hasnt_user(name) OWNER TO postgres;

--
-- Name: hasnt_user(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_user(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _has_user($1), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_user(name, text) OWNER TO postgres;

--
-- Name: hasnt_view(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_view(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT hasnt_view( $1, 'View ' || quote_ident($1) || ' should not exist' );
$_$;


ALTER FUNCTION samplesdb.hasnt_view(name) OWNER TO postgres;

--
-- Name: hasnt_view(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_view(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'v', $1 ), $2 );
$_$;


ALTER FUNCTION samplesdb.hasnt_view(name, text) OWNER TO postgres;

--
-- Name: hasnt_view(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION hasnt_view(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _rexists( 'v', $1, $2 ), $3 );
$_$;


ALTER FUNCTION samplesdb.hasnt_view(name, name, text) OWNER TO postgres;

--
-- Name: ialike(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION ialike(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~~* $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.ialike(anyelement, text) OWNER TO postgres;

--
-- Name: ialike(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION ialike(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~~* $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.ialike(anyelement, text, text) OWNER TO postgres;

--
-- Name: imatches(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION imatches(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~* $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.imatches(anyelement, text) OWNER TO postgres;

--
-- Name: imatches(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION imatches(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~* $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.imatches(anyelement, text, text) OWNER TO postgres;

--
-- Name: in_todo(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION in_todo() RETURNS boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    todos integer;
BEGIN
    todos := _get('todo');
    RETURN CASE WHEN todos IS NULL THEN FALSE ELSE TRUE END;
END;
$$;


ALTER FUNCTION samplesdb.in_todo() OWNER TO postgres;

--
-- Name: index_is_primary(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_primary(name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisprimary
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
     WHERE ci.relname = $1
       AND pg_catalog.pg_table_is_visible(ct.oid)
      INTO res;

      RETURN ok(
          COALESCE(res, false),
          'Index ' || quote_ident($1) || ' should be on a primary key'
      );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_primary(name) OWNER TO postgres;

--
-- Name: index_is_primary(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_primary(name, name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisprimary
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
     WHERE ct.relname = $1
       AND ci.relname = $2
       AND pg_catalog.pg_table_is_visible(ct.oid)
     INTO res;

      RETURN ok(
          COALESCE(res, false),
          'Index ' || quote_ident($2) || ' should be on a primary key'
      );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_primary(name, name) OWNER TO postgres;

--
-- Name: index_is_primary(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_primary(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT index_is_primary(
        $1, $2, $3,
        'Index ' || quote_ident($3) || ' should be on a primary key'
    );
$_$;


ALTER FUNCTION samplesdb.index_is_primary(name, name, name) OWNER TO postgres;

--
-- Name: index_is_primary(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_primary(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisprimary
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
     WHERE ct.relname = $2
       AND ci.relname = $3
       AND n.nspname  = $1
      INTO res;

      RETURN ok( COALESCE(res, false), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_primary(name, name, name, text) OWNER TO postgres;

--
-- Name: index_is_type(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_type(name, name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    aname name;
BEGIN
    SELECT am.amname
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_am am    ON ci.relam = am.oid
     WHERE ci.relname = $1
      INTO aname;

      return is(
          aname, $2,
          'Index ' || quote_ident($1) || ' should be a ' || quote_ident($2) || ' index'
      );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_type(name, name) OWNER TO postgres;

--
-- Name: index_is_type(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_type(name, name, name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    aname name;
BEGIN
    SELECT am.amname
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_am am    ON ci.relam = am.oid
     WHERE ct.relname = $1
       AND ci.relname = $2
      INTO aname;

      return is(
          aname, $3,
          'Index ' || quote_ident($2) || ' should be a ' || quote_ident($3) || ' index'
      );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_type(name, name, name) OWNER TO postgres;

--
-- Name: index_is_type(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_type(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT index_is_type(
        $1, $2, $3, $4,
        'Index ' || quote_ident($3) || ' should be a ' || quote_ident($4) || ' index'
    );
$_$;


ALTER FUNCTION samplesdb.index_is_type(name, name, name, name) OWNER TO postgres;

--
-- Name: index_is_type(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_type(name, name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    aname name;
BEGIN
    SELECT am.amname
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
      JOIN pg_catalog.pg_am am       ON ci.relam = am.oid
     WHERE ct.relname = $2
       AND ci.relname = $3
       AND n.nspname  = $1
      INTO aname;

      return is( aname, $4, $5 );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_type(name, name, name, name, text) OWNER TO postgres;

--
-- Name: index_is_unique(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_unique(name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisunique
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
     WHERE ci.relname = $1
       AND pg_catalog.pg_table_is_visible(ct.oid)
      INTO res;

      RETURN ok(
          COALESCE(res, false),
          'Index ' || quote_ident($1) || ' should be unique'
      );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_unique(name) OWNER TO postgres;

--
-- Name: index_is_unique(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_unique(name, name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisunique
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
     WHERE ct.relname = $1
       AND ci.relname = $2
       AND pg_catalog.pg_table_is_visible(ct.oid)
      INTO res;

      RETURN ok(
          COALESCE(res, false),
          'Index ' || quote_ident($2) || ' should be unique'
      );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_unique(name, name) OWNER TO postgres;

--
-- Name: index_is_unique(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_unique(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT index_is_unique(
        $1, $2, $3,
        'Index ' || quote_ident($3) || ' should be unique'
    );
$_$;


ALTER FUNCTION samplesdb.index_is_unique(name, name, name) OWNER TO postgres;

--
-- Name: index_is_unique(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_is_unique(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisunique
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
     WHERE ct.relname = $2
       AND ci.relname = $3
       AND n.nspname  = $1
      INTO res;

      RETURN ok( COALESCE(res, false), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.index_is_unique(name, name, name, text) OWNER TO postgres;

--
-- Name: index_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT index_owner_is(
        $1, $2, $3,
        'Index ' || quote_ident($2) || ' ON '
        || quote_ident($1) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.index_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: index_owner_is(name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_owner_is(name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT index_owner_is(
        $1, $2, $3, $4,
        'Index ' || quote_ident($3) || ' ON '
        || quote_ident($1) || '.' || quote_ident($2)
        || ' should be owned by ' || quote_ident($4)
    );
$_$;


ALTER FUNCTION samplesdb.index_owner_is(name, name, name, name) OWNER TO postgres;

--
-- Name: index_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_index_owner($1, $2);
BEGIN
    -- Make sure the index exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Index ' || quote_ident($2) || ' ON ' || quote_ident($1) || ' not found'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.index_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: index_owner_is(name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION index_owner_is(name, name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_index_owner($1, $2, $3);
BEGIN
    -- Make sure the index exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            E'    Index ' || quote_ident($3) || ' ON '
            || quote_ident($1) || '.' || quote_ident($2) || ' not found'
        );
    END IF;

    RETURN is(owner, $4, $5);
END;
$_$;


ALTER FUNCTION samplesdb.index_owner_is(name, name, name, name, text) OWNER TO postgres;

--
-- Name: indexes_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION indexes_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT indexes_are( $1, $2, 'Table ' || quote_ident($1) || ' should have the correct indexes' );
$_$;


ALTER FUNCTION samplesdb.indexes_are(name, name[]) OWNER TO postgres;

--
-- Name: indexes_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION indexes_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'indexes',
        ARRAY(
            SELECT ci.relname
              FROM pg_catalog.pg_index x
              JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
              JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
             WHERE ct.relname = $1
               AND pg_catalog.pg_table_is_visible(ct.oid)
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT ci.relname
              FROM pg_catalog.pg_index x
              JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
              JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
             WHERE ct.relname = $1
               AND pg_catalog.pg_table_is_visible(ct.oid)
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.indexes_are(name, name[], text) OWNER TO postgres;

--
-- Name: indexes_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION indexes_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT indexes_are( $1, $2, $3, 'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should have the correct indexes' );
$_$;


ALTER FUNCTION samplesdb.indexes_are(name, name, name[]) OWNER TO postgres;

--
-- Name: indexes_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION indexes_are(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'indexes',
        ARRAY(
            SELECT ci.relname
              FROM pg_catalog.pg_index x
              JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
              JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
             WHERE ct.relname = $2
               AND n.nspname  = $1
            EXCEPT
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
        ),
        ARRAY(
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
            EXCEPT
            SELECT ci.relname
              FROM pg_catalog.pg_index x
              JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
              JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
             WHERE ct.relname = $2
               AND n.nspname  = $1
        ),
        $4
    );
$_$;


ALTER FUNCTION samplesdb.indexes_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: is(anyelement, anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION "is"(anyelement, anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is( $1, $2, NULL);
$_$;


ALTER FUNCTION samplesdb."is"(anyelement, anyelement) OWNER TO postgres;

--
-- Name: is(anyelement, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION "is"(anyelement, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    result BOOLEAN;
    output TEXT;
BEGIN
    -- Would prefer $1 IS NOT DISTINCT FROM, but that's not supported by 8.1.
    result := NOT $1 IS DISTINCT FROM $2;
    output := ok( result, $3 );
    RETURN output || CASE result WHEN TRUE THEN '' ELSE E'\n' || diag(
           '        have: ' || CASE WHEN $1 IS NULL THEN 'NULL' ELSE $1::text END ||
        E'\n        want: ' || CASE WHEN $2 IS NULL THEN 'NULL' ELSE $2::text END
    ) END;
END;
$_$;


ALTER FUNCTION samplesdb."is"(anyelement, anyelement, text) OWNER TO postgres;

--
-- Name: is_aggregate(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _agg($1), 'Function ' || quote_ident($1) || '() should be an aggregate function' );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name) OWNER TO postgres;

--
-- Name: is_aggregate(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _agg($1, $2),
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should be an aggregate function'
    );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, name[]) OWNER TO postgres;

--
-- Name: is_aggregate(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _agg($1, $2),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '() should be an aggregate function'
    );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, name) OWNER TO postgres;

--
-- Name: is_aggregate(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, _agg($1), $2 );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, text) OWNER TO postgres;

--
-- Name: is_aggregate(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, _agg($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, name[], text) OWNER TO postgres;

--
-- Name: is_aggregate(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _agg($1, $2, $3),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should be an aggregate function'
    );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, name, name[]) OWNER TO postgres;

--
-- Name: is_aggregate(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, _agg($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, name, text) OWNER TO postgres;

--
-- Name: is_aggregate(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_aggregate(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, _agg($1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.is_aggregate(name, name, name[], text) OWNER TO postgres;

--
-- Name: is_clustered(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_clustered(name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisclustered
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
     WHERE ci.relname = $1
      INTO res;

      RETURN ok(
          COALESCE(res, false),
          'Table should be clustered on index ' || quote_ident($1)
      );
END;
$_$;


ALTER FUNCTION samplesdb.is_clustered(name) OWNER TO postgres;

--
-- Name: is_clustered(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_clustered(name, name) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisclustered
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci ON ci.oid = x.indexrelid
     WHERE ct.relname = $1
       AND ci.relname = $2
      INTO res;

      RETURN ok(
          COALESCE(res, false),
          'Table ' || quote_ident($1) || ' should be clustered on index ' || quote_ident($2)
      );
END;
$_$;


ALTER FUNCTION samplesdb.is_clustered(name, name) OWNER TO postgres;

--
-- Name: is_clustered(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_clustered(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is_clustered(
        $1, $2, $3,
        'Table ' || quote_ident($1) || '.' || quote_ident($2) ||
        ' should be clustered on index ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.is_clustered(name, name, name) OWNER TO postgres;

--
-- Name: is_clustered(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_clustered(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res boolean;
BEGIN
    SELECT x.indisclustered
      FROM pg_catalog.pg_index x
      JOIN pg_catalog.pg_class ct    ON ct.oid = x.indrelid
      JOIN pg_catalog.pg_class ci    ON ci.oid = x.indexrelid
      JOIN pg_catalog.pg_namespace n ON n.oid = ct.relnamespace
     WHERE ct.relname = $2
       AND ci.relname = $3
       AND n.nspname  = $1
      INTO res;

      RETURN ok( COALESCE(res, false), $4 );
END;
$_$;


ALTER FUNCTION samplesdb.is_clustered(name, name, name, text) OWNER TO postgres;

--
-- Name: is_definer(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _definer($1), 'Function ' || quote_ident($1) || '() should be security definer' );
$_$;


ALTER FUNCTION samplesdb.is_definer(name) OWNER TO postgres;

--
-- Name: is_definer(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _definer($1, $2),
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should be security definer'
    );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, name[]) OWNER TO postgres;

--
-- Name: is_definer(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _definer($1, $2),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '() should be security definer'
    );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, name) OWNER TO postgres;

--
-- Name: is_definer(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, _definer($1), $2 );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, text) OWNER TO postgres;

--
-- Name: is_definer(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, _definer($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, name[], text) OWNER TO postgres;

--
-- Name: is_definer(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _definer($1, $2, $3),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should be security definer'
    );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, name, name[]) OWNER TO postgres;

--
-- Name: is_definer(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, _definer($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, name, text) OWNER TO postgres;

--
-- Name: is_definer(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_definer(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, _definer($1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.is_definer(name, name, name[], text) OWNER TO postgres;

--
-- Name: is_empty(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_empty(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is_empty( $1, NULL );
$_$;


ALTER FUNCTION samplesdb.is_empty(text) OWNER TO postgres;

--
-- Name: is_empty(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_empty(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    extras  TEXT[]  := '{}';
    res     BOOLEAN := TRUE;
    msg     TEXT    := '';
    rec     RECORD;
BEGIN
    -- Find extra records.
    FOR rec in EXECUTE _query($1) LOOP
        extras := extras || rec::text;
    END LOOP;

    -- What extra records do we have?
    IF extras[1] IS NOT NULL THEN
        res := FALSE;
        msg := E'\n' || diag(
            E'    Unexpected records:\n        '
            ||  array_to_string( extras, E'\n        ' )
        );
    END IF;

    RETURN ok(res, $2) || msg;
END;
$_$;


ALTER FUNCTION samplesdb.is_empty(text, text) OWNER TO postgres;

--
-- Name: is_member_of(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_member_of(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is_member_of( $1, $2, 'Should have members of role ' || quote_ident($1) );
$_$;


ALTER FUNCTION samplesdb.is_member_of(name, name[]) OWNER TO postgres;

--
-- Name: is_member_of(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_member_of(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is_member_of( $1, ARRAY[$2] );
$_$;


ALTER FUNCTION samplesdb.is_member_of(name, name) OWNER TO postgres;

--
-- Name: is_member_of(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_member_of(name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    missing text[];
BEGIN
    IF NOT _has_role($1) THEN
        RETURN fail( $3 ) || E'\n' || diag (
            '    Role ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    SELECT ARRAY(
        SELECT quote_ident($2[i])
          FROM generate_series(1, array_upper($2, 1)) s(i)
          LEFT JOIN pg_catalog.pg_roles r ON rolname = $2[i]
         WHERE r.oid IS NULL
            OR NOT r.oid = ANY ( _grolist($1) )
         ORDER BY s.i
    ) INTO missing;
    IF missing[1] IS NULL THEN
        RETURN ok( true, $3 );
    END IF;
    RETURN ok( false, $3 ) || E'\n' || diag(
        '    Members missing from the ' || quote_ident($1) || E' role:\n        ' ||
        array_to_string( missing, E'\n        ')
    );
END;
$_$;


ALTER FUNCTION samplesdb.is_member_of(name, name[], text) OWNER TO postgres;

--
-- Name: is_member_of(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_member_of(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is_member_of( $1, ARRAY[$2], $3 );
$_$;


ALTER FUNCTION samplesdb.is_member_of(name, name, text) OWNER TO postgres;

--
-- Name: is_strict(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( _strict($1), 'Function ' || quote_ident($1) || '() should be strict' );
$_$;


ALTER FUNCTION samplesdb.is_strict(name) OWNER TO postgres;

--
-- Name: is_strict(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _strict($1, $2),
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should be strict'
    );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, name[]) OWNER TO postgres;

--
-- Name: is_strict(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _strict($1, $2),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '() should be strict'
    );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, name) OWNER TO postgres;

--
-- Name: is_strict(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, _strict($1), $2 );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, text) OWNER TO postgres;

--
-- Name: is_strict(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, _strict($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, name[], text) OWNER TO postgres;

--
-- Name: is_strict(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        _strict($1, $2, $3),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should be strict'
    );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, name, name[]) OWNER TO postgres;

--
-- Name: is_strict(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, _strict($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, name, text) OWNER TO postgres;

--
-- Name: is_strict(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_strict(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, _strict($1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.is_strict(name, name, name[], text) OWNER TO postgres;

--
-- Name: is_superuser(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_superuser(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT is_superuser( $1, 'User ' || quote_ident($1) || ' should be a super user' );
$_$;


ALTER FUNCTION samplesdb.is_superuser(name) OWNER TO postgres;

--
-- Name: is_superuser(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION is_superuser(name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    is_super boolean := _is_super($1);
BEGIN
    IF is_super IS NULL THEN
        RETURN fail( $2 ) || E'\n' || diag( '    User ' || quote_ident($1) || ' does not exist') ;
    END IF;
    RETURN ok( is_super, $2 );
END;
$_$;


ALTER FUNCTION samplesdb.is_superuser(name, text) OWNER TO postgres;

--
-- Name: isa_ok(anyelement, regtype); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isa_ok(anyelement, regtype) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT isa_ok($1, $2, 'the value');
$_$;


ALTER FUNCTION samplesdb.isa_ok(anyelement, regtype) OWNER TO postgres;

--
-- Name: isa_ok(anyelement, regtype, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isa_ok(anyelement, regtype, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    typeof regtype := pg_typeof($1);
BEGIN
    IF typeof = $2 THEN RETURN ok(true, $3 || ' isa ' || $2 ); END IF;
    RETURN ok(false, $3 || ' isa ' || $2 ) || E'\n' ||
        diag('    ' || $3 || ' isn''t a "' || $2 || '" it''s a "' || typeof || '"');
END;
$_$;


ALTER FUNCTION samplesdb.isa_ok(anyelement, regtype, text) OWNER TO postgres;

--
-- Name: isnt(anyelement, anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt(anyelement, anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT isnt( $1, $2, NULL);
$_$;


ALTER FUNCTION samplesdb.isnt(anyelement, anyelement) OWNER TO postgres;

--
-- Name: isnt(anyelement, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt(anyelement, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    result BOOLEAN;
    output TEXT;
BEGIN
    result := $1 IS DISTINCT FROM $2;
    output := ok( result, $3 );
    RETURN output || CASE result WHEN TRUE THEN '' ELSE E'\n' || diag(
           '        have: ' || COALESCE( $1::text, 'NULL' ) ||
        E'\n        want: anything else'
    ) END;
END;
$_$;


ALTER FUNCTION samplesdb.isnt(anyelement, anyelement, text) OWNER TO postgres;

--
-- Name: isnt_empty(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_empty(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT isnt_empty( $1, NULL );
$_$;


ALTER FUNCTION samplesdb.isnt_empty(text) OWNER TO postgres;

--
-- Name: isnt_empty(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_empty(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    res  BOOLEAN := FALSE;
    rec  RECORD;
BEGIN
    -- Find extra records.
    FOR rec in EXECUTE _query($1) LOOP
        res := TRUE;
        EXIT;
    END LOOP;

    RETURN ok(res, $2);
END;
$_$;


ALTER FUNCTION samplesdb.isnt_empty(text, text) OWNER TO postgres;

--
-- Name: isnt_strict(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( NOT _strict($1), 'Function ' || quote_ident($1) || '() should not be strict' );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name) OWNER TO postgres;

--
-- Name: isnt_strict(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _strict($1, $2),
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should not be strict'
    );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, name[]) OWNER TO postgres;

--
-- Name: isnt_strict(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _strict($1, $2),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '() should not be strict'
    );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, name) OWNER TO postgres;

--
-- Name: isnt_strict(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, NOT _strict($1), $2 );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, text) OWNER TO postgres;

--
-- Name: isnt_strict(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, NOT _strict($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, name[], text) OWNER TO postgres;

--
-- Name: isnt_strict(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok(
        NOT _strict($1, $2, $3),
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should not be strict'
    );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, name, name[]) OWNER TO postgres;

--
-- Name: isnt_strict(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, NOT _strict($1, $2), $3 );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, name, text) OWNER TO postgres;

--
-- Name: isnt_strict(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_strict(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, NOT _strict($1, $2, $3), $4 );
$_$;


ALTER FUNCTION samplesdb.isnt_strict(name, name, name[], text) OWNER TO postgres;

--
-- Name: isnt_superuser(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_superuser(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT isnt_superuser( $1, 'User ' || quote_ident($1) || ' should not be a super user' );
$_$;


ALTER FUNCTION samplesdb.isnt_superuser(name) OWNER TO postgres;

--
-- Name: isnt_superuser(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION isnt_superuser(name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    is_super boolean := _is_super($1);
BEGIN
    IF is_super IS NULL THEN
        RETURN fail( $2 ) || E'\n' || diag( '    User ' || quote_ident($1) || ' does not exist') ;
    END IF;
    RETURN ok( NOT is_super, $2 );
END;
$_$;


ALTER FUNCTION samplesdb.isnt_superuser(name, text) OWNER TO postgres;

--
-- Name: language_is_trusted(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION language_is_trusted(name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT language_is_trusted($1, 'Procedural language ' || quote_ident($1) || ' should be trusted' );
$_$;


ALTER FUNCTION samplesdb.language_is_trusted(name) OWNER TO postgres;

--
-- Name: language_is_trusted(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION language_is_trusted(name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    is_trusted boolean := _is_trusted($1);
BEGIN
    IF is_trusted IS NULL THEN
        RETURN fail( $2 ) || E'\n' || diag( '    Procedural language ' || quote_ident($1) || ' does not exist') ;
    END IF;
    RETURN ok( is_trusted, $2 );
END;
$_$;


ALTER FUNCTION samplesdb.language_is_trusted(name, text) OWNER TO postgres;

--
-- Name: language_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION language_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT language_owner_is(
        $1, $2,
        'Language ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.language_owner_is(name, name) OWNER TO postgres;

--
-- Name: language_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION language_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_language_owner($1);
BEGIN
    -- Make sure the language exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Language ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.language_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: language_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION language_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT language_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on language ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.language_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: language_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION language_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_lang_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_language' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Language ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.language_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: languages_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION languages_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT languages_are( $1, 'There should be the correct procedural languages' );
$_$;


ALTER FUNCTION samplesdb.languages_are(name[]) OWNER TO postgres;

--
-- Name: languages_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION languages_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'languages',
        ARRAY(
            SELECT lanname
              FROM pg_catalog.pg_language
             WHERE lanispl
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT lanname
              FROM pg_catalog.pg_language
             WHERE lanispl
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.languages_are(name[], text) OWNER TO postgres;

--
-- Name: lives_ok(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION lives_ok(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT lives_ok( $1, NULL );
$_$;


ALTER FUNCTION samplesdb.lives_ok(text) OWNER TO postgres;

--
-- Name: lives_ok(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION lives_ok(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    code  TEXT := _query($1);
    descr ALIAS FOR $2;
BEGIN
    EXECUTE code;
    RETURN ok( TRUE, descr );
EXCEPTION WHEN OTHERS THEN
    -- There should have been no exception.
    RETURN ok( FALSE, descr ) || E'\n' || diag(
           '        died: ' || SQLSTATE || ': ' || SQLERRM
    );
END;
$_$;


ALTER FUNCTION samplesdb.lives_ok(text, text) OWNER TO postgres;

--
-- Name: matches(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION matches(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~ $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.matches(anyelement, text) OWNER TO postgres;

--
-- Name: matches(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION matches(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _alike( $1 ~ $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.matches(anyelement, text, text) OWNER TO postgres;

--
-- Name: materialized_view_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_view_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT materialized_view_owner_is(
        $1, $2,
        'Materialized view ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.materialized_view_owner_is(name, name) OWNER TO postgres;

--
-- Name: materialized_view_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_view_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT materialized_view_owner_is(
        $1, $2, $3,
        'Materialized view ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.materialized_view_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: materialized_view_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_view_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('m'::char, $1);
BEGIN
    -- Make sure the materialized view exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Materialized view ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.materialized_view_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: materialized_view_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_view_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('m'::char, $1, $2);
BEGIN
    -- Make sure the materialized view exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Materialized view ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.materialized_view_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: materialized_views_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_views_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'Materialized views', _extras('m', $1), _missing('m', $1),
        'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct materialized views'
    );
$_$;


ALTER FUNCTION samplesdb.materialized_views_are(name[]) OWNER TO postgres;

--
-- Name: materialized_views_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_views_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'Materialized views', _extras('m', $1), _missing('m', $1), $2);
$_$;


ALTER FUNCTION samplesdb.materialized_views_are(name[], text) OWNER TO postgres;

--
-- Name: materialized_views_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_views_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'Materialized views', _extras('m', $1, $2), _missing('m', $1, $2),
        'Schema ' || quote_ident($1) || ' should have the correct materialized views'
    );
$_$;


ALTER FUNCTION samplesdb.materialized_views_are(name, name[]) OWNER TO postgres;

--
-- Name: materialized_views_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION materialized_views_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'Materialized views', _extras('m', $1, $2), _missing('m', $1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.materialized_views_are(name, name[], text) OWNER TO postgres;

--
-- Name: no_plan(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION no_plan() RETURNS SETOF boolean
    LANGUAGE plpgsql STRICT
    AS $$
BEGIN
    PERFORM plan(0);
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.no_plan() OWNER TO postgres;

--
-- Name: num_failed(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION num_failed() RETURNS integer
    LANGUAGE sql STRICT
    AS $$
    SELECT _get('failed');
$$;


ALTER FUNCTION samplesdb.num_failed() OWNER TO postgres;

--
-- Name: ok(boolean); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION ok(boolean) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( $1, NULL );
$_$;


ALTER FUNCTION samplesdb.ok(boolean) OWNER TO postgres;

--
-- Name: ok(boolean, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION ok(boolean, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
   aok      ALIAS FOR $1;
   descr    text := $2;
   test_num INTEGER;
   todo_why TEXT;
   ok       BOOL;
BEGIN
   todo_why := _todo();
   ok       := CASE
       WHEN aok = TRUE THEN aok
       WHEN todo_why IS NULL THEN COALESCE(aok, false)
       ELSE TRUE
    END;
    IF _get('plan') IS NULL THEN
        RAISE EXCEPTION 'You tried to run a test without a plan! Gotta have a plan';
    END IF;

    test_num := add_result(
        ok,
        COALESCE(aok, false),
        descr,
        CASE WHEN todo_why IS NULL THEN '' ELSE 'todo' END,
        COALESCE(todo_why, '')
    );

    RETURN (CASE aok WHEN TRUE THEN '' ELSE 'not ' END)
           || 'ok ' || _set( 'curr_test', test_num )
           || CASE descr WHEN '' THEN '' ELSE COALESCE( ' - ' || substr(diag( descr ), 3), '' ) END
           || COALESCE( ' ' || diag( 'TODO ' || todo_why ), '')
           || CASE aok WHEN TRUE THEN '' ELSE E'\n' ||
                diag('Failed ' ||
                CASE WHEN todo_why IS NULL THEN '' ELSE '(TODO) ' END ||
                'test ' || test_num ||
                CASE descr WHEN '' THEN '' ELSE COALESCE(': "' || descr || '"', '') END ) ||
                CASE WHEN aok IS NULL THEN E'\n' || diag('    (test result was NULL)') ELSE '' END
           END;
END;
$_$;


ALTER FUNCTION samplesdb.ok(boolean, text) OWNER TO postgres;

--
-- Name: opclass_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclass_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT opclass_owner_is(
        $1, $2,
        'Operator class ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.opclass_owner_is(name, name) OWNER TO postgres;

--
-- Name: opclass_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclass_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT opclass_owner_is(
        $1, $2, $3,
        'Operator class ' || quote_ident($1) || '.' || quote_ident($2) ||
        ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.opclass_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: opclass_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclass_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_opclass_owner($1);
BEGIN
    -- Make sure the opclass exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Operator class ' || quote_ident($1) || ' not found'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.opclass_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: opclass_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclass_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_opclass_owner($1, $2);
BEGIN
    -- Make sure the opclass exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Operator class ' || quote_ident($1) || '.' || quote_ident($2)
            || ' not found'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.opclass_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: opclasses_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclasses_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT opclasses_are( $1, 'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct operator classes' );
$_$;


ALTER FUNCTION samplesdb.opclasses_are(name[]) OWNER TO postgres;

--
-- Name: opclasses_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclasses_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'operator classes',
        ARRAY(
            SELECT oc.opcname
              FROM pg_catalog.pg_opclass oc
              JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_opclass_is_visible(oc.oid)
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
               FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT oc.opcname
              FROM pg_catalog.pg_opclass oc
              JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_opclass_is_visible(oc.oid)
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.opclasses_are(name[], text) OWNER TO postgres;

--
-- Name: opclasses_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclasses_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT opclasses_are( $1, $2, 'Schema ' || quote_ident($1) || ' should have the correct operator classes' );
$_$;


ALTER FUNCTION samplesdb.opclasses_are(name, name[]) OWNER TO postgres;

--
-- Name: opclasses_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION opclasses_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'operator classes',
        ARRAY(
            SELECT oc.opcname
              FROM pg_catalog.pg_opclass oc
              JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid
             WHERE n.nspname  = $1
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
               FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT oc.opcname
              FROM pg_catalog.pg_opclass oc
              JOIN pg_catalog.pg_namespace n ON oc.opcnamespace = n.oid
             WHERE n.nspname  = $1
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.opclasses_are(name, name[], text) OWNER TO postgres;

--
-- Name: operators_are(text[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION operators_are(text[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT operators_are($1, 'There should be the correct operators')
$_$;


ALTER FUNCTION samplesdb.operators_are(text[]) OWNER TO postgres;

--
-- Name: operators_are(text[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION operators_are(text[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _areni(
        'operators',
        ARRAY(
            SELECT display_oper(o.oprname, o.oid) || ' RETURNS ' || o.oprresult::regtype
              FROM pg_catalog.pg_operator o
              JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid
             WHERE pg_catalog.pg_operator_is_visible(o.oid)
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT display_oper(o.oprname, o.oid) || ' RETURNS ' || o.oprresult::regtype
              FROM pg_catalog.pg_operator o
              JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid
             WHERE pg_catalog.pg_operator_is_visible(o.oid)
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.operators_are(text[], text) OWNER TO postgres;

--
-- Name: operators_are(name, text[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION operators_are(name, text[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT operators_are($1, $2, 'Schema ' || quote_ident($1) || ' should have the correct operators' );
$_$;


ALTER FUNCTION samplesdb.operators_are(name, text[]) OWNER TO postgres;

--
-- Name: operators_are(name, text[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION operators_are(name, text[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _areni(
        'operators',
        ARRAY(
            SELECT display_oper(o.oprname, o.oid) || ' RETURNS ' || o.oprresult::regtype
              FROM pg_catalog.pg_operator o
              JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid
             WHERE n.nspname = $1
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT display_oper(o.oprname, o.oid) || ' RETURNS ' || o.oprresult::regtype
              FROM pg_catalog.pg_operator o
              JOIN pg_catalog.pg_namespace n ON o.oprnamespace = n.oid
             WHERE n.nspname = $1
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.operators_are(name, text[], text) OWNER TO postgres;

--
-- Name: os_name(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION os_name() RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $$SELECT '__OS__'::text;$$;


ALTER FUNCTION samplesdb.os_name() OWNER TO postgres;

--
-- Name: pass(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION pass() RETURNS text
    LANGUAGE sql
    AS $$
    SELECT ok( TRUE, NULL );
$$;


ALTER FUNCTION samplesdb.pass() OWNER TO postgres;

--
-- Name: pass(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION pass(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( TRUE, $1 );
$_$;


ALTER FUNCTION samplesdb.pass(text) OWNER TO postgres;

--
-- Name: performs_ok(text, numeric); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION performs_ok(text, numeric) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT performs_ok(
        $1, $2, 'Should run in less than ' || $2 || ' ms'
    );
$_$;


ALTER FUNCTION samplesdb.performs_ok(text, numeric) OWNER TO postgres;

--
-- Name: performs_ok(text, numeric, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION performs_ok(text, numeric, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    query     TEXT := _query($1);
    max_time  ALIAS FOR $2;
    descr     ALIAS FOR $3;
    starts_at TEXT;
    act_time  NUMERIC;
BEGIN
    starts_at := timeofday();
    EXECUTE query;
    act_time := extract( millisecond from timeofday()::timestamptz - starts_at::timestamptz);
    IF act_time < max_time THEN RETURN ok(TRUE, descr); END IF;
    RETURN ok( FALSE, descr ) || E'\n' || diag(
           '      runtime: ' || act_time || ' ms' ||
        E'\n      exceeds: ' || max_time || ' ms'
    );
END;
$_$;


ALTER FUNCTION samplesdb.performs_ok(text, numeric, text) OWNER TO postgres;

--
-- Name: performs_within(text, numeric, numeric); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION performs_within(text, numeric, numeric) RETURNS text
    LANGUAGE sql
    AS $_$
SELECT performs_within(
          $1, $2, $3, 10,
          'Should run within ' || $2 || ' +/- ' || $3 || ' ms');
$_$;


ALTER FUNCTION samplesdb.performs_within(text, numeric, numeric) OWNER TO postgres;

--
-- Name: performs_within(text, numeric, numeric, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION performs_within(text, numeric, numeric, integer) RETURNS text
    LANGUAGE sql
    AS $_$
SELECT performs_within(
          $1, $2, $3, $4,
          'Should run within ' || $2 || ' +/- ' || $3 || ' ms');
$_$;


ALTER FUNCTION samplesdb.performs_within(text, numeric, numeric, integer) OWNER TO postgres;

--
-- Name: performs_within(text, numeric, numeric, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION performs_within(text, numeric, numeric, text) RETURNS text
    LANGUAGE sql
    AS $_$
SELECT performs_within(
          $1, $2, $3, 10, $4
        );
$_$;


ALTER FUNCTION samplesdb.performs_within(text, numeric, numeric, text) OWNER TO postgres;

--
-- Name: performs_within(text, numeric, numeric, integer, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION performs_within(text, numeric, numeric, integer, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    query          TEXT := _query($1);
    expected_avg   ALIAS FOR $2;
    within         ALIAS FOR $3;
    iterations     ALIAS FOR $4;
    descr          ALIAS FOR $5;
    avg_time       NUMERIC;
BEGIN
  SELECT avg(a_time) FROM _time_trials(query, iterations, 0.8) t1 INTO avg_time;
  IF abs(avg_time - expected_avg) < within THEN RETURN ok(TRUE, descr); END IF;
  RETURN ok(FALSE, descr) || E'\n' || diag(' average runtime: ' || avg_time || ' ms'
     || E'\n desired average: ' || expected_avg || ' +/- ' || within || ' ms'
    );
END;
$_$;


ALTER FUNCTION samplesdb.performs_within(text, numeric, numeric, integer, text) OWNER TO postgres;

--
-- Name: pg_version(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION pg_version() RETURNS text
    LANGUAGE sql IMMUTABLE
    AS $$SELECT current_setting('server_version')$$;


ALTER FUNCTION samplesdb.pg_version() OWNER TO postgres;

--
-- Name: pg_version_num(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION pg_version_num() RETURNS integer
    LANGUAGE sql IMMUTABLE
    AS $$
    SELECT s.a[1]::int * 10000
           + COALESCE(substring(s.a[2] FROM '[[:digit:]]+')::int, 0) * 100
           + COALESCE(substring(s.a[3] FROM '[[:digit:]]+')::int, 0)
      FROM (
          SELECT string_to_array(current_setting('server_version'), '.') AS a
      ) AS s;
$$;


ALTER FUNCTION samplesdb.pg_version_num() OWNER TO postgres;

--
-- Name: plan(integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION plan(integer) RETURNS text
    LANGUAGE plpgsql STRICT
    AS $_$
DECLARE
    rcount INTEGER;
BEGIN
    BEGIN
        EXECUTE '
            CREATE TEMP SEQUENCE __tcache___id_seq;
            CREATE TEMP TABLE __tcache__ (
                id    INTEGER NOT NULL DEFAULT nextval(''__tcache___id_seq''),
                label TEXT    NOT NULL,
                value INTEGER NOT NULL,
                note  TEXT    NOT NULL DEFAULT ''''
            );
            CREATE UNIQUE INDEX __tcache___key ON __tcache__(id);
            GRANT ALL ON TABLE __tcache__ TO PUBLIC;
            GRANT ALL ON TABLE __tcache___id_seq TO PUBLIC;

            CREATE TEMP SEQUENCE __tresults___numb_seq;
            GRANT ALL ON TABLE __tresults___numb_seq TO PUBLIC;
        ';

    EXCEPTION WHEN duplicate_table THEN
        -- Raise an exception if there's already a plan.
        EXECUTE 'SELECT TRUE FROM __tcache__ WHERE label = ''plan''';
      GET DIAGNOSTICS rcount = ROW_COUNT;
        IF rcount > 0 THEN
           RAISE EXCEPTION 'You tried to plan twice!';
        END IF;
    END;

    -- Save the plan and return.
    PERFORM _set('plan', $1 );
    PERFORM _set('failed', 0 );
    RETURN '1..' || $1;
END;
$_$;


ALTER FUNCTION samplesdb.plan(integer) OWNER TO postgres;

--
-- Name: relation_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION relation_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT relation_owner_is(
        $1, $2,
        'Relation ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.relation_owner_is(name, name) OWNER TO postgres;

--
-- Name: relation_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION relation_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT relation_owner_is(
        $1, $2, $3,
        'Relation ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.relation_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: relation_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION relation_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner($1);
BEGIN
    -- Make sure the relation exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Relation ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.relation_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: relation_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION relation_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner($1, $2);
BEGIN
    -- Make sure the relation exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Relation ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.relation_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: results_eq(refcursor, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(refcursor, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_eq( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_eq(refcursor, anyarray) OWNER TO postgres;

--
-- Name: results_eq(refcursor, refcursor); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(refcursor, refcursor) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_eq( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_eq(refcursor, refcursor) OWNER TO postgres;

--
-- Name: results_eq(refcursor, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(refcursor, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_eq( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_eq(refcursor, text) OWNER TO postgres;

--
-- Name: results_eq(text, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(text, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_eq( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_eq(text, anyarray) OWNER TO postgres;

--
-- Name: results_eq(text, refcursor); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(text, refcursor) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_eq( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_eq(text, refcursor) OWNER TO postgres;

--
-- Name: results_eq(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_eq( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_eq(text, text) OWNER TO postgres;

--
-- Name: results_eq(refcursor, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(refcursor, anyarray, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN want FOR SELECT $2[i]
    FROM generate_series(array_lower($2, 1), array_upper($2, 1)) s(i);
    res := results_eq($1, want, $3);
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_eq(refcursor, anyarray, text) OWNER TO postgres;

--
-- Name: results_eq(refcursor, refcursor, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(refcursor, refcursor, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have       ALIAS FOR $1;
    want       ALIAS FOR $2;
    have_rec   RECORD;
    want_rec   RECORD;
    have_found BOOLEAN;
    want_found BOOLEAN;
    rownum     INTEGER := 1;
BEGIN
    FETCH have INTO have_rec;
    have_found := FOUND;
    FETCH want INTO want_rec;
    want_found := FOUND;
    WHILE have_found OR want_found LOOP
        IF have_rec IS DISTINCT FROM want_rec OR have_found <> want_found THEN
            RETURN ok( false, $3 ) || E'\n' || diag(
                '    Results differ beginning at row ' || rownum || E':\n' ||
                '        have: ' || CASE WHEN have_found THEN have_rec::text ELSE 'NULL' END || E'\n' ||
                '        want: ' || CASE WHEN want_found THEN want_rec::text ELSE 'NULL' END
            );
        END IF;
        rownum = rownum + 1;
        FETCH have INTO have_rec;
        have_found := FOUND;
        FETCH want INTO want_rec;
        want_found := FOUND;
    END LOOP;

    RETURN ok( true, $3 );
EXCEPTION
    WHEN datatype_mismatch THEN
        RETURN ok( false, $3 ) || E'\n' || diag(
            E'    Number of columns or their types differ between the queries' ||
            CASE WHEN have_rec::TEXT = want_rec::text THEN '' ELSE E':\n' ||
                '        have: ' || CASE WHEN have_found THEN have_rec::text ELSE 'NULL' END || E'\n' ||
                '        want: ' || CASE WHEN want_found THEN want_rec::text ELSE 'NULL' END
            END
        );
END;
$_$;


ALTER FUNCTION samplesdb.results_eq(refcursor, refcursor, text) OWNER TO postgres;

--
-- Name: results_eq(refcursor, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(refcursor, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN want FOR EXECUTE _query($2);
    res := results_eq($1, want, $3);
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_eq(refcursor, text, text) OWNER TO postgres;

--
-- Name: results_eq(text, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(text, anyarray, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have REFCURSOR;
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN have FOR EXECUTE _query($1);
    OPEN want FOR SELECT $2[i]
    FROM generate_series(array_lower($2, 1), array_upper($2, 1)) s(i);
    res := results_eq(have, want, $3);
    CLOSE have;
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_eq(text, anyarray, text) OWNER TO postgres;

--
-- Name: results_eq(text, refcursor, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(text, refcursor, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have REFCURSOR;
    res  TEXT;
BEGIN
    OPEN have FOR EXECUTE _query($1);
    res := results_eq(have, $2, $3);
    CLOSE have;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_eq(text, refcursor, text) OWNER TO postgres;

--
-- Name: results_eq(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_eq(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have REFCURSOR;
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN have FOR EXECUTE _query($1);
    OPEN want FOR EXECUTE _query($2);
    res := results_eq(have, want, $3);
    CLOSE have;
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_eq(text, text, text) OWNER TO postgres;

--
-- Name: results_ne(refcursor, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(refcursor, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_ne( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_ne(refcursor, anyarray) OWNER TO postgres;

--
-- Name: results_ne(refcursor, refcursor); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(refcursor, refcursor) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_ne( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_ne(refcursor, refcursor) OWNER TO postgres;

--
-- Name: results_ne(refcursor, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(refcursor, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_ne( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_ne(refcursor, text) OWNER TO postgres;

--
-- Name: results_ne(text, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(text, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_ne( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_ne(text, anyarray) OWNER TO postgres;

--
-- Name: results_ne(text, refcursor); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(text, refcursor) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_ne( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_ne(text, refcursor) OWNER TO postgres;

--
-- Name: results_ne(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT results_ne( $1, $2, NULL::text );
$_$;


ALTER FUNCTION samplesdb.results_ne(text, text) OWNER TO postgres;

--
-- Name: results_ne(refcursor, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(refcursor, anyarray, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN want FOR SELECT $2[i]
    FROM generate_series(array_lower($2, 1), array_upper($2, 1)) s(i);
    res := results_ne($1, want, $3);
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_ne(refcursor, anyarray, text) OWNER TO postgres;

--
-- Name: results_ne(refcursor, refcursor, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(refcursor, refcursor, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have       ALIAS FOR $1;
    want       ALIAS FOR $2;
    have_rec   RECORD;
    want_rec   RECORD;
    have_found BOOLEAN;
    want_found BOOLEAN;
BEGIN
    FETCH have INTO have_rec;
    have_found := FOUND;
    FETCH want INTO want_rec;
    want_found := FOUND;
    WHILE have_found OR want_found LOOP
        IF have_rec IS DISTINCT FROM want_rec OR have_found <> want_found THEN
            RETURN ok( true, $3 );
        ELSE
            FETCH have INTO have_rec;
            have_found := FOUND;
            FETCH want INTO want_rec;
            want_found := FOUND;
        END IF;
    END LOOP;
    RETURN ok( false, $3 );
EXCEPTION
    WHEN datatype_mismatch THEN
        RETURN ok( false, $3 ) || E'\n' || diag(
            E'    Columns differ between queries:\n' ||
            '        have: ' || CASE WHEN have_found THEN have_rec::text ELSE 'NULL' END || E'\n' ||
            '        want: ' || CASE WHEN want_found THEN want_rec::text ELSE 'NULL' END
        );
END;
$_$;


ALTER FUNCTION samplesdb.results_ne(refcursor, refcursor, text) OWNER TO postgres;

--
-- Name: results_ne(refcursor, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(refcursor, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN want FOR EXECUTE _query($2);
    res := results_ne($1, want, $3);
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_ne(refcursor, text, text) OWNER TO postgres;

--
-- Name: results_ne(text, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(text, anyarray, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have REFCURSOR;
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN have FOR EXECUTE _query($1);
    OPEN want FOR SELECT $2[i]
    FROM generate_series(array_lower($2, 1), array_upper($2, 1)) s(i);
    res := results_ne(have, want, $3);
    CLOSE have;
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_ne(text, anyarray, text) OWNER TO postgres;

--
-- Name: results_ne(text, refcursor, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(text, refcursor, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have REFCURSOR;
    res  TEXT;
BEGIN
    OPEN have FOR EXECUTE _query($1);
    res := results_ne(have, $2, $3);
    CLOSE have;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_ne(text, refcursor, text) OWNER TO postgres;

--
-- Name: results_ne(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION results_ne(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    have REFCURSOR;
    want REFCURSOR;
    res  TEXT;
BEGIN
    OPEN have FOR EXECUTE _query($1);
    OPEN want FOR EXECUTE _query($2);
    res := results_ne(have, want, $3);
    CLOSE have;
    CLOSE want;
    RETURN res;
END;
$_$;


ALTER FUNCTION samplesdb.results_ne(text, text, text) OWNER TO postgres;

--
-- Name: roles_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION roles_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT roles_are( $1, 'There should be the correct roles' );
$_$;


ALTER FUNCTION samplesdb.roles_are(name[]) OWNER TO postgres;

--
-- Name: roles_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION roles_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'roles',
        ARRAY(
            SELECT rolname
              FROM pg_catalog.pg_roles
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT rolname
              FROM pg_catalog.pg_roles
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.roles_are(name[], text) OWNER TO postgres;

--
-- Name: row_eq(text, anyelement); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION row_eq(text, anyelement) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT row_eq($1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.row_eq(text, anyelement) OWNER TO postgres;

--
-- Name: row_eq(text, anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION row_eq(text, anyelement, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    rec    RECORD;
BEGIN
    EXECUTE _query($1) INTO rec;
    IF NOT rec IS DISTINCT FROM $2 THEN RETURN ok(true, $3); END IF;
    RETURN ok(false, $3 ) || E'\n' || diag(
           '        have: ' || CASE WHEN rec IS NULL THEN 'NULL' ELSE rec::text END ||
        E'\n        want: ' || CASE WHEN $2  IS NULL THEN 'NULL' ELSE $2::text  END
    );
END;
$_$;


ALTER FUNCTION samplesdb.row_eq(text, anyelement, text) OWNER TO postgres;

--
-- Name: rule_is_instead(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_instead(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT rule_is_instead($1, $2, 'Rule ' || quote_ident($2) || ' on relation ' || quote_ident($1) || ' should be an INSTEAD rule' );
$_$;


ALTER FUNCTION samplesdb.rule_is_instead(name, name) OWNER TO postgres;

--
-- Name: rule_is_instead(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_instead(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT rule_is_instead( $1, $2, $3, 'Rule ' || quote_ident($3) || ' on relation ' || quote_ident($1) || '.' || quote_ident($2) || ' should be an INSTEAD rule' );
$_$;


ALTER FUNCTION samplesdb.rule_is_instead(name, name, name) OWNER TO postgres;

--
-- Name: rule_is_instead(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_instead(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    is_it boolean := _is_instead($1, $2);
BEGIN
    IF is_it IS NOT NULL THEN RETURN ok( is_it, $3 ); END IF;
    RETURN ok( FALSE, $3 ) || E'\n' || diag(
        '    Rule ' || quote_ident($2) || ' does not exist'
    );
END;
$_$;


ALTER FUNCTION samplesdb.rule_is_instead(name, name, text) OWNER TO postgres;

--
-- Name: rule_is_instead(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_instead(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    is_it boolean := _is_instead($1, $2, $3);
BEGIN
    IF is_it IS NOT NULL THEN RETURN ok( is_it, $4 ); END IF;
    RETURN ok( FALSE, $4 ) || E'\n' || diag(
        '    Rule ' || quote_ident($3) || ' does not exist'
    );
END;
$_$;


ALTER FUNCTION samplesdb.rule_is_instead(name, name, name, text) OWNER TO postgres;

--
-- Name: rule_is_on(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_on(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT rule_is_on(
        $1, $2, $3,
        'Rule ' || quote_ident($2) || ' should be on '
        || _expand_on(_contract_on($3)::char) || ' to ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.rule_is_on(name, name, text) OWNER TO postgres;

--
-- Name: rule_is_on(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_on(name, name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT rule_is_on(
        $1, $2, $3, $4,
        'Rule ' || quote_ident($3) || ' should be on ' || _expand_on(_contract_on($4)::char)
        || ' to ' || quote_ident($1) || '.' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.rule_is_on(name, name, name, text) OWNER TO postgres;

--
-- Name: rule_is_on(name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_on(name, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want char := _contract_on($3);
    have char := _rule_on($1, $2);
BEGIN
    IF have IS NOT NULL THEN
        RETURN is( _expand_on(have), _expand_on(want), $4 );
    END IF;

    RETURN ok( false, $4 ) || E'\n' || diag(
        '    Rule ' || quote_ident($2) || ' does not exist on '
        || quote_ident($1)
    );
END;
$_$;


ALTER FUNCTION samplesdb.rule_is_on(name, name, text, text) OWNER TO postgres;

--
-- Name: rule_is_on(name, name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rule_is_on(name, name, name, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    want char := _contract_on($4);
    have char := _rule_on($1, $2, $3);
BEGIN
    IF have IS NOT NULL THEN
        RETURN is( _expand_on(have), _expand_on(want), $5 );
    END IF;

    RETURN ok( false, $5 ) || E'\n' || diag(
        '    Rule ' || quote_ident($3) || ' does not exist on '
        || quote_ident($1) || '.' || quote_ident($2)
    );
END;
$_$;


ALTER FUNCTION samplesdb.rule_is_on(name, name, name, text, text) OWNER TO postgres;

--
-- Name: rules_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rules_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT rules_are( $1, $2, 'Relation ' || quote_ident($1) || ' should have the correct rules' );
$_$;


ALTER FUNCTION samplesdb.rules_are(name, name[]) OWNER TO postgres;

--
-- Name: rules_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rules_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'rules',
        ARRAY(
            SELECT r.rulename
              FROM pg_catalog.pg_rewrite r
              JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
              JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid
             WHERE c.relname = $1
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_table_is_visible(c.oid)
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT r.rulename
              FROM pg_catalog.pg_rewrite r
              JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
              JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid
               AND c.relname = $1
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND pg_catalog.pg_table_is_visible(c.oid)
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.rules_are(name, name[], text) OWNER TO postgres;

--
-- Name: rules_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rules_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT rules_are( $1, $2, $3, 'Relation ' || quote_ident($1) || '.' || quote_ident($2) || ' should have the correct rules' );
$_$;


ALTER FUNCTION samplesdb.rules_are(name, name, name[]) OWNER TO postgres;

--
-- Name: rules_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION rules_are(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'rules',
        ARRAY(
            SELECT r.rulename
              FROM pg_catalog.pg_rewrite r
              JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
              JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid
             WHERE c.relname = $2
               AND n.nspname = $1
            EXCEPT
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
        ),
        ARRAY(
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
            EXCEPT
            SELECT r.rulename
              FROM pg_catalog.pg_rewrite r
              JOIN pg_catalog.pg_class c     ON c.oid = r.ev_class
              JOIN pg_catalog.pg_namespace n ON c.relnamespace = n.oid
             WHERE c.relname = $2
               AND n.nspname = $1
        ),
        $4
    );
$_$;


ALTER FUNCTION samplesdb.rules_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: runtests(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION runtests() RETURNS SETOF text
    LANGUAGE sql
    AS $$
    SELECT * FROM runtests( '^test' );
$$;


ALTER FUNCTION samplesdb.runtests() OWNER TO postgres;

--
-- Name: runtests(name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION runtests(name) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM runtests( $1, '^test' );
$_$;


ALTER FUNCTION samplesdb.runtests(name) OWNER TO postgres;

--
-- Name: runtests(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION runtests(text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM _runner(
        findfuncs( '^startup' ),
        findfuncs( '^shutdown' ),
        findfuncs( '^setup' ),
        findfuncs( '^teardown' ),
        findfuncs( $1 )
    );
$_$;


ALTER FUNCTION samplesdb.runtests(text) OWNER TO postgres;

--
-- Name: runtests(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION runtests(name, text) RETURNS SETOF text
    LANGUAGE sql
    AS $_$
    SELECT * FROM _runner(
        findfuncs( $1, '^startup' ),
        findfuncs( $1, '^shutdown' ),
        findfuncs( $1, '^setup' ),
        findfuncs( $1, '^teardown' ),
        findfuncs( $1, $2 )
    );
$_$;


ALTER FUNCTION samplesdb.runtests(name, text) OWNER TO postgres;

--
-- Name: schema_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION schema_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT schema_owner_is(
        $1, $2,
        'Schema ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.schema_owner_is(name, name) OWNER TO postgres;

--
-- Name: schema_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION schema_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_schema_owner($1);
BEGIN
    -- Make sure the schema exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Schema ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.schema_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: schema_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION schema_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT schema_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on schema ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.schema_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: schema_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION schema_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_schema_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'invalid_schema_name' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Schema ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.schema_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: schemas_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION schemas_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT schemas_are( $1, 'There should be the correct schemas' );
$_$;


ALTER FUNCTION samplesdb.schemas_are(name[]) OWNER TO postgres;

--
-- Name: schemas_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION schemas_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'schemas',
        ARRAY(
            SELECT nspname
              FROM pg_catalog.pg_namespace
             WHERE nspname NOT LIKE 'pg_%'
               AND nspname <> 'information_schema'
             EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT nspname
              FROM pg_catalog.pg_namespace
             WHERE nspname NOT LIKE 'pg_%'
               AND nspname <> 'information_schema'
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.schemas_are(name[], text) OWNER TO postgres;

--
-- Name: sequence_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT sequence_owner_is(
        $1, $2,
        'Sequence ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.sequence_owner_is(name, name) OWNER TO postgres;

--
-- Name: sequence_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT sequence_owner_is(
        $1, $2, $3,
        'Sequence ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.sequence_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: sequence_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('S'::char, $1);
BEGIN
    -- Make sure the sequence exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Sequence ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.sequence_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: sequence_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('S'::char, $1, $2);
BEGIN
    -- Make sure the sequence exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Sequence ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.sequence_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: sequence_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT sequence_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on sequence ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.sequence_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: sequence_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_sequence_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Sequence ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.sequence_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: sequence_privs_are(name, name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_privs_are(name, name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT sequence_privs_are(
        $1, $2, $3, $4,
        'Role ' || quote_ident($3) || ' should be granted '
            || CASE WHEN $4[1] IS NULL THEN 'no privileges' ELSE array_to_string($4, ', ') END
            || ' on sequence '|| quote_ident($1) || '.' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.sequence_privs_are(name, name, name, name[]) OWNER TO postgres;

--
-- Name: sequence_privs_are(name, name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequence_privs_are(name, name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_sequence_privs( $3, quote_ident($1) || '.' || quote_ident($2) );
BEGIN
    IF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Sequence ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Role ' || quote_ident($3) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $4, $5);
END;
$_$;


ALTER FUNCTION samplesdb.sequence_privs_are(name, name, name, name[], text) OWNER TO postgres;

--
-- Name: sequences_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequences_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'sequences', _extras('S', $1), _missing('S', $1),
        'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct sequences'
    );
$_$;


ALTER FUNCTION samplesdb.sequences_are(name[]) OWNER TO postgres;

--
-- Name: sequences_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequences_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'sequences', _extras('S', $1), _missing('S', $1), $2);
$_$;


ALTER FUNCTION samplesdb.sequences_are(name[], text) OWNER TO postgres;

--
-- Name: sequences_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequences_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'sequences', _extras('S', $1, $2), _missing('S', $1, $2),
        'Schema ' || quote_ident($1) || ' should have the correct sequences'
    );
$_$;


ALTER FUNCTION samplesdb.sequences_are(name, name[]) OWNER TO postgres;

--
-- Name: sequences_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION sequences_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'sequences', _extras('S', $1, $2), _missing('S', $1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.sequences_are(name, name[], text) OWNER TO postgres;

--
-- Name: server_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION server_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT server_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on server ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.server_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: server_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION server_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_server_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_server' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Server ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.server_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: set_eq(text, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_eq(text, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::text, '' );
$_$;


ALTER FUNCTION samplesdb.set_eq(text, anyarray) OWNER TO postgres;

--
-- Name: set_eq(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_eq(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::text, '' );
$_$;


ALTER FUNCTION samplesdb.set_eq(text, text) OWNER TO postgres;

--
-- Name: set_eq(text, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_eq(text, anyarray, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, '' );
$_$;


ALTER FUNCTION samplesdb.set_eq(text, anyarray, text) OWNER TO postgres;

--
-- Name: set_eq(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_eq(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, '' );
$_$;


ALTER FUNCTION samplesdb.set_eq(text, text, text) OWNER TO postgres;

--
-- Name: set_has(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_has(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::TEXT, 'EXCEPT', 'Missing' );
$_$;


ALTER FUNCTION samplesdb.set_has(text, text) OWNER TO postgres;

--
-- Name: set_has(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_has(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, 'EXCEPT', 'Missing' );
$_$;


ALTER FUNCTION samplesdb.set_has(text, text, text) OWNER TO postgres;

--
-- Name: set_hasnt(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_hasnt(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, NULL::TEXT, 'INTERSECT', 'Extra' );
$_$;


ALTER FUNCTION samplesdb.set_hasnt(text, text) OWNER TO postgres;

--
-- Name: set_hasnt(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_hasnt(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relcomp( $1, $2, $3, 'INTERSECT', 'Extra' );
$_$;


ALTER FUNCTION samplesdb.set_hasnt(text, text, text) OWNER TO postgres;

--
-- Name: set_ne(text, anyarray); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_ne(text, anyarray) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, NULL::text, '' );
$_$;


ALTER FUNCTION samplesdb.set_ne(text, anyarray) OWNER TO postgres;

--
-- Name: set_ne(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_ne(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, NULL::text, '' );
$_$;


ALTER FUNCTION samplesdb.set_ne(text, text) OWNER TO postgres;

--
-- Name: set_ne(text, anyarray, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_ne(text, anyarray, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, $3, '' );
$_$;


ALTER FUNCTION samplesdb.set_ne(text, anyarray, text) OWNER TO postgres;

--
-- Name: set_ne(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION set_ne(text, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _relne( $1, $2, $3, '' );
$_$;


ALTER FUNCTION samplesdb.set_ne(text, text, text) OWNER TO postgres;

--
-- Name: skip(integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION skip(integer) RETURNS text
    LANGUAGE sql
    AS $_$SELECT skip(NULL, $1)$_$;


ALTER FUNCTION samplesdb.skip(integer) OWNER TO postgres;

--
-- Name: skip(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION skip(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT ok( TRUE ) || ' ' || diag( 'SKIP' || COALESCE(' ' || $1, '') );
$_$;


ALTER FUNCTION samplesdb.skip(text) OWNER TO postgres;

--
-- Name: skip(integer, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION skip(integer, text) RETURNS text
    LANGUAGE sql
    AS $_$SELECT skip($2, $1)$_$;


ALTER FUNCTION samplesdb.skip(integer, text) OWNER TO postgres;

--
-- Name: skip(text, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION skip(why text, how_many integer) RETURNS text
    LANGUAGE plpgsql
    AS $$
DECLARE
    output TEXT[];
BEGIN
    output := '{}';
    FOR i IN 1..how_many LOOP
        output = array_append(
            output,
            ok( TRUE ) || ' ' || diag( 'SKIP' || COALESCE( ' ' || why, '') )
        );
    END LOOP;
    RETURN array_to_string(output, E'\n');
END;
$$;


ALTER FUNCTION samplesdb.skip(why text, how_many integer) OWNER TO postgres;

--
-- Name: table_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT table_owner_is(
        $1, $2,
        'Table ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.table_owner_is(name, name) OWNER TO postgres;

--
-- Name: table_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT table_owner_is(
        $1, $2, $3,
        'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.table_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: table_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('r'::char, $1);
BEGIN
    -- Make sure the table exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Table ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.table_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: table_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('r'::char, $1, $2);
BEGIN
    -- Make sure the table exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.table_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: table_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT table_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on table ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.table_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: table_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_table_privs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.table_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: table_privs_are(name, name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_privs_are(name, name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT table_privs_are(
        $1, $2, $3, $4,
        'Role ' || quote_ident($3) || ' should be granted '
            || CASE WHEN $4[1] IS NULL THEN 'no privileges' ELSE array_to_string($4, ', ') END
            || ' on table ' || quote_ident($1) || '.' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.table_privs_are(name, name, name, name[]) OWNER TO postgres;

--
-- Name: table_privs_are(name, name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION table_privs_are(name, name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_table_privs( $3, quote_ident($1) || '.' || quote_ident($2) );
BEGIN
    IF grants[1] = 'undefined_table' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Table ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $5) || E'\n' || diag(
            '    Role ' || quote_ident($3) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $4, $5);
END;
$_$;


ALTER FUNCTION samplesdb.table_privs_are(name, name, name, name[], text) OWNER TO postgres;

--
-- Name: tables_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tables_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'tables', _extras('r', $1), _missing('r', $1),
        'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct tables'
    );
$_$;


ALTER FUNCTION samplesdb.tables_are(name[]) OWNER TO postgres;

--
-- Name: tables_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tables_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'tables', _extras('r', $1), _missing('r', $1), $2);
$_$;


ALTER FUNCTION samplesdb.tables_are(name[], text) OWNER TO postgres;

--
-- Name: tables_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tables_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'tables', _extras('r', $1, $2), _missing('r', $1, $2),
        'Schema ' || quote_ident($1) || ' should have the correct tables'
    );
$_$;


ALTER FUNCTION samplesdb.tables_are(name, name[]) OWNER TO postgres;

--
-- Name: tables_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tables_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'tables', _extras('r', $1, $2), _missing('r', $1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.tables_are(name, name[], text) OWNER TO postgres;

--
-- Name: tablespace_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tablespace_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT tablespace_owner_is(
        $1, $2,
        'Tablespace ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.tablespace_owner_is(name, name) OWNER TO postgres;

--
-- Name: tablespace_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tablespace_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_tablespace_owner($1);
BEGIN
    -- Make sure the tablespace exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Tablespace ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.tablespace_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: tablespace_privs_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tablespace_privs_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT tablespace_privs_are(
        $1, $2, $3,
        'Role ' || quote_ident($2) || ' should be granted '
            || CASE WHEN $3[1] IS NULL THEN 'no privileges' ELSE array_to_string($3, ', ') END
            || ' on tablespace ' || quote_ident($1)
    );
$_$;


ALTER FUNCTION samplesdb.tablespace_privs_are(name, name, name[]) OWNER TO postgres;

--
-- Name: tablespace_privs_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tablespace_privs_are(name, name, name[], text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    grants TEXT[] := _get_tablespaceprivs( $2, quote_ident($1) );
BEGIN
    IF grants[1] = 'undefined_tablespace' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Tablespace ' || quote_ident($1) || ' does not exist'
        );
    ELSIF grants[1] = 'undefined_role' THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            '    Role ' || quote_ident($2) || ' does not exist'
        );
    END IF;
    RETURN _assets_are('privileges', grants, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.tablespace_privs_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: tablespaces_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tablespaces_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT tablespaces_are( $1, 'There should be the correct tablespaces' );
$_$;


ALTER FUNCTION samplesdb.tablespaces_are(name[]) OWNER TO postgres;

--
-- Name: tablespaces_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION tablespaces_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'tablespaces',
        ARRAY(
            SELECT spcname
              FROM pg_catalog.pg_tablespace
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
               FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT spcname
              FROM pg_catalog.pg_tablespace
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.tablespaces_are(name[], text) OWNER TO postgres;

--
-- Name: throws_ilike(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ilike(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_ilike($1, $2, 'Should throw exception like ' || quote_literal($2) );
$_$;


ALTER FUNCTION samplesdb.throws_ilike(text, text) OWNER TO postgres;

--
-- Name: throws_ilike(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ilike(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE _query($1);
    RETURN ok( FALSE, $3 ) || E'\n' || diag( '    no exception thrown' );
EXCEPTION WHEN OTHERS THEN
    return _tlike( SQLERRM ~~* $2, SQLERRM, $2, $3 );
END;
$_$;


ALTER FUNCTION samplesdb.throws_ilike(text, text, text) OWNER TO postgres;

--
-- Name: throws_imatching(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_imatching(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_imatching($1, $2, 'Should throw exception matching ' || quote_literal($2) );
$_$;


ALTER FUNCTION samplesdb.throws_imatching(text, text) OWNER TO postgres;

--
-- Name: throws_imatching(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_imatching(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE _query($1);
    RETURN ok( FALSE, $3 ) || E'\n' || diag( '    no exception thrown' );
EXCEPTION WHEN OTHERS THEN
    return _tlike( SQLERRM ~* $2, SQLERRM, $2, $3 );
END;
$_$;


ALTER FUNCTION samplesdb.throws_imatching(text, text, text) OWNER TO postgres;

--
-- Name: throws_like(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_like(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_like($1, $2, 'Should throw exception like ' || quote_literal($2) );
$_$;


ALTER FUNCTION samplesdb.throws_like(text, text) OWNER TO postgres;

--
-- Name: throws_like(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_like(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE _query($1);
    RETURN ok( FALSE, $3 ) || E'\n' || diag( '    no exception thrown' );
EXCEPTION WHEN OTHERS THEN
    return _tlike( SQLERRM ~~ $2, SQLERRM, $2, $3 );
END;
$_$;


ALTER FUNCTION samplesdb.throws_like(text, text, text) OWNER TO postgres;

--
-- Name: throws_matching(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_matching(text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_matching($1, $2, 'Should throw exception matching ' || quote_literal($2) );
$_$;


ALTER FUNCTION samplesdb.throws_matching(text, text) OWNER TO postgres;

--
-- Name: throws_matching(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_matching(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    EXECUTE _query($1);
    RETURN ok( FALSE, $3 ) || E'\n' || diag( '    no exception thrown' );
EXCEPTION WHEN OTHERS THEN
    return _tlike( SQLERRM ~ $2, SQLERRM, $2, $3 );
END;
$_$;


ALTER FUNCTION samplesdb.throws_matching(text, text, text) OWNER TO postgres;

--
-- Name: throws_ok(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_ok( $1, NULL, NULL, NULL );
$_$;


ALTER FUNCTION samplesdb.throws_ok(text) OWNER TO postgres;

--
-- Name: throws_ok(text, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text, integer) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_ok( $1, $2::char(5), NULL, NULL );
$_$;


ALTER FUNCTION samplesdb.throws_ok(text, integer) OWNER TO postgres;

--
-- Name: throws_ok(text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF octet_length($2) = 5 THEN
        RETURN throws_ok( $1, $2::char(5), NULL, NULL );
    ELSE
        RETURN throws_ok( $1, NULL, $2, NULL );
    END IF;
END;
$_$;


ALTER FUNCTION samplesdb.throws_ok(text, text) OWNER TO postgres;

--
-- Name: throws_ok(text, integer, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text, integer, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_ok( $1, $2::char(5), $3, NULL );
$_$;


ALTER FUNCTION samplesdb.throws_ok(text, integer, text) OWNER TO postgres;

--
-- Name: throws_ok(text, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
BEGIN
    IF octet_length($2) = 5 THEN
        RETURN throws_ok( $1, $2::char(5), $3, NULL );
    ELSE
        RETURN throws_ok( $1, NULL, $2, $3 );
    END IF;
END;
$_$;


ALTER FUNCTION samplesdb.throws_ok(text, text, text) OWNER TO postgres;

--
-- Name: throws_ok(text, character, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text, character, text, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    query     TEXT := _query($1);
    errcode   ALIAS FOR $2;
    errmsg    ALIAS FOR $3;
    desctext  ALIAS FOR $4;
    descr     TEXT;
BEGIN
    descr := COALESCE(
          desctext,
          'threw ' || errcode || ': ' || errmsg,
          'threw ' || errcode,
          'threw ' || errmsg,
          'threw an exception'
    );
    EXECUTE query;
    RETURN ok( FALSE, descr ) || E'\n' || diag(
           '      caught: no exception' ||
        E'\n      wanted: ' || COALESCE( errcode, 'an exception' )
    );
EXCEPTION WHEN OTHERS THEN
    IF (errcode IS NULL OR SQLSTATE = errcode)
        AND ( errmsg IS NULL OR SQLERRM = errmsg)
    THEN
        -- The expected errcode and/or message was thrown.
        RETURN ok( TRUE, descr );
    ELSE
        -- This was not the expected errcode or errmsg.
        RETURN ok( FALSE, descr ) || E'\n' || diag(
               '      caught: ' || SQLSTATE || ': ' || SQLERRM ||
            E'\n      wanted: ' || COALESCE( errcode, 'an exception' ) ||
            COALESCE( ': ' || errmsg, '')
        );
    END IF;
END;
$_$;


ALTER FUNCTION samplesdb.throws_ok(text, character, text, text) OWNER TO postgres;

--
-- Name: throws_ok(text, integer, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION throws_ok(text, integer, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT throws_ok( $1, $2::char(5), $3, $4 );
$_$;


ALTER FUNCTION samplesdb.throws_ok(text, integer, text, text) OWNER TO postgres;

--
-- Name: todo(integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo(how_many integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    PERFORM _add('todo', COALESCE(how_many, 1), '');
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.todo(how_many integer) OWNER TO postgres;

--
-- Name: todo(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo(why text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    PERFORM _add('todo', 1, COALESCE(why, ''));
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.todo(why text) OWNER TO postgres;

--
-- Name: todo(integer, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo(how_many integer, why text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    PERFORM _add('todo', COALESCE(how_many, 1), COALESCE(why, ''));
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.todo(how_many integer, why text) OWNER TO postgres;

--
-- Name: todo(text, integer); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo(why text, how_many integer) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    PERFORM _add('todo', COALESCE(how_many, 1), COALESCE(why, ''));
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.todo(why text, how_many integer) OWNER TO postgres;

--
-- Name: todo_end(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo_end() RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
DECLARE
    id integer;
BEGIN
    id := _get_latest( 'todo', -1 );
    IF id IS NULL THEN
        RAISE EXCEPTION 'todo_end() called without todo_start()';
    END IF;
    EXECUTE 'DELETE FROM __tcache__ WHERE id = ' || id;
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.todo_end() OWNER TO postgres;

--
-- Name: todo_start(); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo_start() RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $$
BEGIN
    PERFORM _add('todo', -1, '');
    RETURN;
END;
$$;


ALTER FUNCTION samplesdb.todo_start() OWNER TO postgres;

--
-- Name: todo_start(text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION todo_start(text) RETURNS SETOF boolean
    LANGUAGE plpgsql
    AS $_$
BEGIN
    PERFORM _add('todo', -1, COALESCE($1, ''));
    RETURN;
END;
$_$;


ALTER FUNCTION samplesdb.todo_start(text) OWNER TO postgres;

--
-- Name: trigger_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION trigger_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT trigger_is(
        $1, $2, $3,
        'Trigger ' || quote_ident($2) || ' should call ' || quote_ident($3) || '()'
    );
$_$;


ALTER FUNCTION samplesdb.trigger_is(name, name, name) OWNER TO postgres;

--
-- Name: trigger_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION trigger_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    pname text;
BEGIN
    SELECT p.proname
      FROM pg_catalog.pg_trigger t
      JOIN pg_catalog.pg_class ct ON ct.oid = t.tgrelid
      JOIN pg_catalog.pg_proc p   ON p.oid = t.tgfoid
     WHERE ct.relname = $1
       AND t.tgname   = $2
       AND pg_catalog.pg_table_is_visible(ct.oid)
      INTO pname;

    RETURN is( pname, $3::text, $4 );
END;
$_$;


ALTER FUNCTION samplesdb.trigger_is(name, name, name, text) OWNER TO postgres;

--
-- Name: trigger_is(name, name, name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION trigger_is(name, name, name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT trigger_is(
        $1, $2, $3, $4, $5,
        'Trigger ' || quote_ident($3) || ' should call ' || quote_ident($4) || '.' || quote_ident($5) || '()'
    );
$_$;


ALTER FUNCTION samplesdb.trigger_is(name, name, name, name, name) OWNER TO postgres;

--
-- Name: trigger_is(name, name, name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION trigger_is(name, name, name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    pname text;
BEGIN
    SELECT quote_ident(ni.nspname) || '.' || quote_ident(p.proname)
      FROM pg_catalog.pg_trigger t
      JOIN pg_catalog.pg_class ct     ON ct.oid = t.tgrelid
      JOIN pg_catalog.pg_namespace nt ON nt.oid = ct.relnamespace
      JOIN pg_catalog.pg_proc p       ON p.oid = t.tgfoid
      JOIN pg_catalog.pg_namespace ni ON ni.oid = p.pronamespace
     WHERE nt.nspname = $1
       AND ct.relname = $2
       AND t.tgname   = $3
      INTO pname;

    RETURN is( pname, quote_ident($4) || '.' || quote_ident($5), $6 );
END;
$_$;


ALTER FUNCTION samplesdb.trigger_is(name, name, name, name, name, text) OWNER TO postgres;

--
-- Name: triggers_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION triggers_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT triggers_are( $1, $2, 'Table ' || quote_ident($1) || ' should have the correct triggers' );
$_$;


ALTER FUNCTION samplesdb.triggers_are(name, name[]) OWNER TO postgres;

--
-- Name: triggers_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION triggers_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'triggers',
        ARRAY(
            SELECT t.tgname
              FROM pg_catalog.pg_trigger t
              JOIN pg_catalog.pg_class c ON c.oid = t.tgrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
             WHERE c.relname = $1
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND NOT t.tgisinternal
            EXCEPT
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
        ),
        ARRAY(
            SELECT $2[i]
              FROM generate_series(1, array_upper($2, 1)) s(i)
            EXCEPT
            SELECT t.tgname
              FROM pg_catalog.pg_trigger t
              JOIN pg_catalog.pg_class c ON c.oid = t.tgrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
               AND n.nspname NOT IN ('pg_catalog', 'information_schema')
               AND NOT t.tgisinternal
        ),
        $3
    );
$_$;


ALTER FUNCTION samplesdb.triggers_are(name, name[], text) OWNER TO postgres;

--
-- Name: triggers_are(name, name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION triggers_are(name, name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT triggers_are( $1, $2, $3, 'Table ' || quote_ident($1) || '.' || quote_ident($2) || ' should have the correct triggers' );
$_$;


ALTER FUNCTION samplesdb.triggers_are(name, name, name[]) OWNER TO postgres;

--
-- Name: triggers_are(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION triggers_are(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'triggers',
        ARRAY(
            SELECT t.tgname
              FROM pg_catalog.pg_trigger t
              JOIN pg_catalog.pg_class c     ON c.oid = t.tgrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
             WHERE n.nspname = $1
               AND c.relname = $2
               AND NOT t.tgisinternal
            EXCEPT
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
        ),
        ARRAY(
            SELECT $3[i]
              FROM generate_series(1, array_upper($3, 1)) s(i)
            EXCEPT
            SELECT t.tgname
              FROM pg_catalog.pg_trigger t
              JOIN pg_catalog.pg_class c     ON c.oid = t.tgrelid
              JOIN pg_catalog.pg_namespace n ON n.oid = c.relnamespace
             WHERE n.nspname = $1
               AND c.relname = $2
               AND NOT t.tgisinternal
        ),
        $4
    );
$_$;


ALTER FUNCTION samplesdb.triggers_are(name, name, name[], text) OWNER TO postgres;

--
-- Name: type_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION type_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT type_owner_is(
        $1, $2,
        'Type ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.type_owner_is(name, name) OWNER TO postgres;

--
-- Name: type_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION type_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT type_owner_is(
        $1, $2, $3,
        'Type ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.type_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: type_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION type_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_type_owner($1);
BEGIN
    -- Make sure the type exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    Type ' || quote_ident($1) || ' not found'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.type_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: type_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION type_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_type_owner($1, $2);
BEGIN
    -- Make sure the type exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    Type ' || quote_ident($1) || '.' || quote_ident($2) || ' not found'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.type_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: types_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION types_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, 'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct types', NULL );
$_$;


ALTER FUNCTION samplesdb.types_are(name[]) OWNER TO postgres;

--
-- Name: types_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION types_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.types_are(name[], text) OWNER TO postgres;

--
-- Name: types_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION types_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, 'Schema ' || quote_ident($1) || ' should have the correct types', NULL );
$_$;


ALTER FUNCTION samplesdb.types_are(name, name[]) OWNER TO postgres;

--
-- Name: types_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION types_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _types_are( $1, $2, $3, NULL );
$_$;


ALTER FUNCTION samplesdb.types_are(name, name[], text) OWNER TO postgres;

--
-- Name: unalike(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION unalike(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~~ $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.unalike(anyelement, text) OWNER TO postgres;

--
-- Name: unalike(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION unalike(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~~ $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.unalike(anyelement, text, text) OWNER TO postgres;

--
-- Name: unialike(anyelement, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION unialike(anyelement, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~~* $2, $1, $2, NULL );
$_$;


ALTER FUNCTION samplesdb.unialike(anyelement, text) OWNER TO postgres;

--
-- Name: unialike(anyelement, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION unialike(anyelement, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _unalike( $1 !~~* $2, $1, $2, $3 );
$_$;


ALTER FUNCTION samplesdb.unialike(anyelement, text, text) OWNER TO postgres;

--
-- Name: users_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION users_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT users_are( $1, 'There should be the correct users' );
$_$;


ALTER FUNCTION samplesdb.users_are(name[]) OWNER TO postgres;

--
-- Name: users_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION users_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'users',
        ARRAY(
            SELECT usename
              FROM pg_catalog.pg_user
            EXCEPT
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
        ),
        ARRAY(
            SELECT $1[i]
              FROM generate_series(1, array_upper($1, 1)) s(i)
            EXCEPT
            SELECT usename
              FROM pg_catalog.pg_user
        ),
        $2
    );
$_$;


ALTER FUNCTION samplesdb.users_are(name[], text) OWNER TO postgres;

--
-- Name: view_owner_is(name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION view_owner_is(name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT view_owner_is(
        $1, $2,
        'View ' || quote_ident($1) || ' should be owned by ' || quote_ident($2)
    );
$_$;


ALTER FUNCTION samplesdb.view_owner_is(name, name) OWNER TO postgres;

--
-- Name: view_owner_is(name, name, name); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION view_owner_is(name, name, name) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT view_owner_is(
        $1, $2, $3,
        'View ' || quote_ident($1) || '.' || quote_ident($2) || ' should be owned by ' || quote_ident($3)
    );
$_$;


ALTER FUNCTION samplesdb.view_owner_is(name, name, name) OWNER TO postgres;

--
-- Name: view_owner_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION view_owner_is(name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('v'::char, $1);
BEGIN
    -- Make sure the view exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $3) || E'\n' || diag(
            E'    View ' || quote_ident($1) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $2, $3);
END;
$_$;


ALTER FUNCTION samplesdb.view_owner_is(name, name, text) OWNER TO postgres;

--
-- Name: view_owner_is(name, name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION view_owner_is(name, name, name, text) RETURNS text
    LANGUAGE plpgsql
    AS $_$
DECLARE
    owner NAME := _get_rel_owner('v'::char, $1, $2);
BEGIN
    -- Make sure the view exists.
    IF owner IS NULL THEN
        RETURN ok(FALSE, $4) || E'\n' || diag(
            E'    View ' || quote_ident($1) || '.' || quote_ident($2) || ' does not exist'
        );
    END IF;

    RETURN is(owner, $3, $4);
END;
$_$;


ALTER FUNCTION samplesdb.view_owner_is(name, name, name, text) OWNER TO postgres;

--
-- Name: views_are(name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION views_are(name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'views', _extras('v', $1), _missing('v', $1),
        'Search path ' || pg_catalog.current_setting('search_path') || ' should have the correct views'
    );
$_$;


ALTER FUNCTION samplesdb.views_are(name[]) OWNER TO postgres;

--
-- Name: views_are(name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION views_are(name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'views', _extras('v', $1), _missing('v', $1), $2);
$_$;


ALTER FUNCTION samplesdb.views_are(name[], text) OWNER TO postgres;

--
-- Name: views_are(name, name[]); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION views_are(name, name[]) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are(
        'views', _extras('v', $1, $2), _missing('v', $1, $2),
        'Schema ' || quote_ident($1) || ' should have the correct views'
    );
$_$;


ALTER FUNCTION samplesdb.views_are(name, name[]) OWNER TO postgres;

--
-- Name: views_are(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION views_are(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _are( 'views', _extras('v', $1, $2), _missing('v', $1, $2), $3);
$_$;


ALTER FUNCTION samplesdb.views_are(name, name[], text) OWNER TO postgres;

--
-- Name: volatility_is(name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT volatility_is(
        $1, $2,
        'Function ' || quote_ident($1) || '() should be ' || _refine_vol($2)
    );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, text) OWNER TO postgres;

--
-- Name: volatility_is(name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT volatility_is(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '(' ||
        array_to_string($2, ', ') || ') should be ' || _refine_vol($3)
    );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, name[], text) OWNER TO postgres;

--
-- Name: volatility_is(name, name, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, name, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT volatility_is(
        $1, $2, $3,
        'Function ' || quote_ident($1) || '.' || quote_ident($2)
        || '() should be ' || _refine_vol($3)
    );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, name, text) OWNER TO postgres;

--
-- Name: volatility_is(name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, _vol($1), _refine_vol($2), $3 );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, text, text) OWNER TO postgres;

--
-- Name: volatility_is(name, name[], text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, name[], text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare(NULL, $1, $2, _vol($1, $2), _refine_vol($3), $4 );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, name[], text, text) OWNER TO postgres;

--
-- Name: volatility_is(name, name, name[], text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, name, name[], text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT volatility_is(
        $1, $2, $3, $4,
        'Function ' || quote_ident($1) || '.' || quote_ident($2) || '(' ||
        array_to_string($3, ', ') || ') should be ' || _refine_vol($4)
    );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, name, name[], text) OWNER TO postgres;

--
-- Name: volatility_is(name, name, text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, name, text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, _vol($1, $2), _refine_vol($3), $4 );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, name, text, text) OWNER TO postgres;

--
-- Name: volatility_is(name, name, name[], text, text); Type: FUNCTION; Schema: samplesdb; Owner: postgres
--

CREATE FUNCTION volatility_is(name, name, name[], text, text) RETURNS text
    LANGUAGE sql
    AS $_$
    SELECT _func_compare($1, $2, $3, _vol($1, $2, $3), _refine_vol($4), $5 );
$_$;


ALTER FUNCTION samplesdb.volatility_is(name, name, name[], text, text) OWNER TO postgres;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: __cdna_system_10_0; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE __cdna_system_10_0 (
    study_name character varying(100),
    cdna_id character varying(100),
    rna_id character varying(100),
    cdna_concentration numeric,
    cdna_yield numeric,
    cdna_260 numeric,
    cdna_280 numeric,
    cdna_260_280 numeric,
    cdna_260_230 numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    ss_cdna_yield numeric,
    ss_cdna_concentration numeric,
    ss_cdna_260 numeric,
    ss_cdna_280 numeric,
    ss_cdna_260_280 numeric,
    ss_cdna_260_230 numeric,
    cdna_total_input numeric,
    cdna_sufficient numeric,
    cdna_batch character varying(100),
    cdna_date date,
    cdna_note character varying(1000),
    cdna_well character varying(2),
    cdna_merge character varying(100),
    original_cdna_id character varying,
    cdna_operator character varying,
    cdna_variable character varying,
    rna_input numeric,
    cdna_volume numeric,
    mac_id character varying,
    rna_measure character varying,
    nucleic_acid_type character varying,
    input_rna_extraction_method character varying,
    rna_input_original character varying,
    finalized_cdna boolean
);


ALTER TABLE __cdna_system_10_0 OWNER TO administrator;

--
-- Name: __cdna_system_5_0; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE __cdna_system_5_0 (
    study_name character varying(100),
    cdna_id character varying(100),
    rna_id character varying(100),
    cdna_concentration numeric,
    cdna_yield numeric,
    cdna_260 numeric,
    cdna_280 numeric,
    cdna_260_280 numeric,
    cdna_260_230 numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    ss_cdna_yield numeric,
    ss_cdna_concentration numeric,
    ss_cdna_260 numeric,
    ss_cdna_280 numeric,
    ss_cdna_260_280 numeric,
    ss_cdna_260_230 numeric,
    cdna_total_input numeric,
    cdna_sufficient numeric,
    cdna_batch character varying(100),
    cdna_date date,
    cdna_note character varying(1000),
    cdna_well character varying(2),
    cdna_merge character varying(100),
    original_cdna_id character varying,
    cdna_operator character varying,
    cdna_variable character varying,
    rna_input numeric,
    cdna_volume numeric,
    mac_id character varying,
    rna_measure character varying,
    nucleic_acid_type character varying,
    input_rna_extraction_method character varying,
    rna_input_original character varying,
    finalized_cdna boolean
);


ALTER TABLE __cdna_system_5_0 OWNER TO administrator;

--
-- Name: __cdna_system_time_1000; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE __cdna_system_time_1000 (
    study_name character varying(100),
    cdna_id character varying(100),
    rna_id character varying(100),
    cdna_concentration numeric,
    cdna_yield numeric,
    cdna_260 numeric,
    cdna_280 numeric,
    cdna_260_280 numeric,
    cdna_260_230 numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    ss_cdna_yield numeric,
    ss_cdna_concentration numeric,
    ss_cdna_260 numeric,
    ss_cdna_280 numeric,
    ss_cdna_260_280 numeric,
    ss_cdna_260_230 numeric,
    cdna_total_input numeric,
    cdna_sufficient numeric,
    cdna_batch character varying(100),
    cdna_date date,
    cdna_note character varying(1000),
    cdna_well character varying(2),
    cdna_merge character varying(100),
    original_cdna_id character varying,
    cdna_operator character varying,
    cdna_variable character varying,
    rna_input numeric,
    cdna_volume numeric,
    mac_id character varying,
    rna_measure character varying,
    nucleic_acid_type character varying,
    input_rna_extraction_method character varying,
    rna_input_original character varying,
    finalized_cdna boolean
);


ALTER TABLE __cdna_system_time_1000 OWNER TO administrator;

--
-- Name: a; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE a (
    grid_celfile_name character varying
);


ALTER TABLE a OWNER TO administrator;

--
-- Name: abc; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE abc (
    grid_celfile_name character varying
);


ALTER TABLE abc OWNER TO administrator;

--
-- Name: backups; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE backups (
    schema character varying(64),
    file character varying(1024),
    created timestamp without time zone,
    created_by character varying(25),
    tables character varying(64)[],
    comment character varying(256)
);


ALTER TABLE backups OWNER TO samplesdb_table_owner;

--
-- Name: cdna; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE cdna (
    study_name character varying(100) NOT NULL,
    cdna_id character varying(100) NOT NULL,
    rna_id character varying(100) NOT NULL,
    cdna_concentration numeric,
    cdna_yield numeric,
    cdna_260 numeric,
    cdna_280 numeric,
    cdna_260_280 numeric,
    cdna_260_230 numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    ss_cdna_yield numeric,
    ss_cdna_concentration numeric,
    ss_cdna_260 numeric,
    ss_cdna_280 numeric,
    ss_cdna_260_280 numeric,
    ss_cdna_260_230 numeric,
    cdna_total_input numeric,
    cdna_sufficient numeric,
    cdna_batch character varying(100),
    cdna_date date,
    cdna_note character varying(1000),
    cdna_well character varying(2),
    cdna_merge character varying(100),
    original_cdna_id character varying,
    cdna_operator character varying,
    cdna_variable character varying,
    rna_input numeric,
    cdna_volume numeric,
    mac_id character varying,
    rna_measure character varying,
    nucleic_acid_type character varying,
    input_rna_extraction_method character varying,
    rna_input_original character varying,
    finalized_cdna boolean,
    cdna_reported boolean
);


ALTER TABLE cdna OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN cdna.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.study_name IS 'Foreign Key to STUDIES table';


--
-- Name: COLUMN cdna.cdna_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.cdna_id IS 'Primary Key for the CDNA table';


--
-- Name: COLUMN cdna.rna_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.rna_id IS 'Foreign Key to RNA table';


--
-- Name: COLUMN cdna.ss_cdna_yield; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.ss_cdna_yield IS 'single strand cDNA yield from Nugen v2 Kits (ng)
';


--
-- Name: COLUMN cdna.cdna_total_input; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.cdna_total_input IS 'Amount loaded onto array (ug)';


--
-- Name: COLUMN cdna.cdna_note; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.cdna_note IS 'Notes regarding a cDNA sample (ie: why it was repeated, why no MP3 results were mapped to it)
';


--
-- Name: COLUMN cdna.cdna_merge; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.cdna_merge IS 'cdna_id indicating which this cdna sample was merged INTO';


--
-- Name: COLUMN cdna.original_cdna_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.original_cdna_id IS 'original cdna id as used in nanodrop results
';


--
-- Name: COLUMN cdna.cdna_operator; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.cdna_operator IS 'Initial of operator processing the cdna
';


--
-- Name: COLUMN cdna.cdna_variable; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN cdna.cdna_variable IS 'Changing variable to the processing of the cDNA sample - usually related to analytical validations';


--
-- Name: celfiles; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE celfiles (
    study_name character varying(100),
    celfile_name character varying(100) NOT NULL,
    cdna_id character varying(100),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    chip_id character varying(100),
    erg_gene numeric,
    etv1_gene numeric,
    etv4_gene numeric,
    erg_fusion numeric,
    erg_type character varying(100),
    pten_deletion character varying(100),
    spink1_up numeric,
    celfile_lab character varying(20),
    qc_batch_id character varying(20),
    location character varying(1024),
    qc_flag character varying(20),
    rsi_scan numeric,
    rf22_scan numeric,
    gcc_scan numeric,
    gcs_scan numeric,
    rf22_scan_5yr numeric,
    rf22_prod_5yr numeric,
    rf22_scan_3yrbcr numeric,
    rf22_prod_3yrbcr numeric,
    percentpresent numeric,
    rf22_percentpresent numeric,
    rf22_prod numeric,
    celfile_batch character varying(100),
    posneg_auc numeric,
    hybcontrol character varying(25),
    hybpercentpresent numeric,
    rehyb character varying(100),
    celfile_note character varying(500),
    scanner_celfile_name character varying(100),
    scanner character varying(32),
    bcm_version character varying,
    celfile_name_frma character varying,
    hybcontrol_v3 character varying,
    hybpercentpresent_v3 numeric,
    celfile_date date,
    qc_flag_v3 character varying,
    scan_date date,
    qc_flag_v2 character varying,
    hybcontrol_v2 character varying,
    hybpercentpresent_v2 numeric,
    rf15_percentpresent numeric,
    percentpresent_rd numeric,
    posneg_auc_rd numeric,
    qc_flag_v1 character varying(20),
    hybcontrol_v1 character varying(20),
    hybpercentpresent_v1 numeric,
    wm_celfile_name character varying(100),
    geo_celfile_name character varying(100),
    qc_flag_bladder character varying,
    finalized_celfile boolean,
    celfile_reported boolean,
    CONSTRAINT qc_batch_id_check CHECK ((((qc_batch_id)::text ~ similar_escape('CHLA-[0-9]{8}'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('GBX-Batch-[0-9]{1,5}'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('CHLA-Batch-[0-9]{3}'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('CHLA-[0-9]{8}-[1-9]{1}'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('CHLA-[0-9]{1,2}'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('external_(FIMIM|DKFZ|MSKCC|ICR|EMC|GSE13066)'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('GBX-CLIAVal'::text, NULL::text)) OR ((qc_batch_id)::text ~ similar_escape('GBX-BCAPreAV'::text, NULL::text))))
);


ALTER TABLE celfiles OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN celfiles.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.study_name IS 'Soft link to STUDIES table';


--
-- Name: COLUMN celfiles.celfile_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.celfile_name IS 'Primary Key for the CELFILES table';


--
-- Name: COLUMN celfiles.cdna_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.cdna_id IS 'Soft link to CDNA table';


--
-- Name: COLUMN celfiles.chip_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.chip_id IS 'CHIP_ID of the original chip which was scanned.';


--
-- Name: COLUMN celfiles.erg_gene; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.erg_gene IS 'ERG expression (FISH)';


--
-- Name: COLUMN celfiles.etv1_gene; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.etv1_gene IS 'ETV1 expression';


--
-- Name: COLUMN celfiles.etv4_gene; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.etv4_gene IS 'ETV4 expression';


--
-- Name: COLUMN celfiles.erg_fusion; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.erg_fusion IS 'ERG fusion type';


--
-- Name: COLUMN celfiles.erg_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.erg_type IS 'ERG fusion type';


--
-- Name: COLUMN celfiles.pten_deletion; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.pten_deletion IS 'PTEN deletion status';


--
-- Name: COLUMN celfiles.spink1_up; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.spink1_up IS 'SPINK1 upregulation';


--
-- Name: COLUMN celfiles.celfile_lab; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.celfile_lab IS 'Lab in which celfiles were scanned';


--
-- Name: COLUMN celfiles.qc_batch_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.qc_batch_id IS 'Batch ID (LAB-YYYYMMDD-X)
LAB = (''CHLA'', ''GBX'', ''EXT'')
X - numeric batch for batches ran on the same day';


--
-- Name: COLUMN celfiles.location; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.location IS 'Celfile location';


--
-- Name: COLUMN celfiles.qc_flag; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.qc_flag IS 'PASS or FAIL
';


--
-- Name: COLUMN celfiles.posneg_auc; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.posneg_auc IS 'Production Positive vs negative AUC - standard criteria: >0.6';


--
-- Name: COLUMN celfiles.celfile_note; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.celfile_note IS 'Notes regarding a celfile - specifically if it''ll cause failures at normalization or QC';


--
-- Name: COLUMN celfiles.scanner_celfile_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.scanner_celfile_name IS 'Original CEL file name from scanner - prior to MP3 processing - corresponds to CEL file column in MP3 output';


--
-- Name: COLUMN celfiles.bcm_version; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.bcm_version IS 'MP3 version
';


--
-- Name: COLUMN celfiles.celfile_name_frma; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.celfile_name_frma IS 'Original CEL file produced by MP3 frma production version';


--
-- Name: COLUMN celfiles.hybcontrol_v3; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.hybcontrol_v3 IS 'Version 3 (R2 scan) hyb control (PASS/FAIL)';


--
-- Name: COLUMN celfiles.hybpercentpresent_v3; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.hybpercentpresent_v3 IS 'Hyb control percent present (R2 SCAN)';


--
-- Name: COLUMN celfiles.qc_flag_v3; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.qc_flag_v3 IS 'QC flag based on hybcontrol_v3 (SCAN R2)';


--
-- Name: COLUMN celfiles.qc_flag_v2; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.qc_flag_v2 IS 'QC flag based on hybcontrol_v2 (fRMA R2)';


--
-- Name: COLUMN celfiles.hybcontrol_v2; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.hybcontrol_v2 IS 'Version 2 (R2 fRMA) hyb control (PASS/FAIL)';


--
-- Name: COLUMN celfiles.hybpercentpresent_v2; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.hybpercentpresent_v2 IS 'Hyb control percent present (R2 fRMA)';


--
-- Name: COLUMN celfiles.rf15_percentpresent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.rf15_percentpresent IS 'R&D PercentPresent of the 15 features in RF15';


--
-- Name: COLUMN celfiles.percentpresent_rd; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.percentpresent_rd IS 'R&D PercentPresent - APT based with different feature set - tends to be lower than Production Percent Present';


--
-- Name: COLUMN celfiles.posneg_auc_rd; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfiles.posneg_auc_rd IS 'R&D Positive vs negative AUC - based on RMA - tends to be higher than Production posneg_auc';


--
-- Name: patients; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE patients (
    patient_id character varying(100) NOT NULL,
    study_name character varying(100) NOT NULL,
    clings numeric,
    clings_p numeric,
    clings_s numeric,
    pathgs numeric,
    pathgs_p numeric,
    pathgs_s numeric,
    clings_p_percent numeric,
    clings_s_percent numeric,
    pathgs_p_percent numeric,
    pathgs_s_percent numeric,
    age numeric,
    pstage character varying(100),
    preop_psa numeric,
    bcr numeric,
    adt numeric,
    rt numeric,
    external_id character varying(100),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    age_rt numeric,
    race character varying(100),
    capra_s numeric,
    comments character varying(300),
    dob timestamp(6) without time zone,
    ece numeric,
    fu_rp_time numeric,
    fu_time numeric,
    lastfu_date timestamp(6) without time zone,
    met numeric,
    met_fu_date timestamp(6) without time zone,
    met_time numeric,
    neoadj_adt numeric,
    os numeric,
    os_time numeric,
    pathid numeric,
    pelvis numeric,
    postrp_psa numeric,
    prert_psa numeric,
    psadt numeric,
    psadt_rp_rt numeric,
    rp_date timestamp(6) without time zone,
    rp_to_bcr_time numeric,
    rp_to_rt_months numeric,
    rt_dose numeric,
    rt_end timestamp(6) without time zone,
    rt_start timestamp(6) without time zone,
    original_bcr_time numeric,
    original_met_time numeric,
    original_os_time numeric,
    rt_type character varying(100),
    srt_nadir_psa character(100),
    stephenson_2yr numeric,
    stephenson_5yr numeric,
    stephenson_7yr numeric,
    stephenson_rev numeric,
    stephenson_srt numeric,
    stephenson_srt_rev numeric,
    surgery_id character varying(100),
    stephenson_10yr numeric,
    os_fu_date timestamp(6) without time zone,
    lni numeric,
    cstage character varying(100),
    tumor_vol numeric,
    mayo_def character varying(100),
    psa1 numeric,
    psa2 numeric,
    psa1_time numeric,
    psa2_time numeric,
    pcsm numeric,
    pcsm_time numeric,
    lcr numeric,
    lcr_time numeric,
    adjuvant_adt_flag numeric,
    adt_a numeric,
    adt_s numeric,
    adjuvant_rt_flag numeric,
    rt_a numeric,
    rt_s numeric,
    adt_time numeric,
    rt_time numeric,
    cc numeric,
    ploidy numeric,
    cure1 numeric,
    cure2 numeric,
    kattan_5yr numeric,
    swog_group character varying(100),
    gpsm numeric,
    bcr_time numeric,
    nodes_pos numeric,
    rp_type character varying(100),
    chemo character varying(100),
    adt_type character varying(100),
    predx_psa numeric,
    pretx_psa numeric,
    neoadj_rt character varying(20),
    ece_type character varying(100),
    copy_number_cluster character varying(100),
    nodes_rmd numeric,
    lvi numeric,
    adjctx numeric,
    gender character varying(20),
    iv_rx numeric,
    rec numeric,
    rec_time numeric,
    ibcnc numeric,
    bcr_fu_date timestamp(6) without time zone,
    family_history character varying(50),
    bfailure numeric,
    bfailure_time numeric,
    death_cause character varying(100),
    original_pathgs numeric,
    pathgs_t numeric,
    original_pathgs_p numeric,
    original_pathgs_s numeric,
    user_role character varying(32),
    svi numeric,
    sm numeric,
    original_clings numeric,
    original_clings_p numeric,
    original_clings_s numeric,
    neoplastic_cell_count numeric,
    match character varying(100),
    tumor_weight numeric,
    pten_ihc boolean,
    pten_fish boolean,
    myc_inc boolean,
    lpl_dec boolean,
    bmi double precision,
    bx_date timestamp without time zone,
    cfailure numeric,
    dominant_anterior_nodule numeric,
    original_met numeric,
    original_os numeric,
    original_bcr numeric,
    original_pcsm numeric,
    original_pcsm_time numeric,
    patient_note character varying,
    source character varying,
    catalog_no character varying,
    xrt numeric,
    cnode numeric,
    cpos_nodes numeric,
    cmet numeric,
    cmet_location character varying,
    postchemo_stage numeric,
    postchemo_node numeric,
    postchemo_met numeric,
    ene numeric,
    aicc numeric,
    trg_pooled numeric,
    trg_pt numeric,
    trg_met numeric,
    neoadj_ctx numeric,
    neoadj_ctx_type character varying,
    neoadj_ctx_cycle numeric,
    neoadj_ctx_secondary numeric,
    neoadj_ctx_secondary_type character varying,
    neoadj_ctx_secondary_cycle numeric,
    csm numeric,
    prechemo_met numeric,
    lni_level numeric,
    chemo_response character varying,
    post_rp_patients numeric,
    post_bcr_patients numeric,
    idc numeric,
    post_rp_patients_cchdef character varying(100),
    post_bcr_patients_cchdef character varying(100),
    original_pstage character varying(100),
    rp_year numeric,
    bx_pos_cores numeric,
    bx_total_cores numeric,
    bx_max_percent numeric,
    original_sm numeric,
    height numeric,
    weight numeric,
    chemo_s numeric,
    chemo_time numeric,
    original_epe numeric,
    egg15 numeric,
    previous_treatment character varying(100),
    dod date,
    pni numeric,
    family_history_type character varying(100),
    raw_cstage character varying(100),
    met_site character varying,
    received_blocks numeric,
    rt_s_date date,
    adt_s_date date,
    crpc_date date,
    bfailure_fu_date date,
    sample_source character varying(100),
    rt_s_type character varying(100),
    received_batch character varying(100),
    crpc numeric,
    received_unstained_slides numeric,
    received_he_slides numeric,
    adt_length numeric,
    rt_to_met_time numeric,
    rt_to_bfailure_time numeric,
    rt_to_os_time numeric,
    ordering_physician character varying,
    practice_name character varying,
    practice_city character varying,
    practice_state character varying,
    practice_zipcode character varying(5),
    practice_country character varying,
    practice_province character varying,
    practice_postal_code character varying,
    birth_year numeric,
    age_bx numeric,
    bx_year numeric,
    bx_tumor_involvement character varying,
    physically_altered numeric,
    curative_turbt_treatment numeric,
    clin_grade numeric,
    path_grade numeric,
    site character varying,
    xsn_pathfeatures_notes character varying,
    xsn_specimen_notes character varying,
    pt2_gbx numeric,
    lni_gbx numeric,
    epe_gbx numeric,
    svi_gbx numeric,
    bcr_gbx numeric,
    sm_gbx numeric,
    bni numeric,
    other_highriskfeatures character varying,
    practice_type character varying,
    pstage_gbx character varying,
    preop_psa_groups character varying(10),
    age_groups character varying(10),
    bx_total_cores_left numeric,
    bx_total_cores_right numeric,
    bx_pos_cores_left numeric,
    bx_pos_cores_right numeric,
    outcome character varying(250),
    sm_notes character varying(250),
    surgery_s_notes character varying(250),
    adt_start character varying(250),
    adt_end character varying(250),
    rt_region character varying(250),
    tx_s_type character varying(250),
    surgery_s_date date,
    enzalutamide_date date,
    abiraterone_date date,
    tx_s_date date,
    intact_psa numeric,
    nadir numeric,
    surgery_s numeric,
    enzalutamide numeric,
    abiraterone numeric,
    tx_s numeric,
    pin numeric,
    crpc_time numeric,
    original_rt_to_met_time numeric,
    sfailure numeric,
    sfailure_time numeric,
    order_date date,
    rp_to_order_time numeric,
    cystectomy numeric,
    histology_primary character varying,
    histology_secondary character varying,
    diabetes numeric,
    heart numeric,
    other_cancers character varying,
    surgeon_code character varying,
    surgery_type character varying,
    nodes_pos_notes character varying,
    nodes_pos_max_size numeric,
    number_foci numeric,
    bx_max_length numeric,
    kps numeric,
    smoker numeric,
    alcohol numeric,
    cis numeric,
    hydronephrosis numeric,
    distant_failure_time numeric,
    distant_failure_response numeric,
    distant_failure numeric,
    local_failure numeric,
    local_failure_time numeric,
    local_failure_response numeric,
    regional_failure numeric,
    regional_failure_time numeric,
    regional_failure_response numeric,
    csm_time numeric,
    management character varying,
    grade character varying,
    turbt_complete character varying,
    postrt_biopsy character varying,
    postrt_cytology character varying,
    pgrade character varying,
    path_histology_primary character varying,
    initial_treatment_recommendation character varying,
    turbt_histology_primary character varying,
    tumor_location character varying,
    distant_failure_site character varying,
    distant_failure_tx character varying,
    local_failure_type character varying,
    local_failure_tx character varying,
    local_failure_grade character varying,
    regional_failure_site character varying,
    regional_failure_tx character varying,
    chemo_type character varying,
    capra numeric,
    bx_to_rp_months numeric,
    bx_pos_length numeric,
    bx_total_length_taken numeric,
    turbt_date date,
    rc_date date,
    path_histology_secondary character varying,
    bx_percent_pos_cores numeric,
    test_type character varying,
    bx_risk_group character varying,
    pathology_review_site character varying,
    preop_psa_time numeric,
    rt_end_time numeric,
    chemo_start date,
    chemo_end date,
    prert_psa_max numeric,
    rt_recommended numeric,
    adt_recommended numeric,
    psa3 numeric,
    dre character varying,
    dre_notes character varying,
    ct_scan character varying,
    chest_x_ray character varying,
    bone_scan character varying,
    mri character varying,
    dx_to_rc_time numeric,
    chemo_a_type character varying(100),
    chemo_a_start date,
    chemo_a_end date,
    clinical_trial character varying,
    number_peripheral_foci numeric,
    number_transition_foci numeric,
    number_multiple_zone_foci numeric,
    psa_detectable character varying,
    psa_detectable_time character varying,
    patient_reported boolean,
    dx_to_turbt numeric,
    clvi numeric,
    rc_diversion_type character varying,
    rec_type character varying,
    turbt_concurrent_cis character varying,
    path_concurrent_cis character varying,
    chemo_s_start date,
    cci numeric,
    node_dissection_type character varying
);


ALTER TABLE patients OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN patients.patient_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.patient_id IS 'PRIMARY Key to PATIENTS table';


--
-- Name: COLUMN patients.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.study_name IS 'Foreign Key to STUDIES table';


--
-- Name: COLUMN patients.clings; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.clings IS 'Clinical Gleason Score';


--
-- Name: COLUMN patients.clings_p; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.clings_p IS 'Primary Clinical Gleason Score';


--
-- Name: COLUMN patients.clings_s; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.clings_s IS 'Secondary Clinical Gleason Score';


--
-- Name: COLUMN patients.pathgs; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pathgs IS 'Pathological Gleason Score';


--
-- Name: COLUMN patients.pathgs_p; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pathgs_p IS 'Primary Pathological Gleason Score';


--
-- Name: COLUMN patients.pathgs_s; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pathgs_s IS 'Secondary Pathological Gleason Score';


--
-- Name: COLUMN patients.clings_p_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.clings_p_percent IS 'Primary Clinical Gleason Score Percentage ';


--
-- Name: COLUMN patients.clings_s_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.clings_s_percent IS 'Secondary Clinical Gleason Score Percentage ';


--
-- Name: COLUMN patients.pathgs_p_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pathgs_p_percent IS 'Primary Pathological Gleason Score Percentage ';


--
-- Name: COLUMN patients.pathgs_s_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pathgs_s_percent IS 'Secondary Pathological Gleason Score Percentage';


--
-- Name: COLUMN patients.age; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.age IS 'Patient Age (Years)';


--
-- Name: COLUMN patients.pstage; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pstage IS 'Pathologic Tumor Stage';


--
-- Name: COLUMN patients.preop_psa; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.preop_psa IS 'Preoperative PSA';


--
-- Name: COLUMN patients.bcr; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.bcr IS 'Biochemical Recurrence Occurred';


--
-- Name: COLUMN patients.adt; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adt IS 'any hormone therapy (0 = no treated, 1 = treated)
';


--
-- Name: COLUMN patients.rt; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rt IS 'any radiation therapy (0 = no treated, 1 = treated)';


--
-- Name: COLUMN patients.external_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.external_id IS 'Accessions, uodb, or other identifiers from other sources';


--
-- Name: COLUMN patients.race; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.race IS 'ethnicity';


--
-- Name: COLUMN patients.cstage; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.cstage IS 'clinical stage';


--
-- Name: COLUMN patients.tumor_vol; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.tumor_vol IS 'tumor volume';


--
-- Name: COLUMN patients.mayo_def; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.mayo_def IS 'mayo definition of SYS NED BCR';


--
-- Name: COLUMN patients.psa1; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.psa1 IS 'first PSA score';


--
-- Name: COLUMN patients.psa2; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.psa2 IS 'second PSA score';


--
-- Name: COLUMN patients.psa1_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.psa1_time IS 'PSA1 time in months';


--
-- Name: COLUMN patients.psa2_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.psa2_time IS 'PSA2 time in months';


--
-- Name: COLUMN patients.pcsm; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pcsm IS 'Prostate cancer specific mortality
0 = survived, 1 = death';


--
-- Name: COLUMN patients.pcsm_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pcsm_time IS 'PCSM time in months';


--
-- Name: COLUMN patients.lcr; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.lcr IS 'local recurrence
0 = no recurrence, 1 = recurrence';


--
-- Name: COLUMN patients.lcr_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.lcr_time IS 'LCR time in months';


--
-- Name: COLUMN patients.adjuvant_adt_flag; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adjuvant_adt_flag IS 'adjuvant flag for hormone treatment
0 - salvage 
1 - adjuvant
(definition is specific to study/cohort)';


--
-- Name: COLUMN patients.adt_a; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adt_a IS 'Adjuvant Hormone Treatment
0 - no treatment
1 - treatment';


--
-- Name: COLUMN patients.adt_s; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adt_s IS 'salvage hormone treatment
0 - no treatment
1 - treatment';


--
-- Name: COLUMN patients.adjuvant_rt_flag; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adjuvant_rt_flag IS 'adjuvant flag radiation therapy
0 - salvage
1 - adjuvant
(study specific)';


--
-- Name: COLUMN patients.rt_a; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rt_a IS 'adjuvant radiation therapy';


--
-- Name: COLUMN patients.rt_s; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rt_s IS 'salvage radiation therapy';


--
-- Name: COLUMN patients.adt_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adt_time IS 'hormone therapy time in months';


--
-- Name: COLUMN patients.rt_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rt_time IS 'radiation therapy in time';


--
-- Name: COLUMN patients.cc; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.cc IS 'Clinical Classifier Score';


--
-- Name: COLUMN patients.gpsm; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.gpsm IS 'GPSM scores';


--
-- Name: COLUMN patients.bcr_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.bcr_time IS 'BCR time in months';


--
-- Name: COLUMN patients.nodes_pos; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.nodes_pos IS 'The number of positive nodes exacted.';


--
-- Name: COLUMN patients.rp_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rp_type IS 'The type of RP the patient had.';


--
-- Name: COLUMN patients.chemo; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.chemo IS 'Chemotherapy type giving to patient.';


--
-- Name: COLUMN patients.adt_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adt_type IS 'The type of hormone therapy given.';


--
-- Name: COLUMN patients.predx_psa; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.predx_psa IS 'Pre-diagnosis PSA level.';


--
-- Name: COLUMN patients.pretx_psa; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pretx_psa IS 'Pre-treament PSA level.';


--
-- Name: COLUMN patients.neoadj_rt; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.neoadj_rt IS 'Type of Neoadjuvant radiation';


--
-- Name: COLUMN patients.ece_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.ece_type IS 'The type of ECE found in the patient.';


--
-- Name: COLUMN patients.copy_number_cluster; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.copy_number_cluster IS 'Clustering group based on copy number variation data';


--
-- Name: COLUMN patients.nodes_rmd; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.nodes_rmd IS 'Number of nodes removed during surgery';


--
-- Name: COLUMN patients.lvi; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.lvi IS 'Lymphovascular Invasion (0 - no invasion, 1 - invasion)';


--
-- Name: COLUMN patients.adjctx; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.adjctx IS 'Adjuvant Chemo Therapy (0 - no therapy, 1 - therapy) [Bladder]
';


--
-- Name: COLUMN patients.gender; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.gender IS 'Female or Male (NULL for prostate cancer)';


--
-- Name: COLUMN patients.iv_rx; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.iv_rx IS '[Bladder]';


--
-- Name: COLUMN patients.rec; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rec IS 'Recurrence [Bladder]';


--
-- Name: COLUMN patients.rec_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rec_time IS 'Recurrence time (months)';


--
-- Name: COLUMN patients.ibcnc; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.ibcnc IS 'International Bladder Cancer Nomogram Consortium - predicting bladder cancer recurrence (0 to 1)';


--
-- Name: COLUMN patients.cfailure; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.cfailure IS 'clinical failure
';


--
-- Name: COLUMN patients.chemo_response; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.chemo_response IS 'Response to Chemotherapy
1 = complete responders (pStage T0 and Node negative)
2 = partial responders (pStage T1)
3 = non responders (pStage T2-T4 and/or Node positive)';


--
-- Name: COLUMN patients.post_rp_patients; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.post_rp_patients IS 'are patients in post rp cohort (1 = yes, 0 = no)
';


--
-- Name: COLUMN patients.post_bcr_patients; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.post_bcr_patients IS 'are patients in post bcr cohort (1 = yes, 0 = no)
';


--
-- Name: COLUMN patients.idc; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.idc IS 'intraductal carcinoma';


--
-- Name: COLUMN patients.post_rp_patients_cchdef; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.post_rp_patients_cchdef IS 'post RP cohort definitions for JHMI validation study (JHMI-RossSchaeffer-MetsVal)';


--
-- Name: COLUMN patients.post_bcr_patients_cchdef; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.post_bcr_patients_cchdef IS 'post BCR cohort definitions for JHMI validation study (JHMI-RossSchaeffer-MetsVal)';


--
-- Name: COLUMN patients.original_pstage; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.original_pstage IS 'original record of pstage';


--
-- Name: COLUMN patients.rp_year; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rp_year IS 'year RP was done';


--
-- Name: COLUMN patients.bx_pos_cores; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.bx_pos_cores IS 'Number of positive biopsy cores.';


--
-- Name: COLUMN patients.bx_total_cores; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.bx_total_cores IS 'Number of biopsy cores.';


--
-- Name: COLUMN patients.bx_max_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.bx_max_percent IS 'Maximum percent involvement in biopsy.';


--
-- Name: COLUMN patients.original_sm; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.original_sm IS 'old records of surgical margins';


--
-- Name: COLUMN patients.height; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.height IS 'height in inches';


--
-- Name: COLUMN patients.weight; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.weight IS 'weight in lb';


--
-- Name: COLUMN patients.chemo_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.chemo_time IS 'time in months when chemo was received
';


--
-- Name: COLUMN patients.previous_treatment; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.previous_treatment IS 'Information on any previous treatments.';


--
-- Name: COLUMN patients.birth_year; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.birth_year IS 'Birth year of patient - year before 1925 annotated as 1924.99';


--
-- Name: COLUMN patients.pathology_review_site; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.pathology_review_site IS 'where biopsy histopathology was reviewed';


--
-- Name: COLUMN patients.preop_psa_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.preop_psa_time IS 'how long before RP was the PSA measure taken';


--
-- Name: COLUMN patients.rt_end_time; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients.rt_end_time IS 'time from RP  in months when RT ended';


--
-- Name: rna; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE rna (
    study_name character varying(100) NOT NULL,
    rna_id character varying(100) NOT NULL,
    sample_id character varying(100) NOT NULL,
    rna_concentration numeric(10,2),
    rna_yield numeric(10,2),
    rna_260 numeric(10,2),
    rna_280 numeric(10,2),
    rna_260_280 numeric(10,2),
    rna_260_230 numeric(10,2),
    rna_batch_old numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    rna_date date,
    rna_batch character varying(100),
    rna_sufficient numeric,
    rna_well character varying(2),
    volume_100ng numeric,
    volume_h20 numeric,
    vacuum_concentration numeric,
    conc_dilution1to3 numeric,
    rna_note character varying,
    original_rna_id character varying,
    rna_volume numeric,
    rna_concentration_2 numeric,
    rna_260_280_2 numeric,
    submitted_rna_vol numeric,
    submitted_rna_conc numeric,
    submitted_rna_260_280 numeric,
    submitted_rna_260_230 numeric,
    submitted_rin numeric,
    rna_postspeedvac_concentration numeric,
    rna_postspeedvac_yield numeric,
    rna_postspeedvac_260_280 numeric,
    rna_postspeedvac_volume numeric,
    rna_operator character varying(20),
    rna_operator_2 character varying(20),
    rna_date_2 date,
    rna_extraction_method character varying,
    submitted_rna_elution_buffer character varying,
    submitted_rna_quantification_method character varying,
    rna_quantification_method character varying,
    submitted_rna_concentration numeric,
    rna_concentration_nanodrop numeric,
    rna_concentration_qubit numeric,
    nucleic_acid_type character varying,
    finalized_rna boolean,
    rna_reported boolean
);


ALTER TABLE rna OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN rna.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.study_name IS 'Foreign Key to STUDIES table';


--
-- Name: COLUMN rna.rna_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_id IS 'Primary Key for the RNA table';


--
-- Name: COLUMN rna.sample_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.sample_id IS 'Foreign Key to SAMPLES table';


--
-- Name: COLUMN rna.rna_concentration; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_concentration IS 'Measured in ng / ul. ';


--
-- Name: COLUMN rna.rna_yield; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_yield IS 'Measured in ng.';


--
-- Name: COLUMN rna.vacuum_concentration; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.vacuum_concentration IS 'was rna speedvac prior to cDNA processing
0 = no
1 = yes';


--
-- Name: COLUMN rna.original_rna_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.original_rna_id IS 'original rna id as used in nanodrop results
';


--
-- Name: COLUMN rna.submitted_rna_vol; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.submitted_rna_vol IS 'Volume of RNA received from institution (ul)';


--
-- Name: COLUMN rna.submitted_rna_conc; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.submitted_rna_conc IS 'Concentration of RNA recieved from institution (ng/ul).';


--
-- Name: COLUMN rna.submitted_rna_260_280; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.submitted_rna_260_280 IS 'RNA 260/280 values received from the intitution';


--
-- Name: COLUMN rna.submitted_rna_260_230; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.submitted_rna_260_230 IS 'RNA 260/230 values recieved from the institution.';


--
-- Name: COLUMN rna.submitted_rin; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.submitted_rin IS 'RNA integrity number (RIN) received from the institution.';


--
-- Name: COLUMN rna.rna_postspeedvac_concentration; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_postspeedvac_concentration IS 'Concentration of RNA after Speedvac and resuspended with water';


--
-- Name: COLUMN rna.rna_postspeedvac_yield; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_postspeedvac_yield IS 'Total amount of RNA after Speedvac and resuspended with water';


--
-- Name: COLUMN rna.rna_postspeedvac_260_280; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_postspeedvac_260_280 IS 'Purity of RNA after Speedvac and resuspended with water';


--
-- Name: COLUMN rna.rna_postspeedvac_volume; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna.rna_postspeedvac_volume IS 'Volume of water used for resuspension of RNA after Speedvac';


--
-- Name: samples; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE samples (
    study_name character varying(100) NOT NULL,
    sample_id character varying(100) NOT NULL,
    patient_id character varying(100),
    storage character varying(100),
    sample_type character varying(100),
    tissue_type character varying(100),
    tissue_attribute character varying(100),
    tumor_percent numeric,
    benign_percent numeric,
    stromal_percent numeric,
    blockage numeric,
    tumor_location character varying(100),
    re_core character varying(100),
    core_id character varying(100),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    tumor_length numeric(5,2),
    sample_notes character varying(200),
    path_notes character varying(200),
    date_processed timestamp(6) without time zone,
    date_shipped timestamp(6) without time zone,
    date_received timestamp(6) without time zone,
    met_site character varying(100),
    user_role character varying(32),
    tissue_location character varying(100),
    tube_id character varying(100),
    tissue_gleason numeric,
    bui character varying(100),
    sufficient_block numeric,
    date_sentchla date,
    date_submitted date,
    date_returned date,
    block_id character varying(100),
    block_year numeric,
    path_lab character varying(100),
    slides_available numeric,
    cores_available numeric,
    tissue_id character varying(100),
    specimen_id character varying(100),
    ki67_stain_acis numeric,
    ki67_intensity_acis numeric,
    mdm2_intensity_acis numeric,
    mdm2_stain_acis numeric,
    p53_stain_acis numeric,
    p53_intensity_acis numeric,
    bcl2_intensity_man numeric,
    bcl2_intens_man_grp numeric,
    bcl2_stain_man numeric,
    bcl2_stain_man_grp numeric,
    bax_expr numeric,
    bax_expr_num numeric,
    p16_stain_acis numeric,
    p16_intensity_acis numeric,
    cox2_intensity_acis numeric,
    pka_intensity_acis numeric,
    gdx_id character varying,
    core_type character varying,
    source character varying,
    catalog_no character varying,
    replace_sample character varying(25),
    tissue_epe numeric,
    tissue_svi numeric,
    tissue_gleason_s numeric,
    shipment character varying(100),
    negative_serology character varying,
    bx_core_length numeric,
    bx_macrodissected_percent numeric,
    bx_tumor_percent numeric,
    bx_core_per_block numeric,
    sample_batch character varying,
    punch_diameter numeric,
    punch_id character varying(100),
    tumor_percent_external numeric,
    stromal_percent_external numeric,
    benign_percent_external numeric,
    tissue_gleason_external numeric,
    tissue_gleason_s_external numeric,
    specimen_type character varying(100),
    specimen_measure character varying(100),
    path_date date,
    level_id numeric,
    punch_length character varying(100),
    tissue_length character varying(100),
    tissue_availability_code character varying(100),
    wax_removed numeric,
    external_id character varying(100),
    received_blocks numeric,
    received_slides numeric,
    received_punches numeric,
    received_stained_slides numeric,
    received_rna numeric,
    site_comment character varying,
    sdpath_comment character varying,
    tissue_pathgs numeric,
    tissue_gleason_t numeric,
    failure_reason character varying,
    status character varying,
    mri_region character varying,
    mri_t2w numeric,
    mri_dwi numeric,
    mri_dce numeric,
    mri_adc_value numeric,
    mri_pirads_v1 numeric,
    mri_pirads_v2 numeric,
    accession_id character varying,
    bx_core_per_sample numeric,
    bx_core_tumor_involvement numeric,
    gg4_percent numeric,
    sample_tumor_location character varying,
    sample_suffix character varying,
    replicate_suffix character varying,
    site character varying,
    indication character varying,
    internal_case_id character varying,
    plate_number numeric,
    finalized_sample boolean,
    sample_category character varying,
    shipping_batch character varying,
    sample_location character varying,
    bx_sample_length numeric,
    sampled_focus_rank numeric,
    sampled_focus_volume numeric,
    sample_reported boolean
);


ALTER TABLE samples OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN samples.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.study_name IS 'Foreign Key to STUDIES table';


--
-- Name: COLUMN samples.sample_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.sample_id IS 'Primary Key for the SAMPLES table';


--
-- Name: COLUMN samples.patient_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.patient_id IS 'Foreign Key to PATIENTS table';


--
-- Name: COLUMN samples.sample_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.sample_type IS 'Sample Type is biopsy ( BX ) or radical prostatectomy ( RP )';


--
-- Name: COLUMN samples.blockage; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.blockage IS 'Age of FFPE block (years)';


--
-- Name: COLUMN samples.core_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.core_id IS 'CORE used for specimen';


--
-- Name: COLUMN samples.tumor_length; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.tumor_length IS 'mm';


--
-- Name: COLUMN samples.sample_notes; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.sample_notes IS 'Notes regarding sample';


--
-- Name: COLUMN samples.path_notes; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.path_notes IS 'Notes from path review';


--
-- Name: COLUMN samples.path_lab; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.path_lab IS 'pathology lab where the tissue came from';


--
-- Name: COLUMN samples.slides_available; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.slides_available IS 'number of slides available';


--
-- Name: COLUMN samples.cores_available; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.cores_available IS 'number of cores available for this sample';


--
-- Name: COLUMN samples.gdx_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.gdx_id IS 'GDX ID for the sample';


--
-- Name: COLUMN samples.replace_sample; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.replace_sample IS 'Secondary Gleason Grade of tissue';


--
-- Name: COLUMN samples.tissue_epe; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.tissue_epe IS 'Is tissue from EPE region: 0 = no, 1 = yes';


--
-- Name: COLUMN samples.tissue_svi; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.tissue_svi IS 'Is tissue from SVI region: 0 = no, 1 = yes';


--
-- Name: COLUMN samples.tissue_gleason_s; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.tissue_gleason_s IS 'Secondary Gleason Grade of tissue';


--
-- Name: COLUMN samples.bx_core_length; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.bx_core_length IS 'total length of biopsy cores in mm 
(information collected in macrodissection)';


--
-- Name: COLUMN samples.bx_macrodissected_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.bx_macrodissected_percent IS 'percentage of total biopsy core circled for macrodissection';


--
-- Name: COLUMN samples.bx_tumor_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.bx_tumor_percent IS 'total percent tumor content in all biopsy cores on slides';


--
-- Name: COLUMN samples.path_date; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.path_date IS 'Date of path review and/or macrodissection/punch';


--
-- Name: COLUMN samples.level_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.level_id IS 'Level ID given for NYS samples';


--
-- Name: COLUMN samples.external_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples.external_id IS 'External ID provided by the institution, or assigned by GDX as an external identifyer. This Expernal ID matches patient_id in the Patient table. Format PXXX-###; eg. PHEI-001 ';


--
-- Name: studies; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE studies (
    study_name character varying(100) NOT NULL,
    institution_name character varying(100) NOT NULL,
    disease character varying(100) NOT NULL,
    design character varying(100),
    principal_investigator character varying(100),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    study_name_short character varying(16),
    study_description character varying,
    study_code character varying,
    accession_code character varying(5),
    study_status character varying
);


ALTER TABLE studies OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN studies.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN studies.study_name IS 'Primary Key for the STUDIES table';


--
-- Name: COLUMN studies.disease; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN studies.disease IS 'Prostate Cancer/ Bladder Cancer
- this should refer to the primary cancer type being investigated';


--
-- Name: COLUMN studies.study_name_short; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN studies.study_name_short IS 'Short name used for study';


--
-- Name: COLUMN studies.study_code; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN studies.study_code IS 'Study code (CV for Clinical Validation, CU for Clinical Utility, PS for Pilot Study)';


--
-- Name: COLUMN studies.accession_code; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN studies.accession_code IS 'Accession Study Codes';


--
-- Name: COLUMN studies.study_status; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN studies.study_status IS 'Status of the study';


--
-- Name: celfile_groups; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE celfile_groups (
    celfile_group_name character varying(100) NOT NULL,
    celfile_name character varying(100) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE celfile_groups OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN celfile_groups.celfile_group_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfile_groups.celfile_group_name IS 'Primary Key for the CELFILE_GROUPS table';


--
-- Name: COLUMN celfile_groups.celfile_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfile_groups.celfile_name IS 'Foreign Key to CELFILES table';


--
-- Name: celfile_groups_analysis; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE celfile_groups_analysis (
    celfile_group_name character varying(100) NOT NULL,
    celfile_name character varying(100) NOT NULL,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE celfile_groups_analysis OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN celfile_groups_analysis.celfile_group_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfile_groups_analysis.celfile_group_name IS 'Primary Key for the CELFILE_GROUPS table';


--
-- Name: COLUMN celfile_groups_analysis.celfile_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN celfile_groups_analysis.celfile_name IS 'Foreign Key to CELFILES table';


--
-- Name: celfile_groups_analysis_key; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE celfile_groups_analysis_key (
    celfile_group_name character varying NOT NULL,
    notes character varying,
    count numeric
);


ALTER TABLE celfile_groups_analysis_key OWNER TO samplesdb_table_owner;

--
-- Name: chla_batch_tracker; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE chla_batch_tracker (
    cdna_batch character varying NOT NULL,
    date_expected_shipment date,
    date_received_shipment date,
    date_cdna_completed date,
    date_chip_completed date,
    date_data_downloaded date,
    date_results_shared date,
    date_samples_return_signoff date,
    samples_return_signoff_by character varying,
    date_samples_returned character varying,
    chla_batch_comment character varying
);


ALTER TABLE chla_batch_tracker OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN chla_batch_tracker.cdna_batch; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.cdna_batch IS 'cdna_batch in the format of CHLA-Batch-XXX';


--
-- Name: COLUMN chla_batch_tracker.date_expected_shipment; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_expected_shipment IS 'Date of batch shipment from GenomeDx to CHLA';


--
-- Name: COLUMN chla_batch_tracker.date_received_shipment; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_received_shipment IS 'Date of batch shipment received by CHLA';


--
-- Name: COLUMN chla_batch_tracker.date_cdna_completed; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_cdna_completed IS 'Date cDNA amplification completed by CHLA';


--
-- Name: COLUMN chla_batch_tracker.date_chip_completed; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_chip_completed IS 'Date chip scanning completed by CHLA';


--
-- Name: COLUMN chla_batch_tracker.date_data_downloaded; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_data_downloaded IS 'Date CEL file data downloaded by GenomeDx';


--
-- Name: COLUMN chla_batch_tracker.date_results_shared; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_results_shared IS 'Date MP3 results shared by GenomeDx with CHLA';


--
-- Name: COLUMN chla_batch_tracker.date_samples_return_signoff; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_samples_return_signoff IS 'Date samples return signoff by GenomeDx';


--
-- Name: COLUMN chla_batch_tracker.samples_return_signoff_by; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.samples_return_signoff_by IS 'Initial of individual that signed off samples return';


--
-- Name: COLUMN chla_batch_tracker.date_samples_returned; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.date_samples_returned IS 'Date samples returned by CHLA to GenomeDx';


--
-- Name: COLUMN chla_batch_tracker.chla_batch_comment; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN chla_batch_tracker.chla_batch_comment IS 'Comments regarding CHLA including reasons for sign off hold off and/or history';


--
-- Name: classifications; Type: TABLE; Schema: samplesdb; Owner: predictions_table_owner
--

CREATE TABLE classifications (
    celfile_name character varying(100) NOT NULL,
    "SPINK1" character varying(100),
    etv1 character varying(100),
    etv4 character varying(100),
    etv5 character varying(100),
    fli1 character varying(100),
    spink1 character varying(100),
    erg character varying(100),
    decipher character varying(100),
    molecular_subtype character varying(100)
);


ALTER TABLE classifications OWNER TO predictions_table_owner;

--
-- Name: commercial; Type: VIEW; Schema: samplesdb; Owner: commercial_table_owner
--

CREATE VIEW commercial AS
 SELECT de.grid_celfile_name AS celfile_name,
    p.test_type,
    p.age,
    p.age_bx,
    p.ordering_physician,
    p.genomic_specialist,
    p.practice_city,
    p.practice_state,
    p.practice_zipcode,
    p.practice_country,
    p.order_date,
    p.date_received,
    p.rp_year,
    p.bx_year,
    p.rp_to_order_time,
    p.bx_to_order_time,
    p.preop_psa,
    p.cstage,
    p.pstage,
    p.bx_risk_group,
    p.bx_percent_pos_cores,
    p.clings,
    p.clings_p,
    p.clings_s,
    p.pathgs,
    p.pathgs_p,
    p.pathgs_s,
    p.pathgs_t,
    p.epe,
    p.svi,
    p.sm,
    p.lni,
    p.bcr,
    p.pni,
    p.lvi,
    p.bni,
    p.xsn_pathfeatures_notes,
    p.race,
    s.tissue_gleason_score,
    s.tissue_gleason_p,
    s.tissue_gleason_s,
    s.tumor_percent,
    s.benign_percent,
    s.bx_tumor_length,
    s.sdpath_notes,
    p.bcm_version,
    p.celfile_date,
    p.qc,
    p.percentpresent,
    p.rf22_percentpresent,
    p.posneg_auc,
    p.hybcontrol,
    p.hybpercentpresent,
    p.decipher_gc,
    p.decipher_met5yr,
    p.decipher_met5yr_lowerci,
    p.decipher_met5yr_upperci,
    p.decipher_pcsm10yr,
    p.decipher_pcsm10yr_lowerci,
    p.decipher_pcsm10yr_upperci,
    p.decipher_pgg45,
    p.decipher_pgg45_lowerci,
    p.decipher_pgg45_upperci
   FROM ((commercial.patients p
     LEFT JOIN commercial.sdpath s USING (assay_id))
     JOIN commercial.deidentifier de USING (celfile_name));


ALTER TABLE commercial OWNER TO commercial_table_owner;

--
-- Name: commercial.patients; Type: TABLE; Schema: samplesdb; Owner: rds_superuser
--

CREATE TABLE "commercial.patients" (
    "row.names" text,
    assay_id text,
    test_type text,
    age double precision,
    age_bx double precision,
    ordering_physician text,
    genomic_specialist text,
    order_date text,
    date_received text,
    rp_year double precision,
    bx_year double precision,
    rp_to_order_time double precision,
    bx_to_order_time double precision,
    practice_city text,
    practice_state text,
    practice_zipcode text,
    practice_country text,
    accession_id text,
    preop_psa double precision,
    pathgs double precision,
    pathgs_p double precision,
    pathgs_s double precision,
    clings double precision,
    clings_p double precision,
    clings_s double precision,
    xsn_pathfeatures_notes text,
    pathgs_t double precision,
    ece double precision,
    svi double precision,
    sm double precision,
    lni double precision,
    bcr double precision,
    pni double precision,
    lvi double precision,
    bni double precision,
    bx_risk_group text,
    bx_percent_pos_cores double precision,
    cstage text,
    pstage text,
    bcm_version text,
    task_id double precision,
    celfile_name text,
    celfile_date text,
    qc text,
    decipher_scan text,
    decipher_scan_5yr text,
    decipher_scan_5yr_lowerci text,
    decipher_scan_5yr_upperci text,
    decipher_scan_pcsm10yr text,
    decipher_scan_pcsm10yr_lowerci text,
    decipher_scan_pcsm10yr_upperci text,
    decipher_scan_pgg45 text,
    decipher_scan_pgg45_upperci text,
    decipher_scan_pgg45_lowerci text,
    percentpresent double precision,
    posneg_auc double precision,
    rf22_percentpresent double precision,
    hybcontrol text,
    hybpercentpresent double precision
);


ALTER TABLE "commercial.patients" OWNER TO rds_superuser;

--
-- Name: geo; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE geo (
    celfile_name character varying NOT NULL,
    geo_celfile_name character varying NOT NULL,
    geo_id character varying NOT NULL
);


ALTER TABLE geo OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN geo.celfile_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN geo.celfile_name IS 'Internal CEL file name as indicated in celfile table';


--
-- Name: COLUMN geo.geo_celfile_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN geo.geo_celfile_name IS 'Mapping of corresponding CEL file name in GEO submission';


--
-- Name: COLUMN geo.geo_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN geo.geo_id IS 'GEO Submission ID (Ex: GSE57933)';


--
-- Name: grid_lab_request; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE grid_lab_request (
    tracker_row numeric,
    assay_id character varying NOT NULL,
    gbx_accession_id character varying NOT NULL,
    repeat_from character varying,
    pre_operative_psa_value_ng_ml numeric,
    nccn_risk_group character varying,
    decipher_risk character varying,
    percent_tumor numeric,
    percent_benign_glands numeric,
    percent_present_of_tumor_in_prostate numeric,
    clinical_stage character varying,
    gleason_score_surgical_pathology character varying,
    gleason_score_sd_pathology character varying,
    length_of_tumor_mm numeric,
    mp3_batch numeric,
    task_id numeric NOT NULL,
    request_reason character varying NOT NULL,
    requested_by character varying NOT NULL,
    request_date date NOT NULL,
    lab_data_qc_batch numeric NOT NULL,
    celfile_name character varying NOT NULL,
    completion_date date,
    created_by character varying(25),
    created timestamp without time zone,
    modified_by character varying(25),
    modified timestamp without time zone,
    setting character varying,
    related_sample character varying,
    met_site character varying,
    mibc numeric,
    percent_present_of_tumor_in_bladder numeric
);


ALTER TABLE grid_lab_request OWNER TO samplesdb_table_owner;

--
-- Name: grid_mapping_bioinfo; Type: VIEW; Schema: samplesdb; Owner: commercial_table_owner
--

CREATE VIEW grid_mapping_bioinfo AS
 SELECT deidentifier.mp3_batch,
    deidentifier.grid_batch,
    deidentifier.practice,
    deidentifier.physician_name,
    deidentifier.grid_celfile_name
   FROM commercial.deidentifier
  WHERE (deidentifier.data_confirmed IS TRUE);


ALTER TABLE grid_mapping_bioinfo OWNER TO commercial_table_owner;

--
-- Name: VIEW grid_mapping_bioinfo; Type: COMMENT; Schema: samplesdb; Owner: commercial_table_owner
--

COMMENT ON VIEW grid_mapping_bioinfo IS 'A view created for GRID reporting so that bioinformatics can have access to the tables necessary to create GRID reports without having access to anything that can be easily traced back to patient data. ';


--
-- Name: grid_patients; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW grid_patients AS
 SELECT de.grid_celfile_name AS celfile_name,
    p.test_type,
    p.age,
    p.age_bx,
    p.ordering_physician,
    p.genomic_specialist,
    p.practice_city,
    p.practice_state,
    p.practice_zipcode,
    p.practice_country,
    p.order_date,
    p.date_received,
    p.rp_year,
    p.bx_year,
    p.rp_to_order_time,
    p.bx_to_order_time,
    p.preop_psa,
    p.cstage,
    p.pstage,
    p.bx_risk_group,
    p.bx_percent_pos_cores,
    p.clings,
    p.clings_p,
    p.clings_s,
    p.pathgs,
    p.pathgs_p,
    p.pathgs_s,
    p.pathgs_t,
    p.epe,
    p.svi,
    p.sm,
    p.lni,
    p.bcr,
    p.pni,
    p.lvi,
    p.bni,
    p.xsn_pathfeatures_notes,
    p.bcm_version,
    p.celfile_date,
    p.qc,
    p.percentpresent,
    p.rf22_percentpresent,
    p.posneg_auc,
    p.hybcontrol,
    p.hybpercentpresent,
    p.decipher_gc,
    p.decipher_met5yr,
    p.decipher_met5yr_lowerci,
    p.decipher_met5yr_upperci,
    p.decipher_pcsm10yr,
    p.decipher_pcsm10yr_lowerci,
    p.decipher_pcsm10yr_upperci,
    p.decipher_pgg45,
    p.decipher_pgg45_lowerci,
    p.decipher_pgg45_upperci
   FROM (commercial.patients p
     JOIN commercial.deidentifier de USING (celfile_name));


ALTER TABLE grid_patients OWNER TO administrator;

--
-- Name: import_tracker; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE import_tracker (
    source_file character varying,
    box_location character varying,
    date_added date,
    study_name character varying,
    batch numeric,
    batch_type character varying,
    imported bit(1),
    imported_by character varying,
    plate character varying,
    version character varying
);


ALTER TABLE import_tracker OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN import_tracker.source_file; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN import_tracker.source_file IS 'file name of source file used for import';


--
-- Name: COLUMN import_tracker.batch; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN import_tracker.batch IS 'Batch Number';


--
-- Name: COLUMN import_tracker.batch_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN import_tracker.batch_type IS 'RNA/cDNA/MP3';


--
-- Name: COLUMN import_tracker.imported; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN import_tracker.imported IS 'Has it been imported, 0 for NO, 1 for YES';


--
-- Name: COLUMN import_tracker.imported_by; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN import_tracker.imported_by IS 'name of importer';


--
-- Name: path_info; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE path_info (
    sample_id character varying,
    tumor_percent numeric,
    level_id numeric,
    scrape_id character varying,
    stromal_percent numeric,
    benign_percent numeric,
    path_notes character varying
);


ALTER TABLE path_info OWNER TO samplesdb_table_owner;

--
-- Name: TABLE path_info; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON TABLE path_info IS 'Additional pathology information regarding samples not captured in samples table. The level of information here is in long format - multiple records per sample.';


--
-- Name: COLUMN path_info.sample_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.sample_id IS 'Sample ID as indicated in samples table';


--
-- Name: COLUMN path_info.tumor_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.tumor_percent IS 'tumor percent found in tissue';


--
-- Name: COLUMN path_info.level_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.level_id IS 'Section level e.g 1 = first section, 2= second section';


--
-- Name: COLUMN path_info.scrape_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.scrape_id IS 'Area marked for scraping per set of slide';


--
-- Name: COLUMN path_info.stromal_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.stromal_percent IS 'Stromal percent by surface area';


--
-- Name: COLUMN path_info.benign_percent; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.benign_percent IS 'Percent of benign epithelial by surface area ';


--
-- Name: COLUMN path_info.path_notes; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN path_info.path_notes IS 'Pathology notes ';


--
-- Name: patients_bioinfo; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW patients_bioinfo AS
 SELECT patients.patient_id,
    patients.study_name,
    patients.clings,
    patients.clings_p,
    patients.clings_s,
    patients.pathgs,
    patients.pathgs_p,
    patients.pathgs_s,
    patients.clings_p_percent,
    patients.clings_s_percent,
    patients.pathgs_p_percent,
    patients.pathgs_s_percent,
    patients.age,
    patients.pstage,
    patients.preop_psa,
    patients.bcr,
    patients.adt,
    patients.rt,
    patients.external_id,
    patients.created,
    patients.modified,
    patients.created_by,
    patients.modified_by,
    patients.age_rt,
    patients.race,
    patients.capra_s,
    patients.comments,
    patients.dob,
    patients.ece,
    patients.fu_rp_time,
    patients.fu_time,
    patients.lastfu_date,
    patients.met,
    patients.met_fu_date,
    patients.met_time,
    patients.neoadj_adt,
    patients.os,
    patients.os_time,
    patients.pathid,
    patients.pelvis,
    patients.postrp_psa,
    patients.prert_psa,
    patients.psadt,
    patients.psadt_rp_rt,
    patients.rp_date,
    patients.rp_to_bcr_time,
    patients.rp_to_rt_months,
    patients.rt_dose,
    patients.rt_end,
    patients.rt_start,
    patients.original_bcr_time,
    patients.original_met_time,
    patients.original_os_time,
    patients.rt_type,
    patients.srt_nadir_psa,
    patients.stephenson_2yr,
    patients.stephenson_5yr,
    patients.stephenson_7yr,
    patients.stephenson_rev,
    patients.stephenson_srt,
    patients.stephenson_srt_rev,
    patients.surgery_id,
    patients.stephenson_10yr,
    patients.os_fu_date,
    patients.lni,
    patients.cstage,
    patients.tumor_vol,
    patients.mayo_def,
    patients.psa1,
    patients.psa2,
    patients.psa1_time,
    patients.psa2_time,
    patients.pcsm,
    patients.pcsm_time,
    patients.lcr,
    patients.lcr_time,
    patients.adjuvant_adt_flag,
    patients.adt_a,
    patients.adt_s,
    patients.adjuvant_rt_flag,
    patients.rt_a,
    patients.rt_s,
    patients.adt_time,
    patients.rt_time,
    patients.cc,
    patients.ploidy,
    patients.cure1,
    patients.cure2,
    patients.kattan_5yr,
    patients.swog_group,
    patients.gpsm,
    patients.bcr_time,
    patients.nodes_pos,
    patients.rp_type,
    patients.chemo,
    patients.adt_type,
    patients.predx_psa,
    patients.pretx_psa,
    patients.neoadj_rt,
    patients.ece_type,
    patients.copy_number_cluster,
    patients.nodes_rmd,
    patients.lvi,
    patients.adjctx,
    patients.gender,
    patients.iv_rx,
    patients.rec,
    patients.rec_time,
    patients.ibcnc,
    patients.bcr_fu_date,
    patients.family_history,
    patients.bfailure,
    patients.bfailure_time,
    patients.death_cause,
    patients.original_pathgs,
    patients.pathgs_t,
    patients.original_pathgs_p,
    patients.original_pathgs_s,
    patients.user_role,
    patients.svi,
    patients.sm,
    patients.original_clings,
    patients.original_clings_p,
    patients.original_clings_s,
    patients.neoplastic_cell_count,
    patients.match,
    patients.tumor_weight,
    patients.pten_ihc,
    patients.pten_fish,
    patients.myc_inc,
    patients.lpl_dec,
    patients.bmi,
    patients.bx_date,
    patients.cfailure,
    patients.dominant_anterior_nodule,
    patients.original_met,
    patients.original_os,
    patients.original_bcr,
    patients.original_pcsm,
    patients.original_pcsm_time,
    patients.patient_note,
    patients.source,
    patients.catalog_no,
    patients.xrt,
    patients.cnode,
    patients.cpos_nodes,
    patients.cmet,
    patients.cmet_location,
    patients.postchemo_stage,
    patients.postchemo_node,
    patients.postchemo_met,
    patients.ene,
    patients.aicc,
    patients.trg_pooled,
    patients.trg_pt,
    patients.trg_met,
    patients.neoadj_ctx,
    patients.neoadj_ctx_type,
    patients.neoadj_ctx_cycle,
    patients.neoadj_ctx_secondary,
    patients.neoadj_ctx_secondary_type,
    patients.neoadj_ctx_secondary_cycle,
    patients.csm,
    patients.prechemo_met,
    patients.lni_level,
    patients.chemo_response,
    patients.post_rp_patients,
    patients.post_bcr_patients,
    patients.idc,
    patients.post_rp_patients_cchdef,
    patients.post_bcr_patients_cchdef,
    patients.original_pstage,
    patients.rp_year,
    patients.bx_pos_cores,
    patients.bx_total_cores,
    patients.bx_max_percent,
    patients.original_sm,
    patients.height,
    patients.weight,
    patients.chemo_s,
    patients.chemo_time,
    patients.original_epe,
    patients.egg15,
    patients.previous_treatment,
    patients.dod,
    patients.pni,
    patients.family_history_type,
    patients.raw_cstage,
    patients.met_site,
    patients.received_blocks,
    patients.rt_s_date,
    patients.adt_s_date,
    patients.crpc_date,
    patients.bfailure_fu_date,
    patients.sample_source,
    patients.rt_s_type,
    patients.received_batch,
    patients.crpc,
    patients.received_unstained_slides,
    patients.received_he_slides,
    patients.adt_length,
    patients.rt_to_met_time,
    patients.rt_to_bfailure_time,
    patients.rt_to_os_time,
    patients.ordering_physician,
    patients.practice_name,
    patients.practice_city,
    patients.practice_state,
    patients.practice_zipcode,
    patients.practice_country,
    patients.practice_province,
    patients.practice_postal_code,
    patients.birth_year,
    patients.age_bx,
    patients.bx_year,
    patients.bx_tumor_involvement,
    patients.physically_altered,
    patients.curative_turbt_treatment,
    patients.clin_grade,
    patients.path_grade,
    patients.site,
    patients.xsn_pathfeatures_notes,
    patients.xsn_specimen_notes,
    patients.pt2_gbx,
    patients.lni_gbx,
    patients.epe_gbx,
    patients.svi_gbx,
    patients.bcr_gbx,
    patients.sm_gbx,
    patients.bni,
    patients.other_highriskfeatures,
    patients.practice_type,
    patients.pstage_gbx,
    patients.preop_psa_groups,
    patients.age_groups,
    patients.bx_total_cores_left,
    patients.bx_total_cores_right,
    patients.bx_pos_cores_left,
    patients.bx_pos_cores_right,
    patients.outcome,
    patients.sm_notes,
    patients.surgery_s_notes,
    patients.adt_start,
    patients.adt_end,
    patients.rt_region,
    patients.tx_s_type,
    patients.surgery_s_date,
    patients.enzalutamide_date,
    patients.abiraterone_date,
    patients.tx_s_date,
    patients.intact_psa,
    patients.nadir,
    patients.surgery_s,
    patients.enzalutamide,
    patients.abiraterone,
    patients.tx_s,
    patients.pin,
    patients.crpc_time,
    patients.original_rt_to_met_time,
    patients.sfailure,
    patients.sfailure_time,
    patients.order_date,
    patients.rp_to_order_time,
    patients.cystectomy,
    patients.histology_primary,
    patients.histology_secondary,
    patients.diabetes,
    patients.heart,
    patients.other_cancers,
    patients.surgeon_code,
    patients.surgery_type,
    patients.nodes_pos_notes,
    patients.nodes_pos_max_size,
    patients.number_foci,
    patients.bx_max_length,
    patients.kps,
    patients.smoker,
    patients.alcohol,
    patients.cis,
    patients.hydronephrosis,
    patients.distant_failure_time,
    patients.distant_failure_response,
    patients.distant_failure,
    patients.local_failure,
    patients.local_failure_time,
    patients.local_failure_response,
    patients.regional_failure,
    patients.regional_failure_time,
    patients.regional_failure_response,
    patients.csm_time,
    patients.management,
    patients.grade,
    patients.turbt_complete,
    patients.postrt_biopsy,
    patients.postrt_cytology,
    patients.pgrade,
    patients.path_histology_primary,
    patients.initial_treatment_recommendation,
    patients.turbt_histology_primary,
    patients.tumor_location,
    patients.distant_failure_site,
    patients.distant_failure_tx,
    patients.local_failure_type,
    patients.local_failure_tx,
    patients.local_failure_grade,
    patients.regional_failure_site,
    patients.regional_failure_tx,
    patients.chemo_type,
    patients.capra,
    patients.bx_to_rp_months,
    patients.bx_pos_length,
    patients.bx_total_length_taken,
    patients.turbt_date,
    patients.rc_date,
    patients.path_histology_secondary,
    patients.bx_percent_pos_cores,
    patients.test_type,
    patients.bx_risk_group,
    patients.pathology_review_site,
    patients.preop_psa_time,
    patients.rt_end_time,
    patients.chemo_start,
    patients.chemo_end,
    patients.prert_psa_max,
    patients.rt_recommended,
    patients.adt_recommended,
    patients.psa3,
    patients.dre,
    patients.dre_notes,
    patients.ct_scan,
    patients.chest_x_ray,
    patients.bone_scan,
    patients.mri,
    patients.dx_to_rc_time,
    patients.chemo_a_type,
    patients.chemo_a_start,
    patients.chemo_a_end,
    patients.clinical_trial,
    patients.number_peripheral_foci,
    patients.number_transition_foci,
    patients.number_multiple_zone_foci,
    patients.psa_detectable,
    patients.psa_detectable_time
   FROM patients
  WHERE ((patients.user_role)::text = 'bioinfo'::text);


ALTER TABLE patients_bioinfo OWNER TO samplesdb_table_owner;

--
-- Name: patients_biostats; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW patients_biostats AS
 SELECT patients.patient_id,
    patients.study_name,
    patients.clings,
    patients.clings_p,
    patients.clings_s,
    patients.pathgs,
    patients.pathgs_p,
    patients.pathgs_s,
    patients.clings_p_percent,
    patients.clings_s_percent,
    patients.pathgs_p_percent,
    patients.pathgs_s_percent,
    patients.age,
    patients.pstage,
    patients.preop_psa,
    patients.bcr,
    patients.adt,
    patients.rt,
    patients.external_id,
    patients.created,
    patients.modified,
    patients.created_by,
    patients.modified_by,
    patients.age_rt,
    patients.race,
    patients.capra_s,
    patients.comments,
    patients.dob,
    patients.ece,
    patients.fu_rp_time,
    patients.fu_time,
    patients.lastfu_date,
    patients.met,
    patients.met_fu_date,
    patients.met_time,
    patients.neoadj_adt,
    patients.os,
    patients.os_time,
    patients.pathid,
    patients.pelvis,
    patients.postrp_psa,
    patients.prert_psa,
    patients.psadt,
    patients.psadt_rp_rt,
    patients.rp_date,
    patients.rp_to_bcr_time,
    patients.rp_to_rt_months,
    patients.rt_dose,
    patients.rt_end,
    patients.rt_start,
    patients.original_bcr_time,
    patients.original_met_time,
    patients.original_os_time,
    patients.rt_type,
    patients.srt_nadir_psa,
    patients.stephenson_2yr,
    patients.stephenson_5yr,
    patients.stephenson_7yr,
    patients.stephenson_rev,
    patients.stephenson_srt,
    patients.stephenson_srt_rev,
    patients.surgery_id,
    patients.stephenson_10yr,
    patients.os_fu_date,
    patients.lni,
    patients.cstage,
    patients.tumor_vol,
    patients.mayo_def,
    patients.psa1,
    patients.psa2,
    patients.psa1_time,
    patients.psa2_time,
    patients.pcsm,
    patients.pcsm_time,
    patients.lcr,
    patients.lcr_time,
    patients.adjuvant_adt_flag,
    patients.adt_a,
    patients.adt_s,
    patients.adjuvant_rt_flag,
    patients.rt_a,
    patients.rt_s,
    patients.adt_time,
    patients.rt_time,
    patients.cc,
    patients.ploidy,
    patients.cure1,
    patients.cure2,
    patients.kattan_5yr,
    patients.swog_group,
    patients.gpsm,
    patients.bcr_time,
    patients.nodes_pos,
    patients.rp_type,
    patients.chemo,
    patients.adt_type,
    patients.predx_psa,
    patients.pretx_psa,
    patients.neoadj_rt,
    patients.ece_type,
    patients.copy_number_cluster,
    patients.nodes_rmd,
    patients.lvi,
    patients.adjctx,
    patients.gender,
    patients.iv_rx,
    patients.rec,
    patients.rec_time,
    patients.ibcnc,
    patients.bcr_fu_date,
    patients.family_history,
    patients.bfailure,
    patients.bfailure_time,
    patients.death_cause,
    patients.original_pathgs,
    patients.pathgs_t,
    patients.original_pathgs_p,
    patients.original_pathgs_s,
    patients.user_role,
    patients.svi,
    patients.sm,
    patients.original_clings,
    patients.original_clings_p,
    patients.original_clings_s,
    patients.neoplastic_cell_count,
    patients.match,
    patients.tumor_weight,
    patients.pten_ihc,
    patients.pten_fish,
    patients.myc_inc,
    patients.lpl_dec,
    patients.bmi,
    patients.bx_date,
    patients.cfailure,
    patients.dominant_anterior_nodule,
    patients.original_met,
    patients.original_os,
    patients.original_bcr,
    patients.original_pcsm,
    patients.original_pcsm_time,
    patients.patient_note,
    patients.source,
    patients.catalog_no,
    patients.xrt,
    patients.cnode,
    patients.cpos_nodes,
    patients.cmet,
    patients.cmet_location,
    patients.postchemo_stage,
    patients.postchemo_node,
    patients.postchemo_met,
    patients.ene,
    patients.aicc,
    patients.trg_pooled,
    patients.trg_pt,
    patients.trg_met,
    patients.neoadj_ctx,
    patients.neoadj_ctx_type,
    patients.neoadj_ctx_cycle,
    patients.neoadj_ctx_secondary,
    patients.neoadj_ctx_secondary_type,
    patients.neoadj_ctx_secondary_cycle,
    patients.csm,
    patients.prechemo_met,
    patients.lni_level,
    patients.chemo_response,
    patients.post_rp_patients,
    patients.post_bcr_patients,
    patients.idc,
    patients.post_rp_patients_cchdef,
    patients.post_bcr_patients_cchdef,
    patients.original_pstage,
    patients.rp_year,
    patients.bx_pos_cores,
    patients.bx_total_cores,
    patients.bx_max_percent,
    patients.original_sm,
    patients.height,
    patients.weight,
    patients.chemo_s,
    patients.chemo_time,
    patients.original_epe,
    patients.egg15,
    patients.previous_treatment,
    patients.dod,
    patients.pni,
    patients.family_history_type,
    patients.raw_cstage,
    patients.met_site,
    patients.received_blocks,
    patients.rt_s_date,
    patients.adt_s_date,
    patients.crpc_date,
    patients.bfailure_fu_date,
    patients.sample_source,
    patients.rt_s_type,
    patients.received_batch,
    patients.crpc,
    patients.received_unstained_slides,
    patients.received_he_slides,
    patients.adt_length,
    patients.rt_to_met_time,
    patients.rt_to_bfailure_time,
    patients.rt_to_os_time,
    patients.ordering_physician,
    patients.practice_name,
    patients.practice_city,
    patients.practice_state,
    patients.practice_zipcode,
    patients.practice_country,
    patients.practice_province,
    patients.practice_postal_code,
    patients.birth_year,
    patients.age_bx,
    patients.bx_year,
    patients.bx_tumor_involvement,
    patients.physically_altered,
    patients.curative_turbt_treatment,
    patients.clin_grade,
    patients.path_grade,
    patients.site,
    patients.xsn_pathfeatures_notes,
    patients.xsn_specimen_notes,
    patients.pt2_gbx,
    patients.lni_gbx,
    patients.epe_gbx,
    patients.svi_gbx,
    patients.bcr_gbx,
    patients.sm_gbx,
    patients.bni,
    patients.other_highriskfeatures,
    patients.practice_type,
    patients.pstage_gbx,
    patients.preop_psa_groups,
    patients.age_groups,
    patients.bx_total_cores_left,
    patients.bx_total_cores_right,
    patients.bx_pos_cores_left,
    patients.bx_pos_cores_right,
    patients.outcome,
    patients.sm_notes,
    patients.surgery_s_notes,
    patients.adt_start,
    patients.adt_end,
    patients.rt_region,
    patients.tx_s_type,
    patients.surgery_s_date,
    patients.enzalutamide_date,
    patients.abiraterone_date,
    patients.tx_s_date,
    patients.intact_psa,
    patients.nadir,
    patients.surgery_s,
    patients.enzalutamide,
    patients.abiraterone,
    patients.tx_s,
    patients.pin,
    patients.crpc_time,
    patients.original_rt_to_met_time,
    patients.sfailure,
    patients.sfailure_time,
    patients.order_date,
    patients.rp_to_order_time,
    patients.cystectomy,
    patients.histology_primary,
    patients.histology_secondary,
    patients.diabetes,
    patients.heart,
    patients.other_cancers,
    patients.surgeon_code,
    patients.surgery_type,
    patients.nodes_pos_notes,
    patients.nodes_pos_max_size,
    patients.number_foci,
    patients.bx_max_length,
    patients.kps,
    patients.smoker,
    patients.alcohol,
    patients.cis,
    patients.hydronephrosis,
    patients.distant_failure_time,
    patients.distant_failure_response,
    patients.distant_failure,
    patients.local_failure,
    patients.local_failure_time,
    patients.local_failure_response,
    patients.regional_failure,
    patients.regional_failure_time,
    patients.regional_failure_response,
    patients.csm_time,
    patients.management,
    patients.grade,
    patients.turbt_complete,
    patients.postrt_biopsy,
    patients.postrt_cytology,
    patients.pgrade,
    patients.path_histology_primary,
    patients.initial_treatment_recommendation,
    patients.turbt_histology_primary,
    patients.tumor_location,
    patients.distant_failure_site,
    patients.distant_failure_tx,
    patients.local_failure_type,
    patients.local_failure_tx,
    patients.local_failure_grade,
    patients.regional_failure_site,
    patients.regional_failure_tx,
    patients.chemo_type,
    patients.capra,
    patients.bx_to_rp_months,
    patients.bx_pos_length,
    patients.bx_total_length_taken,
    patients.turbt_date,
    patients.rc_date,
    patients.path_histology_secondary,
    patients.bx_percent_pos_cores,
    patients.test_type,
    patients.bx_risk_group,
    patients.pathology_review_site,
    patients.preop_psa_time,
    patients.rt_end_time,
    patients.chemo_start,
    patients.chemo_end,
    patients.prert_psa_max,
    patients.rt_recommended,
    patients.adt_recommended,
    patients.psa3,
    patients.dre,
    patients.dre_notes,
    patients.ct_scan,
    patients.chest_x_ray,
    patients.bone_scan,
    patients.mri,
    patients.dx_to_rc_time,
    patients.chemo_a_type,
    patients.chemo_a_start,
    patients.chemo_a_end,
    patients.clinical_trial,
    patients.number_peripheral_foci,
    patients.number_transition_foci,
    patients.number_multiple_zone_foci,
    patients.psa_detectable,
    patients.psa_detectable_time,
    patients.patient_reported,
    patients.dx_to_turbt,
    patients.clvi,
    patients.rc_diversion_type,
    patients.rec_type,
    patients.turbt_concurrent_cis,
    patients.path_concurrent_cis,
    patients.chemo_s_start,
    patients.cci,
    patients.node_dissection_type
   FROM patients
  WHERE ((patients.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'biostats'::character varying, 'mandeep'::character varying, 'jon'::character varying])::text[]));


ALTER TABLE patients_biostats OWNER TO samplesdb_table_owner;

--
-- Name: patients_breast; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE patients_breast (
    patient_id character varying(100) NOT NULL,
    study_name character varying,
    age numeric,
    grade numeric,
    tamoxifen numeric,
    tamoxifen_trmnt_durn numeric,
    tamoxifen_response numeric
);


ALTER TABLE patients_breast OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN patients_breast.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients_breast.study_name IS 'Foreign key in studies table';


--
-- Name: COLUMN patients_breast.age; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients_breast.age IS 'age (years) at surgery';


--
-- Name: COLUMN patients_breast.grade; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients_breast.grade IS 'Any mention of grade, including Nottingham criteria and nuclear grade.';


--
-- Name: COLUMN patients_breast.tamoxifen; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients_breast.tamoxifen IS 'Received tamoxifen: 1. Otherwise, 0.';


--
-- Name: COLUMN patients_breast.tamoxifen_trmnt_durn; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients_breast.tamoxifen_trmnt_durn IS 'Length of tamoxifen treatment in days.';


--
-- Name: COLUMN patients_breast.tamoxifen_response; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN patients_breast.tamoxifen_response IS 'Responded to tamoxifen treatment: 1. Otherwise, 0.';


--
-- Name: patients_jon; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW patients_jon AS
 SELECT patients.patient_id,
    patients.study_name,
    patients.clings,
    patients.clings_p,
    patients.clings_s,
    patients.pathgs,
    patients.pathgs_p,
    patients.pathgs_s,
    patients.clings_p_percent,
    patients.clings_s_percent,
    patients.pathgs_p_percent,
    patients.pathgs_s_percent,
    patients.age,
    patients.pstage,
    patients.preop_psa,
    patients.bcr,
    patients.adt,
    patients.rt,
    patients.external_id,
    patients.created,
    patients.modified,
    patients.created_by,
    patients.modified_by,
    patients.age_rt,
    patients.race,
    patients.capra_s,
    patients.comments,
    patients.dob,
    patients.ece,
    patients.fu_rp_time,
    patients.fu_time,
    patients.lastfu_date,
    patients.met,
    patients.met_fu_date,
    patients.met_time,
    patients.neoadj_adt,
    patients.os,
    patients.os_time,
    patients.pathid,
    patients.pelvis,
    patients.postrp_psa,
    patients.prert_psa,
    patients.psadt,
    patients.psadt_rp_rt,
    patients.rp_date,
    patients.rp_to_bcr_time,
    patients.rp_to_rt_months,
    patients.rt_dose,
    patients.rt_end,
    patients.rt_start,
    patients.original_bcr_time,
    patients.original_met_time,
    patients.original_os_time,
    patients.rt_type,
    patients.srt_nadir_psa,
    patients.stephenson_2yr,
    patients.stephenson_5yr,
    patients.stephenson_7yr,
    patients.stephenson_rev,
    patients.stephenson_srt,
    patients.stephenson_srt_rev,
    patients.surgery_id,
    patients.stephenson_10yr,
    patients.os_fu_date,
    patients.lni,
    patients.cstage,
    patients.tumor_vol,
    patients.mayo_def,
    patients.psa1,
    patients.psa2,
    patients.psa1_time,
    patients.psa2_time,
    patients.pcsm,
    patients.pcsm_time,
    patients.lcr,
    patients.lcr_time,
    patients.adjuvant_adt_flag,
    patients.adt_a,
    patients.adt_s,
    patients.adjuvant_rt_flag,
    patients.rt_a,
    patients.rt_s,
    patients.adt_time,
    patients.rt_time,
    patients.cc,
    patients.ploidy,
    patients.cure1,
    patients.cure2,
    patients.kattan_5yr,
    patients.swog_group,
    patients.gpsm,
    patients.bcr_time,
    patients.nodes_pos,
    patients.rp_type,
    patients.chemo,
    patients.adt_type,
    patients.predx_psa,
    patients.pretx_psa,
    patients.neoadj_rt,
    patients.ece_type,
    patients.copy_number_cluster,
    patients.nodes_rmd,
    patients.lvi,
    patients.adjctx,
    patients.gender,
    patients.iv_rx,
    patients.rec,
    patients.rec_time,
    patients.ibcnc,
    patients.bcr_fu_date,
    patients.family_history,
    patients.bfailure,
    patients.bfailure_time,
    patients.death_cause,
    patients.original_pathgs,
    patients.pathgs_t,
    patients.original_pathgs_p,
    patients.original_pathgs_s,
    patients.user_role,
    patients.svi,
    patients.sm,
    patients.original_clings,
    patients.original_clings_p,
    patients.original_clings_s,
    patients.neoplastic_cell_count,
    patients.match,
    patients.tumor_weight,
    patients.pten_ihc,
    patients.pten_fish,
    patients.myc_inc,
    patients.lpl_dec,
    patients.bmi,
    patients.bx_date,
    patients.cfailure,
    patients.dominant_anterior_nodule,
    patients.original_met,
    patients.original_os,
    patients.original_bcr,
    patients.original_pcsm,
    patients.original_pcsm_time,
    patients.patient_note,
    patients.source,
    patients.catalog_no,
    patients.xrt,
    patients.cnode,
    patients.cpos_nodes,
    patients.cmet,
    patients.cmet_location,
    patients.postchemo_stage,
    patients.postchemo_node,
    patients.postchemo_met,
    patients.ene,
    patients.aicc,
    patients.trg_pooled,
    patients.trg_pt,
    patients.trg_met,
    patients.neoadj_ctx,
    patients.neoadj_ctx_type,
    patients.neoadj_ctx_cycle,
    patients.neoadj_ctx_secondary,
    patients.neoadj_ctx_secondary_type,
    patients.neoadj_ctx_secondary_cycle,
    patients.csm,
    patients.prechemo_met,
    patients.lni_level,
    patients.chemo_response,
    patients.post_rp_patients,
    patients.post_bcr_patients,
    patients.idc,
    patients.post_rp_patients_cchdef,
    patients.post_bcr_patients_cchdef,
    patients.original_pstage,
    patients.rp_year,
    patients.bx_pos_cores,
    patients.bx_total_cores,
    patients.bx_max_percent,
    patients.original_sm,
    patients.height,
    patients.weight,
    patients.chemo_s,
    patients.chemo_time,
    patients.original_epe,
    patients.egg15,
    patients.previous_treatment,
    patients.dod,
    patients.pni,
    patients.family_history_type,
    patients.raw_cstage,
    patients.met_site,
    patients.received_blocks,
    patients.rt_s_date,
    patients.adt_s_date,
    patients.crpc_date,
    patients.bfailure_fu_date,
    patients.sample_source,
    patients.rt_s_type,
    patients.received_batch,
    patients.crpc,
    patients.received_unstained_slides,
    patients.received_he_slides,
    patients.adt_length,
    patients.rt_to_met_time,
    patients.rt_to_bfailure_time,
    patients.rt_to_os_time,
    patients.ordering_physician,
    patients.practice_name,
    patients.practice_city,
    patients.practice_state,
    patients.practice_zipcode,
    patients.practice_country,
    patients.practice_province,
    patients.practice_postal_code,
    patients.birth_year,
    patients.age_bx,
    patients.bx_year,
    patients.bx_tumor_involvement,
    patients.physically_altered,
    patients.curative_turbt_treatment,
    patients.clin_grade,
    patients.path_grade,
    patients.site,
    patients.xsn_pathfeatures_notes,
    patients.xsn_specimen_notes,
    patients.pt2_gbx,
    patients.lni_gbx,
    patients.epe_gbx,
    patients.svi_gbx,
    patients.bcr_gbx,
    patients.sm_gbx,
    patients.bni,
    patients.other_highriskfeatures,
    patients.practice_type,
    patients.pstage_gbx,
    patients.preop_psa_groups,
    patients.age_groups,
    patients.bx_total_cores_left,
    patients.bx_total_cores_right,
    patients.bx_pos_cores_left,
    patients.bx_pos_cores_right,
    patients.outcome,
    patients.sm_notes,
    patients.surgery_s_notes,
    patients.adt_start,
    patients.adt_end,
    patients.rt_region,
    patients.tx_s_type,
    patients.surgery_s_date,
    patients.enzalutamide_date,
    patients.abiraterone_date,
    patients.tx_s_date,
    patients.intact_psa,
    patients.nadir,
    patients.surgery_s,
    patients.enzalutamide,
    patients.abiraterone,
    patients.tx_s,
    patients.pin,
    patients.crpc_time,
    patients.original_rt_to_met_time,
    patients.sfailure,
    patients.sfailure_time,
    patients.order_date,
    patients.rp_to_order_time,
    patients.cystectomy,
    patients.histology_primary,
    patients.histology_secondary,
    patients.diabetes,
    patients.heart,
    patients.other_cancers,
    patients.surgeon_code,
    patients.surgery_type,
    patients.nodes_pos_notes,
    patients.nodes_pos_max_size,
    patients.number_foci,
    patients.bx_max_length,
    patients.kps,
    patients.smoker,
    patients.alcohol,
    patients.cis,
    patients.hydronephrosis,
    patients.distant_failure_time,
    patients.distant_failure_response,
    patients.distant_failure,
    patients.local_failure,
    patients.local_failure_time,
    patients.local_failure_response,
    patients.regional_failure,
    patients.regional_failure_time,
    patients.regional_failure_response,
    patients.csm_time,
    patients.management,
    patients.grade,
    patients.turbt_complete,
    patients.postrt_biopsy,
    patients.postrt_cytology,
    patients.pgrade,
    patients.path_histology_primary,
    patients.initial_treatment_recommendation,
    patients.turbt_histology_primary,
    patients.tumor_location,
    patients.distant_failure_site,
    patients.distant_failure_tx,
    patients.local_failure_type,
    patients.local_failure_tx,
    patients.local_failure_grade,
    patients.regional_failure_site,
    patients.regional_failure_tx,
    patients.chemo_type,
    patients.capra,
    patients.bx_to_rp_months,
    patients.bx_pos_length,
    patients.bx_total_length_taken,
    patients.turbt_date,
    patients.rc_date,
    patients.path_histology_secondary,
    patients.bx_percent_pos_cores,
    patients.test_type,
    patients.bx_risk_group,
    patients.pathology_review_site,
    patients.preop_psa_time,
    patients.rt_end_time,
    patients.chemo_start,
    patients.chemo_end,
    patients.prert_psa_max,
    patients.rt_recommended,
    patients.adt_recommended,
    patients.psa3,
    patients.dre,
    patients.dre_notes,
    patients.ct_scan,
    patients.chest_x_ray,
    patients.bone_scan,
    patients.mri,
    patients.dx_to_rc_time,
    patients.chemo_a_type,
    patients.chemo_a_start,
    patients.chemo_a_end,
    patients.clinical_trial
   FROM patients
  WHERE ((patients.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'jon'::character varying])::text[]));


ALTER TABLE patients_jon OWNER TO samplesdb_table_owner;

--
-- Name: permissions_external; Type: TABLE; Schema: samplesdb; Owner: predictions_table_owner
--

CREATE TABLE permissions_external (
    celfile_group_name character varying NOT NULL,
    view_george boolean,
    view_laura boolean,
    view_roland boolean,
    view_bruce boolean,
    view_michael_freeman boolean DEFAULT false,
    view_robert_den boolean,
    view_tamara_lotan boolean DEFAULT false,
    view_firas_abdollah boolean DEFAULT false,
    view_kenneth_pienta boolean DEFAULT false,
    view_patrick_karabon boolean DEFAULT false,
    view_mgh boolean DEFAULT false
);


ALTER TABLE permissions_external OWNER TO predictions_table_owner;

--
-- Name: COLUMN permissions_external.view_george; Type: COMMENT; Schema: samplesdb; Owner: predictions_table_owner
--

COMMENT ON COLUMN permissions_external.view_george IS 'permission in view_george';


--
-- Name: COLUMN permissions_external.view_laura; Type: COMMENT; Schema: samplesdb; Owner: predictions_table_owner
--

COMMENT ON COLUMN permissions_external.view_laura IS 'permission in view_laura';


--
-- Name: pg_all_foreign_keys; Type: VIEW; Schema: samplesdb; Owner: postgres
--

CREATE VIEW pg_all_foreign_keys AS
 SELECT n1.nspname AS fk_schema_name,
    c1.relname AS fk_table_name,
    k1.conname AS fk_constraint_name,
    c1.oid AS fk_table_oid,
    _pg_sv_column_array(k1.conrelid, k1.conkey) AS fk_columns,
    n2.nspname AS pk_schema_name,
    c2.relname AS pk_table_name,
    k2.conname AS pk_constraint_name,
    c2.oid AS pk_table_oid,
    ci.relname AS pk_index_name,
    _pg_sv_column_array(k1.confrelid, k1.confkey) AS pk_columns,
        CASE k1.confmatchtype
            WHEN 'f'::"char" THEN 'FULL'::text
            WHEN 'p'::"char" THEN 'PARTIAL'::text
            WHEN 'u'::"char" THEN 'NONE'::text
            ELSE NULL::text
        END AS match_type,
        CASE k1.confdeltype
            WHEN 'a'::"char" THEN 'NO ACTION'::text
            WHEN 'c'::"char" THEN 'CASCADE'::text
            WHEN 'd'::"char" THEN 'SET DEFAULT'::text
            WHEN 'n'::"char" THEN 'SET NULL'::text
            WHEN 'r'::"char" THEN 'RESTRICT'::text
            ELSE NULL::text
        END AS on_delete,
        CASE k1.confupdtype
            WHEN 'a'::"char" THEN 'NO ACTION'::text
            WHEN 'c'::"char" THEN 'CASCADE'::text
            WHEN 'd'::"char" THEN 'SET DEFAULT'::text
            WHEN 'n'::"char" THEN 'SET NULL'::text
            WHEN 'r'::"char" THEN 'RESTRICT'::text
            ELSE NULL::text
        END AS on_update,
    k1.condeferrable AS is_deferrable,
    k1.condeferred AS is_deferred
   FROM ((((((((pg_constraint k1
     JOIN pg_namespace n1 ON ((n1.oid = k1.connamespace)))
     JOIN pg_class c1 ON ((c1.oid = k1.conrelid)))
     JOIN pg_class c2 ON ((c2.oid = k1.confrelid)))
     JOIN pg_namespace n2 ON ((n2.oid = c2.relnamespace)))
     JOIN pg_depend d ON (((d.classid = ('pg_constraint'::regclass)::oid) AND (d.objid = k1.oid) AND (d.objsubid = 0) AND (d.deptype = 'n'::"char") AND (d.refclassid = ('pg_class'::regclass)::oid) AND (d.refobjsubid = 0))))
     JOIN pg_class ci ON (((ci.oid = d.refobjid) AND (ci.relkind = 'i'::"char"))))
     LEFT JOIN pg_depend d2 ON (((d2.classid = ('pg_class'::regclass)::oid) AND (d2.objid = ci.oid) AND (d2.objsubid = 0) AND (d2.deptype = 'i'::"char") AND (d2.refclassid = ('pg_constraint'::regclass)::oid) AND (d2.refobjsubid = 0))))
     LEFT JOIN pg_constraint k2 ON (((k2.oid = d2.refobjid) AND (k2.contype = ANY (ARRAY['p'::"char", 'u'::"char"])))))
  WHERE ((k1.conrelid <> (0)::oid) AND (k1.confrelid <> (0)::oid) AND (k1.contype = 'f'::"char") AND _pg_sv_table_accessible(n1.oid, c1.oid));


ALTER TABLE pg_all_foreign_keys OWNER TO postgres;

--
-- Name: predictions2; Type: TABLE; Schema: samplesdb; Owner: predictions_table_owner
--

CREATE TABLE predictions2 (
    celfile_name character varying(100) NOT NULL,
    decipher double precision,
    penney2011frma double precision,
    ntv3 double precision,
    ntv2 double precision,
    bcr_knn56 double precision,
    bcr_rf13 double precision,
    psadt_rf13 double precision,
    upgrading_knn16 double precision,
    knn392frma double precision,
    knn104frma double precision,
    rf72frma double precision,
    spink1 double precision,
    etv1 double precision,
    etv4 double precision,
    etv5 double precision,
    fli1 double precision,
    pten double precision,
    erg double precision,
    rf22 double precision,
    rf22scan double precision,
    rf22s double precision,
    ccp double precision,
    knn392scan double precision,
    knn104scan double precision,
    rf72scan double precision,
    penney2011scan double precision,
    ntv4 double precision,
    sawyers double precision,
    rsi double precision,
    gps_rf double precision,
    old boolean,
    rf15frma double precision,
    knn51lni double precision,
    ccp_rf double precision,
    penney2011_rf double precision
);


ALTER TABLE predictions2 OWNER TO predictions_table_owner;

--
-- Name: COLUMN predictions2.knn51lni; Type: COMMENT; Schema: samplesdb; Owner: predictions_table_owner
--

COMMENT ON COLUMN predictions2.knn51lni IS 'LNI model for Bladder Cancer';


--
-- Name: predictions; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW predictions AS
 SELECT predictions2.celfile_name,
    predictions2.decipher,
    predictions2.penney2011frma,
    predictions2.ntv3,
    predictions2.ntv2,
    predictions2.bcr_knn56,
    predictions2.bcr_rf13,
    predictions2.psadt_rf13,
    predictions2.upgrading_knn16,
    predictions2.knn392frma,
    predictions2.knn104frma,
    predictions2.rf72frma,
    predictions2.spink1,
    predictions2.etv1,
    predictions2.etv4,
    predictions2.etv5,
    predictions2.fli1,
    predictions2.pten,
    predictions2.erg,
    predictions2.rf22,
    predictions2.rf22scan,
    predictions2.rf22s,
    predictions2.ccp,
    predictions2.knn392scan,
    predictions2.knn104scan,
    predictions2.rf72scan,
    predictions2.penney2011scan,
    predictions2.ntv4,
    predictions2.sawyers,
    predictions2.rsi,
    predictions2.gps_rf,
    predictions2.old,
    predictions2.rf15frma,
    predictions2.knn51lni,
    predictions2.ccp_rf,
    predictions2.penney2011_rf
   FROM predictions2
  WHERE (predictions2.old = true);


ALTER TABLE predictions OWNER TO administrator;

--
-- Name: predictions_blinded; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE predictions_blinded (
    celfile_name character varying(100) NOT NULL,
    decipher_frma double precision,
    decipher_frma_5yr double precision,
    decipher_frma_5yr_upperci double precision,
    decipher_frma_5yr_lowerci double precision,
    decipher_scan double precision,
    decipher_scan_5yr double precision,
    decipher_scan_5yr_upperci double precision,
    decipher_scan_5yr_lowerci double precision,
    user_role character varying,
    dbt_nac27 numeric,
    dbt_basal numeric,
    dbt_luminal numeric,
    dbt_p53 numeric,
    dbt_subtype character varying,
    dbt_subtype_model4 character varying,
    dbt_basal_model4 numeric,
    dbt_luminal_model4 numeric,
    dbt_p53_model4 numeric,
    dbt_basal_model3 numeric,
    dbt_luminal_model3 numeric,
    dbt_p53_model3 numeric,
    dbt_subtype_model3 character varying,
    dbt_basal_model2 numeric,
    dbt_luminal_model2 numeric,
    dbt_p53_model2 numeric,
    dbt_subtype_model2 character varying,
    dbt_basal_model1 numeric,
    dbt_luminal_model1 numeric,
    dbt_p53_model1 numeric,
    dbt_subtype_model1 character varying,
    dbt_basal_model5 numeric,
    dbt_luminal_model5 numeric,
    dbt_p53_model5 numeric,
    dbt_subtype_model5 character varying,
    dpr_capras_glmnet109 numeric,
    dpr_gg_deepnet79 numeric,
    dpr_oc_glmnet68 numeric,
    ergmodel numeric,
    neat1 numeric,
    schlap1 numeric,
    pdcd1 numeric,
    ccnd1 numeric,
    ar numeric,
    klk3 numeric,
    mki67 numeric,
    nkx31 numeric,
    rb1 numeric,
    etv1 numeric,
    etv4 numeric,
    etv5 numeric,
    spink1 numeric,
    fli1 numeric,
    sparcl1 numeric,
    top2a numeric,
    chga numeric,
    cd276 numeric,
    cd274 numeric,
    erbb3 numeric,
    met numeric,
    ezh2 numeric,
    mycn numeric,
    aurka numeric,
    erbb2 numeric,
    egfr numeric,
    kdr numeric,
    gstp1 numeric,
    hif1a numeric,
    il6 numeric,
    klk2 numeric,
    srd5a1 numeric,
    folh1 numeric,
    pca3 numeric,
    dpr_ars_glmnet12 numeric,
    dpr_ccp_orig numeric,
    dpr_decipherv2 numeric,
    dpr_gps_glmnet12 numeric,
    dbt_knn51scan numeric,
    dbt_rf15scan numeric,
    decipher_scan_pcsm10yr numeric,
    decipher_scan_pcsm10yr_upperci numeric,
    decipher_scan_pcsm10yr_lowerci numeric,
    decipher_scan_pgg45 numeric,
    decipher_scan_pgg45_upperci numeric,
    decipher_scan_pgg45_lowerci numeric,
    dpr_smallcellv1 numeric,
    bca_subtyping_basal_0 numeric,
    bca_subtyping_claudin_low_0 numeric,
    bca_subtyping_infiltrated_luminal_0 numeric,
    bca_subtyping_luminal_0 numeric,
    genomic_gleason_grade_2 numeric,
    bca_subtyping_basal_1 numeric,
    bca_subtyping_claudin_low_1 numeric,
    bca_subtyping_infiltrated_luminal_1 numeric,
    bca_subtyping_luminal_1 numeric,
    decipher_scan_class character varying(100)
);


ALTER TABLE predictions_blinded OWNER TO samplesdb_table_owner;

--
-- Name: predictions_blinded_bioinfo; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW predictions_blinded_bioinfo AS
 SELECT predictions_blinded.celfile_name,
    predictions_blinded.decipher_frma,
    predictions_blinded.decipher_frma_5yr,
    predictions_blinded.decipher_frma_5yr_upperci,
    predictions_blinded.decipher_frma_5yr_lowerci,
    predictions_blinded.decipher_scan,
    predictions_blinded.decipher_scan_5yr,
    predictions_blinded.decipher_scan_5yr_upperci,
    predictions_blinded.decipher_scan_5yr_lowerci,
    predictions_blinded.user_role,
    predictions_blinded.dbt_nac27,
    predictions_blinded.dbt_basal,
    predictions_blinded.dbt_luminal,
    predictions_blinded.dbt_p53,
    predictions_blinded.dbt_subtype,
    predictions_blinded.dbt_subtype_model4,
    predictions_blinded.dbt_basal_model4,
    predictions_blinded.dbt_luminal_model4,
    predictions_blinded.dbt_p53_model4,
    predictions_blinded.dbt_basal_model3,
    predictions_blinded.dbt_luminal_model3,
    predictions_blinded.dbt_p53_model3,
    predictions_blinded.dbt_subtype_model3,
    predictions_blinded.dbt_basal_model2,
    predictions_blinded.dbt_luminal_model2,
    predictions_blinded.dbt_p53_model2,
    predictions_blinded.dbt_subtype_model2,
    predictions_blinded.dbt_basal_model1,
    predictions_blinded.dbt_luminal_model1,
    predictions_blinded.dbt_p53_model1,
    predictions_blinded.dbt_subtype_model1,
    predictions_blinded.dbt_basal_model5,
    predictions_blinded.dbt_luminal_model5,
    predictions_blinded.dbt_p53_model5,
    predictions_blinded.dbt_subtype_model5,
    predictions_blinded.dpr_capras_glmnet109,
    predictions_blinded.dpr_gg_deepnet79,
    predictions_blinded.dpr_oc_glmnet68,
    predictions_blinded.ergmodel,
    predictions_blinded.neat1,
    predictions_blinded.schlap1,
    predictions_blinded.pdcd1,
    predictions_blinded.ccnd1,
    predictions_blinded.ar,
    predictions_blinded.klk3,
    predictions_blinded.mki67,
    predictions_blinded.nkx31,
    predictions_blinded.rb1,
    predictions_blinded.etv1,
    predictions_blinded.etv4,
    predictions_blinded.etv5,
    predictions_blinded.spink1,
    predictions_blinded.fli1,
    predictions_blinded.sparcl1,
    predictions_blinded.top2a,
    predictions_blinded.chga,
    predictions_blinded.cd276,
    predictions_blinded.cd274,
    predictions_blinded.erbb3,
    predictions_blinded.met,
    predictions_blinded.ezh2,
    predictions_blinded.mycn,
    predictions_blinded.aurka,
    predictions_blinded.erbb2,
    predictions_blinded.egfr,
    predictions_blinded.kdr,
    predictions_blinded.gstp1,
    predictions_blinded.hif1a,
    predictions_blinded.il6,
    predictions_blinded.klk2,
    predictions_blinded.srd5a1,
    predictions_blinded.folh1,
    predictions_blinded.pca3,
    predictions_blinded.dpr_ars_glmnet12,
    predictions_blinded.dpr_ccp_orig,
    predictions_blinded.dpr_decipherv2,
    predictions_blinded.dpr_gps_glmnet12,
    predictions_blinded.dbt_knn51scan,
    predictions_blinded.dbt_rf15scan,
    predictions_blinded.decipher_scan_pcsm10yr,
    predictions_blinded.decipher_scan_pcsm10yr_upperci,
    predictions_blinded.decipher_scan_pcsm10yr_lowerci,
    predictions_blinded.decipher_scan_pgg45,
    predictions_blinded.decipher_scan_pgg45_upperci,
    predictions_blinded.decipher_scan_pgg45_lowerci,
    predictions_blinded.dpr_smallcellv1,
    predictions_blinded.bca_subtyping_basal_0,
    predictions_blinded.bca_subtyping_claudin_low_0,
    predictions_blinded.bca_subtyping_infiltrated_luminal_0,
    predictions_blinded.bca_subtyping_luminal_0,
    predictions_blinded.genomic_gleason_grade_2,
    predictions_blinded.bca_subtyping_basal_1,
    predictions_blinded.bca_subtyping_claudin_low_1,
    predictions_blinded.bca_subtyping_infiltrated_luminal_1,
    predictions_blinded.bca_subtyping_luminal_1
   FROM predictions_blinded
  WHERE ((predictions_blinded.user_role)::text = 'bioinfo'::text);


ALTER TABLE predictions_blinded_bioinfo OWNER TO samplesdb_table_owner;

--
-- Name: predictions_blinded_biostats; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW predictions_blinded_biostats AS
 SELECT predictions_blinded.celfile_name,
    predictions_blinded.decipher_frma,
    predictions_blinded.decipher_frma_5yr,
    predictions_blinded.decipher_frma_5yr_upperci,
    predictions_blinded.decipher_frma_5yr_lowerci,
    predictions_blinded.decipher_scan,
    predictions_blinded.decipher_scan_5yr,
    predictions_blinded.decipher_scan_5yr_upperci,
    predictions_blinded.decipher_scan_5yr_lowerci,
    predictions_blinded.user_role,
    predictions_blinded.dbt_nac27,
    predictions_blinded.dbt_basal,
    predictions_blinded.dbt_luminal,
    predictions_blinded.dbt_p53,
    predictions_blinded.dbt_subtype,
    predictions_blinded.dbt_subtype_model4,
    predictions_blinded.dbt_basal_model4,
    predictions_blinded.dbt_luminal_model4,
    predictions_blinded.dbt_p53_model4,
    predictions_blinded.dbt_basal_model3,
    predictions_blinded.dbt_luminal_model3,
    predictions_blinded.dbt_p53_model3,
    predictions_blinded.dbt_subtype_model3,
    predictions_blinded.dbt_basal_model2,
    predictions_blinded.dbt_luminal_model2,
    predictions_blinded.dbt_p53_model2,
    predictions_blinded.dbt_subtype_model2,
    predictions_blinded.dbt_basal_model1,
    predictions_blinded.dbt_luminal_model1,
    predictions_blinded.dbt_p53_model1,
    predictions_blinded.dbt_subtype_model1,
    predictions_blinded.dbt_basal_model5,
    predictions_blinded.dbt_luminal_model5,
    predictions_blinded.dbt_p53_model5,
    predictions_blinded.dbt_subtype_model5,
    predictions_blinded.dpr_capras_glmnet109,
    predictions_blinded.dpr_gg_deepnet79,
    predictions_blinded.dpr_oc_glmnet68,
    predictions_blinded.ergmodel,
    predictions_blinded.neat1,
    predictions_blinded.schlap1,
    predictions_blinded.pdcd1,
    predictions_blinded.ccnd1,
    predictions_blinded.ar,
    predictions_blinded.klk3,
    predictions_blinded.mki67,
    predictions_blinded.nkx31,
    predictions_blinded.rb1,
    predictions_blinded.etv1,
    predictions_blinded.etv4,
    predictions_blinded.etv5,
    predictions_blinded.spink1,
    predictions_blinded.fli1,
    predictions_blinded.sparcl1,
    predictions_blinded.top2a,
    predictions_blinded.chga,
    predictions_blinded.cd276,
    predictions_blinded.cd274,
    predictions_blinded.erbb3,
    predictions_blinded.met,
    predictions_blinded.ezh2,
    predictions_blinded.mycn,
    predictions_blinded.aurka,
    predictions_blinded.erbb2,
    predictions_blinded.egfr,
    predictions_blinded.kdr,
    predictions_blinded.gstp1,
    predictions_blinded.hif1a,
    predictions_blinded.il6,
    predictions_blinded.klk2,
    predictions_blinded.srd5a1,
    predictions_blinded.folh1,
    predictions_blinded.pca3,
    predictions_blinded.dpr_ars_glmnet12,
    predictions_blinded.dpr_ccp_orig,
    predictions_blinded.dpr_decipherv2,
    predictions_blinded.dpr_gps_glmnet12,
    predictions_blinded.dbt_knn51scan,
    predictions_blinded.dbt_rf15scan,
    predictions_blinded.decipher_scan_pcsm10yr,
    predictions_blinded.decipher_scan_pcsm10yr_upperci,
    predictions_blinded.decipher_scan_pcsm10yr_lowerci,
    predictions_blinded.decipher_scan_pgg45,
    predictions_blinded.decipher_scan_pgg45_upperci,
    predictions_blinded.decipher_scan_pgg45_lowerci,
    predictions_blinded.dpr_smallcellv1,
    predictions_blinded.bca_subtyping_basal_0,
    predictions_blinded.bca_subtyping_claudin_low_0,
    predictions_blinded.bca_subtyping_infiltrated_luminal_0,
    predictions_blinded.bca_subtyping_luminal_0,
    predictions_blinded.genomic_gleason_grade_2,
    predictions_blinded.bca_subtyping_basal_1,
    predictions_blinded.bca_subtyping_claudin_low_1,
    predictions_blinded.bca_subtyping_infiltrated_luminal_1,
    predictions_blinded.bca_subtyping_luminal_1,
    predictions_blinded.decipher_scan_class
   FROM predictions_blinded
  WHERE ((predictions_blinded.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'biostats'::character varying, 'mandeep'::character varying, 'jon'::character varying])::text[]));


ALTER TABLE predictions_blinded_biostats OWNER TO samplesdb_table_owner;

--
-- Name: predictions_blinded_kasra; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW predictions_blinded_kasra AS
 SELECT predictions_blinded.celfile_name,
    predictions_blinded.decipher_frma,
    predictions_blinded.decipher_frma_5yr,
    predictions_blinded.decipher_frma_5yr_upperci,
    predictions_blinded.decipher_frma_5yr_lowerci,
    predictions_blinded.decipher_scan,
    predictions_blinded.decipher_scan_5yr,
    predictions_blinded.decipher_scan_5yr_upperci,
    predictions_blinded.decipher_scan_5yr_lowerci,
    predictions_blinded.user_role,
    predictions_blinded.dbt_nac27,
    predictions_blinded.dbt_basal,
    predictions_blinded.dbt_luminal,
    predictions_blinded.dbt_p53,
    predictions_blinded.dbt_subtype,
    predictions_blinded.dbt_subtype_model4,
    predictions_blinded.dbt_basal_model4,
    predictions_blinded.dbt_luminal_model4,
    predictions_blinded.dbt_p53_model4,
    predictions_blinded.dbt_basal_model3,
    predictions_blinded.dbt_luminal_model3,
    predictions_blinded.dbt_p53_model3,
    predictions_blinded.dbt_subtype_model3,
    predictions_blinded.dbt_basal_model2,
    predictions_blinded.dbt_luminal_model2,
    predictions_blinded.dbt_p53_model2,
    predictions_blinded.dbt_subtype_model2,
    predictions_blinded.dbt_basal_model1,
    predictions_blinded.dbt_luminal_model1,
    predictions_blinded.dbt_p53_model1,
    predictions_blinded.dbt_subtype_model1,
    predictions_blinded.dbt_basal_model5,
    predictions_blinded.dbt_luminal_model5,
    predictions_blinded.dbt_p53_model5,
    predictions_blinded.dbt_subtype_model5,
    predictions_blinded.dpr_capras_glmnet109,
    predictions_blinded.dpr_gg_deepnet79,
    predictions_blinded.dpr_oc_glmnet68,
    predictions_blinded.ergmodel,
    predictions_blinded.neat1,
    predictions_blinded.schlap1,
    predictions_blinded.pdcd1,
    predictions_blinded.ccnd1,
    predictions_blinded.ar,
    predictions_blinded.klk3,
    predictions_blinded.mki67,
    predictions_blinded.nkx31,
    predictions_blinded.rb1,
    predictions_blinded.etv1,
    predictions_blinded.etv4,
    predictions_blinded.etv5,
    predictions_blinded.spink1,
    predictions_blinded.fli1,
    predictions_blinded.sparcl1,
    predictions_blinded.top2a,
    predictions_blinded.chga,
    predictions_blinded.cd276,
    predictions_blinded.cd274,
    predictions_blinded.erbb3,
    predictions_blinded.met,
    predictions_blinded.ezh2,
    predictions_blinded.mycn,
    predictions_blinded.aurka,
    predictions_blinded.erbb2,
    predictions_blinded.egfr,
    predictions_blinded.kdr,
    predictions_blinded.gstp1,
    predictions_blinded.hif1a,
    predictions_blinded.il6,
    predictions_blinded.klk2,
    predictions_blinded.srd5a1,
    predictions_blinded.folh1,
    predictions_blinded.pca3,
    predictions_blinded.dpr_ars_glmnet12,
    predictions_blinded.dpr_ccp_orig,
    predictions_blinded.dpr_decipherv2,
    predictions_blinded.dpr_gps_glmnet12,
    predictions_blinded.dbt_knn51scan,
    predictions_blinded.dbt_rf15scan,
    predictions_blinded.decipher_scan_pcsm10yr,
    predictions_blinded.decipher_scan_pcsm10yr_upperci,
    predictions_blinded.decipher_scan_pcsm10yr_lowerci,
    predictions_blinded.decipher_scan_pgg45,
    predictions_blinded.decipher_scan_pgg45_upperci,
    predictions_blinded.decipher_scan_pgg45_lowerci,
    predictions_blinded.dpr_smallcellv1,
    predictions_blinded.bca_subtyping_basal_0,
    predictions_blinded.bca_subtyping_claudin_low_0,
    predictions_blinded.bca_subtyping_infiltrated_luminal_0,
    predictions_blinded.bca_subtyping_luminal_0,
    predictions_blinded.genomic_gleason_grade_2,
    predictions_blinded.bca_subtyping_basal_1,
    predictions_blinded.bca_subtyping_claudin_low_1,
    predictions_blinded.bca_subtyping_infiltrated_luminal_1,
    predictions_blinded.bca_subtyping_luminal_1
   FROM predictions_blinded
  WHERE ((predictions_blinded.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'biostats'::character varying, 'mandeep'::character varying, 'kasra'::character varying])::text[]));


ALTER TABLE predictions_blinded_kasra OWNER TO samplesdb_table_owner;

--
-- Name: predictions_blinded_mandeep; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW predictions_blinded_mandeep AS
 SELECT predictions_blinded.celfile_name,
    predictions_blinded.decipher_frma,
    predictions_blinded.decipher_frma_5yr,
    predictions_blinded.decipher_frma_5yr_upperci,
    predictions_blinded.decipher_frma_5yr_lowerci,
    predictions_blinded.decipher_scan,
    predictions_blinded.decipher_scan_5yr,
    predictions_blinded.decipher_scan_5yr_upperci,
    predictions_blinded.decipher_scan_5yr_lowerci,
    predictions_blinded.user_role,
    predictions_blinded.dbt_nac27,
    predictions_blinded.dbt_basal,
    predictions_blinded.dbt_luminal,
    predictions_blinded.dbt_p53,
    predictions_blinded.dbt_subtype,
    predictions_blinded.dbt_subtype_model4,
    predictions_blinded.dbt_basal_model4,
    predictions_blinded.dbt_luminal_model4,
    predictions_blinded.dbt_p53_model4,
    predictions_blinded.dbt_basal_model3,
    predictions_blinded.dbt_luminal_model3,
    predictions_blinded.dbt_p53_model3,
    predictions_blinded.dbt_subtype_model3,
    predictions_blinded.dbt_basal_model2,
    predictions_blinded.dbt_luminal_model2,
    predictions_blinded.dbt_p53_model2,
    predictions_blinded.dbt_subtype_model2,
    predictions_blinded.dbt_basal_model1,
    predictions_blinded.dbt_luminal_model1,
    predictions_blinded.dbt_p53_model1,
    predictions_blinded.dbt_subtype_model1,
    predictions_blinded.dbt_basal_model5,
    predictions_blinded.dbt_luminal_model5,
    predictions_blinded.dbt_p53_model5,
    predictions_blinded.dbt_subtype_model5,
    predictions_blinded.dpr_capras_glmnet109,
    predictions_blinded.dpr_gg_deepnet79,
    predictions_blinded.dpr_oc_glmnet68,
    predictions_blinded.ergmodel,
    predictions_blinded.neat1,
    predictions_blinded.schlap1,
    predictions_blinded.pdcd1,
    predictions_blinded.ccnd1,
    predictions_blinded.ar,
    predictions_blinded.klk3,
    predictions_blinded.mki67,
    predictions_blinded.nkx31,
    predictions_blinded.rb1,
    predictions_blinded.etv1,
    predictions_blinded.etv4,
    predictions_blinded.etv5,
    predictions_blinded.spink1,
    predictions_blinded.fli1,
    predictions_blinded.sparcl1,
    predictions_blinded.top2a,
    predictions_blinded.chga,
    predictions_blinded.cd276,
    predictions_blinded.cd274,
    predictions_blinded.erbb3,
    predictions_blinded.met,
    predictions_blinded.ezh2,
    predictions_blinded.mycn,
    predictions_blinded.aurka,
    predictions_blinded.erbb2,
    predictions_blinded.egfr,
    predictions_blinded.kdr,
    predictions_blinded.gstp1,
    predictions_blinded.hif1a,
    predictions_blinded.il6,
    predictions_blinded.klk2,
    predictions_blinded.srd5a1,
    predictions_blinded.folh1,
    predictions_blinded.pca3,
    predictions_blinded.dpr_ars_glmnet12,
    predictions_blinded.dpr_ccp_orig,
    predictions_blinded.dpr_decipherv2,
    predictions_blinded.dpr_gps_glmnet12,
    predictions_blinded.dbt_knn51scan,
    predictions_blinded.dbt_rf15scan,
    predictions_blinded.decipher_scan_pcsm10yr,
    predictions_blinded.decipher_scan_pcsm10yr_upperci,
    predictions_blinded.decipher_scan_pcsm10yr_lowerci,
    predictions_blinded.decipher_scan_pgg45,
    predictions_blinded.decipher_scan_pgg45_upperci,
    predictions_blinded.decipher_scan_pgg45_lowerci,
    predictions_blinded.dpr_smallcellv1,
    predictions_blinded.bca_subtyping_basal_0,
    predictions_blinded.bca_subtyping_claudin_low_0,
    predictions_blinded.bca_subtyping_infiltrated_luminal_0,
    predictions_blinded.bca_subtyping_luminal_0,
    predictions_blinded.genomic_gleason_grade_2,
    predictions_blinded.bca_subtyping_basal_1,
    predictions_blinded.bca_subtyping_claudin_low_1,
    predictions_blinded.bca_subtyping_infiltrated_luminal_1,
    predictions_blinded.bca_subtyping_luminal_1
   FROM predictions_blinded
  WHERE ((predictions_blinded.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'mandeep'::character varying])::text[]));


ALTER TABLE predictions_blinded_mandeep OWNER TO samplesdb_table_owner;

--
-- Name: predictions_blinded_voleak; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW predictions_blinded_voleak AS
 SELECT predictions_blinded.celfile_name,
    predictions_blinded.decipher_frma,
    predictions_blinded.decipher_frma_5yr,
    predictions_blinded.decipher_frma_5yr_upperci,
    predictions_blinded.decipher_frma_5yr_lowerci,
    predictions_blinded.decipher_scan,
    predictions_blinded.decipher_scan_5yr,
    predictions_blinded.decipher_scan_5yr_upperci,
    predictions_blinded.decipher_scan_5yr_lowerci,
    predictions_blinded.user_role,
    predictions_blinded.dbt_nac27,
    predictions_blinded.dbt_basal,
    predictions_blinded.dbt_luminal,
    predictions_blinded.dbt_p53,
    predictions_blinded.dbt_subtype,
    predictions_blinded.dbt_subtype_model4,
    predictions_blinded.dbt_basal_model4,
    predictions_blinded.dbt_luminal_model4,
    predictions_blinded.dbt_p53_model4,
    predictions_blinded.dbt_basal_model3,
    predictions_blinded.dbt_luminal_model3,
    predictions_blinded.dbt_p53_model3,
    predictions_blinded.dbt_subtype_model3,
    predictions_blinded.dbt_basal_model2,
    predictions_blinded.dbt_luminal_model2,
    predictions_blinded.dbt_p53_model2,
    predictions_blinded.dbt_subtype_model2,
    predictions_blinded.dbt_basal_model1,
    predictions_blinded.dbt_luminal_model1,
    predictions_blinded.dbt_p53_model1,
    predictions_blinded.dbt_subtype_model1,
    predictions_blinded.dbt_basal_model5,
    predictions_blinded.dbt_luminal_model5,
    predictions_blinded.dbt_p53_model5,
    predictions_blinded.dbt_subtype_model5,
    predictions_blinded.dpr_capras_glmnet109,
    predictions_blinded.dpr_gg_deepnet79,
    predictions_blinded.dpr_oc_glmnet68,
    predictions_blinded.ergmodel,
    predictions_blinded.neat1,
    predictions_blinded.schlap1,
    predictions_blinded.pdcd1,
    predictions_blinded.ccnd1,
    predictions_blinded.ar,
    predictions_blinded.klk3,
    predictions_blinded.mki67,
    predictions_blinded.nkx31,
    predictions_blinded.rb1,
    predictions_blinded.etv1,
    predictions_blinded.etv4,
    predictions_blinded.etv5,
    predictions_blinded.spink1,
    predictions_blinded.fli1,
    predictions_blinded.sparcl1,
    predictions_blinded.top2a,
    predictions_blinded.chga,
    predictions_blinded.cd276,
    predictions_blinded.cd274,
    predictions_blinded.erbb3,
    predictions_blinded.met,
    predictions_blinded.ezh2,
    predictions_blinded.mycn,
    predictions_blinded.aurka,
    predictions_blinded.erbb2,
    predictions_blinded.egfr,
    predictions_blinded.kdr,
    predictions_blinded.gstp1,
    predictions_blinded.hif1a,
    predictions_blinded.il6,
    predictions_blinded.klk2,
    predictions_blinded.srd5a1,
    predictions_blinded.folh1,
    predictions_blinded.pca3,
    predictions_blinded.dpr_ars_glmnet12,
    predictions_blinded.dpr_ccp_orig,
    predictions_blinded.dpr_decipherv2,
    predictions_blinded.dpr_gps_glmnet12,
    predictions_blinded.dbt_knn51scan,
    predictions_blinded.dbt_rf15scan,
    predictions_blinded.decipher_scan_pcsm10yr,
    predictions_blinded.decipher_scan_pcsm10yr_upperci,
    predictions_blinded.decipher_scan_pcsm10yr_lowerci,
    predictions_blinded.decipher_scan_pgg45,
    predictions_blinded.decipher_scan_pgg45_upperci,
    predictions_blinded.decipher_scan_pgg45_lowerci,
    predictions_blinded.dpr_smallcellv1,
    predictions_blinded.bca_subtyping_basal_0,
    predictions_blinded.bca_subtyping_claudin_low_0,
    predictions_blinded.bca_subtyping_infiltrated_luminal_0,
    predictions_blinded.bca_subtyping_luminal_0,
    predictions_blinded.genomic_gleason_grade_2,
    predictions_blinded.bca_subtyping_basal_1,
    predictions_blinded.bca_subtyping_claudin_low_1,
    predictions_blinded.bca_subtyping_infiltrated_luminal_1,
    predictions_blinded.bca_subtyping_luminal_1
   FROM predictions_blinded
  WHERE ((predictions_blinded.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'biostats'::character varying, 'mandeep'::character varying, 'voleak'::character varying])::text[]));


ALTER TABLE predictions_blinded_voleak OWNER TO samplesdb_table_owner;

--
-- Name: production_export; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE production_export (
    "CelFileNameShort" character varying,
    "TaskId" bigint,
    "Name" character varying,
    "Value" character varying
);


ALTER TABLE production_export OWNER TO administrator;

--
-- Name: production_export_view; Type: VIEW; Schema: samplesdb; Owner: rds_superuser
--

CREATE VIEW production_export_view AS
 SELECT DISTINCT production_export."CelFileNameShort" AS celfilenameshort,
    production_export."TaskId" AS taskid,
    (production_export_1."Value")::double precision AS gcprobability,
    (production_export_2."Value")::double precision AS rp5yr,
    (production_export_3."Value")::double precision AS rp5yrlowerci,
    (production_export_4."Value")::double precision AS rp5yrupperci,
    (production_export_5."Value")::integer AS qcresult,
    (production_export_6."Value")::double precision AS posvsnegauc,
    (production_export_7."Value")::double precision AS percentpresentrf22,
    (production_export_8."Value")::double precision AS percentpresent,
    (production_export_9."Value")::double precision AS hybridpercentpresent,
    (production_export_10."Value")::integer AS hybridcontroltest
   FROM ((((((((((production_export
     JOIN production_export production_export_1 ON ((production_export_1."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_2 ON ((production_export_2."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_3 ON ((production_export_3."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_4 ON ((production_export_4."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_5 ON ((production_export_5."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_6 ON ((production_export_6."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_7 ON ((production_export_7."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_8 ON ((production_export_8."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_9 ON ((production_export_9."TaskId" = production_export."TaskId")))
     JOIN production_export production_export_10 ON ((production_export_10."TaskId" = production_export."TaskId")))
  WHERE (((production_export_1."Name")::text = 'gc.probability'::text) AND ((production_export_2."Name")::text = 'rp.5yr'::text) AND ((production_export_3."Name")::text = 'rp.5yr.lower.ci'::text) AND ((production_export_4."Name")::text = 'rp.5yr.upper.ci'::text) AND ((production_export_5."Name")::text = 'qc.result'::text) AND ((production_export_6."Name")::text = 'pos.vs.neg.auc'::text) AND ((production_export_7."Name")::text = 'percent.present.rf22'::text) AND ((production_export_8."Name")::text = 'percent.present'::text) AND ((production_export_9."Name")::text = 'hybrid.percent.present'::text) AND ((production_export_10."Name")::text = 'hybrid.control.test'::text))
  ORDER BY production_export."CelFileNameShort";


ALTER TABLE production_export_view OWNER TO rds_superuser;

--
-- Name: proimpact_patients; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE proimpact_patients (
    site_id numeric,
    case_id numeric NOT NULL,
    questionnaire_outcome character varying,
    questionnaire_outcome_comments character varying,
    study_arm character varying,
    accession_id character varying,
    practice character varying,
    specimen_id character varying,
    ordering_physician character varying,
    patient_status character varying,
    patient_status_comment character varying,
    date_received date,
    date_test_ordered date,
    date_report_delivery date,
    assay_id character varying,
    celfile_name character varying,
    decipher_risk_reported character varying
);


ALTER TABLE proimpact_patients OWNER TO samplesdb_table_owner;

--
-- Name: proimpact_qol; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE proimpact_qol (
    visit character varying NOT NULL,
    session_id character varying,
    site_id numeric,
    case_id numeric NOT NULL,
    onsite numeric,
    eq5d_mobility numeric,
    eq5d_selfcare numeric,
    eq5d_activity numeric,
    eq5d_pain numeric,
    eq5d_anxiety numeric,
    eq5d_scale numeric,
    maxpc_question01 numeric,
    maxpc_question02 numeric,
    maxpc_question03 numeric,
    maxpc_question04 numeric,
    maxpc_question05 numeric,
    maxpc_question06 numeric,
    maxpc_question07 numeric,
    maxpc_question08 numeric,
    maxpc_question09 numeric,
    maxpc_question10 numeric,
    maxpc_question11 numeric,
    maxpc_question12 numeric,
    maxpc_question13 numeric,
    maxpc_question14 numeric,
    maxpc_question15 numeric,
    maxpc_question16 numeric,
    maxpc_question17 numeric,
    maxpc_question18 numeric,
    epic_urine1 numeric,
    epic_urine2 numeric,
    epic_urine3 numeric,
    epic_urine4a numeric,
    epic_urine4b numeric,
    epic_urine4c numeric,
    epic_urine4d numeric,
    epic_urine4e numeric,
    epic_urine5 numeric,
    epic_bowel1a numeric,
    epic_bowel1b numeric,
    epic_bowel1c numeric,
    epic_bowel1d numeric,
    epic_bowel1e numeric,
    epic_bowel2 numeric,
    epic_sexual1a numeric,
    epic_sexual1b numeric,
    epic_sexual2 numeric,
    epic_sexual3 numeric,
    epic_sexual4 numeric,
    epic_sexual5 numeric,
    epic_hormone1 numeric,
    epic_hormone2 numeric,
    epic_hormone3 numeric,
    epic_hormone4 numeric,
    epic_hormone5 numeric,
    epic_urineincontinence_reportedscore numeric,
    epic_urineirritation_reportedscore numeric,
    epic_bowel_reportedscore numeric,
    epic_sexual_reportedscore numeric,
    epic_hormone_reportedscore numeric,
    epic_urineincontinence_score numeric,
    epic_urineirritation_score numeric,
    epic_bowel_score numeric,
    epic_sexual_score numeric,
    epic_hormone_score numeric,
    questionnaire_date date,
    questionnaire_submitted_date date,
    questionnaire_status character varying
);


ALTER TABLE proimpact_qol OWNER TO samplesdb_table_owner;

--
-- Name: TABLE proimpact_qol; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON TABLE proimpact_qol IS 'patient quality of life questionnaire answer';


--
-- Name: proimpact_sdq; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE proimpact_sdq (
    visit character varying NOT NULL,
    session_id character varying,
    treatment character varying,
    treatment_others character varying,
    site_id numeric,
    case_id numeric NOT NULL,
    onsite numeric,
    rt_1 numeric,
    rt_2 numeric,
    rt_3 numeric,
    rt_4 numeric,
    treatment_01 numeric,
    treatment_02 numeric,
    treatment_03 numeric,
    treatment_04 numeric,
    treatment_05 numeric,
    treatment_06 numeric,
    treatment_07 numeric,
    treatment_08 numeric,
    treatment_09 numeric,
    treatment_10 numeric,
    treatment_11 numeric,
    treatment_12 numeric,
    treatment_13 numeric,
    treatment_14 numeric,
    treatment_15 numeric,
    treatment_16 numeric,
    questionnaire_date date,
    questionnaire_submitted_date date,
    questionnaire_status character varying
);


ALTER TABLE proimpact_sdq OWNER TO samplesdb_table_owner;

--
-- Name: TABLE proimpact_sdq; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON TABLE proimpact_sdq IS 'PROIMPACT shared decision questionnaire';


--
-- Name: proimpact_sites; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE proimpact_sites (
    site_id numeric NOT NULL,
    path_lab_onsite numeric,
    pi_first character varying,
    pi_last character varying,
    title character varying,
    institution character varying,
    address character varying,
    state character varying,
    irb character varying,
    site_comments character varying
);


ALTER TABLE proimpact_sites OWNER TO samplesdb_table_owner;

--
-- Name: psa; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE psa (
    patient_id character varying(100) NOT NULL,
    study_name character varying(100) NOT NULL,
    psa_time numeric,
    psa_order numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    modified_by character varying,
    created_by character varying,
    psa_value character varying NOT NULL
);


ALTER TABLE psa OWNER TO samplesdb_table_owner;

--
-- Name: rna_batch; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE rna_batch (
    rna_batch character varying NOT NULL,
    rna_batch_date date,
    operator character varying,
    lab character varying
);


ALTER TABLE rna_batch OWNER TO samplesdb_table_owner;

--
-- Name: TABLE rna_batch; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON TABLE rna_batch IS 'Information regarding each RNA batch';


--
-- Name: COLUMN rna_batch.rna_batch; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna_batch.rna_batch IS 'RNA Batch ID (GBX-Batch-XXX, CHLA-XXXXXXXX)';


--
-- Name: COLUMN rna_batch.rna_batch_date; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna_batch.rna_batch_date IS 'Date of RNA batch start';


--
-- Name: COLUMN rna_batch.operator; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna_batch.operator IS 'Initials of operator';


--
-- Name: COLUMN rna_batch.lab; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN rna_batch.lab IS 'Lab batch performed in (Dunhill, CHLA, ScienceCenter)';


--
-- Name: rnafrag; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE rnafrag (
    rnafrag_id character varying(100) NOT NULL,
    rnafrag_batch character varying(100) NOT NULL,
    rnafrag_range character varying(100) NOT NULL,
    rnafrag_well character varying(3) NOT NULL,
    cdna_batch character varying(25),
    cdna_id character varying(100),
    rna_batch character varying(25),
    rna_id character varying(100),
    rnafrag_ngul numeric,
    rnafrag_totalpercent numeric,
    rnafrag_nmolel numeric,
    rnafrag_avgsize numeric,
    rnafrag_cv numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE rnafrag OWNER TO samplesdb_table_owner;

--
-- Name: samples_bioinfo; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW samples_bioinfo AS
 SELECT samples.study_name,
    samples.sample_id,
    samples.patient_id,
    samples.storage,
    samples.sample_type,
    samples.tissue_type,
    samples.tissue_attribute,
    samples.tumor_percent,
    samples.benign_percent,
    samples.stromal_percent,
    samples.blockage,
    samples.tumor_location,
    samples.re_core,
    samples.core_id,
    samples.created,
    samples.modified,
    samples.created_by,
    samples.modified_by,
    samples.tumor_length,
    samples.sample_notes,
    samples.path_notes,
    samples.date_processed,
    samples.date_shipped,
    samples.date_received,
    samples.met_site,
    samples.user_role,
    samples.tissue_location,
    samples.tube_id,
    samples.tissue_gleason,
    samples.bui,
    samples.sufficient_block,
    samples.date_sentchla,
    samples.date_submitted,
    samples.date_returned,
    samples.block_id,
    samples.block_year,
    samples.path_lab,
    samples.slides_available,
    samples.cores_available,
    samples.tissue_id,
    samples.specimen_id,
    samples.ki67_stain_acis,
    samples.ki67_intensity_acis,
    samples.mdm2_intensity_acis,
    samples.mdm2_stain_acis,
    samples.p53_stain_acis,
    samples.p53_intensity_acis,
    samples.bcl2_intensity_man,
    samples.bcl2_intens_man_grp,
    samples.bcl2_stain_man,
    samples.bcl2_stain_man_grp,
    samples.bax_expr,
    samples.bax_expr_num,
    samples.p16_stain_acis,
    samples.p16_intensity_acis,
    samples.cox2_intensity_acis,
    samples.pka_intensity_acis,
    samples.gdx_id,
    samples.core_type,
    samples.source,
    samples.catalog_no,
    samples.replace_sample,
    samples.tissue_epe,
    samples.tissue_svi,
    samples.tissue_gleason_s,
    samples.shipment,
    samples.negative_serology,
    samples.bx_core_length,
    samples.bx_macrodissected_percent,
    samples.bx_tumor_percent,
    samples.bx_core_per_block,
    samples.sample_batch,
    samples.punch_diameter,
    samples.punch_id,
    samples.tumor_percent_external,
    samples.stromal_percent_external,
    samples.benign_percent_external,
    samples.tissue_gleason_external,
    samples.tissue_gleason_s_external,
    samples.specimen_type,
    samples.specimen_measure,
    samples.path_date,
    samples.level_id,
    samples.punch_length,
    samples.tissue_length,
    samples.tissue_availability_code,
    samples.wax_removed,
    samples.external_id,
    samples.received_blocks,
    samples.received_slides,
    samples.received_punches,
    samples.received_stained_slides,
    samples.received_rna,
    samples.site_comment,
    samples.sdpath_comment,
    samples.tissue_pathgs,
    samples.tissue_gleason_t,
    samples.failure_reason,
    samples.status,
    samples.mri_region,
    samples.mri_t2w,
    samples.mri_dwi,
    samples.mri_dce,
    samples.mri_adc_value,
    samples.mri_pirads_v1,
    samples.mri_pirads_v2,
    samples.accession_id,
    samples.bx_core_per_sample,
    samples.bx_core_tumor_involvement,
    samples.gg4_percent,
    samples.sample_tumor_location,
    samples.sample_suffix,
    samples.replicate_suffix,
    samples.site,
    samples.indication,
    samples.internal_case_id,
    samples.plate_number,
    samples.finalized_sample,
    samples.sample_category,
    samples.shipping_batch,
    samples.sample_location,
    samples.bx_sample_length,
    samples.sampled_focus_rank,
    samples.sampled_focus_volume
   FROM samples
  WHERE ((samples.user_role)::text = 'bioinfo'::text);


ALTER TABLE samples_bioinfo OWNER TO samplesdb_table_owner;

--
-- Name: samples_biostats; Type: VIEW; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE VIEW samples_biostats AS
 SELECT samples.study_name,
    samples.sample_id,
    samples.patient_id,
    samples.storage,
    samples.sample_type,
    samples.tissue_type,
    samples.tissue_attribute,
    samples.tumor_percent,
    samples.benign_percent,
    samples.stromal_percent,
    samples.blockage,
    samples.tumor_location,
    samples.re_core,
    samples.core_id,
    samples.created,
    samples.modified,
    samples.created_by,
    samples.modified_by,
    samples.tumor_length,
    samples.sample_notes,
    samples.path_notes,
    samples.date_processed,
    samples.date_shipped,
    samples.date_received,
    samples.met_site,
    samples.user_role,
    samples.tissue_location,
    samples.tube_id,
    samples.tissue_gleason,
    samples.bui,
    samples.sufficient_block,
    samples.date_sentchla,
    samples.date_submitted,
    samples.date_returned,
    samples.block_id,
    samples.block_year,
    samples.path_lab,
    samples.slides_available,
    samples.cores_available,
    samples.tissue_id,
    samples.specimen_id,
    samples.ki67_stain_acis,
    samples.ki67_intensity_acis,
    samples.mdm2_intensity_acis,
    samples.mdm2_stain_acis,
    samples.p53_stain_acis,
    samples.p53_intensity_acis,
    samples.bcl2_intensity_man,
    samples.bcl2_intens_man_grp,
    samples.bcl2_stain_man,
    samples.bcl2_stain_man_grp,
    samples.bax_expr,
    samples.bax_expr_num,
    samples.p16_stain_acis,
    samples.p16_intensity_acis,
    samples.cox2_intensity_acis,
    samples.pka_intensity_acis,
    samples.gdx_id,
    samples.core_type,
    samples.source,
    samples.catalog_no,
    samples.replace_sample,
    samples.tissue_epe,
    samples.tissue_svi,
    samples.tissue_gleason_s,
    samples.shipment,
    samples.negative_serology,
    samples.bx_core_length,
    samples.bx_macrodissected_percent,
    samples.bx_tumor_percent,
    samples.bx_core_per_block,
    samples.sample_batch,
    samples.punch_diameter,
    samples.punch_id,
    samples.tumor_percent_external,
    samples.stromal_percent_external,
    samples.benign_percent_external,
    samples.tissue_gleason_external,
    samples.tissue_gleason_s_external,
    samples.specimen_type,
    samples.specimen_measure,
    samples.path_date,
    samples.level_id,
    samples.punch_length,
    samples.tissue_length,
    samples.tissue_availability_code,
    samples.wax_removed,
    samples.external_id,
    samples.received_blocks,
    samples.received_slides,
    samples.received_punches,
    samples.received_stained_slides,
    samples.received_rna,
    samples.site_comment,
    samples.sdpath_comment,
    samples.tissue_pathgs,
    samples.tissue_gleason_t,
    samples.failure_reason,
    samples.status,
    samples.mri_region,
    samples.mri_t2w,
    samples.mri_dwi,
    samples.mri_dce,
    samples.mri_adc_value,
    samples.mri_pirads_v1,
    samples.mri_pirads_v2,
    samples.accession_id,
    samples.bx_core_per_sample,
    samples.bx_core_tumor_involvement,
    samples.gg4_percent,
    samples.sample_tumor_location,
    samples.sample_suffix,
    samples.replicate_suffix,
    samples.site,
    samples.indication,
    samples.internal_case_id,
    samples.plate_number,
    samples.finalized_sample,
    samples.sample_category,
    samples.shipping_batch,
    samples.sample_location,
    samples.bx_sample_length,
    samples.sampled_focus_rank,
    samples.sampled_focus_volume
   FROM samples
  WHERE ((samples.user_role)::text = ANY ((ARRAY['bioinfo'::character varying, 'biostats'::character varying, 'mandeep'::character varying, 'jon'::character varying])::text[]));


ALTER TABLE samples_biostats OWNER TO samplesdb_table_owner;

--
-- Name: samples_breast; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE samples_breast (
    sample_id character varying NOT NULL,
    patient_id character varying,
    study_name character varying,
    tissue_type character varying,
    specimen_type character varying,
    path_dx character varying,
    triple_neg numeric,
    storage character varying,
    sample_type character varying,
    microdissection character varying,
    her2 numeric,
    hr_unspecified numeric,
    er numeric,
    pr numeric
);


ALTER TABLE samples_breast OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN samples_breast.patient_id; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.patient_id IS 'Patient foreign key';


--
-- Name: COLUMN samples_breast.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.study_name IS 'Study foreign key';


--
-- Name: COLUMN samples_breast.tissue_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.tissue_type IS 'tumor, normal';


--
-- Name: COLUMN samples_breast.specimen_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.specimen_type IS 'What form did section come in?
e.g. Punch, section, RNA';


--
-- Name: COLUMN samples_breast.path_dx; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.path_dx IS 'Pathology
e.g. Cancer-NS (cancer not-specified), DCIS, LCIS, IDC, ILC, Cancer-mixed-IDC-ILC';


--
-- Name: COLUMN samples_breast.triple_neg; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.triple_neg IS 'HER2, ER and PR receptors not present on sample.';


--
-- Name: COLUMN samples_breast.storage; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.storage IS 'FFPE, FF, RNAlater';


--
-- Name: COLUMN samples_breast.sample_type; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.sample_type IS 'biopsy, lumpectomy, mastectomy';


--
-- Name: COLUMN samples_breast.microdissection; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.microdissection IS 'epithelium or stroma';


--
-- Name: COLUMN samples_breast.her2; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.her2 IS 'Presence of HER2 receptor as detected by IHC, FISH, etc';


--
-- Name: COLUMN samples_breast.hr_unspecified; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.hr_unspecified IS 'Presence of hormone receptor, but unspecified whether ER or PR';


--
-- Name: COLUMN samples_breast.er; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.er IS 'Presence of estrogen receptor';


--
-- Name: COLUMN samples_breast.pr; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN samples_breast.pr IS 'presence of progesterone receptor';


--
-- Name: study_variable; Type: TABLE; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TABLE study_variable (
    study_name character varying(100) NOT NULL,
    table_name character varying(30) NOT NULL,
    column_name character varying(30) NOT NULL,
    specific_comment character varying(4000)
);


ALTER TABLE study_variable OWNER TO samplesdb_table_owner;

--
-- Name: COLUMN study_variable.study_name; Type: COMMENT; Schema: samplesdb; Owner: samplesdb_table_owner
--

COMMENT ON COLUMN study_variable.study_name IS 'test column comment';


--
-- Name: tap_funky; Type: VIEW; Schema: samplesdb; Owner: postgres
--

CREATE VIEW tap_funky AS
 SELECT p.oid,
    n.nspname AS schema,
    p.proname AS name,
    pg_get_userbyid(p.proowner) AS owner,
    array_to_string((p.proargtypes)::regtype[], ','::text) AS args,
    (
        CASE p.proretset
            WHEN true THEN 'setof '::text
            ELSE ''::text
        END || (p.prorettype)::regtype) AS returns,
    p.prolang AS langoid,
    p.proisstrict AS is_strict,
    p.proisagg AS is_agg,
    p.prosecdef AS is_definer,
    p.proretset AS returns_set,
    (p.provolatile)::character(1) AS volatility,
    pg_function_is_visible(p.oid) AS is_visible
   FROM (pg_proc p
     JOIN pg_namespace n ON ((p.pronamespace = n.oid)));


ALTER TABLE tap_funky OWNER TO postgres;

--
-- Name: view_bruce; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_bruce AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.clings,
    patients_bioinfo.clings_p,
    patients_bioinfo.clings_s,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.clings_p_percent,
    patients_bioinfo.clings_s_percent,
    patients_bioinfo.pathgs_p_percent,
    patients_bioinfo.pathgs_s_percent,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.external_id,
    patients_bioinfo.created,
    patients_bioinfo.modified,
    patients_bioinfo.created_by,
    patients_bioinfo.modified_by,
    patients_bioinfo.age_rt,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.comments,
    patients_bioinfo.dob,
    patients_bioinfo.ece,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.met,
    patients_bioinfo.met_fu_date,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.pathid,
    patients_bioinfo.pelvis,
    patients_bioinfo.postrp_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.psadt,
    patients_bioinfo.psadt_rp_rt,
    patients_bioinfo.rp_date,
    patients_bioinfo.rp_to_bcr_time,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_end,
    patients_bioinfo.rt_start,
    patients_bioinfo.original_bcr_time,
    patients_bioinfo.original_met_time,
    patients_bioinfo.original_os_time,
    patients_bioinfo.rt_type,
    patients_bioinfo.srt_nadir_psa,
    patients_bioinfo.stephenson_2yr,
    patients_bioinfo.stephenson_5yr,
    patients_bioinfo.stephenson_7yr,
    patients_bioinfo.stephenson_rev,
    patients_bioinfo.stephenson_srt,
    patients_bioinfo.stephenson_srt_rev,
    patients_bioinfo.surgery_id,
    patients_bioinfo.stephenson_10yr,
    patients_bioinfo.os_fu_date,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.psa1,
    patients_bioinfo.psa2,
    patients_bioinfo.psa1_time,
    patients_bioinfo.psa2_time,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.lcr,
    patients_bioinfo.lcr_time,
    patients_bioinfo.adjuvant_adt_flag,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.adjuvant_rt_flag,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.adt_time,
    patients_bioinfo.rt_time,
    patients_bioinfo.cc,
    patients_bioinfo.ploidy,
    patients_bioinfo.cure1,
    patients_bioinfo.cure2,
    patients_bioinfo.kattan_5yr,
    patients_bioinfo.swog_group,
    patients_bioinfo.gpsm,
    patients_bioinfo.bcr_time,
    patients_bioinfo.nodes_pos,
    patients_bioinfo.rp_type,
    patients_bioinfo.chemo,
    patients_bioinfo.adt_type,
    patients_bioinfo.predx_psa,
    patients_bioinfo.pretx_psa,
    patients_bioinfo.neoadj_rt,
    patients_bioinfo.ece_type,
    patients_bioinfo.copy_number_cluster,
    patients_bioinfo.nodes_rmd,
    patients_bioinfo.lvi,
    patients_bioinfo.adjctx,
    patients_bioinfo.gender,
    patients_bioinfo.iv_rx,
    patients_bioinfo.rec,
    patients_bioinfo.rec_time,
    patients_bioinfo.ibcnc,
    patients_bioinfo.bcr_fu_date,
    patients_bioinfo.family_history,
    patients_bioinfo.bfailure,
    patients_bioinfo.bfailure_time,
    patients_bioinfo.death_cause,
    patients_bioinfo.original_pathgs,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.original_pathgs_p,
    patients_bioinfo.original_pathgs_s,
    patients_bioinfo.user_role,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.original_clings,
    patients_bioinfo.original_clings_p,
    patients_bioinfo.original_clings_s,
    patients_bioinfo.neoplastic_cell_count,
    patients_bioinfo.match,
    patients_bioinfo.tumor_weight,
    patients_bioinfo.pten_ihc,
    patients_bioinfo.pten_fish,
    patients_bioinfo.myc_inc,
    patients_bioinfo.lpl_dec,
    patients_bioinfo.bmi,
    patients_bioinfo.bx_date,
    patients_bioinfo.cfailure,
    patients_bioinfo.dominant_anterior_nodule,
    patients_bioinfo.original_met,
    patients_bioinfo.original_os,
    patients_bioinfo.original_bcr,
    patients_bioinfo.original_pcsm,
    patients_bioinfo.original_pcsm_time,
    patients_bioinfo.patient_note,
    patients_bioinfo.source,
    patients_bioinfo.catalog_no,
    patients_bioinfo.xrt,
    patients_bioinfo.cnode,
    patients_bioinfo.cpos_nodes,
    patients_bioinfo.cmet,
    patients_bioinfo.cmet_location,
    patients_bioinfo.postchemo_stage,
    patients_bioinfo.postchemo_node,
    patients_bioinfo.postchemo_met,
    patients_bioinfo.ene,
    patients_bioinfo.aicc,
    patients_bioinfo.trg_pooled,
    patients_bioinfo.trg_pt,
    patients_bioinfo.trg_met,
    patients_bioinfo.neoadj_ctx,
    patients_bioinfo.neoadj_ctx_type,
    patients_bioinfo.neoadj_ctx_cycle,
    patients_bioinfo.neoadj_ctx_secondary,
    patients_bioinfo.neoadj_ctx_secondary_type,
    patients_bioinfo.neoadj_ctx_secondary_cycle,
    patients_bioinfo.csm,
    patients_bioinfo.prechemo_met,
    patients_bioinfo.lni_level,
    patients_bioinfo.chemo_response,
    patients_bioinfo.post_rp_patients,
    patients_bioinfo.post_bcr_patients,
    patients_bioinfo.idc,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.original_pstage,
    patients_bioinfo.rp_year,
    patients_bioinfo.bx_pos_cores,
    patients_bioinfo.bx_total_cores,
    patients_bioinfo.bx_max_percent,
    patients_bioinfo.original_sm,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    patients_bioinfo.chemo_s,
    patients_bioinfo.chemo_time,
    patients_bioinfo.original_epe,
    patients_bioinfo.egg15,
    patients_bioinfo.previous_treatment,
    patients_bioinfo.dod,
    patients_bioinfo.pni,
    patients_bioinfo.family_history_type,
    patients_bioinfo.raw_cstage,
    patients_bioinfo.met_site,
    patients_bioinfo.received_blocks,
    patients_bioinfo.rt_s_date,
    patients_bioinfo.adt_s_date,
    patients_bioinfo.crpc_date,
    patients_bioinfo.bfailure_fu_date,
    patients_bioinfo.sample_source,
    patients_bioinfo.rt_s_type,
    patients_bioinfo.received_batch,
    patients_bioinfo.crpc,
    patients_bioinfo.received_unstained_slides,
    patients_bioinfo.received_he_slides,
    patients_bioinfo.adt_length,
    patients_bioinfo.rt_to_met_time,
    patients_bioinfo.rt_to_bfailure_time,
    patients_bioinfo.rt_to_os_time,
    patients_bioinfo.ordering_physician,
    patients_bioinfo.practice_name,
    patients_bioinfo.practice_city,
    patients_bioinfo.practice_state,
    patients_bioinfo.practice_zipcode,
    patients_bioinfo.practice_country,
    patients_bioinfo.practice_province,
    patients_bioinfo.practice_postal_code,
    patients_bioinfo.birth_year,
    patients_bioinfo.age_bx,
    patients_bioinfo.bx_year,
    patients_bioinfo.bx_tumor_involvement,
    patients_bioinfo.physically_altered,
    patients_bioinfo.curative_turbt_treatment,
    patients_bioinfo.clin_grade,
    patients_bioinfo.path_grade,
    patients_bioinfo.site,
    patients_bioinfo.xsn_pathfeatures_notes,
    patients_bioinfo.xsn_specimen_notes,
    patients_bioinfo.pt2_gbx,
    patients_bioinfo.lni_gbx,
    patients_bioinfo.epe_gbx,
    patients_bioinfo.svi_gbx,
    patients_bioinfo.bcr_gbx,
    patients_bioinfo.sm_gbx,
    patients_bioinfo.bni,
    patients_bioinfo.other_highriskfeatures,
    patients_bioinfo.practice_type,
    patients_bioinfo.pstage_gbx,
    patients_bioinfo.preop_psa_groups,
    patients_bioinfo.age_groups,
    patients_bioinfo.bx_total_cores_left,
    patients_bioinfo.bx_total_cores_right,
    patients_bioinfo.bx_pos_cores_left,
    patients_bioinfo.bx_pos_cores_right,
    patients_bioinfo.outcome,
    patients_bioinfo.sm_notes,
    patients_bioinfo.surgery_s_notes,
    patients_bioinfo.adt_start,
    patients_bioinfo.adt_end,
    patients_bioinfo.rt_region,
    patients_bioinfo.tx_s_type,
    patients_bioinfo.surgery_s_date,
    patients_bioinfo.enzalutamide_date,
    patients_bioinfo.abiraterone_date,
    patients_bioinfo.tx_s_date,
    patients_bioinfo.intact_psa,
    patients_bioinfo.nadir,
    patients_bioinfo.surgery_s,
    patients_bioinfo.enzalutamide,
    patients_bioinfo.abiraterone,
    patients_bioinfo.tx_s,
    patients_bioinfo.pin,
    patients_bioinfo.crpc_time,
    patients_bioinfo.original_rt_to_met_time,
    patients_bioinfo.sfailure,
    patients_bioinfo.sfailure_time,
    patients_bioinfo.order_date,
    patients_bioinfo.rp_to_order_time,
    patients_bioinfo.cystectomy,
    patients_bioinfo.histology_primary,
    patients_bioinfo.histology_secondary,
    patients_bioinfo.diabetes,
    patients_bioinfo.heart,
    patients_bioinfo.other_cancers,
    patients_bioinfo.surgeon_code,
    patients_bioinfo.surgery_type,
    patients_bioinfo.nodes_pos_notes,
    patients_bioinfo.nodes_pos_max_size,
    patients_bioinfo.number_foci,
    patients_bioinfo.bx_max_length,
    patients_bioinfo.kps,
    patients_bioinfo.smoker,
    patients_bioinfo.alcohol,
    patients_bioinfo.cis,
    patients_bioinfo.hydronephrosis,
    patients_bioinfo.distant_failure_time,
    patients_bioinfo.distant_failure_response,
    patients_bioinfo.distant_failure,
    patients_bioinfo.local_failure,
    patients_bioinfo.local_failure_time,
    patients_bioinfo.local_failure_response,
    patients_bioinfo.regional_failure,
    patients_bioinfo.regional_failure_time,
    patients_bioinfo.regional_failure_response,
    patients_bioinfo.csm_time,
    patients_bioinfo.management,
    patients_bioinfo.grade,
    patients_bioinfo.turbt_complete,
    patients_bioinfo.postrt_biopsy,
    patients_bioinfo.postrt_cytology,
    patients_bioinfo.pgrade,
    patients_bioinfo.path_histology_primary,
    patients_bioinfo.initial_treatment_recommendation,
    patients_bioinfo.turbt_histology_primary,
    patients_bioinfo.tumor_location,
    patients_bioinfo.distant_failure_site,
    patients_bioinfo.distant_failure_tx,
    patients_bioinfo.local_failure_type,
    patients_bioinfo.local_failure_tx,
    patients_bioinfo.local_failure_grade,
    patients_bioinfo.regional_failure_site,
    patients_bioinfo.regional_failure_tx,
    patients_bioinfo.chemo_type,
    patients_bioinfo.capra,
    patients_bioinfo.bx_to_rp_months,
    patients_bioinfo.bx_pos_length,
    patients_bioinfo.bx_total_length_taken,
    patients_bioinfo.turbt_date,
    patients_bioinfo.rc_date,
    patients_bioinfo.path_histology_secondary,
    patients_bioinfo.bx_percent_pos_cores,
    patients_bioinfo.test_type,
    patients_bioinfo.bx_risk_group,
    patients_bioinfo.pathology_review_site,
    patients_bioinfo.preop_psa_time,
    patients_bioinfo.rt_end_time,
    patients_bioinfo.chemo_start,
    patients_bioinfo.chemo_end,
    patients_bioinfo.prert_psa_max,
    patients_bioinfo.rt_recommended,
    patients_bioinfo.adt_recommended,
    patients_bioinfo.psa3,
    patients_bioinfo.dre,
    patients_bioinfo.dre_notes,
    patients_bioinfo.ct_scan,
    patients_bioinfo.chest_x_ray,
    patients_bioinfo.bone_scan,
    patients_bioinfo.mri,
    patients_bioinfo.dx_to_rc_time,
    patients_bioinfo.chemo_a_type,
    patients_bioinfo.chemo_a_start,
    patients_bioinfo.chemo_a_end,
    patients_bioinfo.clinical_trial,
    cdna.cdna_id,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.ss_cdna_yield,
    cdna.ss_cdna_concentration,
    cdna.ss_cdna_260,
    cdna.ss_cdna_280,
    cdna.ss_cdna_260_280,
    cdna.ss_cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.cdna_note,
    celfiles.chip_id,
    celfiles.erg_gene,
    celfiles.etv1_gene,
    celfiles.etv4_gene,
    celfiles.qc_batch_id,
    celfiles.location,
    celfiles.qc_flag,
    celfiles.rsi_scan,
    celfiles.rf22_scan,
    celfiles.gcc_scan,
    celfiles.gcs_scan,
    celfiles.rf22_scan_5yr,
    celfiles.rf22_prod_5yr,
    celfiles.rf22_scan_3yrbcr,
    celfiles.rf22_prod_3yrbcr,
    celfiles.percentpresent,
    celfiles.rf22_percentpresent,
    celfiles.rf22_prod,
    celfiles.posneg_auc,
    celfiles.hybcontrol,
    celfiles.hybpercentpresent,
    predictions.celfile_name,
    predictions.decipher,
    predictions.penney2011frma,
    predictions.ntv3,
    predictions.ntv2,
    predictions.bcr_knn56,
    predictions.bcr_rf13,
    predictions.psadt_rf13,
    predictions.upgrading_knn16,
    predictions.knn392frma,
    predictions.knn104frma,
    predictions.rf72frma,
    predictions.spink1,
    predictions.etv1,
    predictions.etv4,
    predictions.etv5,
    predictions.fli1,
    predictions.pten,
    predictions.erg,
    predictions.rf22,
    predictions.rf22scan,
    predictions.rf22s,
    predictions.ccp,
    predictions.knn392scan,
    predictions.knn104scan,
    predictions.rf72scan,
    predictions.penney2011scan,
    predictions.ntv4,
    predictions.sawyers,
    predictions.rsi,
    predictions.gps_rf,
    predictions.old,
    predictions.rf15frma,
    predictions.knn51lni,
    predictions.ccp_rf,
    predictions.penney2011_rf,
    rna.rna_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_note,
    samples_bioinfo.storage,
    samples_bioinfo.sample_type,
    samples_bioinfo.tissue_type,
    samples_bioinfo.blockage,
    samples_bioinfo.core_id,
    samples_bioinfo.date_processed,
    samples_bioinfo.date_shipped,
    samples_bioinfo.date_received,
    samples_bioinfo.tissue_gleason,
    samples_bioinfo.tissue_gleason_s,
    samples_bioinfo.gdx_id,
    samples_bioinfo.core_type
   FROM (((((patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_bruce IS TRUE))))));


ALTER TABLE view_bruce OWNER TO administrator;

--
-- Name: view_firas_abdollah; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_firas_abdollah AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.clings,
    patients_bioinfo.clings_p,
    patients_bioinfo.clings_s,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.clings_p_percent,
    patients_bioinfo.clings_s_percent,
    patients_bioinfo.pathgs_p_percent,
    patients_bioinfo.pathgs_s_percent,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.external_id,
    patients_bioinfo.created,
    patients_bioinfo.modified,
    patients_bioinfo.created_by,
    patients_bioinfo.modified_by,
    patients_bioinfo.age_rt,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.comments,
    patients_bioinfo.dob,
    patients_bioinfo.ece,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.met,
    patients_bioinfo.met_fu_date,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.pathid,
    patients_bioinfo.pelvis,
    patients_bioinfo.postrp_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.psadt,
    patients_bioinfo.psadt_rp_rt,
    patients_bioinfo.rp_date,
    patients_bioinfo.rp_to_bcr_time,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_end,
    patients_bioinfo.rt_start,
    patients_bioinfo.original_bcr_time,
    patients_bioinfo.original_met_time,
    patients_bioinfo.original_os_time,
    patients_bioinfo.rt_type,
    patients_bioinfo.srt_nadir_psa,
    patients_bioinfo.stephenson_2yr,
    patients_bioinfo.stephenson_5yr,
    patients_bioinfo.stephenson_7yr,
    patients_bioinfo.stephenson_rev,
    patients_bioinfo.stephenson_srt,
    patients_bioinfo.stephenson_srt_rev,
    patients_bioinfo.surgery_id,
    patients_bioinfo.stephenson_10yr,
    patients_bioinfo.os_fu_date,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.psa1,
    patients_bioinfo.psa2,
    patients_bioinfo.psa1_time,
    patients_bioinfo.psa2_time,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.lcr,
    patients_bioinfo.lcr_time,
    patients_bioinfo.adjuvant_adt_flag,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.adjuvant_rt_flag,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.adt_time,
    patients_bioinfo.rt_time,
    patients_bioinfo.cc,
    patients_bioinfo.ploidy,
    patients_bioinfo.cure1,
    patients_bioinfo.cure2,
    patients_bioinfo.kattan_5yr,
    patients_bioinfo.swog_group,
    patients_bioinfo.gpsm,
    patients_bioinfo.bcr_time,
    patients_bioinfo.nodes_pos,
    patients_bioinfo.rp_type,
    patients_bioinfo.chemo,
    patients_bioinfo.adt_type,
    patients_bioinfo.predx_psa,
    patients_bioinfo.pretx_psa,
    patients_bioinfo.neoadj_rt,
    patients_bioinfo.ece_type,
    patients_bioinfo.copy_number_cluster,
    patients_bioinfo.nodes_rmd,
    patients_bioinfo.lvi,
    patients_bioinfo.adjctx,
    patients_bioinfo.gender,
    patients_bioinfo.iv_rx,
    patients_bioinfo.rec,
    patients_bioinfo.rec_time,
    patients_bioinfo.ibcnc,
    patients_bioinfo.bcr_fu_date,
    patients_bioinfo.family_history,
    patients_bioinfo.bfailure,
    patients_bioinfo.bfailure_time,
    patients_bioinfo.death_cause,
    patients_bioinfo.original_pathgs,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.original_pathgs_p,
    patients_bioinfo.original_pathgs_s,
    patients_bioinfo.user_role,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.original_clings,
    patients_bioinfo.original_clings_p,
    patients_bioinfo.original_clings_s,
    patients_bioinfo.neoplastic_cell_count,
    patients_bioinfo.match,
    patients_bioinfo.tumor_weight,
    patients_bioinfo.pten_ihc,
    patients_bioinfo.pten_fish,
    patients_bioinfo.myc_inc,
    patients_bioinfo.lpl_dec,
    patients_bioinfo.bmi,
    patients_bioinfo.bx_date,
    patients_bioinfo.cfailure,
    patients_bioinfo.dominant_anterior_nodule,
    patients_bioinfo.original_met,
    patients_bioinfo.original_os,
    patients_bioinfo.original_bcr,
    patients_bioinfo.original_pcsm,
    patients_bioinfo.original_pcsm_time,
    patients_bioinfo.patient_note,
    patients_bioinfo.source,
    patients_bioinfo.catalog_no,
    patients_bioinfo.xrt,
    patients_bioinfo.cnode,
    patients_bioinfo.cpos_nodes,
    patients_bioinfo.cmet,
    patients_bioinfo.cmet_location,
    patients_bioinfo.postchemo_stage,
    patients_bioinfo.postchemo_node,
    patients_bioinfo.postchemo_met,
    patients_bioinfo.ene,
    patients_bioinfo.aicc,
    patients_bioinfo.trg_pooled,
    patients_bioinfo.trg_pt,
    patients_bioinfo.trg_met,
    patients_bioinfo.neoadj_ctx,
    patients_bioinfo.neoadj_ctx_type,
    patients_bioinfo.neoadj_ctx_cycle,
    patients_bioinfo.neoadj_ctx_secondary,
    patients_bioinfo.neoadj_ctx_secondary_type,
    patients_bioinfo.neoadj_ctx_secondary_cycle,
    patients_bioinfo.csm,
    patients_bioinfo.prechemo_met,
    patients_bioinfo.lni_level,
    patients_bioinfo.chemo_response,
    patients_bioinfo.post_rp_patients,
    patients_bioinfo.post_bcr_patients,
    patients_bioinfo.idc,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.original_pstage,
    patients_bioinfo.rp_year,
    patients_bioinfo.bx_pos_cores,
    patients_bioinfo.bx_total_cores,
    patients_bioinfo.bx_max_percent,
    patients_bioinfo.original_sm,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    patients_bioinfo.chemo_s,
    patients_bioinfo.chemo_time,
    patients_bioinfo.original_epe,
    patients_bioinfo.egg15,
    patients_bioinfo.previous_treatment,
    patients_bioinfo.dod,
    patients_bioinfo.pni,
    patients_bioinfo.family_history_type,
    patients_bioinfo.raw_cstage,
    patients_bioinfo.met_site,
    patients_bioinfo.received_blocks,
    patients_bioinfo.rt_s_date,
    patients_bioinfo.adt_s_date,
    patients_bioinfo.crpc_date,
    patients_bioinfo.bfailure_fu_date,
    patients_bioinfo.sample_source,
    patients_bioinfo.rt_s_type,
    patients_bioinfo.received_batch,
    patients_bioinfo.crpc,
    patients_bioinfo.received_unstained_slides,
    patients_bioinfo.received_he_slides,
    patients_bioinfo.adt_length,
    cdna.cdna_id,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.ss_cdna_yield,
    cdna.ss_cdna_concentration,
    cdna.ss_cdna_260,
    cdna.ss_cdna_280,
    cdna.ss_cdna_260_280,
    cdna.ss_cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.cdna_note,
    celfiles.chip_id,
    celfiles.erg_gene,
    celfiles.etv1_gene,
    celfiles.etv4_gene,
    celfiles.qc_batch_id,
    celfiles.location,
    celfiles.qc_flag,
    celfiles.rsi_scan,
    celfiles.percentpresent,
    celfiles.posneg_auc,
    celfiles.hybcontrol,
    celfiles.hybpercentpresent,
    predictions.celfile_name,
    predictions.ntv3,
    predictions.ntv2,
    predictions.spink1,
    predictions.etv1,
    predictions.etv4,
    predictions.etv5,
    predictions.fli1,
    predictions.pten,
    predictions.erg,
    predictions.ntv4,
    predictions.rf22scan,
    predictions.decipher,
    predictions.penney2011scan,
    classifications.molecular_subtype,
    rna.rna_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_note,
    samples_bioinfo.storage,
    samples_bioinfo.sample_type,
    samples_bioinfo.tissue_type,
    samples_bioinfo.blockage,
    samples_bioinfo.core_id,
    samples_bioinfo.date_processed,
    samples_bioinfo.date_shipped,
    samples_bioinfo.date_received,
    samples_bioinfo.tissue_gleason,
    samples_bioinfo.tissue_gleason_s,
    samples_bioinfo.gdx_id,
    samples_bioinfo.core_type
   FROM ((((((patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
     JOIN classifications USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_firas_abdollah IS TRUE))))));


ALTER TABLE view_firas_abdollah OWNER TO administrator;

--
-- Name: view_george; Type: VIEW; Schema: samplesdb; Owner: fabiola
--

CREATE VIEW view_george AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.clings,
    patients_bioinfo.clings_p,
    patients_bioinfo.clings_s,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.clings_p_percent,
    patients_bioinfo.clings_s_percent,
    patients_bioinfo.pathgs_p_percent,
    patients_bioinfo.pathgs_s_percent,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.external_id,
    patients_bioinfo.created,
    patients_bioinfo.modified,
    patients_bioinfo.created_by,
    patients_bioinfo.modified_by,
    patients_bioinfo.age_rt,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.comments,
    patients_bioinfo.dob,
    patients_bioinfo.ece,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.met,
    patients_bioinfo.met_fu_date,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.pathid,
    patients_bioinfo.pelvis,
    patients_bioinfo.postrp_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.psadt,
    patients_bioinfo.psadt_rp_rt,
    patients_bioinfo.rp_date,
    patients_bioinfo.rp_to_bcr_time,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_end,
    patients_bioinfo.rt_start,
    patients_bioinfo.original_bcr_time,
    patients_bioinfo.original_met_time,
    patients_bioinfo.original_os_time,
    patients_bioinfo.rt_type,
    patients_bioinfo.srt_nadir_psa,
    patients_bioinfo.stephenson_2yr,
    patients_bioinfo.stephenson_5yr,
    patients_bioinfo.stephenson_7yr,
    patients_bioinfo.stephenson_rev,
    patients_bioinfo.stephenson_srt,
    patients_bioinfo.stephenson_srt_rev,
    patients_bioinfo.surgery_id,
    patients_bioinfo.stephenson_10yr,
    patients_bioinfo.os_fu_date,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.psa1,
    patients_bioinfo.psa2,
    patients_bioinfo.psa1_time,
    patients_bioinfo.psa2_time,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.lcr,
    patients_bioinfo.lcr_time,
    patients_bioinfo.adjuvant_adt_flag,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.adjuvant_rt_flag,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.adt_time,
    patients_bioinfo.rt_time,
    patients_bioinfo.cc,
    patients_bioinfo.ploidy,
    patients_bioinfo.cure1,
    patients_bioinfo.cure2,
    patients_bioinfo.kattan_5yr,
    patients_bioinfo.swog_group,
    patients_bioinfo.gpsm,
    patients_bioinfo.bcr_time,
    patients_bioinfo.nodes_pos,
    patients_bioinfo.rp_type,
    patients_bioinfo.chemo,
    patients_bioinfo.adt_type,
    patients_bioinfo.predx_psa,
    patients_bioinfo.pretx_psa,
    patients_bioinfo.neoadj_rt,
    patients_bioinfo.ece_type,
    patients_bioinfo.copy_number_cluster,
    patients_bioinfo.nodes_rmd,
    patients_bioinfo.lvi,
    patients_bioinfo.adjctx,
    patients_bioinfo.gender,
    patients_bioinfo.iv_rx,
    patients_bioinfo.rec,
    patients_bioinfo.rec_time,
    patients_bioinfo.ibcnc,
    patients_bioinfo.bcr_fu_date,
    patients_bioinfo.family_history,
    patients_bioinfo.bfailure,
    patients_bioinfo.bfailure_time,
    patients_bioinfo.death_cause,
    patients_bioinfo.original_pathgs,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.original_pathgs_p,
    patients_bioinfo.original_pathgs_s,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.original_clings,
    patients_bioinfo.original_clings_p,
    patients_bioinfo.original_clings_s,
    patients_bioinfo.neoplastic_cell_count,
    patients_bioinfo.match,
    patients_bioinfo.tumor_weight,
    patients_bioinfo.pten_ihc,
    patients_bioinfo.pten_fish,
    patients_bioinfo.myc_inc,
    patients_bioinfo.lpl_dec,
    patients_bioinfo.bmi,
    patients_bioinfo.bx_date,
    patients_bioinfo.cfailure,
    patients_bioinfo.dominant_anterior_nodule,
    patients_bioinfo.original_met,
    patients_bioinfo.original_os,
    patients_bioinfo.original_bcr,
    patients_bioinfo.original_pcsm,
    patients_bioinfo.original_pcsm_time,
    patients_bioinfo.patient_note,
    patients_bioinfo.source,
    patients_bioinfo.catalog_no,
    patients_bioinfo.xrt,
    patients_bioinfo.cnode,
    patients_bioinfo.cpos_nodes,
    patients_bioinfo.cmet,
    patients_bioinfo.cmet_location,
    patients_bioinfo.postchemo_stage,
    patients_bioinfo.postchemo_node,
    patients_bioinfo.postchemo_met,
    patients_bioinfo.ene,
    patients_bioinfo.aicc,
    patients_bioinfo.trg_pooled,
    patients_bioinfo.trg_pt,
    patients_bioinfo.trg_met,
    patients_bioinfo.neoadj_ctx,
    patients_bioinfo.neoadj_ctx_type,
    patients_bioinfo.neoadj_ctx_cycle,
    patients_bioinfo.neoadj_ctx_secondary,
    patients_bioinfo.neoadj_ctx_secondary_type,
    patients_bioinfo.neoadj_ctx_secondary_cycle,
    patients_bioinfo.csm,
    patients_bioinfo.prechemo_met,
    patients_bioinfo.lni_level,
    patients_bioinfo.chemo_response,
    patients_bioinfo.post_rp_patients,
    patients_bioinfo.post_bcr_patients,
    patients_bioinfo.idc,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.original_pstage,
    patients_bioinfo.rp_year,
    patients_bioinfo.bx_pos_cores,
    patients_bioinfo.bx_total_cores,
    patients_bioinfo.bx_max_percent,
    patients_bioinfo.original_sm,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    patients_bioinfo.chemo_s,
    patients_bioinfo.chemo_time,
    patients_bioinfo.original_epe,
    patients_bioinfo.egg15,
    patients_bioinfo.previous_treatment,
    patients_bioinfo.dod,
    patients_bioinfo.pni,
    patients_bioinfo.family_history_type,
    patients_bioinfo.raw_cstage,
    patients_bioinfo.met_site,
    patients_bioinfo.received_blocks,
    patients_bioinfo.rt_s_date,
    patients_bioinfo.adt_s_date,
    patients_bioinfo.crpc_date,
    patients_bioinfo.bfailure_fu_date,
    patients_bioinfo.sample_source,
    patients_bioinfo.rt_s_type,
    patients_bioinfo.received_batch,
    patients_bioinfo.crpc,
    patients_bioinfo.received_unstained_slides,
    patients_bioinfo.received_he_slides,
    patients_bioinfo.adt_length,
    patients_bioinfo.rt_to_met_time,
    patients_bioinfo.rt_to_bfailure_time,
    patients_bioinfo.rt_to_os_time,
    patients_bioinfo.ordering_physician,
    patients_bioinfo.practice_name,
    patients_bioinfo.practice_city,
    patients_bioinfo.practice_state,
    patients_bioinfo.practice_zipcode,
    patients_bioinfo.practice_country,
    patients_bioinfo.practice_province,
    patients_bioinfo.practice_postal_code,
    patients_bioinfo.birth_year,
    patients_bioinfo.age_bx,
    patients_bioinfo.bx_year,
    patients_bioinfo.bx_tumor_involvement,
    patients_bioinfo.physically_altered,
    patients_bioinfo.curative_turbt_treatment,
    patients_bioinfo.clin_grade,
    patients_bioinfo.path_grade,
    patients_bioinfo.site,
    patients_bioinfo.xsn_pathfeatures_notes,
    patients_bioinfo.xsn_specimen_notes,
    patients_bioinfo.bni,
    patients_bioinfo.other_highriskfeatures,
    patients_bioinfo.practice_type,
    patients_bioinfo.pstage_gbx,
    patients_bioinfo.preop_psa_groups,
    patients_bioinfo.age_groups,
    patients_bioinfo.bx_total_cores_left,
    patients_bioinfo.bx_total_cores_right,
    patients_bioinfo.bx_pos_cores_left,
    patients_bioinfo.bx_pos_cores_right,
    patients_bioinfo.outcome,
    patients_bioinfo.sm_notes,
    patients_bioinfo.surgery_s_notes,
    patients_bioinfo.adt_start,
    patients_bioinfo.adt_end,
    patients_bioinfo.rt_region,
    patients_bioinfo.tx_s_type,
    patients_bioinfo.surgery_s_date,
    patients_bioinfo.enzalutamide_date,
    patients_bioinfo.abiraterone_date,
    patients_bioinfo.tx_s_date,
    patients_bioinfo.intact_psa,
    patients_bioinfo.nadir,
    patients_bioinfo.surgery_s,
    patients_bioinfo.enzalutamide,
    patients_bioinfo.abiraterone,
    patients_bioinfo.tx_s,
    patients_bioinfo.pin,
    patients_bioinfo.crpc_time,
    cdna.cdna_id,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.ss_cdna_yield,
    cdna.ss_cdna_concentration,
    cdna.ss_cdna_260,
    cdna.ss_cdna_280,
    cdna.ss_cdna_260_280,
    cdna.ss_cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.cdna_note,
    celfiles.chip_id,
    celfiles.erg_gene,
    celfiles.etv1_gene,
    celfiles.etv4_gene,
    celfiles.qc_batch_id,
    celfiles.location,
    celfiles.qc_flag,
    celfiles.rsi_scan,
    celfiles.rf22_scan,
    celfiles.gcc_scan,
    celfiles.gcs_scan,
    celfiles.rf22_scan_5yr,
    celfiles.rf22_prod_5yr,
    celfiles.rf22_scan_3yrbcr,
    celfiles.rf22_prod_3yrbcr,
    celfiles.percentpresent,
    celfiles.rf22_percentpresent,
    celfiles.rf22_prod,
    celfiles.posneg_auc,
    celfiles.hybcontrol,
    celfiles.hybpercentpresent,
    predictions.celfile_name,
    predictions.decipher,
    predictions.penney2011frma,
    predictions.ntv3,
    predictions.ntv2,
    predictions.bcr_knn56,
    predictions.bcr_rf13,
    predictions.psadt_rf13,
    predictions.upgrading_knn16,
    predictions.knn392frma,
    predictions.knn104frma,
    predictions.rf72frma,
    predictions.spink1,
    predictions.etv1,
    predictions.etv4,
    predictions.etv5,
    predictions.fli1,
    predictions.pten,
    predictions.erg,
    predictions.rf22,
    predictions.rf22scan,
    predictions.rf22s,
    predictions.ccp,
    predictions.knn392scan,
    predictions.knn104scan,
    predictions.rf72scan,
    predictions.penney2011scan,
    predictions.ntv4,
    predictions.sawyers,
    predictions.rsi,
    predictions.gps_rf,
    predictions.old,
    predictions.rf15frma,
    predictions.knn51lni,
    predictions.ccp_rf,
    predictions.penney2011_rf,
    classifications.molecular_subtype,
    rna.rna_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_note,
    samples_bioinfo.storage,
    samples_bioinfo.sample_type,
    samples_bioinfo.tissue_type,
    samples_bioinfo.blockage,
    samples_bioinfo.core_id,
    samples_bioinfo.date_processed,
    samples_bioinfo.date_shipped,
    samples_bioinfo.date_received,
    samples_bioinfo.tissue_gleason,
    samples_bioinfo.tissue_gleason_s,
    samples_bioinfo.gdx_id,
    samples_bioinfo.core_type,
    celgrp.mayodisc_grouping,
    gridclass.etv1_status,
    gridclass.etv4_status,
    gridclass.etv5_status,
    gridclass.fli1_status,
    gridclass.spink1_status,
    gridclass.erg_status
   FROM ((((((((patients_bioinfo
     JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     JOIN celfiles USING (cdna_id))
     JOIN predictions USING (celfile_name))
     LEFT JOIN ( SELECT class.celfile_name,
            class.etv1 AS etv1_status,
            class.etv4 AS etv4_status,
            class.etv5 AS etv5_status,
            class.fli1 AS fli1_status,
            class.spink1 AS spink1_status,
            class.ergmodel AS erg_status
           FROM analysis.class) gridclass USING (celfile_name))
     LEFT JOIN classifications USING (celfile_name))
     LEFT JOIN ( SELECT celfile_groups_analysis.celfile_name,
            celfile_groups_analysis.celfile_group_name AS mayodisc_grouping
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = ANY (ARRAY[('Mayo-trn'::character varying)::text, ('Mayo-val'::character varying)::text]))) celgrp USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_george IS TRUE))))));


ALTER TABLE view_george OWNER TO fabiola;

--
-- Name: view_kenneth_pienta; Type: VIEW; Schema: samplesdb; Owner: fabiola
--

CREATE VIEW view_kenneth_pienta AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.ece,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.met,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.lcr,
    patients_bioinfo.lcr_time,
    patients_bioinfo.adjuvant_adt_flag,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.adjuvant_rt_flag,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.cc,
    patients_bioinfo.ploidy,
    patients_bioinfo.bcr_time,
    patients_bioinfo.lvi,
    patients_bioinfo.gender,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    celfiles.qc_batch_id,
    celfiles.qc_flag,
    celfiles.celfile_name
   FROM (((((patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_kenneth_pienta IS TRUE))))));


ALTER TABLE view_kenneth_pienta OWNER TO fabiola;

--
-- Name: view_laura; Type: VIEW; Schema: samplesdb; Owner: fabiola
--

CREATE VIEW view_laura AS
 SELECT patients.patient_id,
    samples.study_name,
    patients.clings,
    patients.clings_p,
    patients.clings_s,
    patients.pathgs,
    patients.pathgs_p,
    patients.pathgs_s,
    patients.clings_p_percent,
    patients.clings_s_percent,
    patients.pathgs_p_percent,
    patients.pathgs_s_percent,
    patients.age,
    patients.pstage,
    patients.preop_psa,
    patients.bcr,
    patients.adt,
    patients.rt,
    patients.created,
    patients.modified,
    patients.created_by,
    patients.modified_by,
    patients.age_rt,
    patients.race,
    patients.capra_s,
    patients.comments,
    patients.dob,
    patients.ece,
    patients.fu_rp_time,
    patients.fu_time,
    patients.lastfu_date,
    patients.met,
    patients.met_fu_date,
    patients.met_time,
    patients.neoadj_adt,
    patients.os,
    patients.os_time,
    patients.pathid,
    patients.pelvis,
    patients.postrp_psa,
    patients.prert_psa,
    patients.psadt,
    patients.psadt_rp_rt,
    patients.rp_date,
    patients.rp_to_bcr_time,
    patients.rp_to_rt_months,
    patients.rt_dose,
    patients.rt_end,
    patients.rt_start,
    patients.original_bcr_time,
    patients.original_met_time,
    patients.original_os_time,
    patients.rt_type,
    patients.srt_nadir_psa,
    patients.stephenson_2yr,
    patients.stephenson_5yr,
    patients.stephenson_7yr,
    patients.stephenson_rev,
    patients.stephenson_srt,
    patients.stephenson_srt_rev,
    patients.surgery_id,
    patients.stephenson_10yr,
    patients.os_fu_date,
    patients.lni,
    patients.cstage,
    patients.tumor_vol,
    patients.mayo_def,
    patients.psa1,
    patients.psa2,
    patients.psa1_time,
    patients.psa2_time,
    patients.pcsm,
    patients.pcsm_time,
    patients.lcr,
    patients.lcr_time,
    patients.adjuvant_adt_flag,
    patients.adt_a,
    patients.adt_s,
    patients.adjuvant_rt_flag,
    patients.rt_a,
    patients.rt_s,
    patients.adt_time,
    patients.rt_time,
    patients.cc,
    patients.ploidy,
    patients.cure1,
    patients.cure2,
    patients.kattan_5yr,
    patients.swog_group,
    patients.gpsm,
    patients.bcr_time,
    patients.nodes_pos,
    patients.rp_type,
    patients.chemo,
    patients.adt_type,
    patients.predx_psa,
    patients.pretx_psa,
    patients.neoadj_rt,
    patients.ece_type,
    patients.copy_number_cluster,
    patients.nodes_rmd,
    patients.lvi,
    patients.adjctx,
    patients.gender,
    patients.iv_rx,
    patients.rec,
    patients.rec_time,
    patients.ibcnc,
    patients.bcr_fu_date,
    patients.family_history,
    patients.bfailure,
    patients.bfailure_time,
    patients.death_cause,
    patients.original_pathgs,
    patients.pathgs_t,
    patients.original_pathgs_p,
    patients.original_pathgs_s,
    patients.user_role,
    patients.svi,
    patients.sm,
    patients.original_clings,
    patients.original_clings_p,
    patients.original_clings_s,
    patients.neoplastic_cell_count,
    patients.match,
    patients.tumor_weight,
    patients.pten_ihc,
    patients.pten_fish,
    patients.myc_inc,
    patients.lpl_dec,
    patients.bmi,
    patients.bx_date,
    patients.cfailure,
    patients.dominant_anterior_nodule,
    patients.original_met,
    patients.original_os,
    patients.original_bcr,
    patients.original_pcsm,
    patients.original_pcsm_time,
    patients.patient_note,
    patients.source,
    patients.catalog_no,
    patients.xrt,
    patients.cnode,
    patients.cpos_nodes,
    patients.cmet,
    patients.cmet_location,
    patients.postchemo_stage,
    patients.postchemo_node,
    patients.postchemo_met,
    patients.ene,
    patients.aicc,
    patients.trg_pooled,
    patients.trg_pt,
    patients.trg_met,
    patients.neoadj_ctx,
    patients.neoadj_ctx_type,
    patients.neoadj_ctx_cycle,
    patients.neoadj_ctx_secondary,
    patients.neoadj_ctx_secondary_type,
    patients.neoadj_ctx_secondary_cycle,
    patients.csm,
    patients.prechemo_met,
    patients.lni_level,
    patients.chemo_response,
    patients.post_rp_patients,
    patients.post_bcr_patients,
    patients.idc,
    patients.post_rp_patients_cchdef,
    patients.post_bcr_patients_cchdef,
    patients.original_pstage,
    patients.rp_year,
    patients.bx_pos_cores,
    patients.bx_total_cores,
    patients.bx_max_percent,
    patients.original_sm,
    patients.height,
    patients.weight,
    patients.chemo_s,
    patients.chemo_time,
    patients.original_epe,
    patients.egg15,
    patients.previous_treatment,
    patients.dod,
    patients.pni,
    patients.family_history_type,
    patients.raw_cstage,
    patients.met_site,
    patients.received_blocks,
    patients.rt_s_date,
    patients.adt_s_date,
    patients.crpc_date,
    patients.bfailure_fu_date,
    patients.sample_source,
    patients.rt_s_type,
    patients.received_batch,
    patients.crpc,
    patients.received_unstained_slides,
    patients.received_he_slides,
    patients.adt_length,
    patients.rt_to_met_time,
    patients.rt_to_bfailure_time,
    patients.rt_to_os_time,
    patients.ordering_physician,
    patients.practice_name,
    patients.practice_city,
    patients.practice_state,
    patients.practice_zipcode,
    patients.practice_country,
    patients.practice_province,
    patients.practice_postal_code,
    patients.birth_year,
    patients.age_bx,
    patients.bx_year,
    patients.bx_tumor_involvement,
    patients.physically_altered,
    patients.curative_turbt_treatment,
    patients.clin_grade,
    patients.path_grade,
    patients.site,
    patients.xsn_pathfeatures_notes,
    patients.xsn_specimen_notes,
    patients.pt2_gbx,
    patients.lni_gbx,
    patients.epe_gbx,
    patients.svi_gbx,
    patients.bcr_gbx,
    patients.sm_gbx,
    patients.bni,
    patients.other_highriskfeatures,
    patients.practice_type,
    patients.pstage_gbx,
    patients.preop_psa_groups,
    patients.age_groups,
    patients.bx_total_cores_left,
    patients.bx_total_cores_right,
    patients.bx_pos_cores_left,
    patients.bx_pos_cores_right,
    patients.outcome,
    patients.sm_notes,
    patients.surgery_s_notes,
    patients.adt_start,
    patients.adt_end,
    patients.rt_region,
    patients.tx_s_type,
    patients.surgery_s_date,
    patients.enzalutamide_date,
    patients.abiraterone_date,
    patients.tx_s_date,
    patients.intact_psa,
    patients.nadir,
    patients.surgery_s,
    patients.enzalutamide,
    patients.abiraterone,
    patients.tx_s,
    patients.pin,
    patients.crpc_time,
    cdna.cdna_id,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.ss_cdna_yield,
    cdna.ss_cdna_concentration,
    cdna.ss_cdna_260,
    cdna.ss_cdna_280,
    cdna.ss_cdna_260_280,
    cdna.ss_cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.cdna_note,
    celfiles.chip_id,
    celfiles.erg_gene,
    celfiles.etv1_gene,
    celfiles.etv4_gene,
    celfiles.qc_batch_id,
    celfiles.location,
    celfiles.qc_flag,
    celfiles.rsi_scan,
    celfiles.rf22_scan,
    celfiles.gcc_scan,
    celfiles.gcs_scan,
    celfiles.rf22_scan_5yr,
    celfiles.rf22_prod_5yr,
    celfiles.rf22_scan_3yrbcr,
    celfiles.rf22_prod_3yrbcr,
    celfiles.percentpresent,
    celfiles.rf22_percentpresent,
    celfiles.rf22_prod,
    celfiles.posneg_auc,
    celfiles.hybcontrol,
    celfiles.hybpercentpresent,
    celfiles.celfile_name,
    celfiles.qc_flag_bladder,
    predictions.decipher,
    predictions.penney2011frma,
    predictions.ntv3,
    predictions.ntv2,
    predictions.bcr_knn56,
    predictions.bcr_rf13,
    predictions.psadt_rf13,
    predictions.upgrading_knn16,
    predictions.knn392frma,
    predictions.knn104frma,
    predictions.rf72frma,
    predictions.spink1,
    predictions.etv1,
    predictions.etv4,
    predictions.etv5,
    predictions.fli1,
    predictions.pten,
    predictions.erg,
    predictions.rf22,
    predictions.rf22scan,
    predictions.rf22s,
    predictions.ccp,
    predictions.knn392scan,
    predictions.knn104scan,
    predictions.rf72scan,
    predictions.penney2011scan,
    predictions.ntv4,
    predictions.sawyers,
    predictions.rsi,
    predictions.gps_rf,
    predictions.old,
    predictions.rf15frma,
    predictions.knn51lni,
    predictions.ccp_rf,
    predictions.penney2011_rf,
    classifications.molecular_subtype,
    rna.rna_id,
    rna.rna_extraction_method,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_note,
    samples.storage,
    samples.sample_type,
    samples.specimen_type,
    samples.tissue_type,
    samples.external_id,
    samples.blockage,
    samples.core_id,
    samples.date_processed,
    samples.date_shipped,
    samples.date_received,
    samples.tissue_gleason,
    samples.tissue_gleason_s,
    samples.gdx_id,
    samples.core_type,
    samples.tumor_percent,
    samples.benign_percent,
    samples.stromal_percent,
    samples.bx_core_length,
    samples.bx_core_per_block,
    samples.bx_tumor_percent,
    samples.bx_macrodissected_percent,
    celgrp3.mayodisc_grouping,
    gridclass.etv1_status,
    gridclass.etv4_status,
    gridclass.etv5_status,
    gridclass.fli1_status,
    gridclass.spink1_status,
    gridclass.erg_status
   FROM ((((((((patients
     RIGHT JOIN samples USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
     LEFT JOIN ( SELECT class.celfile_name,
            class.etv1 AS etv1_status,
            class.etv4 AS etv4_status,
            class.etv5 AS etv5_status,
            class.fli1 AS fli1_status,
            class.spink1 AS spink1_status,
            class.ergmodel AS erg_status
           FROM analysis.class) gridclass USING (celfile_name))
     LEFT JOIN classifications USING (celfile_name))
     LEFT JOIN ( SELECT celfile_groups_analysis.celfile_name,
            celfile_groups_analysis.celfile_group_name AS mayodisc_grouping
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = ANY (ARRAY[('Mayo-trn'::character varying)::text, ('Mayo-val'::character varying)::text]))) celgrp3 USING (celfile_name))
  WHERE ((((samples.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_laura IS TRUE)))))) OR (((samples.user_role)::text = 'biostats'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = 'Brigham-RTADT-Analysis'::text)))));


ALTER TABLE view_laura OWNER TO fabiola;

--
-- Name: view_mgh; Type: VIEW; Schema: samplesdb; Owner: mandeep
--

CREATE VIEW view_mgh AS
 SELECT cdna.cdna_id,
    rna.rna_id,
    samples_bioinfo.sample_id,
    patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    studies.institution_name,
    studies.disease,
    studies.principal_investigator,
    studies.study_code,
    studies.accession_code,
    studies.study_status,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.race,
    patients_bioinfo.fu_time,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_type,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.chemo,
    patients_bioinfo.lvi,
    patients_bioinfo.gender,
    patients_bioinfo.bmi,
    patients_bioinfo.patient_note,
    patients_bioinfo.cnode,
    patients_bioinfo.cmet,
    patients_bioinfo.csm,
    patients_bioinfo.chemo_response,
    patients_bioinfo.bx_year,
    patients_bioinfo.site,
    patients_bioinfo.surgery_type,
    patients_bioinfo.kps,
    patients_bioinfo.smoker,
    patients_bioinfo.alcohol,
    patients_bioinfo.cis,
    patients_bioinfo.hydronephrosis,
    patients_bioinfo.distant_failure_time,
    patients_bioinfo.distant_failure_response,
    patients_bioinfo.distant_failure,
    patients_bioinfo.local_failure,
    patients_bioinfo.local_failure_time,
    patients_bioinfo.local_failure_response,
    patients_bioinfo.regional_failure,
    patients_bioinfo.regional_failure_time,
    patients_bioinfo.regional_failure_response,
    patients_bioinfo.csm_time,
    patients_bioinfo.management,
    patients_bioinfo.grade,
    patients_bioinfo.turbt_complete,
    patients_bioinfo.postrt_biopsy,
    patients_bioinfo.postrt_cytology,
    patients_bioinfo.pgrade,
    patients_bioinfo.path_histology_primary,
    patients_bioinfo.turbt_histology_primary,
    patients_bioinfo.tumor_location,
    patients_bioinfo.distant_failure_site,
    patients_bioinfo.distant_failure_tx,
    patients_bioinfo.local_failure_tx,
    patients_bioinfo.local_failure_type,
    patients_bioinfo.local_failure_grade,
    patients_bioinfo.regional_failure_site,
    patients_bioinfo.regional_failure_tx,
    patients_bioinfo.chemo_type,
    samples_bioinfo.storage,
    samples_bioinfo.sample_type,
    samples_bioinfo.tissue_type,
    samples_bioinfo.tissue_attribute,
    samples_bioinfo.date_received,
    samples_bioinfo.specimen_type,
    samples_bioinfo.specimen_measure,
    patients_bioinfo.external_id,
    samples_bioinfo.received_blocks,
    samples_bioinfo.received_slides,
    samples_bioinfo.received_punches,
    samples_bioinfo.received_stained_slides,
    samples_bioinfo.received_rna,
    samples_bioinfo.failure_reason,
    samples_bioinfo.status,
    samples_bioinfo.accession_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_volume,
    rna.rna_extraction_method,
    rna.rna_quantification_method,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.rna_input,
    celfiles.celfile_name,
    celfiles.qc_batch_id,
    celfiles.qc_flag,
    celfiles.percentpresent,
    celfiles.posneg_auc,
    celfiles.hybpercentpresent,
    celfiles.celfile_date,
    celfiles.qc_flag_bladder
   FROM (((((studies
     JOIN patients_bioinfo USING (study_name))
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_mgh IS TRUE))))));


ALTER TABLE view_mgh OWNER TO mandeep;

--
-- Name: view_michael_freeman; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_michael_freeman AS
 SELECT studies.study_name_short,
    studies.design,
    patients_bioinfo.mayo_def,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.bcr,
    patients_bioinfo.bcr_time,
    patients_bioinfo.met,
    patients_bioinfo.met_time,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.capra_s,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.ece,
    patients_bioinfo.lni,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_type,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.adt_type,
    samples_bioinfo.storage,
    celfiles.qc_batch_id,
    celfiles.celfile_name,
    classifications.etv1,
    classifications.etv4,
    classifications.etv5,
    classifications.erg,
    classifications.spink1,
    classifications.fli1,
        CASE
            WHEN ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
               FROM celfile_groups_analysis
              WHERE ((celfile_groups_analysis.celfile_group_name)::text = 'JHMI-RossSchaeffer-MetsVal-postBCR'::text))) THEN true
            ELSE false
        END AS jhmi_bcr,
        CASE
            WHEN ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
               FROM celfile_groups_analysis
              WHERE ((celfile_groups_analysis.celfile_group_name)::text = 'JHMI-RossSchaeffer-MetsVal-postRP'::text))) THEN true
            ELSE false
        END AS jhmi_rp
   FROM ((((((((studies
     JOIN patients_bioinfo USING (study_name))
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN classifications USING (celfile_name))
     LEFT JOIN ( SELECT celfile_groups_analysis.celfile_name,
            true AS jhmi_bcr
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = 'JHMI-RossSchaeffer-MetsVal-postBCR'::text)) celgrp USING (celfile_name))
     LEFT JOIN ( SELECT celfile_groups_analysis.celfile_name,
            true AS jhmi_rp
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = 'JHMI-RossSchaeffer-MetsVal-postRP'::text)) celgrp2 USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_michael_freeman IS TRUE))))));


ALTER TABLE view_michael_freeman OWNER TO administrator;

--
-- Name: view_patrick_karabon; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_patrick_karabon AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.ece,
    patients_bioinfo.met,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.bcr_time,
    patients_bioinfo.lvi,
    patients_bioinfo.gender,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    samples_bioinfo.storage,
    celfiles.qc_batch_id,
    celfiles.qc_flag,
    celfiles.celfile_name
   FROM (((((patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_patrick_karabon IS TRUE))))));


ALTER TABLE view_patrick_karabon OWNER TO administrator;

--
-- Name: view_robert_den; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_robert_den AS
 SELECT patients_bioinfo.study_name,
    patients_bioinfo.patient_id,
    patients_bioinfo.external_id,
    patients_bioinfo.pathid,
    patients_bioinfo.surgery_id,
    patients_bioinfo.comments,
    patients_bioinfo.age,
    patients_bioinfo.age_rt,
    patients_bioinfo.dob,
    patients_bioinfo.rp_year,
    patients_bioinfo.rp_date,
    patients_bioinfo.race,
    patients_bioinfo.preop_psa,
    patients_bioinfo.postrp_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.psadt,
    patients_bioinfo.psadt_rp_rt,
    patients_bioinfo.srt_nadir_psa,
    patients_bioinfo.clings,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.pstage,
    patients_bioinfo.sm,
    patients_bioinfo.ece,
    patients_bioinfo.svi,
    patients_bioinfo.lni,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.pelvis,
    patients_bioinfo.adt,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.rt,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.rt_start,
    patients_bioinfo.rt_end,
    patients_bioinfo.rt_type,
    patients_bioinfo.rt_dose,
    patients_bioinfo.bcr,
    patients_bioinfo.bcr_fu_date,
    patients_bioinfo.bcr_time,
    patients_bioinfo.rt_to_bfailure_time,
    patients_bioinfo.met,
    patients_bioinfo.met_fu_date,
    patients_bioinfo.met_time,
    patients_bioinfo.rt_to_met_time,
    patients_bioinfo.os,
    patients_bioinfo.os_fu_date,
    patients_bioinfo.os_time,
    patients_bioinfo.rt_to_os_time,
    patients_bioinfo.capra_s,
    patients_bioinfo.stephenson_2yr,
    patients_bioinfo.stephenson_5yr,
    patients_bioinfo.stephenson_7yr,
    patients_bioinfo.stephenson_rev,
    patients_bioinfo.stephenson_srt,
    patients_bioinfo.stephenson_srt_rev,
    patients_bioinfo.stephenson_10yr,
    samples_bioinfo.sample_id,
    rna.rna_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_batch,
    rna.rna_260_280,
    cdna.cdna_concentration,
    cdna.cdna_batch,
    cdna.cdna_260_280,
    celfiles.qc_batch_id,
    celfiles.percentpresent,
    celfiles.rf22_percentpresent,
    celfiles.posneg_auc,
    celfiles.qc_flag,
    predictions.celfile_name,
    predictions.decipher,
    predictions.penney2011frma,
    predictions.ntv3,
    predictions.ntv2,
    predictions.bcr_knn56,
    predictions.bcr_rf13,
    predictions.psadt_rf13,
    predictions.upgrading_knn16,
    predictions.knn392frma,
    predictions.knn104frma,
    predictions.rf72frma,
    predictions.spink1,
    predictions.etv1,
    predictions.etv4,
    predictions.etv5,
    predictions.fli1,
    predictions.pten,
    predictions.erg,
    predictions.rf22,
    predictions.rf22scan,
    predictions.rf22s,
    predictions.ccp,
    predictions.knn392scan,
    predictions.knn104scan,
    predictions.rf72scan,
    predictions.penney2011scan,
    predictions.ntv4,
    predictions.sawyers,
    predictions.rsi,
    predictions.gps_rf,
    predictions.old,
    predictions.rf15frma,
    predictions.knn51lni,
    predictions.ccp_rf,
    predictions.penney2011_rf,
    classifications.etv1 AS etv1_classification,
    classifications.etv4 AS etv4_classification,
    classifications.etv5 AS etv5_classification,
    classifications.erg AS erg_classification,
    classifications.spink1 AS spink1_classification,
    classifications.fli1 AS fli1_classification
   FROM (((((((studies
     JOIN patients_bioinfo USING (study_name))
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
     LEFT JOIN classifications USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_robert_den IS TRUE))))));


ALTER TABLE view_robert_den OWNER TO administrator;

--
-- Name: view_roland; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_roland AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.clings,
    patients_bioinfo.clings_p,
    patients_bioinfo.clings_s,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.clings_p_percent,
    patients_bioinfo.clings_s_percent,
    patients_bioinfo.pathgs_p_percent,
    patients_bioinfo.pathgs_s_percent,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.external_id,
    patients_bioinfo.created,
    patients_bioinfo.modified,
    patients_bioinfo.created_by,
    patients_bioinfo.modified_by,
    patients_bioinfo.age_rt,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.comments,
    patients_bioinfo.dob,
    patients_bioinfo.ece,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.met,
    patients_bioinfo.met_fu_date,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.pathid,
    patients_bioinfo.pelvis,
    patients_bioinfo.postrp_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.psadt,
    patients_bioinfo.psadt_rp_rt,
    patients_bioinfo.rp_date,
    patients_bioinfo.rp_to_bcr_time,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_end,
    patients_bioinfo.rt_start,
    patients_bioinfo.original_bcr_time,
    patients_bioinfo.original_met_time,
    patients_bioinfo.original_os_time,
    patients_bioinfo.rt_type,
    patients_bioinfo.srt_nadir_psa,
    patients_bioinfo.stephenson_2yr,
    patients_bioinfo.stephenson_5yr,
    patients_bioinfo.stephenson_7yr,
    patients_bioinfo.stephenson_rev,
    patients_bioinfo.stephenson_srt,
    patients_bioinfo.stephenson_srt_rev,
    patients_bioinfo.surgery_id,
    patients_bioinfo.stephenson_10yr,
    patients_bioinfo.os_fu_date,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.psa1,
    patients_bioinfo.psa2,
    patients_bioinfo.psa1_time,
    patients_bioinfo.psa2_time,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.lcr,
    patients_bioinfo.lcr_time,
    patients_bioinfo.adjuvant_adt_flag,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.adjuvant_rt_flag,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.adt_time,
    patients_bioinfo.rt_time,
    patients_bioinfo.cc,
    patients_bioinfo.ploidy,
    patients_bioinfo.cure1,
    patients_bioinfo.cure2,
    patients_bioinfo.kattan_5yr,
    patients_bioinfo.swog_group,
    patients_bioinfo.gpsm,
    patients_bioinfo.bcr_time,
    patients_bioinfo.nodes_pos,
    patients_bioinfo.rp_type,
    patients_bioinfo.chemo,
    patients_bioinfo.adt_type,
    patients_bioinfo.predx_psa,
    patients_bioinfo.pretx_psa,
    patients_bioinfo.neoadj_rt,
    patients_bioinfo.ece_type,
    patients_bioinfo.copy_number_cluster,
    patients_bioinfo.nodes_rmd,
    patients_bioinfo.lvi,
    patients_bioinfo.adjctx,
    patients_bioinfo.gender,
    patients_bioinfo.iv_rx,
    patients_bioinfo.rec,
    patients_bioinfo.rec_time,
    patients_bioinfo.ibcnc,
    patients_bioinfo.bcr_fu_date,
    patients_bioinfo.family_history,
    patients_bioinfo.bfailure,
    patients_bioinfo.bfailure_time,
    patients_bioinfo.death_cause,
    patients_bioinfo.original_pathgs,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.original_pathgs_p,
    patients_bioinfo.original_pathgs_s,
    patients_bioinfo.user_role,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.original_clings,
    patients_bioinfo.original_clings_p,
    patients_bioinfo.original_clings_s,
    patients_bioinfo.neoplastic_cell_count,
    patients_bioinfo.match,
    patients_bioinfo.tumor_weight,
    patients_bioinfo.pten_ihc,
    patients_bioinfo.pten_fish,
    patients_bioinfo.myc_inc,
    patients_bioinfo.lpl_dec,
    patients_bioinfo.bmi,
    patients_bioinfo.bx_date,
    patients_bioinfo.cfailure,
    patients_bioinfo.dominant_anterior_nodule,
    patients_bioinfo.original_met,
    patients_bioinfo.original_os,
    patients_bioinfo.original_bcr,
    patients_bioinfo.original_pcsm,
    patients_bioinfo.original_pcsm_time,
    patients_bioinfo.patient_note,
    patients_bioinfo.source,
    patients_bioinfo.catalog_no,
    patients_bioinfo.xrt,
    patients_bioinfo.cnode,
    patients_bioinfo.cpos_nodes,
    patients_bioinfo.cmet,
    patients_bioinfo.cmet_location,
    patients_bioinfo.postchemo_stage,
    patients_bioinfo.postchemo_node,
    patients_bioinfo.postchemo_met,
    patients_bioinfo.ene,
    patients_bioinfo.aicc,
    patients_bioinfo.trg_pooled,
    patients_bioinfo.trg_pt,
    patients_bioinfo.trg_met,
    patients_bioinfo.neoadj_ctx,
    patients_bioinfo.neoadj_ctx_type,
    patients_bioinfo.neoadj_ctx_cycle,
    patients_bioinfo.neoadj_ctx_secondary,
    patients_bioinfo.neoadj_ctx_secondary_type,
    patients_bioinfo.neoadj_ctx_secondary_cycle,
    patients_bioinfo.csm,
    patients_bioinfo.prechemo_met,
    patients_bioinfo.lni_level,
    patients_bioinfo.chemo_response,
    patients_bioinfo.post_rp_patients,
    patients_bioinfo.post_bcr_patients,
    patients_bioinfo.idc,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.original_pstage,
    patients_bioinfo.rp_year,
    patients_bioinfo.bx_pos_cores,
    patients_bioinfo.bx_total_cores,
    patients_bioinfo.bx_max_percent,
    patients_bioinfo.original_sm,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    patients_bioinfo.chemo_s,
    patients_bioinfo.chemo_time,
    patients_bioinfo.original_epe,
    patients_bioinfo.egg15,
    patients_bioinfo.previous_treatment,
    patients_bioinfo.dod,
    patients_bioinfo.pni,
    patients_bioinfo.family_history_type,
    patients_bioinfo.raw_cstage,
    patients_bioinfo.met_site,
    patients_bioinfo.received_blocks,
    patients_bioinfo.rt_s_date,
    patients_bioinfo.adt_s_date,
    patients_bioinfo.crpc_date,
    patients_bioinfo.bfailure_fu_date,
    patients_bioinfo.sample_source,
    patients_bioinfo.rt_s_type,
    patients_bioinfo.received_batch,
    patients_bioinfo.crpc,
    patients_bioinfo.received_unstained_slides,
    patients_bioinfo.received_he_slides,
    patients_bioinfo.adt_length,
    patients_bioinfo.rt_to_met_time,
    patients_bioinfo.rt_to_bfailure_time,
    patients_bioinfo.rt_to_os_time,
    patients_bioinfo.ordering_physician,
    patients_bioinfo.practice_name,
    patients_bioinfo.practice_city,
    patients_bioinfo.practice_state,
    patients_bioinfo.practice_zipcode,
    patients_bioinfo.practice_country,
    patients_bioinfo.practice_province,
    patients_bioinfo.practice_postal_code,
    patients_bioinfo.birth_year,
    patients_bioinfo.age_bx,
    patients_bioinfo.bx_year,
    patients_bioinfo.bx_tumor_involvement,
    patients_bioinfo.physically_altered,
    patients_bioinfo.curative_turbt_treatment,
    patients_bioinfo.clin_grade,
    patients_bioinfo.path_grade,
    patients_bioinfo.site,
    patients_bioinfo.xsn_pathfeatures_notes,
    patients_bioinfo.xsn_specimen_notes,
    patients_bioinfo.pt2_gbx,
    patients_bioinfo.lni_gbx,
    patients_bioinfo.epe_gbx,
    patients_bioinfo.svi_gbx,
    patients_bioinfo.bcr_gbx,
    patients_bioinfo.sm_gbx,
    patients_bioinfo.bni,
    patients_bioinfo.other_highriskfeatures,
    patients_bioinfo.practice_type,
    patients_bioinfo.pstage_gbx,
    patients_bioinfo.preop_psa_groups,
    patients_bioinfo.age_groups,
    patients_bioinfo.bx_total_cores_left,
    patients_bioinfo.bx_total_cores_right,
    patients_bioinfo.bx_pos_cores_left,
    patients_bioinfo.bx_pos_cores_right,
    patients_bioinfo.outcome,
    patients_bioinfo.sm_notes,
    patients_bioinfo.surgery_s_notes,
    patients_bioinfo.adt_start,
    patients_bioinfo.adt_end,
    patients_bioinfo.rt_region,
    patients_bioinfo.tx_s_type,
    patients_bioinfo.surgery_s_date,
    patients_bioinfo.enzalutamide_date,
    patients_bioinfo.abiraterone_date,
    patients_bioinfo.tx_s_date,
    patients_bioinfo.intact_psa,
    patients_bioinfo.nadir,
    patients_bioinfo.surgery_s,
    patients_bioinfo.enzalutamide,
    patients_bioinfo.abiraterone,
    patients_bioinfo.tx_s,
    patients_bioinfo.pin,
    patients_bioinfo.crpc_time,
    patients_bioinfo.original_rt_to_met_time,
    patients_bioinfo.sfailure,
    patients_bioinfo.sfailure_time,
    patients_bioinfo.order_date,
    patients_bioinfo.rp_to_order_time,
    patients_bioinfo.cystectomy,
    patients_bioinfo.histology_primary,
    patients_bioinfo.histology_secondary,
    patients_bioinfo.diabetes,
    patients_bioinfo.heart,
    patients_bioinfo.other_cancers,
    patients_bioinfo.surgeon_code,
    patients_bioinfo.surgery_type,
    patients_bioinfo.nodes_pos_notes,
    patients_bioinfo.nodes_pos_max_size,
    patients_bioinfo.number_foci,
    patients_bioinfo.bx_max_length,
    patients_bioinfo.kps,
    patients_bioinfo.smoker,
    patients_bioinfo.alcohol,
    patients_bioinfo.cis,
    patients_bioinfo.hydronephrosis,
    patients_bioinfo.distant_failure_time,
    patients_bioinfo.distant_failure_response,
    patients_bioinfo.distant_failure,
    patients_bioinfo.local_failure,
    patients_bioinfo.local_failure_time,
    patients_bioinfo.local_failure_response,
    patients_bioinfo.regional_failure,
    patients_bioinfo.regional_failure_time,
    patients_bioinfo.regional_failure_response,
    patients_bioinfo.csm_time,
    patients_bioinfo.management,
    patients_bioinfo.grade,
    patients_bioinfo.turbt_complete,
    patients_bioinfo.postrt_biopsy,
    patients_bioinfo.postrt_cytology,
    patients_bioinfo.pgrade,
    patients_bioinfo.path_histology_primary,
    patients_bioinfo.initial_treatment_recommendation,
    patients_bioinfo.turbt_histology_primary,
    patients_bioinfo.tumor_location,
    patients_bioinfo.distant_failure_site,
    patients_bioinfo.distant_failure_tx,
    patients_bioinfo.local_failure_type,
    patients_bioinfo.local_failure_tx,
    patients_bioinfo.local_failure_grade,
    patients_bioinfo.regional_failure_site,
    patients_bioinfo.regional_failure_tx,
    patients_bioinfo.chemo_type,
    patients_bioinfo.capra,
    patients_bioinfo.bx_to_rp_months,
    patients_bioinfo.bx_pos_length,
    patients_bioinfo.bx_total_length_taken,
    patients_bioinfo.turbt_date,
    patients_bioinfo.rc_date,
    patients_bioinfo.path_histology_secondary,
    patients_bioinfo.bx_percent_pos_cores,
    patients_bioinfo.test_type,
    patients_bioinfo.bx_risk_group,
    patients_bioinfo.pathology_review_site,
    patients_bioinfo.preop_psa_time,
    patients_bioinfo.rt_end_time,
    patients_bioinfo.chemo_start,
    patients_bioinfo.chemo_end,
    patients_bioinfo.prert_psa_max,
    patients_bioinfo.rt_recommended,
    patients_bioinfo.adt_recommended,
    patients_bioinfo.psa3,
    patients_bioinfo.dre,
    patients_bioinfo.dre_notes,
    patients_bioinfo.ct_scan,
    patients_bioinfo.chest_x_ray,
    patients_bioinfo.bone_scan,
    patients_bioinfo.mri,
    patients_bioinfo.dx_to_rc_time,
    patients_bioinfo.chemo_a_type,
    patients_bioinfo.chemo_a_start,
    patients_bioinfo.chemo_a_end,
    cdna.cdna_id,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.ss_cdna_yield,
    cdna.ss_cdna_concentration,
    cdna.ss_cdna_260,
    cdna.ss_cdna_280,
    cdna.ss_cdna_260_280,
    cdna.ss_cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.cdna_note,
    celfiles.chip_id,
    celfiles.erg_gene,
    celfiles.etv1_gene,
    celfiles.etv4_gene,
    celfiles.qc_batch_id,
    celfiles.location,
    celfiles.qc_flag,
    celfiles.qc_flag_bladder,
    celfiles.rsi_scan,
    celfiles.rf22_scan,
    celfiles.gcc_scan,
    celfiles.gcs_scan,
    celfiles.rf22_scan_5yr,
    celfiles.rf22_prod_5yr,
    celfiles.rf22_scan_3yrbcr,
    celfiles.rf22_prod_3yrbcr,
    celfiles.percentpresent,
    celfiles.rf22_percentpresent,
    celfiles.rf22_prod,
    celfiles.posneg_auc,
    celfiles.hybcontrol,
    celfiles.hybpercentpresent,
    celfiles.celfile_name,
    predictions.knn51lni,
    predictions.rf15frma,
    rna.rna_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_note,
    samples_bioinfo.storage,
    samples_bioinfo.sample_type,
    samples_bioinfo.tissue_attribute,
    samples_bioinfo.tissue_type,
    samples_bioinfo.blockage,
    samples_bioinfo.core_id,
    samples_bioinfo.date_processed,
    samples_bioinfo.date_shipped,
    samples_bioinfo.date_received,
    samples_bioinfo.tissue_gleason,
    samples_bioinfo.tissue_gleason_s,
    samples_bioinfo.gdx_id,
    samples_bioinfo.core_type,
    celgrp2.jnci_grouping,
    celgrp.usc_grouping
   FROM (((((((patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
     LEFT JOIN ( SELECT celfile_groups_analysis.celfile_name,
            celfile_groups_analysis.celfile_group_name AS usc_grouping
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = ANY (ARRAY[('BladderRec-trn'::character varying)::text, ('BladderRec-tst'::character varying)::text]))) celgrp USING (celfile_name))
     LEFT JOIN ( SELECT celfile_groups_analysis.celfile_name,
            celfile_groups_analysis.celfile_group_name AS jnci_grouping
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text = ANY (ARRAY[('BladderRec-censored'::character varying)::text, ('Mitra2014-BladderRec-trn'::character varying)::text, ('Mitra2014-BladderRec-tst'::character varying)::text]))) celgrp2 USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_roland IS TRUE))))));


ALTER TABLE view_roland OWNER TO administrator;

--
-- Name: view_tamara_lotan; Type: VIEW; Schema: samplesdb; Owner: administrator
--

CREATE VIEW view_tamara_lotan AS
 SELECT patients_bioinfo.patient_id,
    patients_bioinfo.study_name,
    patients_bioinfo.clings,
    patients_bioinfo.clings_p,
    patients_bioinfo.clings_s,
    patients_bioinfo.pathgs,
    patients_bioinfo.pathgs_p,
    patients_bioinfo.pathgs_s,
    patients_bioinfo.clings_p_percent,
    patients_bioinfo.clings_s_percent,
    patients_bioinfo.pathgs_p_percent,
    patients_bioinfo.pathgs_s_percent,
    patients_bioinfo.age,
    patients_bioinfo.pstage,
    patients_bioinfo.preop_psa,
    patients_bioinfo.bcr,
    patients_bioinfo.adt,
    patients_bioinfo.rt,
    patients_bioinfo.external_id,
    patients_bioinfo.created,
    patients_bioinfo.modified,
    patients_bioinfo.created_by,
    patients_bioinfo.modified_by,
    patients_bioinfo.age_rt,
    patients_bioinfo.race,
    patients_bioinfo.capra_s,
    patients_bioinfo.comments,
    patients_bioinfo.dob,
    patients_bioinfo.ece,
    patients_bioinfo.fu_rp_time,
    patients_bioinfo.fu_time,
    patients_bioinfo.lastfu_date,
    patients_bioinfo.met,
    patients_bioinfo.met_fu_date,
    patients_bioinfo.met_time,
    patients_bioinfo.neoadj_adt,
    patients_bioinfo.os,
    patients_bioinfo.os_time,
    patients_bioinfo.pathid,
    patients_bioinfo.pelvis,
    patients_bioinfo.postrp_psa,
    patients_bioinfo.prert_psa,
    patients_bioinfo.psadt,
    patients_bioinfo.psadt_rp_rt,
    patients_bioinfo.rp_date,
    patients_bioinfo.rp_to_bcr_time,
    patients_bioinfo.rp_to_rt_months,
    patients_bioinfo.rt_dose,
    patients_bioinfo.rt_end,
    patients_bioinfo.rt_start,
    patients_bioinfo.original_bcr_time,
    patients_bioinfo.original_met_time,
    patients_bioinfo.original_os_time,
    patients_bioinfo.rt_type,
    patients_bioinfo.srt_nadir_psa,
    patients_bioinfo.stephenson_2yr,
    patients_bioinfo.stephenson_5yr,
    patients_bioinfo.stephenson_7yr,
    patients_bioinfo.stephenson_rev,
    patients_bioinfo.stephenson_srt,
    patients_bioinfo.stephenson_srt_rev,
    patients_bioinfo.surgery_id,
    patients_bioinfo.stephenson_10yr,
    patients_bioinfo.os_fu_date,
    patients_bioinfo.lni,
    patients_bioinfo.cstage,
    patients_bioinfo.tumor_vol,
    patients_bioinfo.mayo_def,
    patients_bioinfo.psa1,
    patients_bioinfo.psa2,
    patients_bioinfo.psa1_time,
    patients_bioinfo.psa2_time,
    patients_bioinfo.pcsm,
    patients_bioinfo.pcsm_time,
    patients_bioinfo.lcr,
    patients_bioinfo.lcr_time,
    patients_bioinfo.adjuvant_adt_flag,
    patients_bioinfo.adt_a,
    patients_bioinfo.adt_s,
    patients_bioinfo.adjuvant_rt_flag,
    patients_bioinfo.rt_a,
    patients_bioinfo.rt_s,
    patients_bioinfo.adt_time,
    patients_bioinfo.rt_time,
    patients_bioinfo.cc,
    patients_bioinfo.ploidy,
    patients_bioinfo.cure1,
    patients_bioinfo.cure2,
    patients_bioinfo.kattan_5yr,
    patients_bioinfo.swog_group,
    patients_bioinfo.gpsm,
    patients_bioinfo.bcr_time,
    patients_bioinfo.nodes_pos,
    patients_bioinfo.rp_type,
    patients_bioinfo.chemo,
    patients_bioinfo.adt_type,
    patients_bioinfo.predx_psa,
    patients_bioinfo.pretx_psa,
    patients_bioinfo.neoadj_rt,
    patients_bioinfo.ece_type,
    patients_bioinfo.copy_number_cluster,
    patients_bioinfo.nodes_rmd,
    patients_bioinfo.lvi,
    patients_bioinfo.adjctx,
    patients_bioinfo.gender,
    patients_bioinfo.iv_rx,
    patients_bioinfo.rec,
    patients_bioinfo.rec_time,
    patients_bioinfo.ibcnc,
    patients_bioinfo.bcr_fu_date,
    patients_bioinfo.family_history,
    patients_bioinfo.bfailure,
    patients_bioinfo.bfailure_time,
    patients_bioinfo.death_cause,
    patients_bioinfo.original_pathgs,
    patients_bioinfo.pathgs_t,
    patients_bioinfo.original_pathgs_p,
    patients_bioinfo.original_pathgs_s,
    patients_bioinfo.user_role,
    patients_bioinfo.svi,
    patients_bioinfo.sm,
    patients_bioinfo.original_clings,
    patients_bioinfo.original_clings_p,
    patients_bioinfo.original_clings_s,
    patients_bioinfo.neoplastic_cell_count,
    patients_bioinfo.match,
    patients_bioinfo.tumor_weight,
    patients_bioinfo.pten_ihc,
    patients_bioinfo.pten_fish,
    patients_bioinfo.myc_inc,
    patients_bioinfo.lpl_dec,
    patients_bioinfo.bmi,
    patients_bioinfo.bx_date,
    patients_bioinfo.cfailure,
    patients_bioinfo.dominant_anterior_nodule,
    patients_bioinfo.original_met,
    patients_bioinfo.original_os,
    patients_bioinfo.original_bcr,
    patients_bioinfo.original_pcsm,
    patients_bioinfo.original_pcsm_time,
    patients_bioinfo.patient_note,
    patients_bioinfo.source,
    patients_bioinfo.catalog_no,
    patients_bioinfo.xrt,
    patients_bioinfo.cnode,
    patients_bioinfo.cpos_nodes,
    patients_bioinfo.cmet,
    patients_bioinfo.cmet_location,
    patients_bioinfo.postchemo_stage,
    patients_bioinfo.postchemo_node,
    patients_bioinfo.postchemo_met,
    patients_bioinfo.ene,
    patients_bioinfo.aicc,
    patients_bioinfo.trg_pooled,
    patients_bioinfo.trg_pt,
    patients_bioinfo.trg_met,
    patients_bioinfo.neoadj_ctx,
    patients_bioinfo.neoadj_ctx_type,
    patients_bioinfo.neoadj_ctx_cycle,
    patients_bioinfo.neoadj_ctx_secondary,
    patients_bioinfo.neoadj_ctx_secondary_type,
    patients_bioinfo.neoadj_ctx_secondary_cycle,
    patients_bioinfo.csm,
    patients_bioinfo.prechemo_met,
    patients_bioinfo.lni_level,
    patients_bioinfo.chemo_response,
    patients_bioinfo.post_rp_patients,
    patients_bioinfo.post_bcr_patients,
    patients_bioinfo.idc,
    patients_bioinfo.post_rp_patients_cchdef,
    patients_bioinfo.post_bcr_patients_cchdef,
    patients_bioinfo.original_pstage,
    patients_bioinfo.rp_year,
    patients_bioinfo.bx_pos_cores,
    patients_bioinfo.bx_total_cores,
    patients_bioinfo.bx_max_percent,
    patients_bioinfo.original_sm,
    patients_bioinfo.height,
    patients_bioinfo.weight,
    patients_bioinfo.chemo_s,
    patients_bioinfo.chemo_time,
    patients_bioinfo.original_epe,
    patients_bioinfo.egg15,
    patients_bioinfo.previous_treatment,
    patients_bioinfo.dod,
    patients_bioinfo.pni,
    patients_bioinfo.family_history_type,
    patients_bioinfo.raw_cstage,
    patients_bioinfo.met_site,
    patients_bioinfo.received_blocks,
    patients_bioinfo.rt_s_date,
    patients_bioinfo.adt_s_date,
    patients_bioinfo.crpc_date,
    patients_bioinfo.bfailure_fu_date,
    patients_bioinfo.sample_source,
    patients_bioinfo.rt_s_type,
    patients_bioinfo.received_batch,
    patients_bioinfo.crpc,
    patients_bioinfo.received_unstained_slides,
    patients_bioinfo.received_he_slides,
    patients_bioinfo.adt_length,
    cdna.cdna_id,
    cdna.cdna_concentration,
    cdna.cdna_yield,
    cdna.cdna_260,
    cdna.cdna_280,
    cdna.cdna_260_280,
    cdna.cdna_260_230,
    cdna.ss_cdna_yield,
    cdna.ss_cdna_concentration,
    cdna.ss_cdna_260,
    cdna.ss_cdna_280,
    cdna.ss_cdna_260_280,
    cdna.ss_cdna_260_230,
    cdna.cdna_batch,
    cdna.cdna_date,
    cdna.cdna_note,
    celfiles.chip_id,
    celfiles.erg_gene,
    celfiles.etv1_gene,
    celfiles.etv4_gene,
    celfiles.qc_batch_id,
    celfiles.location,
    celfiles.qc_flag,
    celfiles.rsi_scan,
    celfiles.percentpresent,
    celfiles.posneg_auc,
    celfiles.hybcontrol,
    celfiles.hybpercentpresent,
    predictions.celfile_name,
    predictions.ntv3,
    predictions.ntv2,
    predictions.spink1,
    predictions.etv1,
    predictions.etv4,
    predictions.etv5,
    predictions.fli1,
    predictions.pten,
    predictions.erg,
    predictions.ntv4,
    classifications.molecular_subtype,
    rna.rna_id,
    rna.rna_concentration,
    rna.rna_yield,
    rna.rna_260,
    rna.rna_280,
    rna.rna_260_280,
    rna.rna_260_230,
    rna.rna_date,
    rna.rna_batch,
    rna.rna_note,
    samples_bioinfo.storage,
    samples_bioinfo.sample_type,
    samples_bioinfo.tissue_type,
    samples_bioinfo.blockage,
    samples_bioinfo.core_id,
    samples_bioinfo.date_processed,
    samples_bioinfo.date_shipped,
    samples_bioinfo.date_received,
    samples_bioinfo.tissue_gleason,
    samples_bioinfo.tissue_gleason_s,
    samples_bioinfo.gdx_id,
    samples_bioinfo.core_type
   FROM ((((((patients_bioinfo
     RIGHT JOIN samples_bioinfo USING (patient_id))
     JOIN rna USING (sample_id))
     JOIN cdna USING (rna_id))
     RIGHT JOIN celfiles USING (cdna_id))
     LEFT JOIN predictions USING (celfile_name))
     JOIN classifications USING (celfile_name))
  WHERE (((samples_bioinfo.user_role)::text = 'bioinfo'::text) AND ((celfiles.celfile_name)::text IN ( SELECT celfile_groups_analysis.celfile_name
           FROM celfile_groups_analysis
          WHERE ((celfile_groups_analysis.celfile_group_name)::text IN ( SELECT permissions_external.celfile_group_name
                   FROM permissions_external
                  WHERE (permissions_external.view_tamara_lotan IS TRUE))))));


ALTER TABLE view_tamara_lotan OWNER TO administrator;

--
-- Name: vs_database_diagrams; Type: TABLE; Schema: samplesdb; Owner: administrator
--

CREATE TABLE vs_database_diagrams (
    name character varying(80),
    diadata text,
    comment character varying(1022),
    preview text,
    lockinfo character varying(80),
    locktime timestamp with time zone,
    version character varying(80)
);


ALTER TABLE vs_database_diagrams OWNER TO administrator;

--
-- Name: CDNA_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY cdna
    ADD CONSTRAINT "CDNA_pkey" PRIMARY KEY (cdna_id);


--
-- Name: CELFILES_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY celfiles
    ADD CONSTRAINT "CELFILES_pkey" PRIMARY KEY (celfile_name);


--
-- Name: CELFILE_GROUPS_ANALYSIS_KEY_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY celfile_groups_analysis_key
    ADD CONSTRAINT "CELFILE_GROUPS_ANALYSIS_KEY_pkey" PRIMARY KEY (celfile_group_name);


--
-- Name: CELFILE_GROUPS_ANALYSIS_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY celfile_groups_analysis
    ADD CONSTRAINT "CELFILE_GROUPS_ANALYSIS_pkey" PRIMARY KEY (celfile_group_name, celfile_name);


--
-- Name: CELFILE_GROUPS_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY celfile_groups
    ADD CONSTRAINT "CELFILE_GROUPS_pkey" PRIMARY KEY (celfile_group_name, celfile_name);


--
-- Name: PATIENTS_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY patients
    ADD CONSTRAINT "PATIENTS_pkey" PRIMARY KEY (patient_id);


--
-- Name: RNAFRAG_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY rnafrag
    ADD CONSTRAINT "RNAFRAG_pkey" PRIMARY KEY (rnafrag_batch, rnafrag_range, rnafrag_well);


--
-- Name: RNA_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY rna
    ADD CONSTRAINT "RNA_pkey" PRIMARY KEY (rna_id);


--
-- Name: SAMPLES_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY samples
    ADD CONSTRAINT "SAMPLES_pkey" PRIMARY KEY (sample_id);


--
-- Name: STUDIES_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY studies
    ADD CONSTRAINT "STUDIES_pkey" PRIMARY KEY (study_name);


--
-- Name: celfile_group_name_permissions_external_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: predictions_table_owner
--

ALTER TABLE ONLY permissions_external
    ADD CONSTRAINT celfile_group_name_permissions_external_pkey PRIMARY KEY (celfile_group_name);


--
-- Name: chla_batch_tracker_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY chla_batch_tracker
    ADD CONSTRAINT chla_batch_tracker_pkey PRIMARY KEY (cdna_batch);


--
-- Name: clasifications_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: predictions_table_owner
--

ALTER TABLE ONLY classifications
    ADD CONSTRAINT clasifications_pkey PRIMARY KEY (celfile_name);


--
-- Name: geo_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY geo
    ADD CONSTRAINT geo_pkey PRIMARY KEY (geo_id, celfile_name);


--
-- Name: gridlabrequest_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY grid_lab_request
    ADD CONSTRAINT gridlabrequest_pkey PRIMARY KEY (celfile_name);


--
-- Name: import_tracker_box_location_key; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY import_tracker
    ADD CONSTRAINT import_tracker_box_location_key UNIQUE (box_location);


--
-- Name: import_tracker_source_file_key; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY import_tracker
    ADD CONSTRAINT import_tracker_source_file_key UNIQUE (source_file);


--
-- Name: pkey_primary_breast_patient; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY patients_breast
    ADD CONSTRAINT pkey_primary_breast_patient PRIMARY KEY (patient_id);


--
-- Name: predictions2_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: predictions_table_owner
--

ALTER TABLE ONLY predictions2
    ADD CONSTRAINT predictions2_pkey PRIMARY KEY (celfile_name);


--
-- Name: predictions_blinded_pkey_celfile; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY predictions_blinded
    ADD CONSTRAINT predictions_blinded_pkey_celfile PRIMARY KEY (celfile_name);


--
-- Name: proimpact_patients_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY proimpact_patients
    ADD CONSTRAINT proimpact_patients_pkey PRIMARY KEY (case_id);


--
-- Name: proimpact_qol_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY proimpact_qol
    ADD CONSTRAINT proimpact_qol_pkey PRIMARY KEY (case_id, visit);


--
-- Name: proimpact_sdq_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY proimpact_sdq
    ADD CONSTRAINT proimpact_sdq_pkey PRIMARY KEY (visit, case_id);


--
-- Name: proimpact_sites_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY proimpact_sites
    ADD CONSTRAINT proimpact_sites_pkey PRIMARY KEY (site_id);


--
-- Name: rna_batch_pkey; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY rna_batch
    ADD CONSTRAINT rna_batch_pkey PRIMARY KEY (rna_batch);


--
-- Name: sample_id; Type: CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY samples_breast
    ADD CONSTRAINT sample_id PRIMARY KEY (sample_id);


--
-- Name: celfile_soft_fk; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER celfile_soft_fk BEFORE INSERT OR UPDATE ON celfiles FOR EACH ROW EXECUTE PROCEDURE public.study_cdna_exist();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON cdna FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON celfile_groups FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON celfiles FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON patients FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON rna FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON samples FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON studies FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON backups FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON celfile_groups_analysis FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON rnafrag FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON grid_lab_request FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER created_trigger BEFORE INSERT ON psa FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON cdna FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON celfile_groups FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON celfiles FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON patients FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON rna FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON samples FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON studies FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON celfile_groups_analysis FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON rnafrag FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON grid_lab_request FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON psa FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: samples_soft_fk; Type: TRIGGER; Schema: samplesdb; Owner: samplesdb_table_owner
--

CREATE TRIGGER samples_soft_fk BEFORE INSERT OR UPDATE ON samples FOR EACH ROW EXECUTE PROCEDURE public.patient_id_exist();


--
-- Name: CDNA_RNA_ID_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY cdna
    ADD CONSTRAINT "CDNA_RNA_ID_fkey" FOREIGN KEY (rna_id) REFERENCES rna(rna_id);


--
-- Name: CDNA_STUDY_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY cdna
    ADD CONSTRAINT "CDNA_STUDY_NAME_fkey" FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: CELFILE_GROUPS_ANALYSIS_CELFILE_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY celfile_groups_analysis
    ADD CONSTRAINT "CELFILE_GROUPS_ANALYSIS_CELFILE_NAME_fkey" FOREIGN KEY (celfile_name) REFERENCES celfiles(celfile_name);


--
-- Name: CELFILE_GROUPS_CELFILE_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY celfile_groups
    ADD CONSTRAINT "CELFILE_GROUPS_CELFILE_NAME_fkey" FOREIGN KEY (celfile_name) REFERENCES celfiles(celfile_name);


--
-- Name: PATIENTS_STUDY_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY patients
    ADD CONSTRAINT "PATIENTS_STUDY_NAME_fkey" FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: PSA_STUDY_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY psa
    ADD CONSTRAINT "PSA_STUDY_NAME_fkey" FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: RNA_SAMPLE_ID_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY rna
    ADD CONSTRAINT "RNA_SAMPLE_ID_fkey" FOREIGN KEY (sample_id) REFERENCES samples(sample_id);


--
-- Name: RNA_STUDY_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY rna
    ADD CONSTRAINT "RNA_STUDY_NAME_fkey" FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: SAMPLES_STUDY_NAME_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY samples
    ADD CONSTRAINT "SAMPLES_STUDY_NAME_fkey" FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: geo_celfile_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY geo
    ADD CONSTRAINT geo_celfile_fkey FOREIGN KEY (celfile_name) REFERENCES celfiles(celfile_name);


--
-- Name: gridlabrequest_celfile_name_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY grid_lab_request
    ADD CONSTRAINT gridlabrequest_celfile_name_fkey FOREIGN KEY (celfile_name) REFERENCES commercial.deidentifier(grid_celfile_name);


--
-- Name: patients_breast_studies_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY patients_breast
    ADD CONSTRAINT patients_breast_studies_fkey FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: proimpact_patients_fkey_site; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY proimpact_patients
    ADD CONSTRAINT proimpact_patients_fkey_site FOREIGN KEY (site_id) REFERENCES proimpact_sites(site_id);


--
-- Name: proimpact_qol_fkey_siteid; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY proimpact_qol
    ADD CONSTRAINT proimpact_qol_fkey_siteid FOREIGN KEY (site_id) REFERENCES proimpact_sites(site_id);


--
-- Name: rnafrag_fkey_rnaid; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY rnafrag
    ADD CONSTRAINT rnafrag_fkey_rnaid FOREIGN KEY (rna_id) REFERENCES rna(rna_id);


--
-- Name: samples_breast_studies_fkey; Type: FK CONSTRAINT; Schema: samplesdb; Owner: samplesdb_table_owner
--

ALTER TABLE ONLY samples_breast
    ADD CONSTRAINT samples_breast_studies_fkey FOREIGN KEY (study_name) REFERENCES studies(study_name);


--
-- Name: samplesdb; Type: ACL; Schema: -; Owner: administrator
--

REVOKE ALL ON SCHEMA samplesdb FROM PUBLIC;
REVOKE ALL ON SCHEMA samplesdb FROM administrator;
GRANT ALL ON SCHEMA samplesdb TO administrator;
GRANT USAGE ON SCHEMA samplesdb TO biostats;
GRANT USAGE ON SCHEMA samplesdb TO bioinfo;
GRANT ALL ON SCHEMA samplesdb TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA samplesdb TO readonly;
GRANT USAGE ON SCHEMA samplesdb TO clinops;
GRANT USAGE ON SCHEMA samplesdb TO view_external;
GRANT USAGE ON SCHEMA samplesdb TO engineering;
GRANT ALL ON SCHEMA samplesdb TO external_create;
GRANT ALL ON SCHEMA samplesdb TO commercial_table_owner;
GRANT ALL ON SCHEMA samplesdb TO grid_admin;


--
-- Name: backups; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE backups FROM PUBLIC;
REVOKE ALL ON TABLE backups FROM samplesdb_table_owner;
GRANT ALL ON TABLE backups TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE backups TO biostats;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE backups TO bioinfo;
GRANT SELECT ON TABLE backups TO readonly;
GRANT SELECT ON TABLE backups TO clinops;


--
-- Name: cdna; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE cdna FROM PUBLIC;
REVOKE ALL ON TABLE cdna FROM samplesdb_table_owner;
GRANT ALL ON TABLE cdna TO samplesdb_table_owner;
GRANT SELECT ON TABLE cdna TO readonly;
GRANT SELECT ON TABLE cdna TO clinops;
GRANT SELECT ON TABLE cdna TO bioinfo_readonly;
GRANT SELECT ON TABLE cdna TO biostats_readonly;
GRANT SELECT,REFERENCES ON TABLE cdna TO biostats;
GRANT SELECT,REFERENCES ON TABLE cdna TO bioinfo;
GRANT ALL ON TABLE cdna TO rds_superuser;


--
-- Name: celfiles; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE celfiles FROM PUBLIC;
REVOKE ALL ON TABLE celfiles FROM samplesdb_table_owner;
GRANT ALL ON TABLE celfiles TO samplesdb_table_owner;
GRANT SELECT ON TABLE celfiles TO readonly;
GRANT SELECT ON TABLE celfiles TO clinops;
GRANT SELECT ON TABLE celfiles TO bioinfo_readonly;
GRANT SELECT ON TABLE celfiles TO biostats_readonly;
GRANT SELECT,REFERENCES ON TABLE celfiles TO biostats;
GRANT SELECT,REFERENCES ON TABLE celfiles TO bioinfo;
GRANT SELECT ON TABLE celfiles TO grid_admin;
GRANT ALL ON TABLE celfiles TO rds_superuser;


--
-- Name: patients; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE patients FROM PUBLIC;
REVOKE ALL ON TABLE patients FROM samplesdb_table_owner;
GRANT ALL ON TABLE patients TO samplesdb_table_owner;
GRANT SELECT ON TABLE patients TO clinops;
GRANT ALL ON TABLE patients TO rds_superuser;


--
-- Name: rna; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE rna FROM PUBLIC;
REVOKE ALL ON TABLE rna FROM samplesdb_table_owner;
GRANT ALL ON TABLE rna TO samplesdb_table_owner;
GRANT SELECT ON TABLE rna TO readonly;
GRANT SELECT ON TABLE rna TO clinops;
GRANT SELECT ON TABLE rna TO bioinfo_readonly;
GRANT SELECT ON TABLE rna TO biostats_readonly;
GRANT SELECT,REFERENCES ON TABLE rna TO biostats;
GRANT SELECT,REFERENCES ON TABLE rna TO bioinfo;
GRANT ALL ON TABLE rna TO rds_superuser;


--
-- Name: samples; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE samples FROM PUBLIC;
REVOKE ALL ON TABLE samples FROM samplesdb_table_owner;
GRANT ALL ON TABLE samples TO samplesdb_table_owner;
GRANT SELECT ON TABLE samples TO clinops;
GRANT ALL ON TABLE samples TO rds_superuser;


--
-- Name: studies; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE studies FROM PUBLIC;
REVOKE ALL ON TABLE studies FROM samplesdb_table_owner;
GRANT ALL ON TABLE studies TO samplesdb_table_owner;
GRANT SELECT ON TABLE studies TO readonly;
GRANT SELECT ON TABLE studies TO clinops;
GRANT SELECT ON TABLE studies TO bioinfo_readonly;
GRANT SELECT ON TABLE studies TO biostats_readonly;
GRANT SELECT,REFERENCES ON TABLE studies TO biostats;
GRANT SELECT,REFERENCES ON TABLE studies TO bioinfo;


--
-- Name: celfile_groups; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE celfile_groups FROM PUBLIC;
REVOKE ALL ON TABLE celfile_groups FROM samplesdb_table_owner;
GRANT ALL ON TABLE celfile_groups TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE celfile_groups TO biostats;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE celfile_groups TO bioinfo;
GRANT SELECT ON TABLE celfile_groups TO readonly;
GRANT SELECT ON TABLE celfile_groups TO clinops;
GRANT SELECT ON TABLE celfile_groups TO bioinfo_readonly;
GRANT SELECT ON TABLE celfile_groups TO biostats_readonly;


--
-- Name: celfile_groups_analysis; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE celfile_groups_analysis FROM PUBLIC;
REVOKE ALL ON TABLE celfile_groups_analysis FROM samplesdb_table_owner;
GRANT ALL ON TABLE celfile_groups_analysis TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE celfile_groups_analysis TO lucia;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE celfile_groups_analysis TO biostats;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE celfile_groups_analysis TO bioinfo;
GRANT SELECT ON TABLE celfile_groups_analysis TO readonly;
GRANT SELECT ON TABLE celfile_groups_analysis TO clinops;
GRANT SELECT ON TABLE celfile_groups_analysis TO bioinfo_readonly;
GRANT SELECT ON TABLE celfile_groups_analysis TO biostats_readonly;


--
-- Name: celfile_groups_analysis_key; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE celfile_groups_analysis_key FROM PUBLIC;
REVOKE ALL ON TABLE celfile_groups_analysis_key FROM samplesdb_table_owner;
GRANT ALL ON TABLE celfile_groups_analysis_key TO samplesdb_table_owner;
GRANT SELECT ON TABLE celfile_groups_analysis_key TO bioinfo;
GRANT SELECT ON TABLE celfile_groups_analysis_key TO biostats;
GRANT SELECT ON TABLE celfile_groups_analysis_key TO clinops;


--
-- Name: chla_batch_tracker; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE chla_batch_tracker FROM PUBLIC;
REVOKE ALL ON TABLE chla_batch_tracker FROM samplesdb_table_owner;
GRANT ALL ON TABLE chla_batch_tracker TO samplesdb_table_owner;
GRANT ALL ON TABLE chla_batch_tracker TO clinops;


--
-- Name: classifications; Type: ACL; Schema: samplesdb; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE classifications FROM PUBLIC;
REVOKE ALL ON TABLE classifications FROM predictions_table_owner;
GRANT ALL ON TABLE classifications TO predictions_table_owner;
GRANT SELECT ON TABLE classifications TO bioinfo;
GRANT SELECT ON TABLE classifications TO readonly;
GRANT SELECT ON TABLE classifications TO samplesdb_table_owner;


--
-- Name: commercial; Type: ACL; Schema: samplesdb; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE commercial FROM PUBLIC;
REVOKE ALL ON TABLE commercial FROM commercial_table_owner;
GRANT ALL ON TABLE commercial TO commercial_table_owner;
GRANT SELECT ON TABLE commercial TO bioinfo;
GRANT SELECT ON TABLE commercial TO biostats;
GRANT SELECT ON TABLE commercial TO clinops;
GRANT SELECT ON TABLE commercial TO administrator;


--
-- Name: geo; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE geo FROM PUBLIC;
REVOKE ALL ON TABLE geo FROM samplesdb_table_owner;
GRANT ALL ON TABLE geo TO samplesdb_table_owner;
GRANT SELECT,INSERT ON TABLE geo TO readonly;
GRANT SELECT,INSERT ON TABLE geo TO clinops;
GRANT SELECT,INSERT ON TABLE geo TO bioinfo_readonly;
GRANT SELECT,INSERT ON TABLE geo TO biostats_readonly;
GRANT SELECT,INSERT,REFERENCES ON TABLE geo TO biostats;
GRANT SELECT,INSERT,REFERENCES ON TABLE geo TO bioinfo;


--
-- Name: grid_lab_request; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE grid_lab_request FROM PUBLIC;
REVOKE ALL ON TABLE grid_lab_request FROM samplesdb_table_owner;
GRANT ALL ON TABLE grid_lab_request TO samplesdb_table_owner;
GRANT SELECT ON TABLE grid_lab_request TO readonly;
GRANT SELECT ON TABLE grid_lab_request TO clinops;
GRANT SELECT ON TABLE grid_lab_request TO bioinfo_readonly;
GRANT SELECT ON TABLE grid_lab_request TO biostats_readonly;
GRANT SELECT ON TABLE grid_lab_request TO biostats;
GRANT SELECT ON TABLE grid_lab_request TO bioinfo;


--
-- Name: grid_mapping_bioinfo; Type: ACL; Schema: samplesdb; Owner: commercial_table_owner
--

REVOKE ALL ON TABLE grid_mapping_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE grid_mapping_bioinfo FROM commercial_table_owner;
GRANT ALL ON TABLE grid_mapping_bioinfo TO commercial_table_owner;
GRANT SELECT ON TABLE grid_mapping_bioinfo TO mandeep;
GRANT SELECT ON TABLE grid_mapping_bioinfo TO nicholas;


--
-- Name: grid_patients; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE grid_patients FROM PUBLIC;
REVOKE ALL ON TABLE grid_patients FROM administrator;
GRANT ALL ON TABLE grid_patients TO administrator;
GRANT SELECT ON TABLE grid_patients TO bioinfo;
GRANT SELECT ON TABLE grid_patients TO biostats;
GRANT SELECT ON TABLE grid_patients TO clinops;


--
-- Name: import_tracker; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE import_tracker FROM PUBLIC;
REVOKE ALL ON TABLE import_tracker FROM samplesdb_table_owner;
GRANT ALL ON TABLE import_tracker TO samplesdb_table_owner;
GRANT ALL ON TABLE import_tracker TO clinops;


--
-- Name: path_info; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE path_info FROM PUBLIC;
REVOKE ALL ON TABLE path_info FROM samplesdb_table_owner;
GRANT ALL ON TABLE path_info TO samplesdb_table_owner;
GRANT ALL ON TABLE path_info TO clinops;
GRANT SELECT ON TABLE path_info TO biostats;


--
-- Name: patients_bioinfo; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE patients_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE patients_bioinfo FROM samplesdb_table_owner;
GRANT ALL ON TABLE patients_bioinfo TO samplesdb_table_owner;
GRANT SELECT ON TABLE patients_bioinfo TO bioinfo;
GRANT SELECT ON TABLE patients_bioinfo TO biostats;


--
-- Name: patients_biostats; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE patients_biostats FROM PUBLIC;
REVOKE ALL ON TABLE patients_biostats FROM samplesdb_table_owner;
GRANT ALL ON TABLE patients_biostats TO samplesdb_table_owner;
GRANT SELECT ON TABLE patients_biostats TO biostats;


--
-- Name: patients_breast; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE patients_breast FROM PUBLIC;
REVOKE ALL ON TABLE patients_breast FROM samplesdb_table_owner;
GRANT ALL ON TABLE patients_breast TO samplesdb_table_owner;
GRANT ALL ON TABLE patients_breast TO laura;
GRANT ALL ON TABLE patients_breast TO george;


--
-- Name: patients_jon; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE patients_jon FROM PUBLIC;
REVOKE ALL ON TABLE patients_jon FROM samplesdb_table_owner;
GRANT ALL ON TABLE patients_jon TO samplesdb_table_owner;
GRANT SELECT ON TABLE patients_jon TO jon;


--
-- Name: permissions_external; Type: ACL; Schema: samplesdb; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE permissions_external FROM PUBLIC;
REVOKE ALL ON TABLE permissions_external FROM predictions_table_owner;
GRANT ALL ON TABLE permissions_external TO predictions_table_owner;
GRANT SELECT ON TABLE permissions_external TO bioinfo;
GRANT SELECT ON TABLE permissions_external TO clinops;
GRANT SELECT ON TABLE permissions_external TO biostats;


--
-- Name: pg_all_foreign_keys; Type: ACL; Schema: samplesdb; Owner: postgres
--

REVOKE ALL ON TABLE pg_all_foreign_keys FROM PUBLIC;
REVOKE ALL ON TABLE pg_all_foreign_keys FROM postgres;
GRANT ALL ON TABLE pg_all_foreign_keys TO postgres;
GRANT SELECT ON TABLE pg_all_foreign_keys TO PUBLIC;


--
-- Name: predictions2; Type: ACL; Schema: samplesdb; Owner: predictions_table_owner
--

REVOKE ALL ON TABLE predictions2 FROM PUBLIC;
REVOKE ALL ON TABLE predictions2 FROM predictions_table_owner;
GRANT ALL ON TABLE predictions2 TO predictions_table_owner;
GRANT ALL ON TABLE predictions2 TO nicholas;
GRANT ALL ON TABLE predictions2 TO lucia;
GRANT SELECT ON TABLE predictions2 TO readonly;


--
-- Name: predictions; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE predictions FROM PUBLIC;
REVOKE ALL ON TABLE predictions FROM administrator;
GRANT ALL ON TABLE predictions TO administrator;
GRANT SELECT ON TABLE predictions TO bioinfo;
GRANT SELECT ON TABLE predictions TO biostats;
GRANT SELECT ON TABLE predictions TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions TO readonly;
GRANT SELECT ON TABLE predictions TO clinops;


--
-- Name: predictions_blinded; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE predictions_blinded FROM PUBLIC;
REVOKE ALL ON TABLE predictions_blinded FROM samplesdb_table_owner;
GRANT ALL ON TABLE predictions_blinded TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions_blinded TO clinops;
GRANT ALL ON TABLE predictions_blinded TO rds_superuser;


--
-- Name: predictions_blinded_bioinfo; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE predictions_blinded_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE predictions_blinded_bioinfo FROM samplesdb_table_owner;
GRANT ALL ON TABLE predictions_blinded_bioinfo TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions_blinded_bioinfo TO bioinfo;
GRANT SELECT ON TABLE predictions_blinded_bioinfo TO biostats;


--
-- Name: predictions_blinded_biostats; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE predictions_blinded_biostats FROM PUBLIC;
REVOKE ALL ON TABLE predictions_blinded_biostats FROM samplesdb_table_owner;
GRANT ALL ON TABLE predictions_blinded_biostats TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions_blinded_biostats TO biostats;


--
-- Name: predictions_blinded_kasra; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE predictions_blinded_kasra FROM PUBLIC;
REVOKE ALL ON TABLE predictions_blinded_kasra FROM samplesdb_table_owner;
GRANT ALL ON TABLE predictions_blinded_kasra TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions_blinded_kasra TO kasra;


--
-- Name: predictions_blinded_mandeep; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE predictions_blinded_mandeep FROM PUBLIC;
REVOKE ALL ON TABLE predictions_blinded_mandeep FROM samplesdb_table_owner;
GRANT ALL ON TABLE predictions_blinded_mandeep TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions_blinded_mandeep TO mandeep;


--
-- Name: predictions_blinded_voleak; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE predictions_blinded_voleak FROM PUBLIC;
REVOKE ALL ON TABLE predictions_blinded_voleak FROM samplesdb_table_owner;
GRANT ALL ON TABLE predictions_blinded_voleak TO samplesdb_table_owner;
GRANT SELECT ON TABLE predictions_blinded_voleak TO voleak;


--
-- Name: production_export; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE production_export FROM PUBLIC;
REVOKE ALL ON TABLE production_export FROM administrator;
GRANT ALL ON TABLE production_export TO administrator;
GRANT SELECT,REFERENCES,UPDATE ON TABLE production_export TO lucia;
GRANT INSERT ON TABLE production_export TO lucia WITH GRANT OPTION;
GRANT SELECT ON TABLE production_export TO PUBLIC;
GRANT SELECT,TRIGGER ON TABLE production_export TO nicholas;
GRANT REFERENCES ON TABLE production_export TO nicholas WITH GRANT OPTION;
GRANT SELECT ON TABLE production_export TO readonly;
GRANT SELECT ON TABLE production_export TO clinops;
GRANT SELECT ON TABLE production_export TO bioinfo_readonly;
GRANT SELECT ON TABLE production_export TO biostats_readonly;


--
-- Name: production_export_view; Type: ACL; Schema: samplesdb; Owner: rds_superuser
--

REVOKE ALL ON TABLE production_export_view FROM PUBLIC;
REVOKE ALL ON TABLE production_export_view FROM rds_superuser;
GRANT ALL ON TABLE production_export_view TO rds_superuser;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE production_export_view TO lucia;
GRANT SELECT ON TABLE production_export_view TO nicholas;
GRANT SELECT ON TABLE production_export_view TO readonly;
GRANT SELECT ON TABLE production_export_view TO clinops;
GRANT SELECT ON TABLE production_export_view TO bioinfo_readonly;
GRANT SELECT ON TABLE production_export_view TO biostats_readonly;


--
-- Name: proimpact_patients; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE proimpact_patients FROM PUBLIC;
REVOKE ALL ON TABLE proimpact_patients FROM samplesdb_table_owner;
GRANT ALL ON TABLE proimpact_patients TO samplesdb_table_owner;
GRANT SELECT ON TABLE proimpact_patients TO biostats;
GRANT SELECT ON TABLE proimpact_patients TO clinops;


--
-- Name: proimpact_qol; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE proimpact_qol FROM PUBLIC;
REVOKE ALL ON TABLE proimpact_qol FROM samplesdb_table_owner;
GRANT ALL ON TABLE proimpact_qol TO samplesdb_table_owner;
GRANT SELECT ON TABLE proimpact_qol TO clinops;
GRANT SELECT ON TABLE proimpact_qol TO biostats;


--
-- Name: proimpact_sdq; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE proimpact_sdq FROM PUBLIC;
REVOKE ALL ON TABLE proimpact_sdq FROM samplesdb_table_owner;
GRANT ALL ON TABLE proimpact_sdq TO samplesdb_table_owner;
GRANT SELECT ON TABLE proimpact_sdq TO clinops;
GRANT SELECT ON TABLE proimpact_sdq TO biostats;


--
-- Name: proimpact_sites; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE proimpact_sites FROM PUBLIC;
REVOKE ALL ON TABLE proimpact_sites FROM samplesdb_table_owner;
GRANT ALL ON TABLE proimpact_sites TO samplesdb_table_owner;
GRANT SELECT ON TABLE proimpact_sites TO biostats;
GRANT SELECT ON TABLE proimpact_sites TO clinops;


--
-- Name: psa; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE psa FROM PUBLIC;
REVOKE ALL ON TABLE psa FROM samplesdb_table_owner;
GRANT ALL ON TABLE psa TO samplesdb_table_owner;
GRANT SELECT ON TABLE psa TO clinops;
GRANT ALL ON TABLE psa TO rds_superuser;


--
-- Name: rna_batch; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE rna_batch FROM PUBLIC;
REVOKE ALL ON TABLE rna_batch FROM samplesdb_table_owner;
GRANT ALL ON TABLE rna_batch TO samplesdb_table_owner;
GRANT SELECT ON TABLE rna_batch TO bioinfo;
GRANT SELECT ON TABLE rna_batch TO biostats;
GRANT SELECT ON TABLE rna_batch TO readonly;
GRANT ALL ON TABLE rna_batch TO clinops;


--
-- Name: rnafrag; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE rnafrag FROM PUBLIC;
REVOKE ALL ON TABLE rnafrag FROM samplesdb_table_owner;
GRANT ALL ON TABLE rnafrag TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE rnafrag TO biostats;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE rnafrag TO bioinfo;
GRANT SELECT ON TABLE rnafrag TO readonly;
GRANT SELECT ON TABLE rnafrag TO clinops;
GRANT SELECT ON TABLE rnafrag TO bioinfo_readonly;
GRANT SELECT ON TABLE rnafrag TO biostats_readonly;


--
-- Name: samples_bioinfo; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE samples_bioinfo FROM PUBLIC;
REVOKE ALL ON TABLE samples_bioinfo FROM samplesdb_table_owner;
GRANT ALL ON TABLE samples_bioinfo TO samplesdb_table_owner;
GRANT SELECT ON TABLE samples_bioinfo TO bioinfo;
GRANT SELECT ON TABLE samples_bioinfo TO biostats;


--
-- Name: samples_biostats; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE samples_biostats FROM PUBLIC;
REVOKE ALL ON TABLE samples_biostats FROM samplesdb_table_owner;
GRANT ALL ON TABLE samples_biostats TO samplesdb_table_owner;
GRANT SELECT ON TABLE samples_biostats TO biostats;


--
-- Name: samples_breast; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE samples_breast FROM PUBLIC;
REVOKE ALL ON TABLE samples_breast FROM samplesdb_table_owner;
GRANT ALL ON TABLE samples_breast TO samplesdb_table_owner;
GRANT ALL ON TABLE samples_breast TO george;
GRANT ALL ON TABLE samples_breast TO laura;


--
-- Name: study_variable; Type: ACL; Schema: samplesdb; Owner: samplesdb_table_owner
--

REVOKE ALL ON TABLE study_variable FROM PUBLIC;
REVOKE ALL ON TABLE study_variable FROM samplesdb_table_owner;
GRANT ALL ON TABLE study_variable TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE study_variable TO biostats;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE study_variable TO bioinfo;
GRANT SELECT ON TABLE study_variable TO readonly;
GRANT SELECT ON TABLE study_variable TO clinops;
GRANT SELECT ON TABLE study_variable TO bioinfo_readonly;
GRANT SELECT ON TABLE study_variable TO biostats_readonly;


--
-- Name: tap_funky; Type: ACL; Schema: samplesdb; Owner: postgres
--

REVOKE ALL ON TABLE tap_funky FROM PUBLIC;
REVOKE ALL ON TABLE tap_funky FROM postgres;
GRANT ALL ON TABLE tap_funky TO postgres;
GRANT SELECT ON TABLE tap_funky TO PUBLIC;


--
-- Name: view_bruce; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_bruce FROM PUBLIC;
REVOKE ALL ON TABLE view_bruce FROM administrator;
GRANT ALL ON TABLE view_bruce TO administrator;
GRANT ALL ON TABLE view_bruce TO predictions_table_owner;
GRANT SELECT ON TABLE view_bruce TO bruce;
GRANT SELECT ON TABLE view_bruce TO bioinfo;
GRANT SELECT ON TABLE view_bruce TO biostats;
GRANT ALL ON TABLE view_bruce TO samplesdb_table_owner;
GRANT SELECT ON TABLE view_bruce TO bioinfo_readonly;


--
-- Name: view_firas_abdollah; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_firas_abdollah FROM PUBLIC;
REVOKE ALL ON TABLE view_firas_abdollah FROM administrator;
GRANT ALL ON TABLE view_firas_abdollah TO administrator;
GRANT ALL ON TABLE view_firas_abdollah TO predictions_table_owner;
GRANT SELECT ON TABLE view_firas_abdollah TO firas_abdollah;
GRANT SELECT ON TABLE view_firas_abdollah TO bioinfo;
GRANT SELECT ON TABLE view_firas_abdollah TO biostats;
GRANT SELECT ON TABLE view_firas_abdollah TO bioinfo_readonly;


--
-- Name: view_george; Type: ACL; Schema: samplesdb; Owner: fabiola
--

REVOKE ALL ON TABLE view_george FROM PUBLIC;
REVOKE ALL ON TABLE view_george FROM fabiola;
GRANT ALL ON TABLE view_george TO fabiola;
GRANT ALL ON TABLE view_george TO mandeep;
GRANT ALL ON TABLE view_george TO predictions_table_owner;
GRANT SELECT ON TABLE view_george TO george;
GRANT SELECT ON TABLE view_george TO bioinfo;
GRANT SELECT ON TABLE view_george TO biostats;
GRANT ALL ON TABLE view_george TO samplesdb_table_owner;
GRANT SELECT ON TABLE view_george TO bioinfo_readonly;


--
-- Name: view_kenneth_pienta; Type: ACL; Schema: samplesdb; Owner: fabiola
--

REVOKE ALL ON TABLE view_kenneth_pienta FROM PUBLIC;
REVOKE ALL ON TABLE view_kenneth_pienta FROM fabiola;
GRANT ALL ON TABLE view_kenneth_pienta TO fabiola;
GRANT ALL ON TABLE view_kenneth_pienta TO mandeep;
GRANT ALL ON TABLE view_kenneth_pienta TO predictions_table_owner;
GRANT SELECT ON TABLE view_kenneth_pienta TO kenneth_pienta;
GRANT SELECT ON TABLE view_kenneth_pienta TO princy_parsana;
GRANT SELECT ON TABLE view_kenneth_pienta TO bioinfo;
GRANT SELECT ON TABLE view_kenneth_pienta TO biostats;
GRANT ALL ON TABLE view_kenneth_pienta TO samplesdb_table_owner;
GRANT SELECT ON TABLE view_kenneth_pienta TO bioinfo_readonly;


--
-- Name: view_laura; Type: ACL; Schema: samplesdb; Owner: fabiola
--

REVOKE ALL ON TABLE view_laura FROM PUBLIC;
REVOKE ALL ON TABLE view_laura FROM fabiola;
GRANT ALL ON TABLE view_laura TO fabiola;
GRANT SELECT ON TABLE view_laura TO laura;
GRANT SELECT ON TABLE view_laura TO mandeep;
GRANT SELECT ON TABLE view_laura TO biostats;
GRANT ALL ON TABLE view_laura TO samplesdb_table_owner;


--
-- Name: view_mgh; Type: ACL; Schema: samplesdb; Owner: mandeep
--

REVOKE ALL ON TABLE view_mgh FROM PUBLIC;
REVOKE ALL ON TABLE view_mgh FROM mandeep;
GRANT ALL ON TABLE view_mgh TO mandeep;
GRANT ALL ON TABLE view_mgh TO administrator;
GRANT ALL ON TABLE view_mgh TO predictions_table_owner;
GRANT SELECT ON TABLE view_mgh TO michelle_renee_toups;
GRANT SELECT ON TABLE view_mgh TO bioinfo;
GRANT SELECT ON TABLE view_mgh TO biostats;
GRANT SELECT ON TABLE view_mgh TO bioinfo_readonly;


--
-- Name: view_michael_freeman; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_michael_freeman FROM PUBLIC;
REVOKE ALL ON TABLE view_michael_freeman FROM administrator;
GRANT ALL ON TABLE view_michael_freeman TO administrator;
GRANT ALL ON TABLE view_michael_freeman TO predictions_table_owner;
GRANT SELECT ON TABLE view_michael_freeman TO michael_freeman;
GRANT SELECT ON TABLE view_michael_freeman TO bioinfo;
GRANT SELECT ON TABLE view_michael_freeman TO biostats;
GRANT SELECT ON TABLE view_michael_freeman TO bioinfo_readonly;


--
-- Name: view_patrick_karabon; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_patrick_karabon FROM PUBLIC;
REVOKE ALL ON TABLE view_patrick_karabon FROM administrator;
GRANT ALL ON TABLE view_patrick_karabon TO administrator;
GRANT ALL ON TABLE view_patrick_karabon TO tyler;
GRANT ALL ON TABLE view_patrick_karabon TO predictions_table_owner;
GRANT SELECT ON TABLE view_patrick_karabon TO patrick_karabon;
GRANT SELECT ON TABLE view_patrick_karabon TO firas_abdollah;
GRANT SELECT ON TABLE view_patrick_karabon TO bioinfo;
GRANT SELECT ON TABLE view_patrick_karabon TO biostats;
GRANT ALL ON TABLE view_patrick_karabon TO samplesdb_table_owner;
GRANT SELECT ON TABLE view_patrick_karabon TO bioinfo_readonly;


--
-- Name: view_robert_den; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_robert_den FROM PUBLIC;
REVOKE ALL ON TABLE view_robert_den FROM administrator;
GRANT ALL ON TABLE view_robert_den TO administrator;
GRANT ALL ON TABLE view_robert_den TO tyler;
GRANT ALL ON TABLE view_robert_den TO predictions_table_owner;
GRANT SELECT ON TABLE view_robert_den TO robert_den;
GRANT SELECT ON TABLE view_robert_den TO adam_ertel;
GRANT SELECT ON TABLE view_robert_den TO bioinfo;
GRANT SELECT ON TABLE view_robert_den TO biostats;
GRANT SELECT ON TABLE view_robert_den TO bioinfo_readonly;


--
-- Name: view_roland; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_roland FROM PUBLIC;
REVOKE ALL ON TABLE view_roland FROM administrator;
GRANT ALL ON TABLE view_roland TO administrator;
GRANT ALL ON TABLE view_roland TO tyler;
GRANT SELECT ON TABLE view_roland TO roland;
GRANT SELECT ON TABLE view_roland TO bioinfo;
GRANT SELECT ON TABLE view_roland TO biostats;
GRANT ALL ON TABLE view_roland TO predictions_table_owner;


--
-- Name: view_tamara_lotan; Type: ACL; Schema: samplesdb; Owner: administrator
--

REVOKE ALL ON TABLE view_tamara_lotan FROM PUBLIC;
REVOKE ALL ON TABLE view_tamara_lotan FROM administrator;
GRANT ALL ON TABLE view_tamara_lotan TO administrator;
GRANT ALL ON TABLE view_tamara_lotan TO fabiola;
GRANT ALL ON TABLE view_tamara_lotan TO predictions_table_owner;
GRANT SELECT ON TABLE view_tamara_lotan TO tamara_lotan;
GRANT SELECT ON TABLE view_tamara_lotan TO bioinfo;
GRANT SELECT ON TABLE view_tamara_lotan TO biostats;
GRANT SELECT ON TABLE view_tamara_lotan TO bioinfo_readonly;


--
-- PostgreSQL database dump complete
--

--GO
--rollback DROP SCHEMA samplesdb;
