--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

SET search_path = public, pg_catalog;

--
-- Name: add_time_fields(regclass); Type: FUNCTION; Schema: public; Owner: administrator
--

CREATE FUNCTION add_time_fields(_tbl regclass) RETURNS void
    LANGUAGE plpgsql
    AS $$
BEGIN
  EXECUTE format('ALTER TABLE %s ADD COLUMN created TIMESTAMP WITHOUT TIME ZONE, ADD COLUMN modified TIMESTAMP WITHOUT TIME ZONE, '||
		 'ADD COLUMN created_by CHARACTER VARYING(25), ADD COLUMN modified_by CHARACTER VARYING(25);', _tbl);
END
$$;


ALTER FUNCTION public.add_time_fields(_tbl regclass) OWNER TO administrator;

--
-- Name: created_history(); Type: FUNCTION; Schema: public; Owner: administrator
--

CREATE FUNCTION created_history() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.created := current_timestamp;
  NEW.created_by := current_user;
  RETURN NEW;
END
$$;


ALTER FUNCTION public.created_history() OWNER TO administrator;

--
-- Name: modified_history(); Type: FUNCTION; Schema: public; Owner: administrator
--

CREATE FUNCTION modified_history() RETURNS trigger
    LANGUAGE plpgsql
    AS $$
BEGIN
  NEW.modified := current_timestamp;
  NEW.modified_by := current_user;
  RETURN NEW;
END
$$;


ALTER FUNCTION public.modified_history() OWNER TO administrator;

--
-- Name: patient_id_exist(); Type: FUNCTION; Schema: public; Owner: administrator
--

CREATE FUNCTION patient_id_exist() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  IF (SELECT CASE WHEN EXISTS (SELECT 1 FROM samplesdb.patients WHERE patients.patient_id = NEW.patient_id) THEN 1 ELSE 0 END) = 0 AND NEW.patient_id IS NOT NULL THEN
	RAISE EXCEPTION 'Reference violation, patient_id does not exist';
  END IF;
  RETURN NEW;
END$$;


ALTER FUNCTION public.patient_id_exist() OWNER TO administrator;

--
-- Name: study_cdna_exist(); Type: FUNCTION; Schema: public; Owner: administrator
--

CREATE FUNCTION study_cdna_exist() RETURNS trigger
    LANGUAGE plpgsql
    AS $$BEGIN
  IF (SELECT CASE WHEN EXISTS (SELECT 1 FROM samplesdb.studies WHERE studies.study_name = NEW.study_name) THEN 1 ELSE 0 END) = 0 AND NEW.study_name IS NOT NULL THEN
	RAISE EXCEPTION 'Reference violation, study_name does not exist';
  END IF;
  IF (SELECT CASE WHEN EXISTS (SELECT 1 FROM samplesdb.cdna WHERE cdna.cdna_id = NEW.cdna_id) THEN 1 ELSE 0 END) = 0 AND NEW.cdna_id IS NOT NULL THEN
	RAISE EXCEPTION 'Reference violation, cdna_id does not exist';
  END IF;
  RETURN NEW;
END$$;


ALTER FUNCTION public.study_cdna_exist() OWNER TO administrator;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: qcbatch_matching; Type: TABLE; Schema: public; Owner: administrator
--

CREATE TABLE qcbatch_matching (
    scandate character varying(16),
    batch character varying(16),
    study_names character varying(100)[]
);


ALTER TABLE qcbatch_matching OWNER TO administrator;

--
-- Name: public; Type: ACL; Schema: -; Owner: administrator
--

REVOKE ALL ON SCHEMA public FROM PUBLIC;
REVOKE ALL ON SCHEMA public FROM administrator;
GRANT ALL ON SCHEMA public TO administrator;
GRANT ALL ON SCHEMA public TO rds_superuser;
GRANT ALL ON SCHEMA public TO PUBLIC;


--
-- PostgreSQL database dump complete
--

