package com.genomedx.app

import groovy.sql.Sql

//def loadGrapes(){
//    ClassLoader classLoader = new groovy.lang.GroovyClassLoader()
//    Map[] grapez = [[group : 'org.ccil.cowan.tagsoup', module : 'tagsoup', version : '1.2']]
//    Grape.grab(classLoader: classLoader, grapez)
//    println "Class: " + classLoader.loadClass('org.ccil.cowan.tagsoup.jaxp.SAXParserImpl')
//}

//@Grapes([
//        @Grab(group='org.postgresql',module='postgresql',version='9.4.1211'),
//        @GrabConfig( systemClassLoader=true )
//])

def dbUrl = "jdbc:postgresql://shadow.dev.genomedx.com/prndl?ApplicationName=Liquibase&sslfactory=org.postgresql.ssl.NonValidatingFactory&ssl=true"
def dbUser = "xsnuser"
def dbPassword = 'pow2iebohk'
def dbDriver = "org.postgresql.Driver"

def sql = Sql.newInstance(dbUrl, dbUser, dbPassword, dbDriver)

// iterate over query's result set and "process" each row by printing two names
sql.eachRow("select filename, dateexecuted from databasechangelog order by dateexecuted desc limit 1")
        {
            println "LastChangeLog ${it.filename} Date ${it.dateexecuted} "
        }
