--liquibase formatted sql
--changeset mpdillon:normalization endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: normalization; Type: SCHEMA; Schema: -; Owner: administrator
--

CREATE SCHEMA normalization;


ALTER SCHEMA normalization OWNER TO administrator;

SET search_path = normalization, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: normalization; Type: TABLE; Schema: normalization; Owner: administrator
--

CREATE TABLE normalization (
    normalization_name character varying(128) NOT NULL,
    type character varying(16) NOT NULL,
    vector_id character varying(128),
    gnuse_id character varying(128),
    celfile_group_id character varying(100) NOT NULL,
    description text,
    location character varying(256) NOT NULL,
    normalization_descriptor_name character varying(128),
    target character varying(20),
    software_version_id numeric NOT NULL,
    signal_assessed numeric,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    study_name character varying(100)
);


ALTER TABLE normalization OWNER TO administrator;

--
-- Name: normalization_id_seq; Type: SEQUENCE; Schema: normalization; Owner: rds_superuser
--

CREATE SEQUENCE normalization_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE normalization_id_seq OWNER TO rds_superuser;

--
-- Name: vectors; Type: TABLE; Schema: normalization; Owner: administrator
--

CREATE TABLE vectors (
    vector_id character varying(128) NOT NULL,
    software_version_id numeric,
    celfile_batch_group_id character varying(100) NOT NULL,
    celfile_subset_group_id character varying(100) NOT NULL,
    description character varying(1024),
    type character varying(16),
    sample_size numeric,
    location character varying(256),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE vectors OWNER TO administrator;

--
-- Name: NORMALIZATION_pkey; Type: CONSTRAINT; Schema: normalization; Owner: administrator
--

ALTER TABLE ONLY normalization
    ADD CONSTRAINT "NORMALIZATION_pkey" PRIMARY KEY (normalization_name);


--
-- Name: VECTORS_pkey; Type: CONSTRAINT; Schema: normalization; Owner: administrator
--

ALTER TABLE ONLY vectors
    ADD CONSTRAINT "VECTORS_pkey" PRIMARY KEY (vector_id);


--
-- Name: created_trigger; Type: TRIGGER; Schema: normalization; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON normalization FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: normalization; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON vectors FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: normalization; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON normalization FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: normalization; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON vectors FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: vector_id_fk; Type: FK CONSTRAINT; Schema: normalization; Owner: administrator
--

ALTER TABLE ONLY normalization
    ADD CONSTRAINT vector_id_fk FOREIGN KEY (vector_id) REFERENCES vectors(vector_id);


--
-- Name: normalization; Type: ACL; Schema: -; Owner: administrator
--

REVOKE ALL ON SCHEMA normalization FROM PUBLIC;
REVOKE ALL ON SCHEMA normalization FROM administrator;
GRANT ALL ON SCHEMA normalization TO administrator;
GRANT USAGE ON SCHEMA normalization TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA normalization TO bioinfo;
GRANT USAGE ON SCHEMA normalization TO biostats;
GRANT USAGE ON SCHEMA normalization TO clinops;
GRANT USAGE ON SCHEMA normalization TO engineering;


--
-- Name: normalization; Type: ACL; Schema: normalization; Owner: administrator
--

REVOKE ALL ON TABLE normalization FROM PUBLIC;
REVOKE ALL ON TABLE normalization FROM administrator;
GRANT ALL ON TABLE normalization TO administrator;
GRANT SELECT ON TABLE normalization TO readonly;
GRANT SELECT ON TABLE normalization TO clinops;
GRANT SELECT ON TABLE normalization TO bioinfo_readonly;
GRANT SELECT ON TABLE normalization TO biostats_readonly;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE normalization TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE normalization TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE normalization TO biostats;


--
-- Name: normalization_id_seq; Type: ACL; Schema: normalization; Owner: rds_superuser
--

REVOKE ALL ON SEQUENCE normalization_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE normalization_id_seq FROM rds_superuser;
GRANT ALL ON SEQUENCE normalization_id_seq TO rds_superuser;
GRANT SELECT,USAGE ON SEQUENCE normalization_id_seq TO samplesdb_table_owner;
GRANT SELECT,USAGE ON SEQUENCE normalization_id_seq TO bioinfo;
GRANT SELECT,USAGE ON SEQUENCE normalization_id_seq TO biostats;


--
-- Name: vectors; Type: ACL; Schema: normalization; Owner: administrator
--

REVOKE ALL ON TABLE vectors FROM PUBLIC;
REVOKE ALL ON TABLE vectors FROM administrator;
GRANT ALL ON TABLE vectors TO administrator;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE vectors TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE vectors TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,UPDATE ON TABLE vectors TO biostats;
GRANT SELECT ON TABLE vectors TO readonly;
GRANT SELECT ON TABLE vectors TO clinops;
GRANT SELECT ON TABLE vectors TO bioinfo_readonly;
GRANT SELECT ON TABLE vectors TO biostats_readonly;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA normalization;
