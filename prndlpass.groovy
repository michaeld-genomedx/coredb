def homedir = System.getProperty("user.home")
def osname = System.getProperty("os.name")
def pgpass = ''

if (osname == 'Windows') {
   def appd = System.getenv("APPDATA")
   pgpass = "$appd/postgresql/pgpass.conf"
   }
else {
   pgpass = "$homedir/.pgpass" 
   }
def words = []
new File( "$pgpass" ).eachLine { line ->
    words = line.split(':')
    if (words[0] == 'shadow.dev.genomedx.com') {
       if (words[2] == 'prndl' && words[3] == 'administrator') {
          println words[4]
       }
    }
}

