--liquibase formatted sql
--changeset mpdillon:expression endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: expression; Type: SCHEMA; Schema: -; Owner: grid_content_owner
--

CREATE SCHEMA expression;


ALTER SCHEMA expression OWNER TO grid_content_owner;

SET search_path = expression, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: expression; Type: TABLE; Schema: expression; Owner: grid_content_owner
--

CREATE TABLE expression (
    celfile_name character varying(255) NOT NULL,
    feature character varying(32) NOT NULL,
    level character varying(16) NOT NULL,
    normalization character varying(16) NOT NULL,
    normalization_environment character varying(32) NOT NULL,
    expression double precision NOT NULL
);


ALTER TABLE expression OWNER TO grid_content_owner;

--
-- Name: feature_import; Type: VIEW; Schema: expression; Owner: grid_content_owner
--

CREATE VIEW feature_import AS
 SELECT feature.feature,
    feature.level,
    feature.normalization,
    feature.normalization_environment
   FROM signatures.feature
UNION
 SELECT biomarker.feature,
    biomarker.level,
    biomarker.normalization,
    biomarker.normalization_environment
   FROM grid.biomarker;


ALTER TABLE feature_import OWNER TO grid_content_owner;

--
-- Name: expression_pkey; Type: CONSTRAINT; Schema: expression; Owner: grid_content_owner
--

ALTER TABLE ONLY expression
    ADD CONSTRAINT expression_pkey PRIMARY KEY (celfile_name, feature, level, normalization, normalization_environment);


--
-- Name: expression_celfile_name_idx; Type: INDEX; Schema: expression; Owner: grid_content_owner
--

CREATE INDEX expression_celfile_name_idx ON expression USING btree (celfile_name);


--
-- Name: expression_feature_idx; Type: INDEX; Schema: expression; Owner: grid_content_owner
--

CREATE INDEX expression_feature_idx ON expression USING btree (feature);


--
-- Name: expression_feature_level_normalization_norm_env_idx; Type: INDEX; Schema: expression; Owner: grid_content_owner
--

CREATE INDEX expression_feature_level_normalization_norm_env_idx ON expression USING btree (feature, level, normalization, normalization_environment);


--
-- Name: expression_level_idx; Type: INDEX; Schema: expression; Owner: grid_content_owner
--

CREATE INDEX expression_level_idx ON expression USING btree (level);


--
-- Name: expression_level_normalization_normalization_environment_idx; Type: INDEX; Schema: expression; Owner: grid_content_owner
--

CREATE INDEX expression_level_normalization_normalization_environment_idx ON expression USING btree (level, normalization, normalization_environment);


--
-- Name: fki_expression_environment_name_fkey; Type: INDEX; Schema: expression; Owner: grid_content_owner
--

CREATE INDEX fki_expression_environment_name_fkey ON expression USING btree (normalization_environment);


--
-- Name: expression_environment_name_fkey; Type: FK CONSTRAINT; Schema: expression; Owner: grid_content_owner
--

ALTER TABLE ONLY expression
    ADD CONSTRAINT expression_environment_name_fkey FOREIGN KEY (normalization_environment) REFERENCES signatures.environment(name) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: expression; Type: ACL; Schema: -; Owner: grid_content_owner
--

REVOKE ALL ON SCHEMA expression FROM PUBLIC;
REVOKE ALL ON SCHEMA expression FROM grid_content_owner;
GRANT ALL ON SCHEMA expression TO grid_content_owner;
GRANT USAGE ON SCHEMA expression TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA expression TO clinops;
GRANT USAGE ON SCHEMA expression TO biostats;
GRANT USAGE ON SCHEMA expression TO bioinfo;
GRANT USAGE ON SCHEMA expression TO engineering;
GRANT ALL ON SCHEMA expression TO grid_admin;
GRANT ALL ON SCHEMA expression TO administrator;


--
-- Name: expression; Type: ACL; Schema: expression; Owner: grid_content_owner
--

REVOKE ALL ON TABLE expression FROM PUBLIC;
REVOKE ALL ON TABLE expression FROM grid_content_owner;
GRANT ALL ON TABLE expression TO grid_content_owner;
GRANT SELECT ON TABLE expression TO grid_admin;


--
-- Name: feature_import; Type: ACL; Schema: expression; Owner: grid_content_owner
--

REVOKE ALL ON TABLE feature_import FROM PUBLIC;
REVOKE ALL ON TABLE feature_import FROM grid_content_owner;
GRANT ALL ON TABLE feature_import TO grid_content_owner;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA expression;
