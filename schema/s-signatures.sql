--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: signatures; Type: SCHEMA; Schema: -; Owner: grid_content_owner
--

CREATE SCHEMA signatures;


ALTER SCHEMA signatures OWNER TO grid_content_owner;

--
-- Name: SCHEMA signatures; Type: COMMENT; Schema: -; Owner: grid_content_owner
--

COMMENT ON SCHEMA signatures IS 'standard public schema';


SET search_path = signatures, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: feature; Type: TABLE; Schema: signatures; Owner: grid_content_owner
--

CREATE TABLE feature (
    feature character varying(32) NOT NULL,
    signature_id bigint NOT NULL,
    level character varying(16) NOT NULL,
    normalization character varying(16) NOT NULL,
    normalization_environment character varying(32) NOT NULL,
    "order" bigint NOT NULL
);


ALTER TABLE feature OWNER TO grid_content_owner;

--
-- Name: prediction; Type: TABLE; Schema: signatures; Owner: grid_content_owner
--

CREATE TABLE prediction (
    signature_id bigint NOT NULL,
    celfile_name character varying(255) NOT NULL,
    score double precision NOT NULL,
    class character varying(64),
    percentile double precision
);


ALTER TABLE prediction OWNER TO grid_content_owner;

--
-- Name: signature_signature_id_seq; Type: SEQUENCE; Schema: signatures; Owner: rds_superuser
--

CREATE SEQUENCE signature_signature_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE signature_signature_id_seq OWNER TO rds_superuser;

--
-- Name: signature; Type: TABLE; Schema: signatures; Owner: grid_content_owner
--

CREATE TABLE signature (
    signature_id bigint DEFAULT nextval('signature_signature_id_seq'::regclass) NOT NULL,
    name character varying(128) NOT NULL,
    version double precision,
    date date,
    disease character varying(128),
    endpoint character varying(128),
    model_type character varying(64),
    description text,
    institution character varying(256),
    inventor character varying(128),
    citation character varying(1024),
    predict_function text NOT NULL,
    predict_env character varying(32) NOT NULL,
    dev_script text,
    model_obj text,
    created timestamp without time zone NOT NULL
);


ALTER TABLE signature OWNER TO grid_content_owner;

--
-- Name: TABLE signature; Type: COMMENT; Schema: signatures; Owner: grid_content_owner
--

COMMENT ON TABLE signature IS 'Please use signature_view instead unless you know what you are doing.';


--
-- Name: bca_subtype; Type: VIEW; Schema: signatures; Owner: nicholas
--

CREATE VIEW bca_subtype AS
 SELECT pr.celfile_name,
        CASE
            WHEN ((si.name)::text = 'bca_subtyping_basal'::text) THEN 'Basal'::text
            WHEN ((si.name)::text = 'bca_subtyping_luminal'::text) THEN 'Luminal'::text
            WHEN ((si.name)::text = 'bca_subtyping_infiltrated_luminal'::text) THEN 'Infiltrated Luminal'::text
            WHEN ((si.name)::text = 'bca_subtyping_claudin_low'::text) THEN 'Claudin Low'::text
            ELSE NULL::text
        END AS bca_subtype
   FROM ((prediction pr
     RIGHT JOIN signature si USING (signature_id))
     JOIN ( SELECT prediction.celfile_name,
            max(prediction.score) AS score
           FROM (prediction
             RIGHT JOIN signature USING (signature_id))
          WHERE (signature.signature_id = ANY (ARRAY[(74)::bigint, (75)::bigint, (76)::bigint, (77)::bigint]))
          GROUP BY prediction.celfile_name) mq ON ((((pr.celfile_name)::text = (mq.celfile_name)::text) AND (pr.score = mq.score) AND (si.signature_id = ANY (ARRAY[(74)::bigint, (75)::bigint, (76)::bigint, (77)::bigint])))));


ALTER TABLE bca_subtype OWNER TO nicholas;

--
-- Name: celfilegroup; Type: TABLE; Schema: signatures; Owner: grid_content_owner
--

CREATE TABLE celfilegroup (
    celfile_name character varying(255) NOT NULL,
    signature_id bigint NOT NULL
);


ALTER TABLE celfilegroup OWNER TO grid_content_owner;

--
-- Name: molecular_subtype; Type: VIEW; Schema: signatures; Owner: nicholas
--

CREATE VIEW molecular_subtype AS
 SELECT ct.celfile_name,
        CASE
            WHEN ((pr.class IS NULL) OR (ct."ETV1" IS NULL) OR (ct."ETV4" IS NULL) OR (ct."ETV5" IS NULL) OR (ct."FLI1" IS NULL) OR (ct."SPINK1" IS NULL)) THEN NULL::text
            WHEN ((pr.class)::text = 'ERG positive'::text) THEN 'ERG+'::text
            WHEN (((ct."ETV1")::text = 'High'::text) OR ((ct."ETV4")::text = 'High'::text) OR ((ct."ETV5")::text = 'High'::text) OR ((ct."FLI1")::text = 'High'::text)) THEN 'ETS+'::text
            WHEN ((ct."SPINK1")::text = 'High'::text) THEN 'SPINK1+'::text
            ELSE 'Triple Negative'::text
        END AS molecular_subtype
   FROM (samplesdb.crosstab('SELECT celfile_name, biomarker, class FROM grid.transcription WHERE biomarker IN (''ETV1'', ''ETV4'', ''ETV5'', ''FLI1'', ''SPINK1'') ORDER BY celfile_name'::text, 'SELECT DISTINCT biomarker FROM grid.transcription WHERE biomarker IN (''ETV1'', ''ETV4'', ''ETV5'', ''FLI1'', ''SPINK1'') ORDER BY 1'::text) ct(celfile_name character varying(255), "ETV1" character varying(64), "ETV4" character varying(64), "ETV5" character varying(64), "FLI1" character varying(64), "SPINK1" character varying(64))
     JOIN prediction pr ON ((((pr.celfile_name)::text = (ct.celfile_name)::text) AND (pr.signature_id = 1))));


ALTER TABLE molecular_subtype OWNER TO nicholas;

--
-- Name: class; Type: MATERIALIZED VIEW; Schema: signatures; Owner: nicholas
--

CREATE MATERIALIZED VIEW class AS
 SELECT ct.celfile_name,
    ct.agell2012_lm_1,
    ct.aros_1,
    ct.ars_1,
    ct.beltran2016_1,
    ct.bibikova2007_lm_1,
    ct.bismar2006_lm_1,
    ct.cheville2008_lm_1,
    ct.cuzick2011_1,
    ct.cuzick2011_lm_1,
    ct.dasatinib_sens_1,
    ct.decipher_1,
    ct.decipherv2_1,
    ct.decipherv2_2,
    ct.docetaxel_sens_1,
    ct.ergmodel_1,
    ct.genomic_capras_1,
    ct.genomic_gleason_grade_1,
    ct.genomic_gleason_grade_2,
    ct.glinsky2004_lm_1,
    ct.glinsky2005_lm_1,
    ct.klein2014_lm_1,
    ct.lapointe2004_lm_1,
    ct.larkin2012_lm_1,
    ct.long2011_lm_1,
    ct.long2014_1,
    ct.lotan2016_1,
    ct.nakagawa2008_lm_1,
    ct.nelson2016_1,
    ct."nelson_2016_AR_1",
    ct.non_organ_confined_1,
    ct.normaltumor_1,
    ct.pca_vs_mibc_1,
    ct.penney2011_1,
    ct.penney2011_lm_1,
    ct.portos_1,
    ct.ramaswamy2003_lm_1,
    ct.saal2007_lm_1,
    ct.saal2007_pten_2,
    ct.singh2002_lm_1,
    ct.smallcell_1,
    ct.smallcell_2,
    ct.stephenson2005_lm_1,
    ct.talantov2010_lm_1,
    ct.torresroca2009_1,
    ct.varambally2005_lm_1,
    ct.wu2013_lm_1,
    ct.yu2007_lm_1,
    ct.zhang2016_basal_1,
    ms.molecular_subtype,
    bs.bca_subtype
   FROM ((samplesdb.crosstab('SELECT celfile_name, name || $$_$$ || version AS name, class FROM signatures.prediction JOIN signatures.signature USING (signature_id) ORDER BY celfile_name'::text, 'SELECT DISTINCT name || $$_$$ || version AS name FROM signatures.signature JOIN signatures.cutpoint USING (signature_id) ORDER BY 1'::text) ct(celfile_name character varying(255), agell2012_lm_1 character varying(64), aros_1 character varying(64), ars_1 character varying(64), beltran2016_1 character varying(64), bibikova2007_lm_1 character varying(64), bismar2006_lm_1 character varying(64), cheville2008_lm_1 character varying(64), cuzick2011_1 character varying(64), cuzick2011_lm_1 character varying(64), dasatinib_sens_1 character varying(64), decipher_1 character varying(64), decipherv2_1 character varying(64), decipherv2_2 character varying(64), docetaxel_sens_1 character varying(64), ergmodel_1 character varying(64), genomic_capras_1 character varying(64), genomic_gleason_grade_1 character varying(64), genomic_gleason_grade_2 character varying(64), glinsky2004_lm_1 character varying(64), glinsky2005_lm_1 character varying(64), klein2014_lm_1 character varying(64), lapointe2004_lm_1 character varying(64), larkin2012_lm_1 character varying(64), long2011_lm_1 character varying(64), long2014_1 character varying(64), lotan2016_1 character varying(64), nakagawa2008_lm_1 character varying(64), nelson2016_1 character varying(64), "nelson_2016_AR_1" character varying(64), non_organ_confined_1 character varying(64), normaltumor_1 character varying(64), pca_vs_mibc_1 character varying(64), penney2011_1 character varying(64), penney2011_lm_1 character varying(64), portos_1 character varying(64), ramaswamy2003_lm_1 character varying(64), saal2007_lm_1 character varying(64), saal2007_pten_2 character varying(64), singh2002_lm_1 character varying(64), smallcell_1 character varying(64), smallcell_2 character varying(64), stephenson2005_lm_1 character varying(64), talantov2010_lm_1 character varying(64), torresroca2009_1 character varying(64), varambally2005_lm_1 character varying(64), wu2013_lm_1 character varying(64), yu2007_lm_1 character varying(64), zhang2016_basal_1 character varying(64))
     LEFT JOIN molecular_subtype ms USING (celfile_name))
     LEFT JOIN bca_subtype bs USING (celfile_name))
  WITH NO DATA;


ALTER TABLE class OWNER TO nicholas;

--
-- Name: cutpoint; Type: TABLE; Schema: signatures; Owner: grid_content_owner
--

CREATE TABLE cutpoint (
    signature_id bigint NOT NULL,
    label character varying(64) NOT NULL,
    cutpoint double precision NOT NULL
);


ALTER TABLE cutpoint OWNER TO grid_content_owner;

--
-- Name: dockerport_seq; Type: SEQUENCE; Schema: signatures; Owner: rds_superuser
--

CREATE SEQUENCE dockerport_seq
    START WITH 6311
    INCREMENT BY 1
    MINVALUE 6311
    MAXVALUE 49151
    CACHE 1;


ALTER TABLE dockerport_seq OWNER TO rds_superuser;

--
-- Name: environment; Type: TABLE; Schema: signatures; Owner: grid_content_owner
--

CREATE TABLE environment (
    name character varying(32) NOT NULL,
    repository character varying(1028) NOT NULL,
    tag character varying(128) NOT NULL,
    port integer NOT NULL,
    description text,
    dockerfile text,
    created timestamp without time zone
);


ALTER TABLE environment OWNER TO grid_content_owner;

--
-- Name: percentile; Type: MATERIALIZED VIEW; Schema: signatures; Owner: nicholas
--

CREATE MATERIALIZED VIEW percentile AS
 SELECT ct.celfile_name,
    ct.agell2012_lm_1,
    ct.aros_1,
    ct.ars_1,
    ct.bca_subtyping_basal_0,
    ct.bca_subtyping_basal_1,
    ct.bca_subtyping_claudin_low_0,
    ct.bca_subtyping_claudin_low_1,
    ct.bca_subtyping_infiltrated_luminal_0,
    ct.bca_subtyping_infiltrated_luminal_1,
    ct.bca_subtyping_luminal_0,
    ct.bca_subtyping_luminal_1,
    ct.beltran2016_1,
    ct.bibikova2007_lm_1,
    ct.bismar2006_lm_1,
    ct.cheville2008_lm_1,
    ct.cuzick2011_1,
    ct.cuzick2011_lm_1,
    ct.dasatinib_sens_1,
    ct.decipher_1,
    ct.decipherv2_1,
    ct.decipherv2_2,
    ct.docetaxel_sens_1,
    ct.ergmodel_1,
    ct.genomic_capras_1,
    ct.genomic_gleason_grade_1,
    ct.genomic_gleason_grade_2,
    ct.glinsky2004_lm_1,
    ct.glinsky2005_lm_1,
    ct.klein2014_lm_1,
    ct.lapointe2004_lm_1,
    ct.larkin2012_lm_1,
    ct.long2011_lm_1,
    ct.long2014_1,
    ct.lotan2016_1,
    ct.nakagawa2008_lm_1,
    ct.nelson2016_1,
    ct."nelson_2016_AR_1",
    ct.non_organ_confined_1,
    ct.normaltumor_1,
    ct.pca_vs_mibc_1,
    ct.penney2011_1,
    ct.penney2011_lm_1,
    ct.portos_1,
    ct.ragnum2015_1,
    ct.ramaswamy2003_lm_1,
    ct.saal2007_lm_1,
    ct.saal2007_pten_2,
    ct.singh2002_lm_1,
    ct.smallcell_1,
    ct.smallcell_2,
    ct.stephenson2005_lm_1,
    ct.talantov2010_lm_1,
    ct.torresroca2009_1,
    ct.varambally2005_lm_1,
    ct.wu2013_lm_1,
    ct.yu2007_lm_1,
    ct.zhang2016_basal_1
   FROM samplesdb.crosstab('SELECT celfile_name, name || $$_$$ || version AS name, percentile FROM signatures.prediction JOIN signatures.signature USING (signature_id) ORDER BY celfile_name'::text, 'SELECT name || $$_$$ || version AS name FROM signatures.signature ORDER BY 1'::text) ct(celfile_name character varying(255), agell2012_lm_1 double precision, aros_1 double precision, ars_1 double precision, bca_subtyping_basal_0 double precision, bca_subtyping_basal_1 double precision, bca_subtyping_claudin_low_0 double precision, bca_subtyping_claudin_low_1 double precision, bca_subtyping_infiltrated_luminal_0 double precision, bca_subtyping_infiltrated_luminal_1 double precision, bca_subtyping_luminal_0 double precision, bca_subtyping_luminal_1 double precision, beltran2016_1 double precision, bibikova2007_lm_1 double precision, bismar2006_lm_1 double precision, cheville2008_lm_1 double precision, cuzick2011_1 double precision, cuzick2011_lm_1 double precision, dasatinib_sens_1 double precision, decipher_1 double precision, decipherv2_1 double precision, decipherv2_2 double precision, docetaxel_sens_1 double precision, ergmodel_1 double precision, genomic_capras_1 double precision, genomic_gleason_grade_1 double precision, genomic_gleason_grade_2 double precision, glinsky2004_lm_1 double precision, glinsky2005_lm_1 double precision, klein2014_lm_1 double precision, lapointe2004_lm_1 double precision, larkin2012_lm_1 double precision, long2011_lm_1 double precision, long2014_1 double precision, lotan2016_1 double precision, nakagawa2008_lm_1 double precision, nelson2016_1 double precision, "nelson_2016_AR_1" double precision, non_organ_confined_1 double precision, normaltumor_1 double precision, pca_vs_mibc_1 double precision, penney2011_1 double precision, penney2011_lm_1 double precision, portos_1 double precision, ragnum2015_1 double precision, ramaswamy2003_lm_1 double precision, saal2007_lm_1 double precision, saal2007_pten_2 double precision, singh2002_lm_1 double precision, smallcell_1 double precision, smallcell_2 double precision, stephenson2005_lm_1 double precision, talantov2010_lm_1 double precision, torresroca2009_1 double precision, varambally2005_lm_1 double precision, wu2013_lm_1 double precision, yu2007_lm_1 double precision, zhang2016_basal_1 double precision)
  WITH NO DATA;


ALTER TABLE percentile OWNER TO nicholas;

--
-- Name: score; Type: MATERIALIZED VIEW; Schema: signatures; Owner: nicholas
--

CREATE MATERIALIZED VIEW score AS
 SELECT ct.celfile_name,
    ct.agell2012_lm_1,
    ct.aros_1,
    ct.ars_1,
    ct.bca_subtyping_basal_0,
    ct.bca_subtyping_basal_1,
    ct.bca_subtyping_claudin_low_0,
    ct.bca_subtyping_claudin_low_1,
    ct.bca_subtyping_infiltrated_luminal_0,
    ct.bca_subtyping_infiltrated_luminal_1,
    ct.bca_subtyping_luminal_0,
    ct.bca_subtyping_luminal_1,
    ct.beltran2016_1,
    ct.bibikova2007_lm_1,
    ct.bismar2006_lm_1,
    ct.cheville2008_lm_1,
    ct.cuzick2011_1,
    ct.cuzick2011_lm_1,
    ct.dasatinib_sens_1,
    ct.decipher_1,
    ct.decipherv2_1,
    ct.decipherv2_2,
    ct.docetaxel_sens_1,
    ct.ergmodel_1,
    ct.genomic_capras_1,
    ct.genomic_gleason_grade_1,
    ct.genomic_gleason_grade_2,
    ct.glinsky2004_lm_1,
    ct.glinsky2005_lm_1,
    ct.klein2014_lm_1,
    ct.lapointe2004_lm_1,
    ct.larkin2012_lm_1,
    ct.long2011_lm_1,
    ct.long2014_1,
    ct.lotan2016_1,
    ct.nakagawa2008_lm_1,
    ct.nelson2016_1,
    ct."nelson_2016_AR_1",
    ct.non_organ_confined_1,
    ct.normaltumor_1,
    ct.pca_vs_mibc_1,
    ct.penney2011_1,
    ct.penney2011_lm_1,
    ct.portos_1,
    ct.ragnum2015_1,
    ct.ramaswamy2003_lm_1,
    ct.saal2007_lm_1,
    ct.saal2007_pten_2,
    ct.singh2002_lm_1,
    ct.smallcell_1,
    ct.smallcell_2,
    ct.stephenson2005_lm_1,
    ct.talantov2010_lm_1,
    ct.torresroca2009_1,
    ct.varambally2005_lm_1,
    ct.wu2013_lm_1,
    ct.yu2007_lm_1,
    ct.zhang2016_basal_1
   FROM samplesdb.crosstab('SELECT celfile_name, name || $$_$$ || version AS name, score FROM signatures.prediction JOIN signatures.signature USING (signature_id) ORDER BY celfile_name'::text, 'SELECT name || $$_$$ || version AS name FROM signatures.signature ORDER BY 1'::text) ct(celfile_name character varying(255), agell2012_lm_1 double precision, aros_1 double precision, ars_1 double precision, bca_subtyping_basal_0 double precision, bca_subtyping_basal_1 double precision, bca_subtyping_claudin_low_0 double precision, bca_subtyping_claudin_low_1 double precision, bca_subtyping_infiltrated_luminal_0 double precision, bca_subtyping_infiltrated_luminal_1 double precision, bca_subtyping_luminal_0 double precision, bca_subtyping_luminal_1 double precision, beltran2016_1 double precision, bibikova2007_lm_1 double precision, bismar2006_lm_1 double precision, cheville2008_lm_1 double precision, cuzick2011_1 double precision, cuzick2011_lm_1 double precision, dasatinib_sens_1 double precision, decipher_1 double precision, decipherv2_1 double precision, decipherv2_2 double precision, docetaxel_sens_1 double precision, ergmodel_1 double precision, genomic_capras_1 double precision, genomic_gleason_grade_1 double precision, genomic_gleason_grade_2 double precision, glinsky2004_lm_1 double precision, glinsky2005_lm_1 double precision, klein2014_lm_1 double precision, lapointe2004_lm_1 double precision, larkin2012_lm_1 double precision, long2011_lm_1 double precision, long2014_1 double precision, lotan2016_1 double precision, nakagawa2008_lm_1 double precision, nelson2016_1 double precision, "nelson_2016_AR_1" double precision, non_organ_confined_1 double precision, normaltumor_1 double precision, pca_vs_mibc_1 double precision, penney2011_1 double precision, penney2011_lm_1 double precision, portos_1 double precision, ragnum2015_1 double precision, ramaswamy2003_lm_1 double precision, saal2007_lm_1 double precision, saal2007_pten_2 double precision, singh2002_lm_1 double precision, smallcell_1 double precision, smallcell_2 double precision, stephenson2005_lm_1 double precision, talantov2010_lm_1 double precision, torresroca2009_1 double precision, varambally2005_lm_1 double precision, wu2013_lm_1 double precision, yu2007_lm_1 double precision, zhang2016_basal_1 double precision)
  WITH NO DATA;


ALTER TABLE score OWNER TO nicholas;

--
-- Name: signature_view; Type: VIEW; Schema: signatures; Owner: grid_content_owner
--

CREATE VIEW signature_view AS
 SELECT signature.signature_id,
    signature.name,
    signature.version,
    signature.date,
    signature.disease,
    signature.endpoint,
    signature.model_type,
    signature.description,
    signature.institution,
    signature.inventor,
    signature.citation,
    signature.predict_env,
    signature.created
   FROM signature;


ALTER TABLE signature_view OWNER TO grid_content_owner;

--
-- Name: celfilegroup_pkey; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY celfilegroup
    ADD CONSTRAINT celfilegroup_pkey PRIMARY KEY (signature_id, celfile_name);


--
-- Name: cutpoint_pkey; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY cutpoint
    ADD CONSTRAINT cutpoint_pkey PRIMARY KEY (signature_id, label);


--
-- Name: environment_pkey; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY environment
    ADD CONSTRAINT environment_pkey PRIMARY KEY (name);


--
-- Name: environment_port_key; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY environment
    ADD CONSTRAINT environment_port_key UNIQUE (port);


--
-- Name: features_pkey; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY feature
    ADD CONSTRAINT features_pkey PRIMARY KEY (signature_id, feature, level);


--
-- Name: score_pkey; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY prediction
    ADD CONSTRAINT score_pkey PRIMARY KEY (signature_id, celfile_name);


--
-- Name: signature_name_version_unique; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_name_version_unique UNIQUE (name, version);


--
-- Name: signature_pkey; Type: CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_pkey PRIMARY KEY (signature_id);


--
-- Name: cutpoint_signature_id_index; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX cutpoint_signature_id_index ON cutpoint USING btree (signature_id);


--
-- Name: feature_feature_index; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX feature_feature_index ON feature USING btree (feature);


--
-- Name: feature_feature_level_normalization_normalization_environme_idx; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX feature_feature_level_normalization_normalization_environme_idx ON feature USING btree (feature, level, normalization, normalization_environment);


--
-- Name: feature_level_idx; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX feature_level_idx ON feature USING btree (level);


--
-- Name: feature_normalization_idx; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX feature_normalization_idx ON feature USING btree (normalization);


--
-- Name: feature_signature_id; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX feature_signature_id ON feature USING btree (signature_id);


--
-- Name: score_celfile_name_idx; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX score_celfile_name_idx ON prediction USING btree (celfile_name);


--
-- Name: score_class_idx; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX score_class_idx ON prediction USING btree (class);


--
-- Name: score_signature_id_idx; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX score_signature_id_idx ON prediction USING btree (signature_id);


--
-- Name: signature_name_key; Type: INDEX; Schema: signatures; Owner: grid_content_owner
--

CREATE INDEX signature_name_key ON signature USING btree (name);


--
-- Name: signatures_class_celfile_name; Type: INDEX; Schema: signatures; Owner: nicholas
--

CREATE UNIQUE INDEX signatures_class_celfile_name ON class USING btree (celfile_name);


--
-- Name: signatures_percentile_celfile_name; Type: INDEX; Schema: signatures; Owner: nicholas
--

CREATE UNIQUE INDEX signatures_percentile_celfile_name ON percentile USING btree (celfile_name);


--
-- Name: signatures_score_celfile_name; Type: INDEX; Schema: signatures; Owner: nicholas
--

CREATE UNIQUE INDEX signatures_score_celfile_name ON score USING btree (celfile_name);


--
-- Name: celfilegroup_signature_id_fkey; Type: FK CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY celfilegroup
    ADD CONSTRAINT celfilegroup_signature_id_fkey FOREIGN KEY (signature_id) REFERENCES signature(signature_id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: cutpoint_signature_id_fkey; Type: FK CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY cutpoint
    ADD CONSTRAINT cutpoint_signature_id_fkey FOREIGN KEY (signature_id) REFERENCES signature(signature_id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: feature_environment_normalization_environment_fkey; Type: FK CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY feature
    ADD CONSTRAINT feature_environment_normalization_environment_fkey FOREIGN KEY (normalization_environment) REFERENCES environment(name) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: feature_signature_id_fkey; Type: FK CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY feature
    ADD CONSTRAINT feature_signature_id_fkey FOREIGN KEY (signature_id) REFERENCES signature(signature_id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: score_signature_id_fkey; Type: FK CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY prediction
    ADD CONSTRAINT score_signature_id_fkey FOREIGN KEY (signature_id) REFERENCES signature(signature_id) ON UPDATE RESTRICT ON DELETE CASCADE;


--
-- Name: signature_environment_predict_env_fkey; Type: FK CONSTRAINT; Schema: signatures; Owner: grid_content_owner
--

ALTER TABLE ONLY signature
    ADD CONSTRAINT signature_environment_predict_env_fkey FOREIGN KEY (predict_env) REFERENCES environment(name) ON UPDATE CASCADE ON DELETE RESTRICT;


--
-- Name: signatures; Type: ACL; Schema: -; Owner: grid_content_owner
--

REVOKE ALL ON SCHEMA signatures FROM PUBLIC;
REVOKE ALL ON SCHEMA signatures FROM grid_content_owner;
GRANT ALL ON SCHEMA signatures TO grid_content_owner;
GRANT USAGE ON SCHEMA signatures TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA signatures TO clinops;
GRANT USAGE ON SCHEMA signatures TO biostats;
GRANT USAGE ON SCHEMA signatures TO bioinfo;
GRANT USAGE ON SCHEMA signatures TO engineering;
GRANT ALL ON SCHEMA signatures TO grid_admin;
GRANT ALL ON SCHEMA signatures TO administrator;


--
-- Name: feature; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE feature FROM PUBLIC;
REVOKE ALL ON TABLE feature FROM grid_content_owner;
GRANT ALL ON TABLE feature TO grid_content_owner;
GRANT SELECT ON TABLE feature TO bioinfo;
GRANT SELECT ON TABLE feature TO biostats;
GRANT SELECT ON TABLE feature TO clinops;


--
-- Name: prediction; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE prediction FROM PUBLIC;
REVOKE ALL ON TABLE prediction FROM grid_content_owner;
GRANT ALL ON TABLE prediction TO grid_content_owner;
GRANT SELECT ON TABLE prediction TO bioinfo;
GRANT SELECT ON TABLE prediction TO biostats;
GRANT SELECT ON TABLE prediction TO clinops;


--
-- Name: signature; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE signature FROM PUBLIC;
REVOKE ALL ON TABLE signature FROM grid_content_owner;
GRANT ALL ON TABLE signature TO grid_content_owner;
GRANT SELECT ON TABLE signature TO biostats;
GRANT SELECT ON TABLE signature TO bioinfo;
GRANT SELECT ON TABLE signature TO clinops;


--
-- Name: bca_subtype; Type: ACL; Schema: signatures; Owner: nicholas
--

REVOKE ALL ON TABLE bca_subtype FROM PUBLIC;
REVOKE ALL ON TABLE bca_subtype FROM nicholas;
GRANT ALL ON TABLE bca_subtype TO nicholas;
GRANT SELECT ON TABLE bca_subtype TO bioinfo;
GRANT SELECT ON TABLE bca_subtype TO biostats;
GRANT SELECT ON TABLE bca_subtype TO clinops;


--
-- Name: celfilegroup; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE celfilegroup FROM PUBLIC;
REVOKE ALL ON TABLE celfilegroup FROM grid_content_owner;
GRANT ALL ON TABLE celfilegroup TO grid_content_owner;
GRANT SELECT ON TABLE celfilegroup TO bioinfo;
GRANT SELECT ON TABLE celfilegroup TO biostats;
GRANT SELECT ON TABLE celfilegroup TO clinops;


--
-- Name: molecular_subtype; Type: ACL; Schema: signatures; Owner: nicholas
--

REVOKE ALL ON TABLE molecular_subtype FROM PUBLIC;
REVOKE ALL ON TABLE molecular_subtype FROM nicholas;
GRANT ALL ON TABLE molecular_subtype TO nicholas;
GRANT SELECT ON TABLE molecular_subtype TO bioinfo;
GRANT SELECT ON TABLE molecular_subtype TO biostats;
GRANT SELECT ON TABLE molecular_subtype TO clinops;


--
-- Name: class; Type: ACL; Schema: signatures; Owner: nicholas
--

REVOKE ALL ON TABLE class FROM PUBLIC;
REVOKE ALL ON TABLE class FROM nicholas;
GRANT ALL ON TABLE class TO nicholas;
GRANT SELECT ON TABLE class TO bioinfo;
GRANT SELECT ON TABLE class TO biostats;
GRANT SELECT ON TABLE class TO clinops;


--
-- Name: cutpoint; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE cutpoint FROM PUBLIC;
REVOKE ALL ON TABLE cutpoint FROM grid_content_owner;
GRANT ALL ON TABLE cutpoint TO grid_content_owner;
GRANT SELECT ON TABLE cutpoint TO bioinfo;
GRANT SELECT ON TABLE cutpoint TO biostats;
GRANT SELECT ON TABLE cutpoint TO clinops;


--
-- Name: environment; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE environment FROM PUBLIC;
REVOKE ALL ON TABLE environment FROM grid_content_owner;
GRANT ALL ON TABLE environment TO grid_content_owner;
GRANT SELECT ON TABLE environment TO bioinfo;
GRANT SELECT ON TABLE environment TO biostats;
GRANT SELECT ON TABLE environment TO clinops;


--
-- Name: percentile; Type: ACL; Schema: signatures; Owner: nicholas
--

REVOKE ALL ON TABLE percentile FROM PUBLIC;
REVOKE ALL ON TABLE percentile FROM nicholas;
GRANT ALL ON TABLE percentile TO nicholas;
GRANT SELECT ON TABLE percentile TO bioinfo;
GRANT SELECT ON TABLE percentile TO biostats;
GRANT SELECT ON TABLE percentile TO clinops;


--
-- Name: score; Type: ACL; Schema: signatures; Owner: nicholas
--

REVOKE ALL ON TABLE score FROM PUBLIC;
REVOKE ALL ON TABLE score FROM nicholas;
GRANT ALL ON TABLE score TO nicholas;
GRANT SELECT ON TABLE score TO bioinfo;
GRANT SELECT ON TABLE score TO biostats;
GRANT SELECT ON TABLE score TO clinops;


--
-- Name: signature_view; Type: ACL; Schema: signatures; Owner: grid_content_owner
--

REVOKE ALL ON TABLE signature_view FROM PUBLIC;
REVOKE ALL ON TABLE signature_view FROM grid_content_owner;
GRANT ALL ON TABLE signature_view TO grid_content_owner;
GRANT SELECT ON TABLE signature_view TO bioinfo;
GRANT SELECT ON TABLE signature_view TO biostats;
GRANT SELECT ON TABLE signature_view TO clinops;


--
-- PostgreSQL database dump complete
--

