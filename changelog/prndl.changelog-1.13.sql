--liquibase formatted sql
--changeset mpdillon:preprocessing endDelimiter:"\n--GO" stripComments:false comment:InitialLoad
--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: preprocessing; Type: SCHEMA; Schema: -; Owner: administrator
--

CREATE SCHEMA preprocessing;


ALTER SCHEMA preprocessing OWNER TO administrator;

SET search_path = preprocessing, pg_catalog;

--
-- Name: flag_id_seq; Type: SEQUENCE; Schema: preprocessing; Owner: rds_superuser
--

CREATE SEQUENCE flag_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE flag_id_seq OWNER TO rds_superuser;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: flag_tables; Type: TABLE; Schema: preprocessing; Owner: administrator
--

CREATE TABLE flag_tables (
    flag_id integer NOT NULL,
    celfile_name character varying(200),
    hyb_controls character(1),
    pm_mean character(1),
    background character(1),
    mad character(1),
    rle character(1),
    pos_vs_neg_auc character(1),
    gnuse character(1),
    present_call character(1),
    multivariate_test character(1),
    summary character(1),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE flag_tables OWNER TO administrator;

--
-- Name: image_id_seq; Type: SEQUENCE; Schema: preprocessing; Owner: rds_superuser
--

CREATE SEQUENCE image_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE image_id_seq OWNER TO rds_superuser;

--
-- Name: images; Type: TABLE; Schema: preprocessing; Owner: administrator
--

CREATE TABLE images (
    image_id integer NOT NULL,
    celfile_name character varying(200) NOT NULL,
    intensity character varying(256),
    background character varying(256),
    density character varying(256),
    mva character varying(256),
    detection_call character varying(256),
    pval_detection_call character varying(256),
    normalized_intensity character varying(256),
    probe_signal_distribution character varying(256),
    coefficient_variation_hist character varying(256),
    median_absolute_deviation_hist character varying(256),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE images OWNER TO administrator;

--
-- Name: metrics; Type: TABLE; Schema: preprocessing; Owner: administrator
--

CREATE TABLE metrics (
    metrics_id integer,
    celfile_name character varying(200) NOT NULL,
    bgrd_mean double precision,
    mm_mean double precision,
    pm_mean double precision,
    all_probeset_probesets double precision,
    all_probeset_atoms double precision,
    all_probeset_mean double precision,
    all_probeset_stdev double precision,
    all_probeset_mad_residual_mean double precision,
    all_probeset_mad_residual_std double precision,
    all_probeset_rle_mean double precision,
    all_probeset_rle_stdev double precision,
    bac_spike_probesets double precision,
    bac_spike_atoms double precision,
    bac_spike_mean double precision,
    bac_spike_stdev double precision,
    bac_spike_mad_residual_mean double precision,
    bac_spike_mad_residual_stdev double precision,
    bac_spike_rle_mean double precision,
    bac_spike_rle_stdev double precision,
    neg_control_probesets double precision,
    neg_control_atoms double precision,
    neg_control_mean double precision,
    neg_control_stdev double precision,
    neg_control_mad_residual_mean double precision,
    neg_control_mad_residual_stdev double precision,
    neg_control_rle_mean double precision,
    neg_control_rle_stdev double precision,
    polya_spike_probesets double precision,
    polya_spike_atoms double precision,
    polya_spike_mean double precision,
    polya_spike_stdev double precision,
    polya_spike_mad_residual_mean double precision,
    polya_spike_mad_residual_stdev double precision,
    polya_spike_rle_mean double precision,
    polya_spike_rle_stdev double precision,
    pos_control_probesets double precision,
    pos_control_atoms double precision,
    pos_control_mean double precision,
    pos_control_stdev double precision,
    pos_control_mad_residual_mean double precision,
    pos_control_mad_residual_stdev double precision,
    pos_control_rle_mean double precision,
    pos_control_rle_stdev double precision,
    pos_vs_neg_auc double precision,
    polya_spike_r2_bs_phe_5_st double precision,
    bac_spike_r2_ec_biob_m_at double precision,
    polya_spike_r2_bs_phe_3_st double precision,
    bac_spike_r2_ec_biob_5_at double precision,
    bac_spike_r2_p1_cre_3_at double precision,
    polya_spike_r2_bs_lys_3_st double precision,
    bac_spike_biodn_3_at double precision,
    polya_spike_r2_bs_dap_3_st double precision,
    bac_spike_biob_3_at double precision,
    bac_spike_biob_5_at double precision,
    polya_spike_r2_bs_lys_m_st double precision,
    bac_spike_r2_ec_bioc_3_at double precision,
    bac_spike_r2_p1_cre_5_at double precision,
    polya_spike_r2_bs_dap_m_st double precision,
    polya_spike_r2_bs_phe_m_st double precision,
    bac_spike_crex_5_at double precision,
    polya_spike_r2_bs_thr_5_s_st double precision,
    bac_spike_r2_ec_biob_3_at double precision,
    polya_spike_r2_bs_thr_3_s_st double precision,
    bac_spike_r2_ec_bioc_5_at double precision,
    polya_spike_r2_bs_thr_m_s_st double precision,
    polya_spike_r2_bs_dap_5_st double precision,
    bac_spike_bioc_3_at double precision,
    bac_spike_biodn_5_at double precision,
    polya_spike_bs_trp_x_st double precision,
    bac_spike_crex_3_at double precision,
    bac_spike_bioc_5_at double precision,
    bac_spike_r2_ec_biod_3_at double precision,
    polya_spike_r2_bs_lys_5_st double precision,
    bac_spike_biob_m_at double precision,
    bac_spike_r2_ec_biod_5_at double precision,
    percent_present double precision,
    rf22_percent_present double precision,
    gnuse double precision,
    probe_variation_vectorized double precision,
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25)
);


ALTER TABLE metrics OWNER TO administrator;

--
-- Name: metrics_id_seq; Type: SEQUENCE; Schema: preprocessing; Owner: rds_superuser
--

CREATE SEQUENCE metrics_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE metrics_id_seq OWNER TO rds_superuser;

--
-- Name: quality_control; Type: TABLE; Schema: preprocessing; Owner: administrator
--

CREATE TABLE quality_control (
    quality_control_id character varying(128) NOT NULL,
    celfile_group_id character varying(100),
    normalization_id character varying(128),
    software_version_id integer,
    metrics_id integer,
    image_id integer,
    flag_id integer,
    location character varying(1024),
    created timestamp without time zone,
    modified timestamp without time zone,
    created_by character varying(25),
    modified_by character varying(25),
    quality_control_note character varying
);


ALTER TABLE quality_control OWNER TO administrator;

--
-- Name: COLUMN quality_control.quality_control_note; Type: COMMENT; Schema: preprocessing; Owner: administrator
--

COMMENT ON COLUMN quality_control.quality_control_note IS 'Notes regarding records of QC';


--
-- Name: IMAGES_pkey; Type: CONSTRAINT; Schema: preprocessing; Owner: administrator
--

ALTER TABLE ONLY images
    ADD CONSTRAINT "IMAGES_pkey" PRIMARY KEY (image_id, celfile_name);


--
-- Name: QUALITY_CONTROL_pkey; Type: CONSTRAINT; Schema: preprocessing; Owner: administrator
--

ALTER TABLE ONLY quality_control
    ADD CONSTRAINT "QUALITY_CONTROL_pkey" PRIMARY KEY (quality_control_id);


--
-- Name: metric_pkey; Type: CONSTRAINT; Schema: preprocessing; Owner: administrator
--

ALTER TABLE ONLY metrics
    ADD CONSTRAINT metric_pkey PRIMARY KEY (celfile_name);


--
-- Name: created_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON flag_tables FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON images FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON metrics FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: created_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER created_trigger BEFORE INSERT ON quality_control FOR EACH ROW EXECUTE PROCEDURE public.created_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON flag_tables FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON images FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON metrics FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: modified_trigger; Type: TRIGGER; Schema: preprocessing; Owner: administrator
--

CREATE TRIGGER modified_trigger BEFORE UPDATE ON quality_control FOR EACH ROW EXECUTE PROCEDURE public.modified_history();


--
-- Name: preprocessing; Type: ACL; Schema: -; Owner: administrator
--

REVOKE ALL ON SCHEMA preprocessing FROM PUBLIC;
REVOKE ALL ON SCHEMA preprocessing FROM administrator;
GRANT ALL ON SCHEMA preprocessing TO administrator;
GRANT USAGE ON SCHEMA preprocessing TO samplesdb_table_owner;
GRANT USAGE ON SCHEMA preprocessing TO bioinfo;
GRANT USAGE ON SCHEMA preprocessing TO biostats;
GRANT USAGE ON SCHEMA preprocessing TO clinops;
GRANT USAGE ON SCHEMA preprocessing TO engineering;


--
-- Name: flag_id_seq; Type: ACL; Schema: preprocessing; Owner: rds_superuser
--

REVOKE ALL ON SEQUENCE flag_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE flag_id_seq FROM rds_superuser;
GRANT ALL ON SEQUENCE flag_id_seq TO rds_superuser;
GRANT ALL ON SEQUENCE flag_id_seq TO bioinfo;
GRANT ALL ON SEQUENCE flag_id_seq TO biostats;
GRANT ALL ON SEQUENCE flag_id_seq TO samplesdb_table_owner;


--
-- Name: flag_tables; Type: ACL; Schema: preprocessing; Owner: administrator
--

REVOKE ALL ON TABLE flag_tables FROM PUBLIC;
REVOKE ALL ON TABLE flag_tables FROM administrator;
GRANT ALL ON TABLE flag_tables TO administrator;
GRANT SELECT ON TABLE flag_tables TO readonly;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE flag_tables TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE flag_tables TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE flag_tables TO biostats;
GRANT SELECT ON TABLE flag_tables TO clinops;
GRANT SELECT ON TABLE flag_tables TO bioinfo_readonly;
GRANT SELECT ON TABLE flag_tables TO biostats_readonly;


--
-- Name: image_id_seq; Type: ACL; Schema: preprocessing; Owner: rds_superuser
--

REVOKE ALL ON SEQUENCE image_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE image_id_seq FROM rds_superuser;
GRANT ALL ON SEQUENCE image_id_seq TO rds_superuser;
GRANT ALL ON SEQUENCE image_id_seq TO bioinfo;
GRANT ALL ON SEQUENCE image_id_seq TO biostats;
GRANT ALL ON SEQUENCE image_id_seq TO samplesdb_table_owner;


--
-- Name: images; Type: ACL; Schema: preprocessing; Owner: administrator
--

REVOKE ALL ON TABLE images FROM PUBLIC;
REVOKE ALL ON TABLE images FROM administrator;
GRANT ALL ON TABLE images TO administrator;
GRANT SELECT ON TABLE images TO readonly;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE images TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE images TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE images TO biostats;
GRANT SELECT ON TABLE images TO clinops;
GRANT SELECT ON TABLE images TO bioinfo_readonly;
GRANT SELECT ON TABLE images TO biostats_readonly;


--
-- Name: metrics; Type: ACL; Schema: preprocessing; Owner: administrator
--

REVOKE ALL ON TABLE metrics FROM PUBLIC;
REVOKE ALL ON TABLE metrics FROM administrator;
GRANT ALL ON TABLE metrics TO administrator;
GRANT SELECT ON TABLE metrics TO readonly;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE metrics TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE metrics TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE metrics TO biostats;
GRANT SELECT ON TABLE metrics TO clinops;
GRANT SELECT ON TABLE metrics TO bioinfo_readonly;
GRANT SELECT ON TABLE metrics TO biostats_readonly;


--
-- Name: metrics_id_seq; Type: ACL; Schema: preprocessing; Owner: rds_superuser
--

REVOKE ALL ON SEQUENCE metrics_id_seq FROM PUBLIC;
REVOKE ALL ON SEQUENCE metrics_id_seq FROM rds_superuser;
GRANT ALL ON SEQUENCE metrics_id_seq TO rds_superuser;
GRANT ALL ON SEQUENCE metrics_id_seq TO bioinfo;
GRANT ALL ON SEQUENCE metrics_id_seq TO samplesdb_table_owner;
GRANT ALL ON SEQUENCE metrics_id_seq TO biostats;


--
-- Name: quality_control; Type: ACL; Schema: preprocessing; Owner: administrator
--

REVOKE ALL ON TABLE quality_control FROM PUBLIC;
REVOKE ALL ON TABLE quality_control FROM administrator;
GRANT ALL ON TABLE quality_control TO administrator;
GRANT SELECT ON TABLE quality_control TO readonly;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE quality_control TO samplesdb_table_owner;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE quality_control TO bioinfo;
GRANT SELECT,INSERT,REFERENCES,DELETE,UPDATE ON TABLE quality_control TO biostats;
GRANT SELECT ON TABLE quality_control TO clinops;
GRANT SELECT ON TABLE quality_control TO bioinfo_readonly;
GRANT SELECT ON TABLE quality_control TO biostats_readonly;


--
-- PostgreSQL database dump complete
--
--GO
--rollback DROP SCHEMA preprocessing;

