--
-- PostgreSQL database dump
--

-- Dumped from database version 9.5.4
-- Dumped by pg_dump version 9.5.4

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: gridreport_clindev_v2; Type: SCHEMA; Schema: -; Owner: grid_admin
--

CREATE SCHEMA gridreport_clindev_v2;


ALTER SCHEMA gridreport_clindev_v2 OWNER TO grid_admin;

SET search_path = gridreport_clindev_v2, pg_catalog;

--
-- Name: arsignaling(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION arsignaling(character varying) RETURNS TABLE(celfile_name character varying, percentile double precision, score double precision, class text)
    LANGUAGE sql
    AS $_$
SELECT celfile_name, AVG(percentile) AS percentile, AVG(score) AS score, CASE 
WHEN AVG(percentile) > 0.8 THEN 'High AR Activity'
ELSE 'Average AR Activity' END AS class
FROM signatures_clindev_v2.prediction WHERE signature_id IN (10, 66) AND celfile_name = $1 GROUP BY celfile_name;
$_$;


ALTER FUNCTION gridreport_clindev_v2.arsignaling(character varying) OWNER TO grid_admin;

--
-- Name: biomarkers(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION biomarkers(character varying) RETURNS TABLE(name character varying, category character varying, expression double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
SELECT name, category, expression, percentile, class FROM gridreport_clindev_v2.biomarkersmeta JOIN grid_clindev_v2.transcription USING (biomarker) WHERE celfile_name = $1 ORDER BY category ASC, "order" ASC;
$_$;


ALTER FUNCTION gridreport_clindev_v2.biomarkers(character varying) OWNER TO grid_admin;

--
-- Name: bottom_biomarker(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION bottom_biomarker(character varying) RETURNS TABLE(biomarker character varying, expression double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
    SELECT name, expression, percentile, class FROM gridreport_clindev_v2.biomarkers($1) ORDER BY percentile ASC, expression ASC LIMIT 1;
$_$;


ALTER FUNCTION gridreport_clindev_v2.bottom_biomarker(character varying) OWNER TO grid_admin;

--
-- Name: completed(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION completed(character varying) RETURNS boolean
    LANGUAGE sql
    AS $_$
SELECT SUM((present IS TRUE)::integer) = 1 AND  SUM((present IS FALSE)::integer) = 248 FROM (
	SELECT class IS NULL AS present FROM gridreport_clindev_v2.summary($1)
	UNION ALL
	SELECT percentile IS NULL AS present FROM gridreport_clindev_v2.summary($1)
	UNION ALL
	SELECT class IS NULL AS present FROM gridreport_clindev_v2.summary($1)
	UNION ALL
	SELECT score IS NULL AS present FROM gridreport_clindev_v2.signatures($1)
	UNION ALL
	SELECT percentile IS NULL AS present FROM gridreport_clindev_v2.signatures($1)
	UNION ALL
	SELECT class IS NULL AS present FROM gridreport_clindev_v2.signatures($1)
	UNION ALL
	SELECT expression IS NULL AS present FROM gridreport_clindev_v2.biomarkers($1)
	UNION ALL
	SELECT percentile IS NULL AS present FROM gridreport_clindev_v2.biomarkers($1)
	UNION ALL
	SELECT class IS NULL AS present FROM gridreport_clindev_v2.biomarkers($1)
) AS sql1;
$_$;


ALTER FUNCTION gridreport_clindev_v2.completed(character varying) OWNER TO grid_admin;

--
-- Name: molecular_subtype(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION molecular_subtype(character varying) RETURNS character varying
    LANGUAGE sql
    AS $_$
SELECT 
molecular_subtype
FROM signatures_clindev_v2.molecular_subtype
WHERE celfile_name = $1;
$_$;


ALTER FUNCTION gridreport_clindev_v2.molecular_subtype(character varying) OWNER TO grid_admin;

--
-- Name: prognosticavg(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION prognosticavg(character varying) RETURNS TABLE(celfile_name character varying, percentile double precision, score double precision, class text)
    LANGUAGE sql
    AS $_$
SELECT celfile_name, AVG(percentile) AS percentile, AVG(score) AS score, CASE 
WHEN AVG(percentile) < 0.4 THEN 'Low Metastasis Risk' 
WHEN AVG(percentile) > 0.7 THEN 'High Metastasis Risk'
ELSE 'Average Metastasis Risk' END AS class
FROM signatures_clindev_v2.prediction WHERE signature_id IN (2, 3, 6, 12, 16, 17, 18, 19, 20, 22, 23, 24, 25, 26, 27, 28, 29, 30, 31, 32, 33, 34, 35) AND celfile_name = $1 GROUP BY celfile_name;
$_$;


ALTER FUNCTION gridreport_clindev_v2.prognosticavg(character varying) OWNER TO grid_admin;

--
-- Name: proliferation(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION proliferation(character varying) RETURNS TABLE(celfile_name character varying, percentile double precision, score double precision, class text)
    LANGUAGE sql
    AS $_$
SELECT celfile_name, AVG(percentile) AS percentile, AVG(score) AS score, CASE 
WHEN AVG(percentile) > 0.75 THEN 'High Proliferation'
ELSE 'Average Proliferation' END AS class
FROM signatures_clindev_v2.prediction WHERE signature_id IN (4, 32, 23) AND celfile_name = $1 GROUP BY celfile_name;
$_$;


ALTER FUNCTION gridreport_clindev_v2.proliferation(character varying) OWNER TO grid_admin;

--
-- Name: signatures(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION signatures(character varying) RETURNS TABLE(section character varying, section_description character varying, subsection character varying, subsection_description character varying, signature character varying, signature_description character varying, institution character varying, author character varying, year smallint, citation character varying, score double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
SELECT 
section.name AS section, 
section.description AS section_description, 
subsection.name AS subsection, 
subsection.description AS subsection_description, 
signaturemeta.name AS signature, 
signaturemeta.description AS signature_description, 
institution, author, year, citation, score, percentile, class 
FROM gridreport_clindev_v2.section 
JOIN gridreport_clindev_v2.subsection USING (sectionid) 
JOIN gridreport_clindev_v2.signaturemeta USING (subsectionid) 
JOIN signatures_clindev_v2.prediction USING (signature_id)
WHERE celfile_name = $1
ORDER BY section.order, subsection.order, signaturemeta.order;
$_$;


ALTER FUNCTION gridreport_clindev_v2.signatures(character varying) OWNER TO grid_admin;

--
-- Name: summary(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION summary(character varying) RETURNS TABLE(signature character varying, percentile double precision, score double precision, class character varying)
    LANGUAGE sql
    AS $_$
SELECT name, percentile, score, class FROM (
SELECT 'prognosticavg' AS name, celfile_name, score, percentile, class FROM gridreport_clindev_v2.prognosticavg($1)
UNION
SELECT 'arsignaling' AS name, celfile_name, score, percentile, class FROM gridreport_clindev_v2.arsignaling($1)
UNION
SELECT 'proliferation' AS name, celfile_name, score, percentile, class FROM gridreport_clindev_v2.proliferation($1)
UNION
SELECT 'gleasongrade' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 15
UNION
SELECT 'ars' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 46
UNION
SELECT 'portos' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 65
UNION
SELECT 'docetaxel' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 38
UNION
SELECT 'dasatinib' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 41
UNION
SELECT 'basalluminal' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 11
UNION
SELECT 'smallcell' AS name, celfile_name, score, percentile, class FROM signatures_clindev_v2.prediction WHERE celfile_name = $1 AND signature_id = 68
UNION
SELECT 'molecularsubtype' AS name, $1 AS celfile_name, NULL AS score, NULL AS percentile, gridreport_clindev_v2.molecular_subtype($1) AS class
) AS sql1;
$_$;


ALTER FUNCTION gridreport_clindev_v2.summary(character varying) OWNER TO grid_admin;

--
-- Name: top_biomarker(character varying); Type: FUNCTION; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE FUNCTION top_biomarker(character varying) RETURNS TABLE(biomarker character varying, expression double precision, percentile double precision, class character varying)
    LANGUAGE sql
    AS $_$
    SELECT name, expression, percentile, class FROM gridreport_clindev_v2.biomarkers($1) ORDER BY percentile DESC, expression DESC LIMIT 1;
$_$;


ALTER FUNCTION gridreport_clindev_v2.top_biomarker(character varying) OWNER TO grid_admin;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: biomarkersmeta; Type: TABLE; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE TABLE biomarkersmeta (
    biomarker character varying(32) NOT NULL,
    name character varying(32),
    category character varying(32),
    "order" smallint
);


ALTER TABLE biomarkersmeta OWNER TO grid_admin;

--
-- Name: section; Type: TABLE; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE TABLE section (
    sectionid bigint NOT NULL,
    description character varying(256),
    "order" real,
    name character varying(256)
);


ALTER TABLE section OWNER TO grid_admin;

--
-- Name: section_sectionid_seq; Type: SEQUENCE; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE SEQUENCE section_sectionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE section_sectionid_seq OWNER TO grid_admin;

--
-- Name: section_sectionid_seq; Type: SEQUENCE OWNED BY; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER SEQUENCE section_sectionid_seq OWNED BY section.sectionid;


--
-- Name: signaturemeta; Type: TABLE; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE TABLE signaturemeta (
    signature_id bigint NOT NULL,
    name character varying(128),
    description text,
    institution character varying(256),
    author character varying(128),
    citation character varying(1024),
    year smallint,
    subsectionid bigint,
    "order" real
);


ALTER TABLE signaturemeta OWNER TO grid_admin;

--
-- Name: subsection; Type: TABLE; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE TABLE subsection (
    subsectionid bigint NOT NULL,
    name character varying(256),
    description character varying(256),
    "order" real NOT NULL,
    sectionid bigint
);


ALTER TABLE subsection OWNER TO grid_admin;

--
-- Name: subsection_subsectionid_seq; Type: SEQUENCE; Schema: gridreport_clindev_v2; Owner: grid_admin
--

CREATE SEQUENCE subsection_subsectionid_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE subsection_subsectionid_seq OWNER TO grid_admin;

--
-- Name: subsection_subsectionid_seq; Type: SEQUENCE OWNED BY; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER SEQUENCE subsection_subsectionid_seq OWNED BY subsection.subsectionid;


--
-- Name: sectionid; Type: DEFAULT; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY section ALTER COLUMN sectionid SET DEFAULT nextval('section_sectionid_seq'::regclass);


--
-- Name: subsectionid; Type: DEFAULT; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY subsection ALTER COLUMN subsectionid SET DEFAULT nextval('subsection_subsectionid_seq'::regclass);


--
-- Name: biomarkersmeta_pkey; Type: CONSTRAINT; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY biomarkersmeta
    ADD CONSTRAINT biomarkersmeta_pkey PRIMARY KEY (biomarker);


--
-- Name: section_pkey; Type: CONSTRAINT; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY section
    ADD CONSTRAINT section_pkey PRIMARY KEY (sectionid);


--
-- Name: signaturemeta_pkey; Type: CONSTRAINT; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY signaturemeta
    ADD CONSTRAINT signaturemeta_pkey PRIMARY KEY (signature_id);


--
-- Name: subsection_pkey; Type: CONSTRAINT; Schema: gridreport_clindev_v2; Owner: grid_admin
--

ALTER TABLE ONLY subsection
    ADD CONSTRAINT subsection_pkey PRIMARY KEY (subsectionid);


--
-- Name: gridreport_clindev_v2; Type: ACL; Schema: -; Owner: grid_admin
--

REVOKE ALL ON SCHEMA gridreport_clindev_v2 FROM PUBLIC;
REVOKE ALL ON SCHEMA gridreport_clindev_v2 FROM grid_admin;
GRANT ALL ON SCHEMA gridreport_clindev_v2 TO grid_admin;
GRANT ALL ON SCHEMA gridreport_clindev_v2 TO engineering;
GRANT ALL ON SCHEMA gridreport_clindev_v2 TO administrator;


--
-- Name: arsignaling(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION arsignaling(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION arsignaling(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION arsignaling(character varying) TO grid_admin;
GRANT ALL ON FUNCTION arsignaling(character varying) TO postgres;


--
-- Name: biomarkers(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION biomarkers(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION biomarkers(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION biomarkers(character varying) TO grid_admin;
GRANT ALL ON FUNCTION biomarkers(character varying) TO postgres;


--
-- Name: bottom_biomarker(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION bottom_biomarker(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION bottom_biomarker(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION bottom_biomarker(character varying) TO grid_admin;
GRANT ALL ON FUNCTION bottom_biomarker(character varying) TO postgres;


--
-- Name: completed(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION completed(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION completed(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION completed(character varying) TO grid_admin;
GRANT ALL ON FUNCTION completed(character varying) TO postgres;


--
-- Name: molecular_subtype(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION molecular_subtype(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION molecular_subtype(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION molecular_subtype(character varying) TO grid_admin;
GRANT ALL ON FUNCTION molecular_subtype(character varying) TO postgres;


--
-- Name: prognosticavg(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION prognosticavg(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION prognosticavg(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION prognosticavg(character varying) TO grid_admin;
GRANT ALL ON FUNCTION prognosticavg(character varying) TO postgres;


--
-- Name: proliferation(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION proliferation(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION proliferation(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION proliferation(character varying) TO grid_admin;
GRANT ALL ON FUNCTION proliferation(character varying) TO postgres;


--
-- Name: signatures(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION signatures(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION signatures(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION signatures(character varying) TO grid_admin;
GRANT ALL ON FUNCTION signatures(character varying) TO postgres;


--
-- Name: summary(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION summary(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION summary(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION summary(character varying) TO grid_admin;
GRANT ALL ON FUNCTION summary(character varying) TO postgres;


--
-- Name: top_biomarker(character varying); Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON FUNCTION top_biomarker(character varying) FROM PUBLIC;
REVOKE ALL ON FUNCTION top_biomarker(character varying) FROM grid_admin;
GRANT ALL ON FUNCTION top_biomarker(character varying) TO grid_admin;
GRANT ALL ON FUNCTION top_biomarker(character varying) TO postgres;


--
-- Name: biomarkersmeta; Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE biomarkersmeta FROM PUBLIC;
REVOKE ALL ON TABLE biomarkersmeta FROM grid_admin;
GRANT ALL ON TABLE biomarkersmeta TO grid_admin;
GRANT ALL ON TABLE biomarkersmeta TO postgres;


--
-- Name: section; Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE section FROM PUBLIC;
REVOKE ALL ON TABLE section FROM grid_admin;
GRANT ALL ON TABLE section TO grid_admin;
GRANT ALL ON TABLE section TO postgres;


--
-- Name: signaturemeta; Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE signaturemeta FROM PUBLIC;
REVOKE ALL ON TABLE signaturemeta FROM grid_admin;
GRANT ALL ON TABLE signaturemeta TO grid_admin;
GRANT ALL ON TABLE signaturemeta TO postgres;


--
-- Name: subsection; Type: ACL; Schema: gridreport_clindev_v2; Owner: grid_admin
--

REVOKE ALL ON TABLE subsection FROM PUBLIC;
REVOKE ALL ON TABLE subsection FROM grid_admin;
GRANT ALL ON TABLE subsection TO grid_admin;
GRANT ALL ON TABLE subsection TO postgres;


--
-- PostgreSQL database dump complete
--

